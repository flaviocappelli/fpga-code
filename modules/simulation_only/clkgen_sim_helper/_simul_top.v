
`default_nettype none           // Do not allow undeclared signals.
`include "timescale.vh"         // Timescale defined globally.

(* top *)                       // Mark this as top module, because
module _simul_top (             // sometimes Yosys doesn't detect it.
    input  wire        clk_192MHz,
    input  wire        clk_96MHz,
    input  wire        clk_48MHz,
    output wire        xor_96,
    output wire        xor_48
);

    reg [1:0] cnt = 2'b11;
    always @(negedge clk_192MHz)
        cnt <= cnt + 1;

    assign xor_96 = cnt[0] ^ clk_96MHz;
    assign xor_48 = cnt[1] ^ clk_48MHz;

endmodule
