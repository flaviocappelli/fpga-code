
// (C) 2021-2025 Flavio Cappelli
// Released under the MIT License.
//
// See https://www.fpgatutorial.com/how-to-write-a-basic-verilog-testbench/

`default_nettype none                       // Do not allow undeclared signals.
`define DUMPSTR(x) `"x.vcd`"                // Required for GTKWave.
`define SIMULATION_TIME 975020ns            // Simulation time (in s, ms, us or ns).
`include "clkgen_sim_helper.vh"             // Must be included before timescale.vh
`include "timescale.vh"                     // Timescale defined globally.

module testbench();
    initial begin
        $dumpfile(`DUMPSTR(`VCD_OUTPUT));   // Required for GTKWave.
        $dumpvars(0, testbench);            // Dump all variables in all instantiated modules.
        $printtimescale(testbench);         // Display the time scale and resolution (see above).
        #`SIMULATION_TIME                   // Wait until the simulation time is elapsed.
        $timeformat(-6, 2, "us", 1);        // Display the time elapsed in "us".
        $display("Simulation terminated after %t", $realtime);
        $finish;                            // Terminate the simulation.
    end


    // ---- Clock Simulation ----

    wire clk_16MHz;
    clkgen_sim_helper #(.DELAY_ns(10), .FREQ_MHz(16)) CLKGEN01 (.o__clk(clk_16MHz));

    wire clk_16MHz_DC75;
    clkgen_sim_helper #(.DELAY_ns(10), .FREQ_MHz(16), .DUTYCYCLE(75)) CLKGEN02 (.o__clk(clk_16MHz_DC75));

    wire clk_27MHz;
    clkgen_sim_helper #(.DELAY_ns(10), .FREQ_MHz(27)) CLKGEN03 (.o__clk(clk_27MHz));

    wire clk_48MHz;
    clkgen_sim_helper #(.DELAY_ns(10), .FREQ_MHz(48)) CLKGEN04 (.o__clk(clk_48MHz));

    wire clk_50MHz;
    clkgen_sim_helper #(.DELAY_ns(10), .FREQ_MHz(50)) CLKGEN05 (.o__clk(clk_50MHz));

    wire clk_80MHz;
    clkgen_sim_helper #(.DELAY_ns(10), .FREQ_MHz(80)) CLKGEN06 (.o__clk(clk_80MHz));

    wire clk_96MHz;
    clkgen_sim_helper #(.DELAY_ns(10), .FREQ_MHz(96)) CLKGEN07 (.o__clk(clk_96MHz));

    wire clk_100MHz;
    clkgen_sim_helper #(.DELAY_ns(10), .FREQ_MHz(100)) CLKGEN08 (.o__clk(clk_100MHz));

    wire clk_130MHz;
    clkgen_sim_helper #(.DELAY_ns(10), .FREQ_MHz(130)) CLKGEN09 (.o__clk(clk_130MHz));

    wire clk_192MHz;
    clkgen_sim_helper #(.DELAY_ns(10), .FREQ_MHz(192)) CLKGEN10 (.o__clk(clk_192MHz));

    wire clk_200MHz;
    clkgen_sim_helper #(.DELAY_ns(10), .FREQ_MHz(200)) CLKGEN11 (.o__clk(clk_200MHz));

    wire clk_216MHz;
    clkgen_sim_helper #(.DELAY_ns(10), .FREQ_MHz(216)) CLKGEN12 (.o__clk(clk_216MHz));


    // ---- Logic Simulation ----

    // This module divides the 192MHz clock by 2 and by 4 and compares the results
    // with the 96MHz and 48MHz clocks generated above, to detect any differences
    // (which should not exist). We compare these clocks because they are likely
    // the most affected by the finite precision of the simulation.

    wire xor_96, xor_48;
    _simul_top TOP (                        // Top module.
        .clk_192MHz(clk_192MHz),
        .clk_96MHz(clk_96MHz),
        .clk_48MHz(clk_48MHz),
        .xor_96(xor_96),
        .xor_48(xor_48)
    );

endmodule
