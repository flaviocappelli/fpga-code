
// (C) 2021-2025 Flavio Cappelli
// Released under the MIT License.
//
// Helper for the simulation of an arbitrary clock signal
// (designed to be included in testbenches and PLL modules).
//
// NOTE: This module is intended for simulation only (both
//       behavioral and post-synthesis), not true synthesis.
//       See, for example, the project "TinyFPGA-BX/02_blink"
//       to understand how to use this simulation helper.
//
// NOTE: By default, the clock frequency is undefined, so one
//       FREQ_* parameter must always be specified (only one).
//       The initial delay is the time the clock is undefined:
//       by default, it is set to half clock period; it can be
//       adjusted using one DELAY_* parameter (note that DELAY_*
//       must always be > 0, see the next note). If not set, the
//       dutycycle is 50%; it can be modified with the DUTYCYCLE
//       parameter; however, the clock's on/off interval times
//       must be not shorter than '1 ps' (and also 1 timeunit).
//
// NOTE: To ensure a predictable and deterministic simulation, we
//       must NOT perform a clock transition from 1'bX (simulation
//       init state) to 1'b0 at time t = 0, because doing so, might
//       generate a race condition on any "negedge" triggered block
//       (see http://fpgacpu.ca/fpga/verilog.html#clock). To avoid
//       that chance, we must always defer the clock transition
//       1'bX -> 1'b0. This is why DELAY_* cannot be zero.
//
// Many books and tutorials introduce the following simple method for
// simulating a logic clock in Verilog:
//
//    reg clk = 1'b0;
//    initial forever
//        #delay clk = ~clk;
//
// However, such code should be TOTALLY AVOIDED, as it does not meet the
// above recommendation (it causes 'clk' to transition from 1'bX to 1'b0
// at time t = 0, creating a potential race condition). A simple solution
// is displayed below:
//
//    reg clk;
//    initial forever
//        #delay clk = (clk === 1'b0);
//
// Note the use of Verilog's case-equality operator (===): it returns 1'b1
// if and only if both operands have the exact same value (meaning both are
// '0' or both are '1' or both are 'X' or both are 'Z'; in all other cases,
// it returns 1'b0). In the code above 'clk' is not initialized so we have:
//
//    for any t < delay  -->  clk = 'X'
//    at t = delay       -->  clk = ('X' === 1'b0)   -->  clk = 1'b0
//    at t = 2*delay     -->  clk = (1'b0 === 1'b0)  -->  clk = 1'b1
//    at t = 3*delay     -->  clk = (1'b1 === 1'b0)  -->  clk = 1'b0
//    at t = 4*delay     -->  clk = (1'b0 === 1'b0)  -->  clk = 1'b1
//    ...
// (this process continues indefinitely). THIS METHOD OF CLOCK GENERATION IS
// SIMPLE AND EFFICIENT BUT IS AFFECTED BY THE FINITE ACCURACY OF SIMULATION.
// The theoretical max time resolution in Verilog simulation is 1fs; however,
// in practice, the true resolution is governed by the precision set with the
// "`timescale" directive. In situations involving clock periods with several
// (or unlimited) decimal digits (such as clock frequencies of 27MHz or 48MHz)
// the rounding operation inherent in the '#delay' statement, will introduce a
// cumulative error in each cycle, so the clock edges will quickly deviate from
// the intended timing (and the clock will deviates from intended frequency).
//
// To address such issue, an alternative method is implemented, slower but much
// more precise, and also partially independent from the time unit and precision
// set by the "timescale" directive. Note that, in Verilog, each module can have
// its timescale, but the recommended approach is to put a global directive in an
// include file and include it in each module. However some modules might benefit
// from a different "timescale"; indeed this module has its own timescale (do not
// change it). Modern verilog simulators ensures that timeunits and precisions are
// consistent across the simulation: when signals are passed between modules, the
// simulator will convert between the timeunits (and precisions) aligning them as
// needed (this means that the finer precision in one  module may be scaled to the
// coarser precision of another module, but the result should remain accurate). It
// is the developer's responsibility to ensure that the conversion will not affect
// the correctness of the simulation.

`default_nettype none                   // Do not allow undeclared signals.
`timescale 1ns/1ps                      // Precision for arbitrary clock generation. DO NOT CHANGE.

`ifndef VERILATOR
`ifndef SYNTHESIS
`ifndef _clkgen_sim_helper_vh_          // Must be imported only one time.
`define _clkgen_sim_helper_vh_

module clkgen_sim_helper (
    output reg o__clk
);

    parameter real FREQ_GHz = -99;      // Clock frequency in GHz (not defined).
    parameter real FREQ_MHz = -99;      // Clock frequency in MHz (not defined).
    parameter real FREQ_KHz = -99;      // Clock frequency in KHz (not defined).
    parameter real FREQ_Hz  = -99;      // Clock frequency in Hz (not defined).
    parameter real FREQ_mHz = -99;      // Clock frequency in mHz (not defined).
    parameter real FREQ_uHz = -99;      // Clock frequency in uHz (not defined).
    parameter real DELAY_s  = -99;      // Initial delay in seconds (not defined).
    parameter real DELAY_ms = -99;      // Initial delay in milliseconds (not defined).
    parameter real DELAY_us = -99;      // Initial delay in microseconds (not defined).
    parameter real DELAY_ns = -99;      // Initial delay in nanoseconds (not defined).
    parameter real DELAY_ps = -99;      // Initial delay in picoseconds (not defined).
    parameter real DUTYCYCLE = 50;      // Duty cycle (50%).

    integer f_set = 0;                  // Used to check that only one FREQ_* parameter is set.
    integer d_set = 0;                  // Used to check that only one DELAY_* parameter is set.
    integer i;                          // Used to perform optimized clock generation when possible.

    realtime k = 0;                     // Clock cycle counter, see note (*) below.
    realtime freq_hz;                   // Clock frequency in Hz.
    realtime delay_s;                   // Initial delay in seconds.
    realtime delay_tpu;                 // Initial delay in timeunits.
    realtime T_s;                       // Clock period in seconds.
    realtime T_tpu;                     // Clock period in timeunits.
    realtime T_on_s, T_off_s;           // Clock on/off interval times in seconds.
    realtime T_on_tpu, T_off_tpu;       // Clock on/off interval times in timeunits.
    realtime t_neg_edge, t_pos_edge;    // Calculated time of clock edges in timeunits.

    localparam fp_cmp_prec = 1e-10;     // Precision in floating point compares.

    // NOTE (*): we use "realtime" because "integer" has too small range; also the computation of
    //           clock edges (see below) is faster with k "realtime", at least on Icarus Verilog!

    initial begin

        // I don't know why but sometimes the "vvp" command of Icarus Verilog
        // freezes if the time precision is less than 1ns, and when it happens
        // $fatal does not interrupt the simulation. Fortunately, $stop does.
        if (1ns == 0.0) begin
            $display("ERROR: *** Time precision must be 1ns or better ***");
            $display("ERROR: *** 'vvp' STOPPED; TYPE 'finish' TO EXIT ***");
            $stop;
        end

        // Get clock frequency in Hz (default: not defined).
        if (FREQ_GHz != -99) begin
            freq_hz = 1E9 * FREQ_GHz;
            f_set = f_set + 1;
        end
        if (FREQ_MHz != -99) begin
            freq_hz = 1E6 * FREQ_MHz;
            f_set = f_set + 1;
        end
        if (FREQ_KHz != -99) begin
            freq_hz = 1E3 * FREQ_KHz;
            f_set = f_set + 1;
        end
        if (FREQ_Hz != -99) begin
            freq_hz = FREQ_Hz;
            f_set = f_set + 1;
        end
        if (FREQ_mHz != -99) begin
            freq_hz = 1E-3 * FREQ_mHz;
            f_set = f_set + 1;
        end
        if (FREQ_uHz != -99) begin
            freq_hz = 1E-6 * FREQ_uHz;
            f_set = f_set + 1;
        end
        if (f_set == 0) begin
            $fatal(1, "ERROR: *** Frequency not assigned ***");
        end
        if (f_set > 1) begin
            $fatal(1, "ERROR: *** More than one FREQ_* parameter assigned ***");
        end
        if (freq_hz < 0) begin
            $fatal(1, "ERROR: *** Frequency cannot be negative ***");
        end
        if (freq_hz < 1E-6 || freq_hz > 10E9) begin
            $fatal(1, "ERROR: *** Frequency not in range 1uHz..10GHz ***");
        end

        // Basic check of dutycycle.
        if (DUTYCYCLE <= 0.0 || DUTYCYCLE >= 100.0) begin
            $fatal(1, "ERROR: *** Dutycycle not valid ***");
        end

        // Get T, T_on, T_off in seconds.
        T_s = 1.0/freq_hz;
        T_on_s = 0.01 * DUTYCYCLE * T_s;
        T_off_s = 0.01 * (100.0 - DUTYCYCLE) * T_s;
        if (T_on_s < 1E-12) begin
            $fatal(1, "ERROR: *** Clock ON interval time shorter than 1ps ***");
        end
        if (T_off_s < 1E-12) begin
            $fatal(1, "ERROR: *** Clock OFF interval time shorter than 1ps ***");
        end

        // Convert T, T_on, T_off to timeunits.
        T_tpu = T_s * 1s;
        T_on_tpu = T_on_s * 1s;
        T_off_tpu = T_off_s * 1s;
        if (T_on_tpu < 1.0) begin
            $fatal(1, "ERROR: *** Clock ON interval time shorter than 1 timeunit ***");
        end
        if (T_off_tpu < 1.0) begin
            $fatal(1, "ERROR: *** Clock OFF interval time shorter than 1 timeunit ***");
        end

        // Get initial delay in seconds (default: half clock period).
        delay_s = T_s / 2;
        if (DELAY_s != -99) begin
            delay_s = DELAY_s;
            d_set = d_set + 1;
        end
        if (DELAY_ms != -99) begin
            delay_s = 1E-3*DELAY_ms;
            d_set = d_set + 1;
        end
        if (DELAY_us != -99) begin
            delay_s = 1E-6*DELAY_us;
            d_set = d_set + 1;
        end
        if (DELAY_ns != -99) begin
            delay_s = 1E-9*DELAY_ns;
            d_set = d_set + 1;
        end
        if (DELAY_ps != -99) begin
            delay_s = 1E-12*DELAY_ps;
            d_set = d_set + 1;
        end
        if (d_set > 1) begin
            $fatal(1, "ERROR: *** More than one DELAY_* parameter assigned ***");
        end
        if (delay_s <= 0) begin
            $fatal(1, "ERROR: *** Initial delay must be > 0 ***");
        end

        // Convert initial delay to timeunits.
        delay_tpu = delay_s * 1s;
        if (delay_tpu < 1.0) begin
            $fatal(1, "ERROR: *** Initial delay shorter than 1 timeunit ***");
        end

        // Schedule first edge. Note that the 'o__clk' signal starts
        // undefined (by design, see note above); with an 'X' initial
        // state, the assignment to 0 is considered a transition 1->0.
        #(delay_tpu) o__clk = 0;

        // With 50% DC we can use optimized faster code for some clocks. See e.g.
        // https://www.edaboard.com/threads/how-to-generate-a-48-mhz-clock.111031/
        // ONLY CLOCKS THAT CAN BE GENERATED WITH SIMPLE FAST CODE ARE CONSIDERED.
        if (DUTYCYCLE == 50) begin

            // 16MHz, 50% dutycycle, no jitter. This clock
            // is used at least for the TinyFPGA BX board.
            if ($abs(freq_hz - 16E6) < fp_cmp_prec) begin
                $display("Optimized 16MHz clock generation (DC=50%%)");
                forever begin
                    #31.250ns o__clk = ~o__clk;
                end
            end

            // 27MHz, 50% dutycycle, 1ps jitter. This clock is
            // used at least for the Tang Nano 9K / 20K boards.
            if ($abs(freq_hz - 27E6) < fp_cmp_prec) begin
                $display("Optimized 27MHz clock generation (DC=50%%)");
                forever begin
                    #18.519ns o__clk = ~o__clk;
                    for (i = 0; i < 13; i = i + 1) begin
                        #18.518ns o__clk = ~o__clk;
                        #18.519ns o__clk = ~o__clk;
                    end
                end
            end

            // 48MHz, 50% dutycycle, 1ps jitter. This clock
            // is used at least for the OrangeCrab-R02 board.
            if ($abs(freq_hz - 48E6) < fp_cmp_prec) begin
                $display("Optimized 48MHz clock generation (DC=50%%)");
                forever begin
                    #10.417ns o__clk = ~o__clk;
                    #10.416ns o__clk = ~o__clk;
                    #10.417ns o__clk = ~o__clk;
                end
            end

            // 96MHz, 50% dutycycle, 1ps jitter.
            if ($abs(freq_hz - 96E6) < fp_cmp_prec) begin
                $display("Optimized 96MHz clock generation (DC=50%%)");
                forever begin
                    #5.208ns o__clk = ~o__clk;
                    #5.209ns o__clk = ~o__clk;
                    #5.208ns o__clk = ~o__clk;
                end
            end

            // 130MHz, 50% dutycycle, 1ps jitter.
            if ($abs(freq_hz - 130E6) < fp_cmp_prec) begin
                $display("Optimized 130MHz clock generation (DC=50%%)");
                forever begin
                    for (i = 0; i < 3; i = i + 1) begin
                        #3.846ns o__clk = ~o__clk;
                    end
                    #3.847ns o__clk = ~o__clk;
                    for (i = 0; i < 5; i = i + 1) begin
                        #3.846ns o__clk = ~o__clk;
                    end
                    #3.847ns o__clk = ~o__clk;
                    for (i = 0; i < 3; i = i + 1) begin
                        #3.846ns o__clk = ~o__clk;
                    end
                end
            end
        end

        // ONLY FOR DEBUGGING.
        //$display("T_on_tpu = %21.15f", T_on_tpu);
        //$display("T_off_tpu = %21.15f", T_off_tpu);
        //$display("T_on_tpu * 1000 = %21.15f", T_on_tpu*1000);
        //$display("T_off_tpu * 1000 = %21.15f", T_off_tpu*1000);
        //$display("$abs(T_on_tpu * 1000 - $floor(T_on_tpu * 1000 + 0.5)) = %21.15f", $abs(T_on_tpu * 1000 - $floor(T_on_tpu * 1000 + 0.5)));
        //$display("$abs(T_off_tpu * 1000 - $floor(T_off_tpu * 1000 + 0.5)) = %21.15f", $abs(T_off_tpu * 1000 - $floor(T_off_tpu * 1000 + 0.5)));

        // Some clocks, regardless of dutycycle, can be simply generated using two #delay statements
        // without losing precision. This happens when we have at most 3 decimal digits for T_on_tpu
        // and T_off_tpu (with timeunits = 1000 * timeprecision, see the above timescale directive).
        if (($abs(T_on_tpu * 1000 - $floor(T_on_tpu * 1000 + 0.5)) < fp_cmp_prec) &&
            ($abs(T_off_tpu * 1000 - $floor(T_off_tpu * 1000 + 0.5)) < fp_cmp_prec)) begin
            $display("Optimized arbitrary clock generation (F=%3.12gHz, DC=%g%%)", freq_hz, DUTYCYCLE);
            T_off_tpu = $floor(T_off_tpu * 1000 + 0.5)/1000;        // Reduces approximation errors (if any)
            T_on_tpu = $floor(T_on_tpu * 1000 + 0.5)/1000;          // resulting from previous calculations.
            forever begin
                #(T_off_tpu) o__clk = 1;
                #(T_on_tpu) o__clk = 0;
            end
        end

        // IF THE EXECUTION GETS HERE, NO OPTIMIZED CODE HAS BEEN USED: THE ARBITRARY CLOCK GENERATION
        // PERFORMED BY NEXT LINES WILL BE A BIT SLOWER BUT STILL ACCURATE. The only limitation of such
        // code is the approximation of floating point numbers, due to the finite FP precision. However,
        // this issue should become relevant only after a large number of clock cycles; since simulations
        // with Icarus Verilog (or similar simulators) are inherently slow, simulating a large number of
        // clock cycles is not feasible, so the approximation error should remain negligible within the
        // typical simulation time (even in the most simple logic design, we'll be able to simulate at
        // most few seconds, due to the "wall clock" time required for the simulation and the amount of
        // data stored for GtkWave). For any serious work, it is recommended to use C++ with Verilator,
        // which significantly speeds up the simulation. Note that in my C++ code the clock generation
        // uses 128-bit fixed-point operations, providing both high precision and simulation speed.
        $display("NOT optimized arbitrary clock generation (F=%3.12gHz, DC=%g%%)", freq_hz, DUTYCYCLE);
        forever begin
            k = k + 1;
            t_neg_edge = T_tpu * k + delay_tpu;
            t_pos_edge = t_neg_edge - T_on_tpu;
            #(t_pos_edge - $realtime) o__clk = 1;   // See note (*) below.
            #(t_neg_edge - $realtime) o__clk = 0;   // See note (*) below.
        end

        // (*) Automatically takes temporal precision into account, approximating the timing of clock
        // edges as best as possible, based on the timing resolution (note that '$realtime' tracks time
        // within the Verilog simulation, rounded to the timing precision set by the timescale directive).
    end

endmodule

`endif      // _clkgen_sim_helper_vh_
`endif      // SYNTHESIS
`endif      // VERILATOR


// The following line forces KDE's Kate editor to recognize
// this file as "Verilog" source (for syntax highlighting).
// kate: hl verilog;
