
// (C) 2021-2025 Flavio Cappelli
// Released under the MIT License.
//
// Synchronizer for an external ACTIVE HIGH asynchronous reset.
//
// While the rising edge of an "active high" asynchronous reset
// signal can be fully asynchronous, the falling edge can cause
// metastability (if it happens near the clock transition), so
// we MUST always synchronize it.
//
// NOTE: The synchronized output reset is ACTIVE HIGH (all
//       my logic modules uses an active high input reset).
//
// NOTE: Glitches in the asynchronous input reset must be
//       filtered out by external hardware (RC filters, IC)
//       if we don't want a new reset of the logic circuit.
//
// NOTE: This implementation of the reset synchronizer is called
//       "VDD-based Reset Synchronizer" in some technical articles
//       (while some engineers at Xilinx call it "reset bridge").
//
// NOTE: Should works with both FPGAs and ASICs, see note (*) below.
//
// See:
// http://www.sunburst-design.com/papers/CummingsSNUG2003Boston_Resets.pdf
// https://www.embedded.com/asynchronous-reset-synchronization-and-distribution-challenges-and-solutions/

`default_nettype none               // Do not allow undeclared signals.
`include "timescale.vh"             // Timescale defined globally.


module async_activehigh_reset_synchronizer (
    input  wire i__clk,             // Clock, active on positive edge.
    input  wire i__async_rst,       // Asynchronous reset input, active high.
    output wire o__sync_rst         // Synchronized reset output, active high.
);

    (* async_reg = "true" *)        // Constraints the FFs to be placed as close as possible.
    reg [1:0] sync_pipe = 2'b11;    // FPGA ONLY, see note (*) below.

    always @(posedge i__clk, posedge i__async_rst) begin
        if (i__async_rst)
            sync_pipe <= 2'b11;
        else
            sync_pipe <= {sync_pipe[0], 1'b0};
    end

    assign o__sync_rst = sync_pipe[1];

endmodule

// NOTE (*)
//
// ASICs cannot have initialization values at power-on (initialization
// values are ignored for synthesis on ASICs): an ASIC always needs an
// input reset signal ("i__async_rst" in this case).
//
// FPGAs instead (due to the bitstream) can have initialization values
// at power-on, so on FPGAs the asynchronous input "i__async_rst" can
// even be left unused (i.e. set to 1'b0 or routed to an FPGA input pin
// connected to GND: in this case the "o__sync_rst" reset signal is still
// be generated and remains active til the 2nd positive edge of the clock).
