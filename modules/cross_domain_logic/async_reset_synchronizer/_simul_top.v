
// (C) 2021-2025 Flavio Cappelli
// Released under the MIT License.
//
// This file is used to pass config parameters (if any) to the
// modules under test, ensuring the modules are configured the
// same way in both behavioral and post-synthesis simulation.
// It also makes "apio lint" happy because it guarantees that
// only one "top" module exists (Verilator requires it).

`default_nettype none           // Do not allow undeclared signals.
`include "timescale.vh"         // Timescale defined globally.

(* top *)                       // Mark this as top module, because
module _simul_top (             // sometimes Yosys doesn't detect it.
    input  wire clk,
    // ---
    input  wire async_inp_rst,
    output wire sync_out_from_rst,
    // ---
    input  wire async_inp_rst_n,
    output wire sync_out_from_rst_n
);

    async_activehigh_reset_synchronizer AAHRS (
        .i__clk(clk),
        .i__async_rst(async_inp_rst),
        .o__sync_rst(sync_out_from_rst)
    );

    async_activelow_reset_synchronizer AALRS (
        .i__clk(clk),
        .i__async_rst_n(async_inp_rst_n),
        .o__sync_rst(sync_out_from_rst_n)
    );

endmodule
