
// (C) 2021-2025 Flavio Cappelli
// Released under the MIT License.
//
// Slow Signal Edge Detect Synchronizer (for ACTIVE LOW signals).
//
// NOTE: The time the asynchronous signal is active must be wider than
//       1.5x the cycle width (period) of the synchronizer clock domain.
//
// NOTE: *** THIS SYNCHRONIZER IS DESIGNED FOR AN ACTIVE LOW SIGNAL ***
//       With an active high signal a spurious pulse on "o__sync_negedge"
//       is generated TWO CLOCK CYCLES AFTER THE RESET IS DEASSERTED.
//
// See:
// https://www.youtube.com/watch?v=eyNU6mn_-7g

`default_nettype none               // Do not allow undeclared signals.
`include "timescale.vh"             // Timescale defined globally.


module slow_activelow_signal_edgedetect_synchronizer (
    input  wire i__clk,             // Clock, active on positive edge.
    input  wire i__rst,             // Reset, active high, asynchronous.
    input  wire i__async_sig_n,     // Asynchronous input signal, active low.
    output wire o__sync_posedge,    // Synchronized "positive edge" detect, active high.
    output wire o__sync_negedge     // Synchronized "negative edge" detect, active high.
);

    reg [2:0] sync_pipe;

    always @(posedge i__clk, posedge i__rst) begin
        if (i__rst)
            sync_pipe <= 3'b111;
        else
            sync_pipe <= {sync_pipe[1:0], i__async_sig_n};
    end

    // The following signals will be one clock wide.
    assign o__sync_posedge = ~sync_pipe[2] &  sync_pipe[1];
    assign o__sync_negedge =  sync_pipe[2] & ~sync_pipe[1];

endmodule
