
// (C) 2021-2025 Flavio Cappelli
// Released under the MIT License.
//
// This file is used to pass config parameters (if any) to the
// modules under test, ensuring the modules are configured the
// same way in both behavioral and post-synthesis simulation.
// It also makes "apio lint" happy because it guarantees that
// only one "top" module exists (Verilator requires it).

`default_nettype none           // Do not allow undeclared signals.
`include "timescale.vh"         // Timescale defined globally.

(* top *)                       // Mark this as top module, because
module _simul_top (             // sometimes Yosys doesn't detect it.
    input  wire clk,
    input  wire reset,
    // ---
    input  wire async_inp_sig,
    output wire sync_sig_posedge,
    output wire sync_sig_negedge,
    // ---
    input  wire async_inp_sig_n,
    output wire sync_sig_n_posedge,
    output wire sync_sig_n_negedge
);

    slow_activehigh_signal_edgedetect_synchronizer SAHSEDS (
        .i__clk(clk),
        .i__rst(reset),
        .i__async_sig(async_inp_sig),
        .o__sync_posedge(sync_sig_posedge),
        .o__sync_negedge(sync_sig_negedge)
    );

    slow_activelow_signal_edgedetect_synchronizer SALSEDS (
        .i__clk(clk),
        .i__rst(reset),
        .i__async_sig_n(async_inp_sig_n),
        .o__sync_posedge(sync_sig_n_posedge),
        .o__sync_negedge(sync_sig_n_negedge)
    );

    // Just to test that the above signals can be used as enable.
    reg [3:0] test_counter = 0;
    always @(posedge clk) begin
        if (sync_sig_posedge)
            test_counter <= test_counter + 1'b1;
        if (sync_sig_negedge)
            test_counter <= test_counter + 1'b1;
    end

endmodule
