
// (C) 2021-2025 Flavio Cappelli
// Released under the MIT License.
//
// See https://www.fpgatutorial.com/how-to-write-a-basic-verilog-testbench/

`default_nettype none                       // Do not allow undeclared signals.
`define DUMPSTR(x) `"x.vcd`"                // Required for GTKWave.
`define SIMULATION_TIME 100us               // Simulation time (in s, ms, us or ns).
`include "timescale.vh"                     // Timescale defined globally.

module testbench();
    initial begin
        $dumpfile(`DUMPSTR(`VCD_OUTPUT));   // Required for GTKWave.
        $dumpvars(0, testbench);            // Dump all variables in all instantiated modules.
        $printtimescale(testbench);         // Display the time scale and resolution (see above).
        #`SIMULATION_TIME                   // Wait until the simulation time is elapsed.
        $timeformat(-6, 2, "us", 1);        // Display the time elapsed in "us".
        $display("Simulation terminated after %t", $realtime);
        $finish;                            // Terminate the simulation.
    end


    // ---- Clock simulation ----

    reg clk;
    initial begin
        #2us                                // Simulate slow starting of clock.
        forever
            #0.5us clk = (clk === 1'b0);    // f = 1 / 2*0.5us = 1MHz
    end


    // ---- Async reset input simulation ----

    reg async_inp_rst;                      // Asynchronous inputs reset, active high.
    initial begin
        #0.5us
        async_inp_rst = 0;
        #38.25us
        async_inp_rst = 1;
        #2.00us
        async_inp_rst = 0;
        #36.42us
        async_inp_rst = 1;
        #0.15us
        async_inp_rst = 0;
     end


    // ---- Reset Generator simulation ----

    wire gen_out_from_rst;                  // Generated output reset (from active high input reset).
    wire gen_out_from_rst_n;                // Generated output reset (from active low input reset).

    _simul_top TOP (                        // Top module.
        .clk(clk),
        // ---
        .async_inp_rst(async_inp_rst),
        .gen_out_from_rst(gen_out_from_rst),
        // ---
        .async_inp_rst_n(!async_inp_rst),
        .gen_out_from_rst_n(gen_out_from_rst_n)
    );

endmodule
