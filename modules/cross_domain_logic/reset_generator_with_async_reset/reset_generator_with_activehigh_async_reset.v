
// (C) 2021-2025 Flavio Cappelli
// Released under the MIT License.
//
// Generate an active high reset signal for about
// 2^RESET_COUNTER_WIDTH clock cycles (after the
// active high async reset input is deasserted).
//
// NOTE: RESET_COUNTER_WIDTH must be >= 2 (default 3).
//
// NOTE: Glitches in the asynchronous input reset must be
//       filtered out by external hardware (RC filters, IC)
//       if we don't want a new reset of the logic circuit.
//
// NOTE: Should works with both FPGAs and ASICs (see note below).

`default_nettype none                               // Do not allow undeclared signals.
`include "timescale.vh"                             // Timescale defined globally.


module reset_generator_with_activehigh_async_reset #(
    parameter RESET_COUNTER_WIDTH = 3               // Number of bits in the reset counter.
) (
    input  wire i__clk,                             // Clock, active on positive edge.
    input  wire i__async_rst,                       // Asynchronous input reset, active high.
    output wire o__gen_rst                          // Active high generated reset.
);

    wire synced_input_rst;
    reg [RESET_COUNTER_WIDTH-1:0] rst_state = 0;    // FPGA ONLY, see note (*) below.

    // Reset is high until the counter reach the max value (NAND of state bits).
    assign o__gen_rst = ~&rst_state;

    // Synchronize the external ACTIVE HIGH asynchronous reset.
    async_activehigh_reset_synchronizer AAHRS (
        .i__clk(i__clk),
        .i__async_rst(i__async_rst),
        .o__sync_rst(synced_input_rst)
    );

    // Increment the counter. Note that Verilator might complain about the sum below
    // for two reasons (it depends on the version of Verilator and enabled options):
    // 1# the usage of 'o__gen_rst' is detected as sync operation, but almost all my
    // other modules use 'o__gen_rst' as asynchronous reset input, so a SYNCASYNCNET
    // warning is generated (usage of sync and async operations on the same net); 2#
    // the sum has operands with different sizes (WIDTH warning).  Both warnings are
    // harmless, the logic is still simulated and synthesized correctly if we simply
    // disable them. See https://verilator.org/guide/latest/warnings.html
    always @(posedge i__clk, posedge synced_input_rst)
        if (synced_input_rst)
            rst_state <= 0;
        else
            // verilator lint_off SYNCASYNCNET
            // verilator lint_off WIDTH
            rst_state <= rst_state + o__gen_rst;    // Increment stops when reset is deasserted.
            // verilator lint_on WIDTH
            // verilator lint_on SYNCASYNCNET

endmodule

// NOTE (*)
// ASICs cannot have initialization values at power-on, so initialization
// values are ignored for synthesis on ASICs. ASICs always needs an input
// reset signal ("i__async_rst" in this case).
//
// FPGAs instead (due to the loaded bitstream) can have initialization
// values at power-on, so on FPGAs the asynchronous input "i__async_rst"
// can even be left unused (i.e. set to 1'b0): in this case the reset will
// still be generated and remains active for 2^RESET_COUNTER_WIDTH clock
// cycles after the power-on.
//
// See also the folder fpga_only/fpga_reset_generator/.
