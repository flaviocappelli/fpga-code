
// (C) 2021-2025 Flavio Cappelli
// Released under the MIT License.
//
// Slow Signal Synchronizer (for ACTIVE LOW signals).
// Must be used to synchronize a single bit signal (not a bus).
//
// NOTE: The time the asynchronous signal is active must be wider than
//       1.5x the cycle width (period) of the synchronizer clock domain.
//
// NOTE: *** THIS SYNCHRONIZER IS DESIGNED FOR AN ACTIVE LOW SIGNAL ***
//       With an active high signal (i.e. normally low) an high pulse is
//       generated because the output is set to high at reset and will
//       go down only TWO CLOCK CYCLES AFTER THE RESET IS DEASSERTED.
//
// See:
// https://www.youtube.com/watch?v=eyNU6mn_-7g
// https://daffy1108.wordpress.com/2014/06/08/synchronizers-for-asynchronous-signals/

`default_nettype none               // Do not allow undeclared signals.
`include "timescale.vh"             // Timescale defined globally.


module slow_activelow_signal_synchronizer (
    input  wire i__clk,             // Clock, active on positive edge.
    input  wire i__rst,             // Reset, active high, asynchronous.
    input  wire i__async_sig_n,     // Asynchronous input signal, active low.
    output wire o__sync_sig_n       // Synchronized output signal, active low.
);

    (* async_reg = "true" *)        // Constraints the FFs to be placed as close as possible
    reg [1:0] sync_pipe;            // (note: should be ignored if not supported by the tool).

    always @(posedge i__clk, posedge i__rst) begin
        if (i__rst)
            sync_pipe <= 2'b11;
        else
            sync_pipe <= {sync_pipe[0], i__async_sig_n};
    end

    assign o__sync_sig_n = sync_pipe[1];

endmodule
