
// (C) 2021-2025 Flavio Cappelli
// Released under the MIT License.
//
// Fast Signal Synchronizer (for ACTIVE LOW signals).
// Must be used to synchronize a single bit signal (not a bus).
//
// NOTE: the time the asynchronous signal is active must be smaller
//       than the clock period of the synchronizer clock domain, while
//       the asynchronous pulses must occur with a frequency that must
//       be AT MOST 1/3 the frequency of the synchronizer clock domain,
//       otherwise consecutive asynchronous pulses can be undetected.
//
// NOTE: *** THIS SYNCHRONIZER IS DESIGNED FOR AN ACTIVE LOW SIGNAL ***
//       With an active high signal (i.e. normally low) the output is wrong.
//
// See:
// https://www.youtube.com/watch?v=eyNU6mn_-7g
// https://daffy1108.wordpress.com/2014/06/08/synchronizers-for-asynchronous-signals/

`default_nettype none               // Do not allow undeclared signals.
`include "timescale.vh"             // Timescale defined globally.


module fast_activelow_signal_synchronizer (
    input  wire i__clk,             // Clock, active on positive edge.
    input  wire i__rst,             // Reset, active high, asynchronous.
    input  wire i__async_sig_n,     // Asynchronous input signal, active low.
    output wire o__sync_sig_n       // Synchronized output signal, active low.
);

    reg s1, s2, sync_out;
    wire rst_s1s2 = i__rst | (i__async_sig_n & sync_out);

    // 1st stage: s1.
    always @(negedge i__async_sig_n, posedge rst_s1s2) begin
        if (rst_s1s2)
            s1 <= 1'b0;
        else
            s1 <= 1'b1;
    end

    // 2nd stage: s2.
    always @(posedge i__clk, posedge rst_s1s2) begin
        if (rst_s1s2)
            s2 <= 1'b0;
        else
            s2 <= s1;
    end

    // 3rd stage: sync_out.
    always @(posedge i__clk, posedge i__rst) begin
        if (i__rst)
            sync_out <= 1'b0;
        else
            sync_out <= s2;
    end

    // Output signal.
    assign o__sync_sig_n = ~sync_out;

endmodule
