
// (C) 2021-2025 Flavio Cappelli
// Released under the MIT License.
//
// See https://www.fpgatutorial.com/how-to-write-a-basic-verilog-testbench/

`default_nettype none                       // Do not allow undeclared signals.
`define DUMPSTR(x) `"x.vcd`"                // Required for GTKWave.
`define SIMULATION_TIME 120us               // Simulation time (in s, ms, us or ns).
`include "timescale.vh"                     // Timescale defined globally.

module testbench();
    initial begin
        $dumpfile(`DUMPSTR(`VCD_OUTPUT));   // Required for GTKWave.
        $dumpvars(0, testbench);            // Dump all variables in all instantiated modules.
        $printtimescale(testbench);         // Display the time scale and resolution (see above).
        #`SIMULATION_TIME                   // Wait until the simulation time is elapsed.
        $timeformat(-6, 2, "us", 1);        // Display the time elapsed in "us".
        $display("Simulation terminated after %t", $realtime);
        $finish;                            // Terminate the simulation.
    end


    // ---- Clock simulation ----

    reg clk;
    initial forever
        #0.5us clk = (clk === 1'b0);        // f = 1 / 2*0.5us = 1MHz


    // ---- Reset simulation ----

    reg reset;                              // Reset, active high.
    initial begin
        #0.5us
        reset = 1;
        #5.0us
        reset = 0;
    end


    // ---- Monostable Level Triggered simulation ----

    reg  dummy_async_sig;                   // Dummy asynchronous inputs signal, used to generate synchronized signals.
    wire sync_sig;                          // Active high synchronized signals (from the above async signal).
    wire sync_sig_n;                        // Active low synchronized signals (from the above async signal).

    wire pulse_on_sync_sig;                 // Retriggerable active high pulse on high level.
    wire pulse_n_on_sync_sig;               // Retriggerable active low pulse on high level.
    wire pulse_on_sync_sig_n;               // Retriggerable active high pulse on low level.
    wire pulse_n_on_sync_sig_n;             // Retriggerable active low pulse on low level.

    _simul_top TOP (                        // Top module.
        .clk(clk),
        .reset(reset),
        .dummy_async_sig(dummy_async_sig),
        // ---
        .sync_sig(sync_sig),
        .pulse_on_sync_sig(pulse_on_sync_sig),
        .pulse_n_on_sync_sig(pulse_n_on_sync_sig),
        // ---
        .sync_sig_n(sync_sig_n),
        .pulse_on_sync_sig_n(pulse_on_sync_sig_n),
        .pulse_n_on_sync_sig_n(pulse_n_on_sync_sig_n)
    );

    initial begin
        #0.5us
        dummy_async_sig = 0;
        #6.75us
        dummy_async_sig = 1;
        #1.5us
        dummy_async_sig = 0;
        #14.5us

        dummy_async_sig = 1;
        #2.5us
        dummy_async_sig = 0;
        #14.5us

        dummy_async_sig = 1;
        #3.5us
        dummy_async_sig = 0;
        #14.5us

        dummy_async_sig = 1;
        #4.5us
        dummy_async_sig = 0;
        #14.5us

        dummy_async_sig = 1;
        #1.5us
        dummy_async_sig = 0;
        #3.5us
        dummy_async_sig = 1;
        #1.5us
        dummy_async_sig = 0;
        #14.5us

        dummy_async_sig = 1;
        #1.5us
        dummy_async_sig = 0;
        #4.5us
        dummy_async_sig = 1;
        #1.5us
        dummy_async_sig = 0;
    end

endmodule
