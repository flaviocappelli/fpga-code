
// (C) 2021-2025 Flavio Cappelli
// Released under the MIT License.
//
// This file is used to pass config parameters (if any) to the
// modules under test, ensuring the modules are configured the
// same way in both behavioral and post-synthesis simulation.
// It also makes "apio lint" happy because it guarantees that
// only one "top" module exists (Verilator requires it).

`default_nettype none           // Do not allow undeclared signals.
`include "timescale.vh"         // Timescale defined globally.

(* top *)                       // Mark this as top module, because
module _simul_top (             // sometimes Yosys doesn't detect it.
    input  wire clk,
    input  wire reset,
    input  wire dummy_async_sig,
    // ---
    output wire sync_sig,
    output wire pulse_on_sync_sig,
    output wire pulse_n_on_sync_sig,
    // ---
    output wire sync_sig_n,
    output wire pulse_on_sync_sig_n,
    output wire pulse_n_on_sync_sig_n
);

    localparam PULSE_CLOCK_CYCLES = 5;


    // Use the dummy async signal to generate synchronized signals.

    slow_activehigh_signal_synchronizer SAHSS (
        .i__clk(clk),
        .i__rst(reset),
        .i__async_sig(dummy_async_sig),
        .o__sync_sig(sync_sig)
    );

    assign sync_sig_n = ~sync_sig;


    // Generate pulses.

    activehigh_monostable_highlevel_triggered #(
        .PULSE_CLOCK_CYCLES(PULSE_CLOCK_CYCLES)
    ) AHMHLT (
        .i__clk(clk),
        .i__rst(reset),
        .i__sigin(sync_sig),
        .o__pulse(pulse_on_sync_sig)
    );

    activelow_monostable_highlevel_triggered #(
        .PULSE_CLOCK_CYCLES(PULSE_CLOCK_CYCLES)
    ) ALMHLT (
        .i__clk(clk),
        .i__rst(reset),
        .i__sigin(sync_sig),
        .o__pulse_n(pulse_n_on_sync_sig)
    );

    activehigh_monostable_lowlevel_triggered #(
        .PULSE_CLOCK_CYCLES(PULSE_CLOCK_CYCLES)
    ) AHMLLT (
        .i__clk(clk),
        .i__rst(reset),
        .i__sigin_n(sync_sig_n),
        .o__pulse(pulse_on_sync_sig_n)
    );

    activelow_monostable_lowlevel_triggered #(
        .PULSE_CLOCK_CYCLES(PULSE_CLOCK_CYCLES)
    ) ALMLLT (
        .i__clk(clk),
        .i__rst(reset),
        .i__sigin_n(sync_sig_n),
        .o__pulse_n(pulse_n_on_sync_sig_n)
    );

endmodule
