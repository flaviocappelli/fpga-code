
// (C) 2021-2025 Flavio Cappelli
// Released under the MIT License.
//
// Generate an active high retriggerable oneshot pulse
// on the high level of the (active high) input signal.
//
// NOTE: The input signal must be synchronized with the
//       clock and it must be at least 1 clock wide (for
//       asynchronous signals apply a synchronizer first).
//
// NOTE: The time resolution of the pulse depends on the clock
//       (e.g. a 100MHz clock will give us a 10ns resolution).
//
// NOTE: An active high "edge triggered" monostable can be simply
//       obtained using the output of a "slow signal edgedetector"
//       as input to this module.

`default_nettype none               // Do not allow undeclared signals.
`include "timescale.vh"             // Timescale defined globally.


module activehigh_monostable_highlevel_triggered #(
    parameter PULSE_CLOCK_CYCLES = 8
) (
    input  wire i__clk,
    input  wire i__rst,
    input  wire i__sigin,
    output reg  o__pulse
);

    localparam MAX_COUNT = PULSE_CLOCK_CYCLES - 1;
    localparam COUNTER_SIZE = $clog2(MAX_COUNT + 1);

    reg [COUNTER_SIZE-1:0] pulse_counter = 0;

    always @(posedge i__clk, posedge i__rst) begin
        if (i__rst) begin
            o__pulse <= 1'b0;
            pulse_counter <= 0;
        end else begin
            if (i__sigin == 1'b1) begin
                o__pulse <= 1'b1;
                pulse_counter <= 0;
            end else if (pulse_counter != MAX_COUNT[COUNTER_SIZE-1:0])
                pulse_counter <= pulse_counter + 1;
            else
                o__pulse <= 1'b0;
        end
    end

endmodule
