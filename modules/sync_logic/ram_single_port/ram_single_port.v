
// (C) 2021-2025 Flavio Cappelli
// Released under the MIT License.
//
// Single Port Synchronous RAM.
//
// NOTE: FPGAs and ASICs do not have tri-state drivers inside the core,
//       only at I/O pads. So we don't need inputs like "cs" or "oe" here.
//       Because bi-directional data is possible only at I/O pins, this is
//       why internally everybody uses two distinct data buses for RAM data
//       input and output. See https://stackoverflow.com/questions/57434065
//
// NOTE: The read operation is always performed, so "o__rdata" changes at any
//       positive clock edge. By default, on writes "o__rdata" returns the DATA
//       ALREADY STORED at "i__raddr" address: this mode of operation is called
//       "old data read-during-write behaviour". To return the NEW WRITTEN DATA,
//       (i.e. "i__wdata") set NEWDATA_RDW_BEH to 1. Such last mode of operation
//       is called "new data read-during-write behaviour". Of course, if the write
//       operation is not enabled, "o__rdata" returns the data stored at "i__raddr".
//       You can also set NEWDATA_RDW_BEH to 2 to use a different code pattern for
//       the "new data read-during-write behaviour" (try both and select the best).
//       See "Intel Quartus Prime Pro Edition User Guide Design Recommendations",
//       section 1.4.1.5 & 1.4.1.6 (Single-Clock Synchronous RAM with "Old Data
//       Read-During-Write Behavior" & "New Data Read-During-Write Behavior").
//
// NOTE: One of the two modes described above usually requires some additional
//       logic and it is slower than the other (which one really depends on the
//       used FPGA family). If you don't need the data returned during the write
//       operations, perform some tests on the FPGA and choose the fastest mode.
//
// NOTE: Do not change the code below unless you know what you are doing: to
//       infer RAM functions, synthesis tools recognize certain patters of HDL
//       code and map the detected code to technology-specific implementations.
//       Many criteria must be met for the conversion to take place (See "Intel
//       Quartus Prime Pro Edition User Guide Design Recommendations", section
//       1.4); moreover, some synthesis tools (such as Yosys) are picky, and the
//       recognized patterns may also depend on the used FPGA family. The code
//       below has been carefully selected, and succesfully tested with iCE40
//       and ECP5 FPGAs, but it should work with many other FPGA families.

`default_nettype none                                       // Do not allow undeclared signals.
`include "timescale.vh"                                     // Timescale defined globally.


module ram_single_port #(
    parameter INIT_FILE = "",                               // RAM initialization file (optional).
    parameter DATA_WIDTH = 8,                               // Size of data bus (default 8 bits).
    parameter MEMORY_DEPTH = 64,                            // Memory size (default 64 locations).
    parameter NEWDATA_RDW_BEH = 0,                          // Read-during-write behaviour, see notes above.
    parameter ADDR_WIDTH = $clog2(MEMORY_DEPTH)             // Size of address bus (default depends on MEMORY_DEPTH).
) (
    input  wire                  i__clk,                    // Read/write clock, active on positive edge.
    input  wire                  i__wen,                    // Write enable input, active high.
    input  wire [ADDR_WIDTH-1:0] i__addr,                   // Read/write address input.
    input  wire [DATA_WIDTH-1:0] i__wdata,                  // Data input (write).
    output wire [DATA_WIDTH-1:0] o__rdata                   // Data output (read).
);

    reg [DATA_WIDTH-1:0] mem [0:MEMORY_DEPTH-1];            // Allocated memory.

    // Extract the required bits from the provided address (i__addr). This
    // gets rid of verilator warnings when ADDR_WIDTH > $clog2(MEMORY_DEPTH).
    localparam UPPER_ADDR_BIT = $clog2(MEMORY_DEPTH)-1;
    wire [UPPER_ADDR_BIT:0] addr = i__addr[UPPER_ADDR_BIT:0];

    initial begin
        if (INIT_FILE != 0) begin
            $readmemh(INIT_FILE, mem);                      // INIT (if initialization file is provided).
            `ifndef YOSYS
            $display("Initializing RAM from hex file '%s'.", INIT_FILE);    // CRASH YOSYS (*).
            `endif
        end
    end

    //generate (optional in Verilog-2005)
    if (NEWDATA_RDW_BEH == 0) begin : gen_ODRDWB            // Pattern for "old data read-during-write behaviour".

        reg [DATA_WIDTH-1:0] data_reg;                      // Used to store data output.

        always @(posedge i__clk) begin
            if (i__wen)                                     // WRITE to RAM if allowed.
                mem[addr] <= i__wdata;
            data_reg <= mem[addr];                          // Always READ old data from RAM and store it.
        end
        assign o__rdata = data_reg;                         // Output stored data.

    end else if (NEWDATA_RDW_BEH == 1) begin : gen_NDRDWB1  // Pattern for "new data read-during-write behaviour".

        reg [UPPER_ADDR_BIT:0] addr_reg;                    // Used to store read address.

        always @(posedge i__clk) begin
            if (i__wen)                                     // WRITE to RAM if allowed.
                mem[addr] <= i__wdata;
            addr_reg <= addr;                               // Always store read address.
        end
        assign o__rdata = mem[addr_reg];                    // Output data at read address.

    end else begin : gen_NDRDWB2                            // Another pattern for "new data read-during-write behaviour".

        reg [DATA_WIDTH-1:0] data_reg;                      // Used to store data output.

        always @(posedge i__clk) begin
            data_reg <= mem[addr];                          // Always READ old data from RAM and store it.
            if (i__wen) begin                               // WRITE to RAM if allowed.
                mem[addr] <= i__wdata;
                data_reg <= i__wdata;                       // Override old data with new data, see:
            end                                             // https://github.com/YosysHQ/yosys/issues/3384
        end
        assign o__rdata = data_reg;                         // Output stored data.

    end
    //endgenerate

endmodule

// NOTE (*): Since Yosys-37, the $display() statement causes a segmentation fault in
// the command "yosys -q -p 'read_blif -wideports hw.blif; write_verilog hw.synth'".
// To avoid this issue, we must exclude such statement from the code parsed by Yosys.
// Sadly, this will make the given message visible only in the behavioral simulation.
