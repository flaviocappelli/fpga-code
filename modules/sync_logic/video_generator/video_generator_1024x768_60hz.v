
// (C) 2021-2025 Flavio Cappelli
// Released under the MIT License.
//
// Timimg generator for video mode 1024x768 @ 60Hz.
//
// NOTE: This video mode needs a "pixel clock" of 65.0MHz.

`default_nettype none                       // Do not allow undeclared signals.
`include "timescale.vh"                     // Timescale defined globally.


module video_generator_1024x768_60hz (
    input  wire        i__clk,              // Pixel clock, active on positive edge.
    input  wire        i__rst,              // Reset, active high, synchronous.
    output wire [10:0] o__xp,               // Horizontal screen position.
    output wire  [9:0] o__yp,               // Vertical screen position.
    output wire        o__hs,               // Horizontal sync.
    output wire        o__vs,               // Vertical sync.
    output wire        o__de                // Data enable (low in blanking intervals).
    );

    video_generator_generic #(
        .H_RE(1024),                        // Horizontal resolution (pixel clocks).
        .V_RE(768),                         // Vertical resolution (pixel clocks).
        .H_FP(24),                          // Horizontal front porch (pixel clocks).
        .H_SY(136),                         // Horizontal sync pulse (pixel clocks).
        .H_BP(160),                         // Horizontal back porch (pixel clocks).
        .V_FP(3),                           // Vertical front porch (pixel clocks).
        .V_SY(6),                           // Vertical sync pulse (pixel clocks).
        .V_BP(29),                          // Vertical back porch (pixel clocks).
        .P_HS(0),                           // Polarity of horizontal sync (negative).
        .P_VS(0)                            // Polarity of vertical sync (negative).
    ) VIDEO_GEN (
        .i__clk(i__clk),
        .i__rst(i__rst),
        .o__xp(o__xp),
        .o__yp(o__yp),
        .o__hs(o__hs),
        .o__vs(o__vs),
        .o__de(o__de)
    );

endmodule
