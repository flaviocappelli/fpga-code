
// (C) 2021-2025 Flavio Cappelli
// Released under the MIT License.
//
// Timimg generator for video mode 800x600 @ 60Hz.
//
// NOTE: This video mode needs a "pixel clock" of 40.0MHz.

`default_nettype none                       // Do not allow undeclared signals.
`include "timescale.vh"                     // Timescale defined globally.


module video_generator_800x600_60hz (
    input  wire        i__clk,              // Pixel clock, active on positive edge.
    input  wire        i__rst,              // Reset, active high, synchronous.
    output wire [10:0] o__xp,               // Horizontal screen position.
    output wire  [9:0] o__yp,               // Vertical screen position.
    output wire        o__hs,               // Horizontal sync.
    output wire        o__vs,               // Vertical sync.
    output wire        o__de                // Data enable (low in blanking intervals).
    );

    video_generator_generic #(
        .H_RE(800),                         // Horizontal resolution (pixel clocks).
        .V_RE(600),                         // Vertical resolution (pixel clocks).
        .H_FP(40),                          // Horizontal front porch (pixel clocks).
        .H_SY(128),                         // Horizontal sync pulse (pixel clocks).
        .H_BP(88),                          // Horizontal back porch (pixel clocks).
        .V_FP(1),                           // Vertical front porch (pixel clocks).
        .V_SY(4),                           // Vertical sync pulse (pixel clocks).
        .V_BP(23),                          // Vertical back porch (pixel clocks).
        .P_HS(1),                           // Polarity of horizontal sync (positive).
        .P_VS(1)                            // Polarity of vertical sync (positive).
    ) VIDEO_GEN (
        .i__clk(i__clk),
        .i__rst(i__rst),
        .o__xp(o__xp),
        .o__yp(o__yp),
        .o__hs(o__hs),
        .o__vs(o__vs),
        .o__de(o__de)
    );

endmodule
