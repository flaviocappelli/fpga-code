
// (C) 2021-2025 Flavio Cappelli
// Released under the MIT License.
//
// Timimg generator for video mode 1280x1024 @ 60Hz.
//
// NOTE: This video mode needs a "pixel clock" of 108.0MHz.
//
// NOTE: The iCE40 LP8K on the TinyFPGA BX is not fast enough to perform
//       the comparisons required to display a generic object on the screen
//       (i.e. x>=obj_x && x<obj_x+OBJ_XSZ && y>=obj_y && y<obj_y+OBJ_YSZ)
//       at such pixel clock rate. The combinational delay will take longer
//       than the pixel clock period, probably causing visual artifacts. To
//       solve this, we must split the above test, and pipeline the result
//       over several cycles (and delay opportunely h/v syncs & data enable
//       signal). Note that issues like this (due to the propagation time)
//       cannot be detected with a simulation, but only on real hardware.

`default_nettype none                       // Do not allow undeclared signals.
`include "timescale.vh"                     // Timescale defined globally.


module video_generator_1280x1024_60hz (
    input  wire        i__clk,              // Pixel clock, active on positive edge.
    input  wire        i__rst,              // Reset, active high, synchronous.
    output wire [10:0] o__xp,               // Horizontal screen position.
    output wire [10:0] o__yp,               // Vertical screen position.
    output wire        o__hs,               // Horizontal sync.
    output wire        o__vs,               // Vertical sync.
    output wire        o__de                // Data enable (low in blanking intervals).
    );

    video_generator_generic #(
        .H_RE(1280),                        // Horizontal resolution (pixel clocks).
        .V_RE(1024),                        // Vertical resolution (pixel clocks).
        .H_FP(48),                          // Horizontal front porch (pixel clocks).
        .H_SY(112),                         // Horizontal sync pulse (pixel clocks).
        .H_BP(248),                         // Horizontal back porch (pixel clocks).
        .V_FP(1),                           // Vertical front porch (pixel clocks).
        .V_SY(3),                           // Vertical sync pulse (pixel clocks).
        .V_BP(38),                          // Vertical back porch (pixel clocks).
        .P_HS(1),                           // Polarity of horizontal sync (positive).
        .P_VS(1)                            // Polarity of vertical sync (positive).
    ) VIDEO_GEN (
        .i__clk(i__clk),
        .i__rst(i__rst),
        .o__xp(o__xp),
        .o__yp(o__yp),
        .o__hs(o__hs),
        .o__vs(o__vs),
        .o__de(o__de)
    );

endmodule
