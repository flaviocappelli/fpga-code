
VGA video modes that can be easily implemented on the TinyFPGA BX:

  Video Mode       HFreq (kHz)   VFreq (Hz)   Pixel Clock (MHz)   H/V sync polarity
   (note 1)         (note 2)      (note 3)       (note 4)             (note 5)
------------------------------------------------------------------------------------
  IBM  640x480       31.469        59.940          25.175                -/-
  VESA 640x480       37.500        75.000          31.500                -/-
  VESA 800x600       37.879        60.317          40.000                +/+
  VESA 1024x768      48.363        60.004          65.000                -/-
  VESA 1280x1024     63.981        60.020         108.000                +/+
------------------------------------------------------------------------------------

Notes:
  1. All listed video modes are not interlaced
  2. Horizontal frequency, sometimes reported rounded to the first decimal digit
  3. Vertical frequency / refresh rate, sometimes reported rounded to the integer
  4. Pixel frequency (clock frequency required)
  5. (-) active low, (+) active high
