
// (C) 2021-2025 Flavio Cappelli
// Released under the MIT License.
//
// Timimg generator for several video modes.

`default_nettype none                       // Do not allow undeclared signals.
`include "timescale.vh"                     // Timescale defined globally.


module video_generator_generic (            // I/O ports are defined below.
    i__clk, i__rst, o__xp, o__yp, o__hs, o__vs, o__de
    );

    // Video mode parameters (default 640x480 @ 60Hz).
    parameter H_RE = 640;                   // Horizontal resolution (pixel clocks).
    parameter V_RE = 480;                   // Vertical resolution (pixel clocks).
    parameter H_FP = 16;                    // Horizontal front porch (pixel clocks).
    parameter H_SY = 96;                    // Horizontal sync pulse (pixel clocks).
    parameter H_BP = 48;                    // Horizontal back porch (pixel clocks).
    parameter V_FP = 10;                    // Vertical front porch (pixel clocks).
    parameter V_SY = 2;                     // Vertical sync pulse (pixel clocks).
    parameter V_BP = 33;                    // Vertical back porch (pixel clocks).
    parameter P_HS = 0;                     // Polarity of horizontal sync (1 positive, 0 negative).
    parameter P_VS = 0;                     // Polarity of vertical sync (1 positive, 0 negative).

    // Calculate required constants.
    localparam HA_END = H_RE - 1;           // Last horizontal active pixel.
    localparam VA_END = V_RE - 1;           // Last vertical active line.
    localparam HS_STA = HA_END + H_FP + 1;  // Horizontal sync start (after the horizontal front porch).
    localparam VS_STA = VA_END + V_FP + 1;  // Vertical sync start (after the vertical front porch).
    localparam HS_END = HS_STA + H_SY - 1;  // Last horizontal sync pixel.
    localparam VS_END = VS_STA + V_SY - 1;  // Last vertical sync line.
    localparam LI_END = HS_END + H_BP;      // Last pixel on line (including the horizontal back porch).
    localparam FR_END = VS_END + V_BP;      // Last line on frame (including the vertical back porch).
    localparam XPSIZE = $clog2(LI_END + 1); // Size in bits of horixontal screen position (xp).
    localparam YPSIZE = $clog2(FR_END + 1); // Size in bits of vertical screen position (yp).

    // Define I/O ports.
    input  wire              i__clk;        // Pixel clock, active on positive edge.
    input  wire              i__rst;        // Reset, active high, synchronous.
    output reg  [XPSIZE-1:0] o__xp;         // Horizontal screen position.
    output reg  [YPSIZE-1:0] o__yp;         // Vertical screen position.
    output reg               o__hs;         // Horizontal sync.
    output reg               o__vs;         // Vertical sync.
    output reg               o__de;         // Data enable (low in blanking intervals).

    // Update horizontal and vertical screen positions.
    always @(posedge i__clk) begin
        if (i__rst) begin
            o__xp <= 0;
            o__yp <= 0;
        end else begin
            o__xp <= o__xp + 1;             // By default consider the next pixel.
            if (o__xp == LI_END) begin      // Last pixel on line? (i.e. last pixel of the horizontal back porch?)
                o__xp <= 0;                 // Begin the next line.
                o__yp <= o__yp + 1;         // By default consider the next line.
                if (o__yp == FR_END)        // Last line on frame? (i.e. last line of the vertical back porch?)
                    o__yp <= 0;             // Begin the next frame.
            end
        end
    end

    // Assign H/V sync and data enable.
    reg hs, vs;
    always @(*) begin
        hs = (o__xp >= HS_STA && o__xp <= HS_END);
        vs = (o__yp >= VS_STA && o__yp <= VS_END);

        o__hs = (P_HS ? hs : ~hs);
        o__vs = (P_VS ? vs : ~vs);
        o__de = (o__xp <= HA_END && o__yp <= VA_END && !i__rst);
    end

endmodule
