
// (C) 2021-2025 Flavio Cappelli
// Released under the MIT License.
//
// Double Edge 4bit Synchronous Decade Counter.
//
// See:
// https://circuitdigest.com/tutorial/synchronous-counter
// https://www.codecubix.eu/electronics/synchronous-decade-counter

`default_nettype none           // Do not allow undeclared signals.
`include "timescale.vh"         // Timescale defined globally.


module de_4bit_sync_decade_counter (
    input  wire       i__clk,
    input  wire       i__rst,
    output wire [3:0] o__counter
);

    wire i0, i1, i2, i3;        // FF inputs.
    wire q0, q1, q2, q3;        // FF outputs.

    assign i0 = 1;
    assign i1 = q0 & ~q3;
    assign i2 = q0 & q1;
    assign i3 = (q0 & q3) | (i2 & q2);

    de_t_ff DETFF_Q0 (.i__clk(i__clk), .i__rst(i__rst), .i__tin(i0), .o__qout(q0));
    de_t_ff DETFF_Q1 (.i__clk(i__clk), .i__rst(i__rst), .i__tin(i1), .o__qout(q1));
    de_t_ff DETFF_Q2 (.i__clk(i__clk), .i__rst(i__rst), .i__tin(i2), .o__qout(q2));
    de_t_ff DETFF_Q3 (.i__clk(i__clk), .i__rst(i__rst), .i__tin(i3), .o__qout(q3));

    assign o__counter = {q3, q2, q1, q0};

endmodule
