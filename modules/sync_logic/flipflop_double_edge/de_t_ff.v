
// (C) 2021-2025 Flavio Cappelli
// Released under the MIT License.
//
// Double Edge T Flip Flop.

`default_nettype none       // Do not allow undeclared signals.
`include "timescale.vh"     // Timescale defined globally.


module de_t_ff (
    input  wire i__clk,
    input  wire i__rst,
    input  wire i__tin,
    output wire o__qout
);

    wire ip;
    wire op;

    assign ip = i__tin ^ op;

    de_d_ff DEDFF (
        .i__clk(i__clk),
        .i__rst(i__rst),
        .i__din(ip),
        .o__qout(op)
    );

    assign o__qout = op;

endmodule
