
// (C) 2021-2025 Flavio Cappelli
// Released under the MIT License.
//
// See https://www.fpgatutorial.com/how-to-write-a-basic-verilog-testbench/

`default_nettype none                       // Do not allow undeclared signals.
`define DUMPSTR(x) `"x.vcd`"                // Required for GTKWave.
`define SIMULATION_TIME 30us                // Simulation time (in s, ms, us or ns).
`include "timescale.vh"                     // Timescale defined globally.

module testbench();
    initial begin
        $dumpfile(`DUMPSTR(`VCD_OUTPUT));   // Required for GTKWave.
        $dumpvars(0, testbench);            // Dump all variables in all instantiated modules.
        $printtimescale(testbench);         // Display the time scale and resolution (see above).
        #`SIMULATION_TIME                   // Wait until the simulation time is elapsed.
        $timeformat(-6, 2, "us", 1);        // Display the time elapsed in "us".
        $display("Simulation terminated after %t", $realtime);
        $finish;                            // Terminate the simulation.
    end


    // ---- Clock simulation ----

    reg clk;
    initial forever begin
        #0.4us clk = (clk === 1'b0);        // f = 1 / (0.4us + 0.6us) = 1MHz
        #0.6us clk = ~clk;
    end


    // ---- Reset simulation ----

    reg reset;
    initial begin
        #0.5us
        reset = 0;
        #2us
        reset = 1;
        #4us
        reset = 0;
        #21us
        reset = 1;
    end


    // ---- Double Edge 4-bit Binary Counter simulation ----

    wire [3:0] counter_qA;                  // Outputs.
    wire [3:0] counter_qB;

    _simul_top TOP (                        // Top module.
        .clk(clk),
        .reset(reset),
        .counter_qA(counter_qA),
        .counter_qB(counter_qB)
    );

endmodule
