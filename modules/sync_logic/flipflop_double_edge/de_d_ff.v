
// (C) 2021-2025 Flavio Cappelli
// Released under the MIT License.
//
// Double Edge D Flip Flop.
//
// NOTE: guarantees glitchless operation.
//
// See:
// https://stackoverflow.com/questions/19605881
// http://www.ralf-hildebrandt.de/publication/pdf_dff/pde_dff.pdf

`default_nettype none       // Do not allow undeclared signals.
`include "timescale.vh"     // Timescale defined globally.


module de_d_ff (
    input  wire i__clk,
    input  wire i__rst,
    input  wire i__din,
    output wire o__qout
);

    reg trig1, trig2;

    assign o__qout = trig1 ^ trig2;

    always @(posedge i__clk, posedge i__rst) begin
        if (i__rst)
            trig1 <= 1'b0 ;
        else
            trig1 <= i__din ^ trig2;
    end

    always @(negedge i__clk, posedge i__rst) begin
        if (i__rst)
            trig2 <= 1'b0 ;
        else
            trig2 <= i__din ^ trig1;
    end

endmodule
