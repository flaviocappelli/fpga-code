
// (C) 2021-2025 Flavio Cappelli
// Released under the MIT License.
//
// Divide the input clock by N.
//
// NOTE: If DIVISOR is an even number, the output signal "0__div_by"
//       will always have a 50% dutycycle, regardless of the dutycycle
//       of the input clock. Instead, if DIVISOR is odd, the output will
//       have a 50% dutycycle only with a perfect square input clock.
//
// This module is a reimplementation of
// the code described in the following paper:
// https://onlinelibrary.wiley.com/doi/pdf/10.1002/ecj.11921

`default_nettype none           // Do not allow undeclared signals.
`include "timescale.vh"         // Timescale defined globally.


module clock_divider #(
    parameter DIVISOR = 5
) (
    input  wire i__clk,         // Clock input.
    output wire o__div_by       // Clock divided by DIVISOR.
);

    //generate (optional in Verilog-2005)
    if (DIVISOR == 1) begin : gen_DIV_BY_1

        assign o__div_by = i__clk;

    end else if (DIVISOR == 2) begin : gen_DIV_BY_2

        reg clkout = 0;

        always @(posedge i__clk) begin
            clkout <= ~clkout;
        end

        assign o__div_by = clkout;

    end else begin : gen_DIV_BY_N

        localparam COUNTER_WIDTH = $clog2(DIVISOR);
        localparam DIVISOR_LESS_1_WIDER_TYPE = DIVISOR - 1;
        localparam DIVISOR_LESS_1 = DIVISOR_LESS_1_WIDER_TYPE[COUNTER_WIDTH-1:0];
        localparam DIVISOR_DIV_BY_2_WIDER_TYPE = DIVISOR >> 1;
        localparam DIVISOR_DIV_BY_2 = DIVISOR_DIV_BY_2_WIDER_TYPE[COUNTER_WIDTH-1:0];

        reg q1 = 0, q2 = 0;
        reg [COUNTER_WIDTH-1:0] counter = 0;

        always @(posedge i__clk) begin
            if (counter == DIVISOR_LESS_1)
                counter <= 0;
            else
                counter <= counter + 1'b1;
        end

        always @(posedge i__clk) begin
                q1 <= (counter < DIVISOR_DIV_BY_2);
        end

        always @(negedge i__clk) begin
                q2 <= q1;
        end

        assign o__div_by = q1 | (q2 & DIVISOR[0]);

    end
    //endgenerate

endmodule
