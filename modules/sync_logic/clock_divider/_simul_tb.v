
// (C) 2021-2025 Flavio Cappelli
// Released under the MIT License.
//
// See https://www.fpgatutorial.com/how-to-write-a-basic-verilog-testbench/

`default_nettype none                       // Do not allow undeclared signals.
`define DUMPSTR(x) `"x.vcd`"                // Required for GTKWave.
`define SIMULATION_TIME 35us                // Simulation time (in s, ms, us or ns).
`include "timescale.vh"                     // Timescale defined globally.

module testbench();
    initial begin
        $dumpfile(`DUMPSTR(`VCD_OUTPUT));   // Required for GTKWave.
        $dumpvars(0, testbench);            // Dump all variables in all instantiated modules.
        $printtimescale(testbench);         // Display the time scale and resolution (see above).
        #`SIMULATION_TIME                   // Wait until the simulation time is elapsed.
        $timeformat(-6, 2, "us", 1);        // Display the time elapsed in "us".
        $display("Simulation terminated after %t", $realtime);
        $finish;                            // Terminate the simulation.
    end


    // ---- Clock simulation ----

    localparam SQUARE_CLOCK = 1;            // SQUARE CLOCK (1) or IMPULSE (0).

    reg clk;
    initial forever begin
        if (SQUARE_CLOCK) begin
            #0.5us clk = (clk === 1'b0);    // f = 1 / 2*0.5us = 1MHz
        end else begin
            #0.8us clk = (clk === 1'b0);    // f = 1 / (0.8us + 0.2us) = 1MHz
            #0.2us clk = ~clk;
        end
    end


    // ---- Clock Divider simulation ----

    wire div_by1, div_by2, div_by3, div_by4, div_by5, div_by6;
    wire div_by7, div_by8, div_by9, div_by10, div_by11, div_by12;

    _simul_top TOP (                        // Top module.
        .clk(clk),
        .div_by1(div_by1),
        .div_by2(div_by2),
        .div_by3(div_by3),
        .div_by4(div_by4),
        .div_by5(div_by5),
        .div_by6(div_by6),
        .div_by7(div_by7),
        .div_by8(div_by8),
        .div_by9(div_by9),
        .div_by10(div_by10),
        .div_by11(div_by11),
        .div_by12(div_by12)
    );

endmodule
