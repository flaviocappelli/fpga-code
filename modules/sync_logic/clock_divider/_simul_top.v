
// (C) 2021-2025 Flavio Cappelli
// Released under the MIT License.
//
// This file is used to pass config parameters (if any) to the
// modules under test, ensuring the modules are configured the
// same way in both behavioral and post-synthesis simulation.
// It also makes "apio lint" happy because it guarantees that
// only one "top" module exists (Verilator requires it).

`default_nettype none           // Do not allow undeclared signals.
`include "timescale.vh"         // Timescale defined globally.

(* top *)                       // Mark this as top module, because
module _simul_top (             // sometimes Yosys doesn't detect it.
    input  wire clk,
    output wire div_by1,
    output wire div_by2,
    output wire div_by3,
    output wire div_by4,
    output wire div_by5,
    output wire div_by6,
    output wire div_by7,
    output wire div_by8,
    output wire div_by9,
    output wire div_by10,
    output wire div_by11,
    output wire div_by12
);

    clock_divider #(.DIVISOR(1)) DIV_BY_1 (
        .i__clk(clk),
        .o__div_by(div_by1)
    );

    clock_divider #(.DIVISOR(2)) DIV_BY_2 (
        .i__clk(clk),
        .o__div_by(div_by2)
    );

    clock_divider #(.DIVISOR(3)) DIV_BY_3 (
        .i__clk(clk),
        .o__div_by(div_by3)
    );

    clock_divider #(.DIVISOR(4)) DIV_BY_4 (
        .i__clk(clk),
        .o__div_by(div_by4)
    );

    clock_divider #(.DIVISOR(5)) DIV_BY_5 (
        .i__clk(clk),
        .o__div_by(div_by5)
    );

    clock_divider #(.DIVISOR(6)) DIV_BY_6 (
        .i__clk(clk),
        .o__div_by(div_by6)
    );

    clock_divider #(.DIVISOR(7)) DIV_BY_7 (
        .i__clk(clk),
        .o__div_by(div_by7)
    );

    clock_divider #(.DIVISOR(8)) DIV_BY_8 (
        .i__clk(clk),
        .o__div_by(div_by8)
    );

    clock_divider #(.DIVISOR(9)) DIV_BY_9 (
        .i__clk(clk),
        .o__div_by(div_by9)
    );

    clock_divider #(.DIVISOR(10)) DIV_BY_10 (
        .i__clk(clk),
        .o__div_by(div_by10)
    );

    clock_divider #(.DIVISOR(11)) DIV_BY_11 (
        .i__clk(clk),
        .o__div_by(div_by11)
    );

    clock_divider #(.DIVISOR(12)) DIV_BY_12 (
        .i__clk(clk),
        .o__div_by(div_by12)
    );

endmodule
