
// (C) 2021-2025 Flavio Cappelli
// Released under the MIT License.
//
// This file is used to pass config parameters (if any) to the
// modules under test, ensuring the modules are configured the
// same way in both behavioral and post-synthesis simulation.
// It also makes "apio lint" happy because it guarantees that
// only one "top" module exists (Verilator requires it).

`default_nettype none           // Do not allow undeclared signals.
`include "timescale.vh"         // Timescale defined globally.
`include "config.vh"            // Shared parameters.

(* top *)                       // Mark this as top module, because
module _simul_top (             // sometimes Yosys doesn't detect it.
    input  wire                   clk,
    input  wire                   en,
    input  wire [`ADDR_WIDTH-1:0] addr,
    output wire [`DATA_WIDTH-1:0] data
);

    rom_generic_with_en #(
        .INIT_FILE(`INIT_FILE),
        .DATA_WIDTH(`DATA_WIDTH),
        .MEMORY_DEPTH(`MEMORY_DEPTH)
    ) ROM (
        .i__clk(clk),
        .i__en(en),
        .i__addr(addr),
        .o__data(data)
    );

endmodule
