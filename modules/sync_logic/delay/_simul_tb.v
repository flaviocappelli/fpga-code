
// (C) 2021-2025 Flavio Cappelli
// Released under the MIT License.
//
// See https://www.fpgatutorial.com/how-to-write-a-basic-verilog-testbench/

`default_nettype none                       // Do not allow undeclared signals.
`define DUMPSTR(x) `"x.vcd`"                // Required for GTKWave.
`define SIMULATION_TIME 20us                // Simulation time (in s, ms, us or ns).
`include "timescale.vh"                     // Timescale defined globally.

module testbench();
    initial begin
        $dumpfile(`DUMPSTR(`VCD_OUTPUT));   // Required for GTKWave.
        $dumpvars(0, testbench);            // Dump all variables in all instantiated modules.
        $printtimescale(testbench);         // Display the time scale and resolution (see above).
        #`SIMULATION_TIME                   // Wait until the simulation time is elapsed.
        $timeformat(-6, 2, "us", 1);        // Display the time elapsed in "us".
        $display("Simulation terminated after %t", $realtime);
        $finish;                            // Terminate the simulation.
    end


    // ---- Clock simulation ----

    reg clk;
    initial forever
        #0.5us clk = (clk === 1'b0);        // f = 1 / 2*0.5us = 1MHz


    // ---- Reset simulation ----

    reg reset;                              // Reset, active high.
    initial begin
        #0.5us
        reset = 1;
        #5.0us
        reset = 0;
    end


    // ---- Delay simulation ----

    reg  [3:0] inp_sig;                     // Inputs signals, active high.
    wire [3:0] out_sig;                     // Delayed output of the (active high) input signals.
    wire [3:0] out_sig_n;                   // Like above but for the inverted (active low) input signals.

    _simul_top TOP (                        // Top module.
        .clk(clk),
        .reset(reset),
        // ---
        .inp_sig(inp_sig),
        .out_sig(out_sig),
        // ---
        .inp_sig_n(~inp_sig),
        .out_sig_n(out_sig_n)
    );

    initial begin
        #0.5us
        inp_sig = 0;
        #7us
        inp_sig = 3;
        #1us
        inp_sig = 5;
        #1us
        inp_sig = 7;
        #1us
        inp_sig = 9;
        #1us
        inp_sig = 11;
    end

endmodule
