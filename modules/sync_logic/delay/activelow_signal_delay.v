
// (C) 2021-2025 Flavio Cappelli
// Released under the MIT License.
//
// Delay a multibit signal for some clock cycles.
// From the yosys manual.
//
// NOTE: This module is for active low signals (all
//       bits in all stages are set to 1 at reset).

`default_nettype none                       // Do not allow undeclared signals.
`include "timescale.vh"                     // Timescale defined globally.


module activelow_signal_delay #(
    parameter DATA_WIDTH = 8,
    parameter STAGES = 4
) (
    input  wire                  i__clk,    // Clock, active on positive edge.
    input  wire                  i__rst,    // Reset, active high, asynchronous.
    input  wire [DATA_WIDTH-1:0] i__din_n,  // Input data.
    output wire [DATA_WIDTH-1:0] o__dout_n  // Output data.
);

    integer i;
    reg [DATA_WIDTH-1:0] ffs [STAGES-1:0];

    always @(posedge i__clk, posedge i__rst) begin
        if (i__rst) begin
            for (i = 0; i < STAGES; i = i+1)
                ffs[i] <= {DATA_WIDTH{1'b1}};
        end else begin
            ffs[0] <= i__din_n;
            for (i = 1; i < STAGES; i = i+1)
                ffs[i] <= ffs[i-1];
        end
    end

    assign o__dout_n = ffs[STAGES-1];

endmodule
