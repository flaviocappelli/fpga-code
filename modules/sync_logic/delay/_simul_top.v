
// (C) 2021-2025 Flavio Cappelli
// Released under the MIT License.
//
// This file is used to pass config parameters (if any) to the
// modules under test, ensuring the modules are configured the
// same way in both behavioral and post-synthesis simulation.
// It also makes "apio lint" happy because it guarantees that
// only one "top" module exists (Verilator requires it).

`default_nettype none           // Do not allow undeclared signals.
`include "timescale.vh"         // Timescale defined globally.

(* top *)                       // Mark this as top module, because
module _simul_top (             // sometimes Yosys doesn't detect it.
    input  wire       clk,
    input  wire       reset,
    // ---
    input  wire [3:0] inp_sig,
    output wire [3:0] out_sig,
    // ---
    input  wire [3:0] inp_sig_n,
    output wire [3:0] out_sig_n
);

    localparam STAGES = 3;

    activehigh_signal_delay #(
        .DATA_WIDTH(4),
        .STAGES(STAGES)
    ) DAHS (
        .i__clk(clk),
        .i__rst(reset),
        .i__din(inp_sig),
        .o__dout(out_sig)
    );

    activelow_signal_delay #(
        .DATA_WIDTH(4),
        .STAGES(STAGES)
    ) DALS (
        .i__clk(clk),
        .i__rst(reset),
        .i__din_n(inp_sig_n),
        .o__dout_n(out_sig_n)
    );

endmodule
