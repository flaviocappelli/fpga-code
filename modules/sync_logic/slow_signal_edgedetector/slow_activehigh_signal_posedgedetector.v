
// (C) 2021-2025 Flavio Cappelli
// Released under the MIT License.
//
// Slow Signal Positive Edge Detector (for ACTIVE HIGH signals).
//
// NOTE: The input signal must be synchronized with the clock
//       (for asynchronous signals apply a synchronizer first).
//
// NOTE: *** THIS DETECTOR IS DESIGNED FOR AN ACTIVE HIGH SIGNAL ***
//       With an active low signal a long pulse on "o__posedge" is
//       generated FROM START TO ONE CLOCK AFTER THE RESERT DEASSERT.

`default_nettype none               // Do not allow undeclared signals.
`include "timescale.vh"             // Timescale defined globally.


module slow_activehigh_signal_posedgedetector (
    input  wire i__clk,             // Clock, active on positive edge.
    input  wire i__rst,             // Reset, active high, asynchronous.
    input  wire i__sigin,           // Input signal, active high.
    output wire o__posedge          // Pulse on signal positive edge, active high.
);

    reg sigin_delayed;

    always @(posedge i__clk, posedge i__rst) begin
        if (i__rst)
            sigin_delayed <= 1'b0;
        else
            sigin_delayed <= i__sigin;
    end

    // The output signal will be one clock wide.
    assign o__posedge = (i__sigin) & (~sigin_delayed);

endmodule
