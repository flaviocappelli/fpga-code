
// (C) 2021-2025 Flavio Cappelli
// Released under the MIT License.
//
// Slow Signal Negative Edge Detector (for ACTIVE LOW signals).
//
// NOTE: The input signal must be synchronized with the clock
//       (for asynchronous signals apply a synchronizer first).
//
// NOTE: *** THIS DETECTOR IS DESIGNED FOR AN ACTIVE LOW SIGNAL ***
//       With an active high signal a long pulse on "o__negedge" is
//       generated FROM START TO ONE CLOCK AFTER THE RESERT DEASSERT.

`default_nettype none               // Do not allow undeclared signals.
`include "timescale.vh"             // Timescale defined globally.


module slow_activelow_signal_negedgedetector (
    input  wire i__clk,             // Clock, active on positive edge.
    input  wire i__rst,             // Reset, active high, asynchronous.
    input  wire i__sigin_n,         // Input signal, active low.
    output wire o__negedge          // Pulse on signal negative edge, active high.
);

    reg sigin_delayed_n;

    always @(posedge i__clk, posedge i__rst) begin
        if (i__rst)
            sigin_delayed_n <= 1'b1;
        else
            sigin_delayed_n <= i__sigin_n;
    end

    // The output signal will be one clock wide.
    assign o__negedge = (~i__sigin_n) & (sigin_delayed_n);

endmodule
