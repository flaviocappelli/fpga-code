
// (C) 2021-2025 Flavio Cappelli
// Released under the MIT License.
//
// This file is used to pass config parameters (if any) to the
// modules under test, ensuring the modules are configured the
// same way in both behavioral and post-synthesis simulation.
// It also makes "apio lint" happy because it guarantees that
// only one "top" module exists (Verilator requires it).

`default_nettype none           // Do not allow undeclared signals.
`include "timescale.vh"         // Timescale defined globally.

(* top *)                       // Mark this as top module, because
module _simul_top (             // sometimes Yosys doesn't detect it.
    input  wire clk,
    input  wire reset,
    input  wire dummy_async_sig,
    // ---
    output wire sync_sig,
    output wire sync_sig_posedge,
    output wire sync_sig_negedge,
    // ---
    output wire sync_sig_n,
    output wire sync_sig_n_posedge,
    output wire sync_sig_n_negedge
);

    // Use the dummy async signal to generate synchronized signals.

    slow_activehigh_signal_synchronizer SAHSS (
        .i__clk(clk),
        .i__rst(reset),
        .i__async_sig(dummy_async_sig),
        .o__sync_sig(sync_sig)
    );

    assign sync_sig_n = ~sync_sig;

    // Detect edges.

    slow_activehigh_signal_posedgedetector SAHSPED (
        .i__clk(clk),
        .i__rst(reset),
        .i__sigin(sync_sig),
        .o__posedge(sync_sig_posedge)
    );

    slow_activehigh_signal_negedgedetector SAHSNED (
        .i__clk(clk),
        .i__rst(reset),
        .i__sigin(sync_sig),
        .o__negedge(sync_sig_negedge)
    );

    slow_activelow_signal_posedgedetector SALSPED (
        .i__clk(clk),
        .i__rst(reset),
        .i__sigin_n(sync_sig_n),
        .o__posedge(sync_sig_n_posedge)
    );

    slow_activelow_signal_negedgedetector SALSNED (
        .i__clk(clk),
        .i__rst(reset),
        .i__sigin_n(sync_sig_n),
        .o__negedge(sync_sig_n_negedge)
    );

    // Just to test that the above signals can be used as enable.

    reg [3:0] test_counter = 0;
    always @(posedge clk) begin
        if (sync_sig_posedge)
            test_counter <= test_counter + 1'b1;
        if (sync_sig_negedge)
            test_counter <= test_counter + 1'b1;
    end

endmodule
