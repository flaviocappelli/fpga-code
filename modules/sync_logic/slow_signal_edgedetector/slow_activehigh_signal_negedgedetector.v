
// (C) 2021-2025 Flavio Cappelli
// Released under the MIT License.
//
// Slow Signal Negative Edge Detector (for ACTIVE HIGH signals).
//
// NOTE: The input signal must be synchronized with the clock
//       (for asynchronous signals apply a synchronizer first).

`default_nettype none               // Do not allow undeclared signals.
`include "timescale.vh"             // Timescale defined globally.


module slow_activehigh_signal_negedgedetector (
    input  wire i__clk,             // Clock, active on positive edge.
    input  wire i__rst,             // Reset, active high, asynchronous.
    input  wire i__sigin,           // Input signal, active high.
    output wire o__negedge          // Pulse on signal negative edge, active high.
);

    reg sigin_delayed;

    always @(posedge i__clk, posedge i__rst) begin
        if (i__rst)
            sigin_delayed <= 1'b0;
        else
            sigin_delayed <= i__sigin;
    end

    // The output signal will be one clock wide.
    assign o__negedge = (~i__sigin) & (sigin_delayed);

endmodule
