
// (C) 2021-2025 Flavio Cappelli
// Released under the MIT License.
//
// See https://www.fpgatutorial.com/how-to-write-a-basic-verilog-testbench/

`default_nettype none                       // Do not allow undeclared signals.
`define DUMPSTR(x) `"x.vcd`"                // Required for GTKWave.
`define SIMULATION_TIME 70us                // Simulation time (in s, ms, us or ns).
`include "timescale.vh"                     // Timescale defined globally.

module testbench();
    initial begin
        $dumpfile(`DUMPSTR(`VCD_OUTPUT));   // Required for GTKWave.
        $dumpvars(0, testbench);            // Dump all variables in all instantiated modules.
        $printtimescale(testbench);         // Display the time scale and resolution (see above).
        #`SIMULATION_TIME                   // Wait until the simulation time is elapsed.
        $timeformat(-6, 2, "us", 1);        // Display the time elapsed in "us".
        $display("Simulation terminated after %t", $realtime);
        $finish;                            // Terminate the simulation.
    end


    // ---- Clock simulation ----

    reg clk;
    initial forever
        #0.5us clk = (clk === 1'b0);        // f = 1 / 2*0.5us = 1MHz


    // ---- Reset simulation ----

    reg reset, enable;
    initial begin
        #0.5us
        reset  = 0;
        #1us
        reset  = 1;
        #4us
        reset  = 0;
    end


    // ---- Pseudo Random Number simulation ----

    wire [15:0] val16;                      // Output.
    wire [31:0] val32;
    wire new16, new32;

    _simul_top TOP (                        // Top module.
        .clk(clk),
        .reset(reset),
        .val16(val16),
        .new16(new16),
        .val32(val32),
        .new32(new32)
    );

endmodule
