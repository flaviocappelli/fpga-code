
// (C) 2021-2025 Flavio Cappelli
// Released under the MIT License.
//
// This file is used to pass config parameters (if any) to the
// modules under test, ensuring the modules are configured the
// same way in both behavioral and post-synthesis simulation.
// It also makes "apio lint" happy because it guarantees that
// only one "top" module exists (Verilator requires it).

`default_nettype none           // Do not allow undeclared signals.
`include "timescale.vh"         // Timescale defined globally.

(* top *)                       // Mark this as top module, because
module _simul_top (             // sometimes Yosys doesn't detect it.
    input  wire        clk,
    input  wire        reset,
    output wire [15:0] val16,
    output wire        new16,
    output wire [31:0] val32,
    output wire        new32
);

    random_generator_16bit RNDGEN16 (
        .i__clk(clk),
        .i__rst(reset),
        .o__data(val16),
        .o__newrn(new16)
    );

    random_generator_32bit RNDGEN32 (
        .i__clk(clk),
        .i__rst(reset),
        .o__data(val32),
        .o__newrn(new32)
    );

endmodule
