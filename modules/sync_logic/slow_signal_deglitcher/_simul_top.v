
// (C) 2021-2025 Flavio Cappelli
// Released under the MIT License.
//
// This file is used to pass config parameters (if any) to the
// modules under test, ensuring the modules are configured the
// same way in both behavioral and post-synthesis simulation.
// It also makes "apio lint" happy because it guarantees that
// only one "top" module exists (Verilator requires it).

`default_nettype none           // Do not allow undeclared signals.
`include "timescale.vh"         // Timescale defined globally.

(* top *)                       // Mark this as top module, because
module _simul_top (             // sometimes Yosys doesn't detect it.
    input  wire clk,
    input  wire reset,
    input  wire dummy_async_sig,
    // ---
    output wire sync_sig,
    output wire sync_sig_degl_f0,
    output wire sync_sig_degl_f1,
    output wire sync_sig_degl_f2,
    // ---
    output wire sync_sig_n,
    output wire sync_sig_n_degl_f0,
    output wire sync_sig_n_degl_f1,
    output wire sync_sig_n_degl_f2
);

    localparam N_SAMPLES = 8;

    // Use the dummy async signal to generate synchronized signals.

    slow_activehigh_signal_synchronizer SAHSS (
        .i__clk(clk),
        .i__rst(reset),
        .i__async_sig(dummy_async_sig),
        .o__sync_sig(sync_sig)
    );

    assign sync_sig_n = ~sync_sig;

    // Apply filters.

    slow_activehigh_signal_deglitcher #(
        .N_SAMPLES(N_SAMPLES), .FILTER_TYPE(0)
    ) SAHSGF0 (
        .i__clk(clk),
        .i__rst(reset),
        .i__sigin(sync_sig),
        .o__sigout(sync_sig_degl_f0)
    );

    slow_activehigh_signal_deglitcher #(
        .N_SAMPLES(N_SAMPLES), .FILTER_TYPE(1)
    ) SAHSGF1 (
        .i__clk(clk),
        .i__rst(reset),
        .i__sigin(sync_sig),
        .o__sigout(sync_sig_degl_f1)
    );

    slow_activehigh_signal_deglitcher #(
        .N_SAMPLES(N_SAMPLES), .FILTER_TYPE(2)
    ) SAHSGF2 (
        .i__clk(clk),
        .i__rst(reset),
        .i__sigin(sync_sig),
        .o__sigout(sync_sig_degl_f2)
    );

    slow_activelow_signal_deglitcher #(
        .N_SAMPLES(N_SAMPLES), .FILTER_TYPE(0)
    ) SALSGF0 (
        .i__clk(clk),
        .i__rst(reset),
        .i__sigin_n(sync_sig_n),
        .o__sigout_n(sync_sig_n_degl_f0)
    );

    slow_activelow_signal_deglitcher #(
        .N_SAMPLES(N_SAMPLES), .FILTER_TYPE(1)
    ) SALSGF1 (
        .i__clk(clk),
        .i__rst(reset),
        .i__sigin_n(sync_sig_n),
        .o__sigout_n(sync_sig_n_degl_f1)
    );

    slow_activelow_signal_deglitcher #(
        .N_SAMPLES(N_SAMPLES), .FILTER_TYPE(2)
    ) SALSGF2 (
        .i__clk(clk),
        .i__rst(reset),
        .i__sigin_n(sync_sig_n),
        .o__sigout_n(sync_sig_n_degl_f2)
    );

endmodule
