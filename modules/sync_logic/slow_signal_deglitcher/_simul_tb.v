
// (C) 2021-2025 Flavio Cappelli
// Released under the MIT License.
//
// See https://www.fpgatutorial.com/how-to-write-a-basic-verilog-testbench/

`default_nettype none                       // Do not allow undeclared signals.
`define DUMPSTR(x) `"x.vcd`"                // Required for GTKWave.
`define SIMULATION_TIME 100us               // Simulation time (in s, ms, us or ns).
`include "timescale.vh"                     // Timescale defined globally.

module testbench();
    initial begin
        $dumpfile(`DUMPSTR(`VCD_OUTPUT));   // Required for GTKWave.
        $dumpvars(0, testbench);            // Dump all variables in all instantiated modules.
        $printtimescale(testbench);         // Display the time scale and resolution (see above).
        #`SIMULATION_TIME                   // Wait until the simulation time is elapsed.
        $timeformat(-6, 2, "us", 1);        // Display the time elapsed in "us".
        $display("Simulation terminated after %t", $realtime);
        $finish;                            // Terminate the simulation.
    end


    // ---- Clock simulation ----

    reg clk;
    initial forever
        #0.5us clk = (clk === 1'b0);        // f = 1 / 2*0.5us = 1MHz


    // ---- Reset simulation ----

    reg reset;                              // Reset, active high.
    initial begin
        #0.5us
        reset = 1;
        #5.0us
        reset = 0;
    end


    // ---- Slow Signal Deglitcher simulation ----

    localparam GLITCH_FREE_SIGNAL = 0;

    reg  dummy_async_sig;                   // Dummy asynchronous inputs signal, used to generate synchronized signals.
    wire sync_sig;                          // Active high synchronized signals (from the above async signal).
    wire sync_sig_n;                        // Active low synchronized signals (from the above async signal).
    wire sync_sig_degl_f0;                  // Output deglitched with filter 0 (active high).
    wire sync_sig_degl_f1;                  // Output deglitched with filter 1 (active high).
    wire sync_sig_degl_f2;                  // Output deglitched with filter 2 (active high).
    wire sync_sig_n_degl_f0;                // Output deglitched with filter 0 (active low).
    wire sync_sig_n_degl_f1;                // Output deglitched with filter 1 (active low).
    wire sync_sig_n_degl_f2;                // Output deglitched with filter 2 (active low).

    _simul_top TOP (                        // Top module.
        .clk(clk),
        .reset(reset),
        .dummy_async_sig(dummy_async_sig),
        // ---
        .sync_sig(sync_sig),
        .sync_sig_degl_f0(sync_sig_degl_f0),
        .sync_sig_degl_f1(sync_sig_degl_f1),
        .sync_sig_degl_f2(sync_sig_degl_f2),
        // ---
        .sync_sig_n(sync_sig_n),
        .sync_sig_n_degl_f0(sync_sig_n_degl_f0),
        .sync_sig_n_degl_f1(sync_sig_n_degl_f1),
        .sync_sig_n_degl_f2(sync_sig_n_degl_f2)
    );

    initial begin
        #0.5us
        dummy_async_sig = 0;
        #7us
        dummy_async_sig = 1;
        #1us
        dummy_async_sig = 0;
        #10us
        dummy_async_sig = 1;

        if (GLITCH_FREE_SIGNAL) begin
            #24us
            dummy_async_sig = 0;
        end else begin
            #10us
            dummy_async_sig = 0;
            #1us
            dummy_async_sig = 1;
            #2us
            dummy_async_sig = 0;
            #2us
            dummy_async_sig = 1;
            #9us
            dummy_async_sig = 0;
            #5us
            dummy_async_sig = 1;
            #2us
            dummy_async_sig = 0;
            #2us
            dummy_async_sig = 1;
            #1us
            dummy_async_sig = 0;
            #4us
            dummy_async_sig = 1;
            #1us
            dummy_async_sig = 0;
            #20us
            dummy_async_sig = 1;
            #1us
            dummy_async_sig = 0;
            #2us
            dummy_async_sig = 1;
            #1us
            dummy_async_sig = 0;
        end
    end

endmodule
