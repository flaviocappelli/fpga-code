
// (C) 2021-2025 Flavio Cappelli
// Released under the MIT License.
//
// Slow Signal Deglitch Filter (for ACTIVE LOW signals).
//
// Three implementations for the filter are provided:
//
//  * 1st implementation is based on a shift register
//  * 2nd implementation is based on a simple up counter
//  * 3rd implementation is based on an up/down counter
//
// In the 1st and 2nd implementations, the output is set to logic 0 when the current
// and previous N-1 input samples are logic 0, and set to logic 1 when the current and
// previous N-1 input samples are logic 1, otherwise the output is unchanged from its
// current value. So, the two implementations have the same input/output behaviour, but
// they differ on the amount of FPGA cells required for the synthesis: with the Lattice
// iCE40 LP family, and "yosys" + "nextpnr" as synthesis tools, the 1st implementation
// seems to use less cells only for N <= 6 (see the table below). Other FPGAs and other
// tools could show a slightly different result, so try both and see for yourself.
//
// The 3rd implementation has a totally different behaviour. The counter is incremented
// when the current input sample is logic 0 and decremented when the current input sample
// is logic 1: on the upper count limit (N-1), the output is set to logic 0 and the counter
// is prevented from further counting up; on the lower count limit (0), the output is set
// to logic 1 and the counter is prevented from further counting down; if the counter is
// between these two values the output is unchanged from its current value. So, in this 3rd
// implementation the time required for the output to switch depends on how many glitches
// have occurred on the last N-1 input samples (rather than how many clock cycles have
// passed from the last glitch, as in the other twos implementations). Thus, we can say
// that this 3rd implementation has a better response time on a glitched signal.
//
// With a clean signal all implementations have the same response time (N-1 clock cycles).
// Of course, which implementation to use depends on the developed application.
//
// The following table show the FPGA cells used for the three implementations of the filter
// on the Lattice iCE40 LP8K using "yosys" (0.9+932) and "nextpnr-ice40" (git sha1 dd7f7a5).
// These values are obtained from the output of "nextpnr-ice40" (i.e. xx/7680), subtracting
// the number of used cells for the same design without the filter.
//
//    N_SAMPLES   FILTER_TYPE=0   FILTER_TYPE=1   FILTER_TYPE=2
//   ===========================================================
//        3             4               4               3
//        4             5               6               3
//        5             8               6               8
//        6             9              11               9
//        7            12              10               9
//        8            13              11               9
//        9            16              10              10
//       10            17              12              10
//       11            19              12              10
//       12            20              12              10
//       13            24              12              10
//       14            25              12              10
//       15            26              12              10
//       16            27              12              10
//      256           427              21              18
//      512           853              18              19
//     1024          1707              19              20
//     2048          3413              21              23
//
// NOTE: N_SAMPLES must be >= 3. Keep N_SAMPLES small to not delay
//       too much the output signal and to not use too much logic.
//
// NOTE: For active low signals, the output is set to 1 at reset.
//
// NOTE: The input signal must be synchronized with the clock
//       (for asynchronous signals apply a synchronizer first).

`default_nettype none               // Do not allow undeclared signals.
`include "timescale.vh"             // Timescale defined globally.


module slow_activelow_signal_deglitcher #(
    parameter N_SAMPLES = 4,        // Number of samples (including current input) to check.
    parameter FILTER_TYPE = -1      // 0: shift register, 1: simple up counter,
                                    // 2: up/down counter, -1: auto (see below).
) (
    input  wire i__clk,             // Clock, active on positive edge.
    input  wire i__rst,             // Reset, active high, asynchronous.
    input  wire i__sigin_n,         // Input signal (active low).
    output reg  o__sigout_n         // Output signal (active low).
);

    //generate (optional in Verilog-2005)
    if ((FILTER_TYPE == 0) || (FILTER_TYPE == -1 && N_SAMPLES <= 6)) begin : gen_SALSD_FT0

        //
        // FILTER TYPE 0 - IMPLEMENTATION WITH A SHIFT REGISTER.
        //

        reg [N_SAMPLES-2:0] filter;

        always @(posedge i__clk, posedge i__rst) begin
            if (i__rst) begin
                o__sigout_n <= 1'b1;
                filter <= {N_SAMPLES-1{1'b1}};
            end else begin
                if (&{filter, i__sigin_n} == 1'b1)              // If ALL 1 set the output state to 1.
                    o__sigout_n <= 1'b1;
                else if (|{filter, i__sigin_n} == 1'b0)         // If ALL 0 set the output state to 0.
                    o__sigout_n <= 1'b0;
                filter <= {filter[N_SAMPLES-3:0], i__sigin_n};
            end
        end

    end else if (FILTER_TYPE == 1 || FILTER_TYPE == -1) begin : gen_SALSD_FT1

        //
        // FILTER TYPE 1 - IMPLEMENTATION WITH A SIMPLE UP COUNTER.
        //
        // This implementation use a simple counter and a FF. At each positive clock edge the input
        // level is sampled into the FF. The old stored level in the FF is compared with the current
        // input value: if they differ the counter is reset; if they stay identical for a specified
        // count, then the stored value is assigned to the output and the counter is prevented from
        // further counting up. Because the input is presampled we must count N-1 samples, not N.

        localparam COUNTER_WIDTH = $clog2(N_SAMPLES-1);
        localparam COUNTER_UPPER_LIMIT_WIDER_TYPE = N_SAMPLES - 2;
        localparam COUNTER_UPPER_LIMIT = COUNTER_UPPER_LIMIT_WIDER_TYPE[COUNTER_WIDTH-1:0];

        reg i__sigin_n_old;
        reg [COUNTER_WIDTH-1:0] counter;

        // FF.
        always @(posedge i__clk, posedge i__rst) begin
            if (i__rst)
                i__sigin_n_old <= 1'b1;
            else
                i__sigin_n_old <= i__sigin_n;
        end

        // Counter.
        always @(posedge i__clk, posedge i__rst) begin
            if (i__rst) begin
                o__sigout_n <= 1'b1;
                counter <= {COUNTER_WIDTH{1'b0}};
            end else if (i__sigin_n_old != i__sigin_n)
                counter <= {COUNTER_WIDTH{1'b0}};
            else if (counter == COUNTER_UPPER_LIMIT)
                o__sigout_n <= i__sigin_n_old;
            else
                counter <= counter + 1'b1;
        end

    end else begin : gen_SALSD_FT2

        //
        // FILTER TYPE 2 - IMPLEMENTATION WITH AN UP/DOWN COUNTER.
        //

        localparam COUNTER_WIDTH = $clog2(N_SAMPLES);
        localparam COUNTER_UPPER_LIMIT_WIDER_TYPE = N_SAMPLES - 1;
        localparam COUNTER_UPPER_LIMIT = COUNTER_UPPER_LIMIT_WIDER_TYPE[COUNTER_WIDTH-1:0];

        reg [COUNTER_WIDTH-1:0] updowncnt;
        integer direction;                                      // Less logic is required using an integer variable for
                                                                // the conditioned increment/decrement of the counter.
        always @(posedge i__clk, posedge i__rst) begin
            if (i__rst) begin
                o__sigout_n <= 1'b1;
                updowncnt <= {COUNTER_WIDTH{1'b0}};
            end else begin
                direction = 0;
                case (i__sigin_n)
                    1'b1 :                                      // The input has normal level. If the counter has reached
                        if (updowncnt == 0)                     // the lower limit set the output to the normal logic level.
                            o__sigout_n <= 1'b1;
                        else
                            direction = -1;
                    1'b0 :                                      // The input has active level. If the counter has reached
                        if (updowncnt == COUNTER_UPPER_LIMIT)   // the upper limit set the output to the active logic level.
                            o__sigout_n <= 1'b0;
                        else
                            direction = +1;
                endcase
                updowncnt <= updowncnt + direction[COUNTER_WIDTH-1:0];
            end
        end

    end
    //endgenerate

endmodule
