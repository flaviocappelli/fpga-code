
// (C) 2021-2025 Flavio Cappelli
// Released under the MIT License.
//
// JK flip flop.

`default_nettype none       // Do not allow undeclared signals.
`include "timescale.vh"     // Timescale defined globally.


module jk_ff (
    input  wire i__clk,
    input  wire i__rst,
    input  wire i__jin,
    input  wire i__kin,
    output reg  o__qout
);

    always @(posedge i__clk, posedge i__rst) begin
        if (i__rst)
            o__qout <= 1'b0;
        else
            case ({i__jin,i__kin})
                2'b00 : o__qout <= o__qout;
                2'b01 : o__qout <= 0;
                2'b10 : o__qout <= 1;
                2'b11 : o__qout <= ~o__qout;
            endcase
    end

endmodule
