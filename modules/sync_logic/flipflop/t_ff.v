
// (C) 2021-2025 Flavio Cappelli
// Released under the MIT License.
//
// T Flip Flop.

`default_nettype none       // Do not allow undeclared signals.
`include "timescale.vh"     // Timescale defined globally.


module t_ff (
    input  wire i__clk,
    input  wire i__rst,
    input  wire i__tin,
    output wire o__qout
);

    wire ip;
    wire op;

    assign ip = i__tin ^ op;

    d_ff DFF (
        .i__clk(i__clk),
        .i__rst(i__rst),
        .i__din(ip),
        .o__qout(op)
    );

    assign o__qout = op;

endmodule
