
// (C) 2021-2025 Flavio Cappelli
// Released under the MIT License.
//
// See https://www.fpgatutorial.com/how-to-write-a-basic-verilog-testbench/

`default_nettype none                       // Do not allow undeclared signals.
`define DUMPSTR(x) `"x.vcd`"                // Required for GTKWave.
`define SIMULATION_TIME 50us                // Simulation time (in s, ms, us or ns).
`include "timescale.vh"                     // Timescale defined globally.

module testbench();
    initial begin
        $dumpfile(`DUMPSTR(`VCD_OUTPUT));   // Required for GTKWave.
        $dumpvars(0, testbench);            // Dump all variables in all instantiated modules.
        $printtimescale(testbench);         // Display the time scale and resolution (see above).
        #`SIMULATION_TIME                   // Wait until the simulation time is elapsed.
        $timeformat(-6, 2, "us", 1);        // Display the time elapsed in "us".
        $display("Simulation terminated after %t", $realtime);
        $finish;                            // Terminate the simulation.
    end


    // ---- Clock simulation ----

    reg clk;
    initial forever
        #0.5us clk = (clk === 1'b0);        // f = 1 / 2*0.5us = 1MHz


    // ---- Reset simulation ----

    reg reset;
    initial begin
        #0.5us
        reset = 0;
        #2us
        reset = 1;
        #4us
        reset = 0;
        #35us
        reset = 1;
        #4us
        reset = 0;
    end


    // ---- Flop Flop JK Master Slave simulation ----

    reg d, j, k, t;                         // Inputs.
    wire d_q, jk_q, t_q;                    // Outputs.

    _simul_top TOP (                        // Top module.
        .clk(clk),
        .reset(reset),
        .d(d),
        .j(j),
        .k(k),
        .t(t),
        .d_q(d_q),
        .jk_q(jk_q),
        .t_q(t_q)
    );

    initial begin
        #0.5us
        d = 0;
        j = 0;
        k = 0;
        t = 0;
        #10us
        d = 1;
        j = 1;
        t = 1;
        #5us
        d = 0;
        j = 0;
        t = 0;
        #5us
        k = 1;
        #5us
        k = 0;
        #5us
        j = 1;
        k = 1;
        #5us
        j = 0;
        k = 0;
    end

endmodule
