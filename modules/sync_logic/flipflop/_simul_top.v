
// (C) 2021-2025 Flavio Cappelli
// Released under the MIT License.
//
// This file is used to pass config parameters (if any) to the
// modules under test, ensuring the modules are configured the
// same way in both behavioral and post-synthesis simulation.
// It also makes "apio lint" happy because it guarantees that
// only one "top" module exists (Verilator requires it).

`default_nettype none           // Do not allow undeclared signals.
`include "timescale.vh"         // Timescale defined globally.

(* top *)                       // Mark this as top module, because
module _simul_top (             // sometimes Yosys doesn't detect it.
    input  wire clk,
    input  wire reset,
    input  wire d,
    input  wire j,
    input  wire k,
    input  wire t,
    output wire d_q,
    output wire jk_q,
    output wire t_q
);

    d_ff DFF (
        .i__clk(clk),
        .i__rst(reset),
        .i__din(d),
        .o__qout(d_q)
    );

    jk_ff JKFF (
        .i__clk(clk),
        .i__rst(reset),
        .i__jin(j),
        .i__kin(k),
        .o__qout(jk_q)
    );

    t_ff TFF (
        .i__clk(clk),
        .i__rst(reset),
        .i__tin(t),
        .o__qout(t_q)
    );

endmodule
