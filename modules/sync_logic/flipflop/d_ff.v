
// (C) 2021-2025 Flavio Cappelli
// Released under the MIT License.
//
// D Flip Flop.

`default_nettype none       // Do not allow undeclared signals.
`include "timescale.vh"     // Timescale defined globally.


module d_ff (
    input  wire i__clk,
    input  wire i__rst,
    input  wire i__din,
    output reg  o__qout
);

    always @(posedge i__clk, posedge i__rst) begin
        if (i__rst)
            o__qout <= 1'b0;
        else
            o__qout <= i__din;
    end

endmodule
