
// (C) 2021-2025 Flavio Cappelli
// Released under the MIT License.
//
// Generic Synchronous Counter.

`default_nettype none                               // Do not allow undeclared signals.
`include "timescale.vh"                             // Timescale defined globally.


module counter_generic #(
    parameter COUNTER_WIDTH = 4,                    // Number of bits in the counter.
    parameter COUNT_FROM    = 0,                    // Counter first value (reset value).
    parameter COUNT_TO      = 2^COUNTER_WIDTH-1,    // Counter last value (before restarting).
    parameter STEP          = 1                     // Counter increment (use -1 to count backward).
) (
    input  wire                     i__clk,         // Clock, active on positive edge.
    input  wire                     i__rst,         // Reset, active high, asynchronous.
    input  wire                     i__en,          // Count enable, active high.
    output wire [COUNTER_WIDTH-1:0] o__counter,     // Output counter value.
    output wire                     o__overflow     // Counter overflow.
);

    reg overflow;
    reg [COUNTER_WIDTH-1:0] counter;

    always @(posedge i__clk, posedge i__rst) begin
        if (i__rst) begin
            counter <= COUNT_FROM;                  // Reset counter value and output.
            overflow <= 1'b0;                       // Reset overflow signal.
        end else if (i__en) begin                   // Count only when enable is active (high).
            if (counter == COUNT_TO) begin          // Check if counter has reached the final value.
                counter <= COUNT_FROM;              // Reset counter value and output.
                overflow <= 1'b1;                   // Set the overflow signal.
            end else begin
                /* verilator lint_off WIDTH */      // Suppress an annoying warning of Verilator (safe).
                counter <= counter + STEP;          // Update counter value (and output).
                /* verilator lint_on WIDTH */
                overflow <= 1'b0;                   // Reset the overflow signal.
            end
        end
    end
    assign o__counter = counter;
    assign o__overflow = overflow;

endmodule
