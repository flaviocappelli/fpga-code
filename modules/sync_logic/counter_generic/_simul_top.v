
// (C) 2021-2025 Flavio Cappelli
// Released under the MIT License.
//
// This file is used to pass config parameters (if any) to the
// modules under test, ensuring the modules are configured the
// same way in both behavioral and post-synthesis simulation.
// It also makes "apio lint" happy because it guarantees that
// only one "top" module exists (Verilator requires it).

`default_nettype none           // Do not allow undeclared signals.
`include "timescale.vh"         // Timescale defined globally.
`include "config.vh"            // Shared parameters.

(* top *)                       // Mark this as top module, because
module _simul_top (             // sometimes Yosys doesn't detect it.
    input  wire                      clk,
    input  wire                      reset,
    input  wire                      enable,
    output wire [`COUNTER_WIDTH-1:0] counter,
    output wire                      overflow
);

    counter_generic #(
        .COUNTER_WIDTH(`COUNTER_WIDTH),
        .COUNT_FROM(`COUNT_FROM),
        .COUNT_TO(`COUNT_TO),
        .STEP(`STEP)
    ) CNT (
        .i__clk(clk),
        .i__rst(reset),
        .i__en(enable),
        .o__counter(counter),
        .o__overflow(overflow)
    );

endmodule
