
// File included by testbench and top module: set shared parameters.

`ifndef _config_vh_
`define _config_vh_

`define COUNTER_WIDTH   5
`define COUNT_FROM     20
`define COUNT_TO        5
`define STEP           -1

`endif  //_config_vh_


// The following line forces KDE's Kate editor to recognize
// this file as "Verilog" source (for syntax highlighting).
// kate: hl verilog;
