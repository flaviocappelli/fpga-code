
// (C) 2021-2025 Flavio Cappelli
// Released under the MIT License.
//
// See https://www.fpgatutorial.com/how-to-write-a-basic-verilog-testbench/

`default_nettype none                       // Do not allow undeclared signals.
`define DUMPSTR(x) `"x.vcd`"                // Required for GTKWave.
`define SIMULATION_TIME 70us                // Simulation time (in s, ms, us or ns).
`include "timescale.vh"                     // Timescale defined globally.
`include "config.vh"                        // Shared parameters.

module testbench();
    initial begin
        $dumpfile(`DUMPSTR(`VCD_OUTPUT));   // Required for GTKWave.
        $dumpvars(0, testbench);            // Dump all variables in all instantiated modules.
        $printtimescale(testbench);         // Display the time scale and resolution (see above).
        #`SIMULATION_TIME                   // Wait until the simulation time is elapsed.
        $timeformat(-6, 2, "us", 1);        // Display the time elapsed in "us".
        $display("Simulation terminated after %t", $realtime);
        $finish;                            // Terminate the simulation.
    end


    // ---- Clock simulation ----

    reg clk;
    initial forever
        #0.5us clk = (clk === 1'b0);        // f = 1 / 2*0.5us = 1MHz


    // ---- Reset/Enable simulation ----

    reg reset, enable;
    initial begin
        #0.5us
        reset  = 0;
        enable = 0;                         // Counting disabled.
        #2us
        reset  = 1;
        #4us
        reset  = 0;
        #4us
        enable = 1;                         // Counting enabled.
        #45us
        enable = 0;                         // Counting disabled.
        #4us
        reset  = 1;
        #4us
        reset  = 0;
    end


    // ---- Generic Counter simulation ----

    wire [`COUNTER_WIDTH-1:0] counter;      // Output.
    wire overflow;                          // Output.

    _simul_top TOP (                        // Top module.
        .clk(clk),
        .reset(reset),
        .enable(enable),
        .counter(counter),
        .overflow(overflow)
    );

endmodule
