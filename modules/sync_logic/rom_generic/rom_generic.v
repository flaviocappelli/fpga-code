
// (C) 2021-2025 Flavio Cappelli
// Released under the MIT License.
//
// Generic Synchronous ROM.
//
// NOTE: FPGAs and ASICs do not have tri-state drivers inside the core,
//       only at I/O pads. So we don't need inputs like "cs" or "oe" here.
//
// NOTE: ROM must be initialized using an hex datafile, see the INIT_FILE
//       parameters below (for the syntax allowed into the init file, see
//       https://projectf.io/posts/initialize-memory-in-verilog/). Due to
//       current limitations in Yosys, the verilog code is able to detect
//       if the initialization file has been specified (and raise an error
//       if not) only running the behavioural simulation, but not running
//       a build or a post-synthesis simulation (unfortunately), so BEWARE!

`default_nettype none                               // Do not allow undeclared signals.
`include "timescale.vh"                             // Timescale defined globally.


module rom_generic #(
    parameter INIT_FILE = "",                       // ROM initialization file (mandatory).
    parameter DATA_WIDTH = 8,                       // Size of data bus (default 8 bits).
    parameter MEMORY_DEPTH = 64,                    // Memory size (default 64 locations).
    parameter ADDR_WIDTH = $clog2(MEMORY_DEPTH)     // Size of address bus (default depends on MEMORY_DEPTH).
) (
    input  wire                  i__clk,            // Clock, active on positive edge.
    input  wire [ADDR_WIDTH-1:0] i__addr,           // Address input.
    output reg  [DATA_WIDTH-1:0] o__data            // Data output.
);

    reg [DATA_WIDTH-1:0] mem [0:MEMORY_DEPTH-1];

    // Extract the required bits from the provided address (i__addr). This
    // gets rid of verilator warnings when ADDR_WIDTH > $clog2(MEMORY_DEPTH).
    localparam UPPER_ADDR_BIT = $clog2(MEMORY_DEPTH)-1;
    wire [UPPER_ADDR_BIT:0] addr = i__addr[UPPER_ADDR_BIT:0];

    // NOTE: there is currently a nasty bug in YOSYS: without 'if (INIT_FILE != 0)'
    // YOSYS takes the default value for INIT_FILE ("") even when INIT_FILE is not
    // "", raising an error in $readmemh(). Also there is currently no clean way in
    // YOSYS to thrown an error in order to notify the user of invalid parameters.
    initial begin
        if (INIT_FILE != 0) begin
            $readmemh(INIT_FILE, mem);              // INIT.
        `ifndef YOSYS
            $display("Initializing ROM from hex file '%s'.", INIT_FILE);    // CRASH YOSYS (*).
        end else begin
            $fatal(1, "*** Missing ROM initialization file ***");
        `endif
        end
    end

    always @(posedge i__clk) begin
        o__data <= mem[addr];                       // READ.
    end

endmodule

// NOTE (*): Since Yosys-37, the $display() statement causes a segmentation fault in
// the command "yosys -q -p 'read_blif -wideports hw.blif; write_verilog hw.synth'".
// To avoid this issue, we must exclude such statement from the code parsed by Yosys.
// Sadly, this will make the given message visible only in the behavioral simulation.
