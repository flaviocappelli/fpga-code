
// (C) 2021-2025 Flavio Cappelli
// Released under the MIT License.
//
// See https://www.fpgatutorial.com/how-to-write-a-basic-verilog-testbench/

`default_nettype none                       // Do not allow undeclared signals.
`define DUMPSTR(x) `"x.vcd`"                // Required for GTKWave.
`define SIMULATION_TIME 35us                // Simulation time (in s, ms, us or ns).
`include "timescale.vh"                     // Timescale defined globally.
`include "config.vh"                        // Shared parameters.

module testbench();
    initial begin
        $dumpfile(`DUMPSTR(`VCD_OUTPUT));   // Required for GTKWave.
        $dumpvars(0, testbench);            // Dump all variables in all instantiated modules.
        $printtimescale(testbench);         // Display the time scale and resolution (see above).
        #`SIMULATION_TIME                   // Wait until the simulation time is elapsed.
        $timeformat(-6, 2, "us", 1);        // Display the time elapsed in "us".
        $display("Simulation terminated after %t", $realtime);
        $finish;                            // Terminate the simulation.
    end


    // ---- Clock simulation ----

    reg clk;
    initial forever
        #0.5us clk = (clk === 1'b0);        // f = 1 / 2*0.5us = 1MHz


    // ---- Synchronous ROM simulation ----

    reg  [`ADDR_WIDTH-1:0] addr;            // Address.
    wire [`DATA_WIDTH-1:0] data;            // Data output.

    _simul_top TOP (                        // Top module.
        .clk(clk),
        .addr(addr),
        .data(data)
    );

    integer i;
    initial begin
        #0.5us
        addr = 0;
        #1.3us
        for (i = 0; i < `MEMORY_DEPTH; ++i) begin
            addr = i;
            #1.0us
            ;
        end
    end

endmodule
