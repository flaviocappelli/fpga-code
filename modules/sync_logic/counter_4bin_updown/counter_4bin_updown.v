
// (C) 2021-2025 Flavio Cappelli
// Released under the MIT License.
//
// 4-Bit Synchronous Binary Up/Down Counter.

`default_nettype none                               // Do not allow undeclared signals.
`include "timescale.vh"                             // Timescale defined globally.


module counter_4bin_updown (
    input  wire       i__clk,                       // Clock, active on positive edge.
    input  wire       i__rst,                       // Reset, active high, asynchronous.
    input  wire       i__en,                        // Count enable, active high.
    input  wire       i__updown,                    // Up/Down mode: low -> up, high -> down.
    output reg  [3:0] o__counter                    // Output counter value.
);

    integer direction;                              // With a direction variable the logic is more optimized!

    always @(posedge i__clk, posedge i__rst) begin
        if (i__rst)
            o__counter <= 0;                        // Reset counter value and output.
        else begin
            if (~i__updown)
                direction = 1;
            else
                direction = -1;
            if (i__en)
                o__counter <= o__counter + direction[3:0];
        end
    end

endmodule
