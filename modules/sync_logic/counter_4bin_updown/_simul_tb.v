
// (C) 2021-2025 Flavio Cappelli
// Released under the MIT License.
//
// See https://www.fpgatutorial.com/how-to-write-a-basic-verilog-testbench/

`default_nettype none                       // Do not allow undeclared signals.
`define DUMPSTR(x) `"x.vcd`"                // Required for GTKWave.
`define SIMULATION_TIME 90us                // Simulation time (in s, ms, us or ns).
`include "timescale.vh"                     // Timescale defined globally.

module testbench();
    initial begin
        $dumpfile(`DUMPSTR(`VCD_OUTPUT));   // Required for GTKWave.
        $dumpvars(0, testbench);            // Dump all variables in all instantiated modules.
        $printtimescale(testbench);         // Display the time scale and resolution (see above).
        #`SIMULATION_TIME                   // Wait until the simulation time is elapsed.
        $timeformat(-6, 2, "us", 1);        // Display the time elapsed in "us".
        $display("Simulation terminated after %t", $realtime);
        $finish;                            // Terminate the simulation.
    end


    // ---- Clock simulation ----

    reg clk;
    initial forever
        #0.5us clk = (clk === 1'b0);        // f = 1 / 2*0.5us = 1MHz


    // ---- Reset/Enable/UpDown simulation ----

    reg reset, enable, updown;
    initial begin
        #0.5us
        reset  = 0;
        enable = 0;                         // Counting disabled.
        updown = 0;                         // Increment mode.
        #2us
        reset  = 1;
        #4us
        reset  = 0;
        #4us
        enable = 1;                         // Counting enabled.
        #35us
        enable = 0;                         // Counting disabled.
        #4us
        updown = 1;                         // Decrement mode.
        enable = 1;                         // Counting enabled.
        #35us
        enable = 0;                         // Counting enabled.
    end


    // ---- 4-Bit Synchronous Binary Up/Down Counter simulation ----

    wire[3:0] counter_q;                    // Output.

    counter_4bin_updown CNT (
        .i__clk(clk),
        .i__rst(reset),
        .i__en(enable),
        .i__updown(updown),
        .o__counter(counter_q)
    );

endmodule
