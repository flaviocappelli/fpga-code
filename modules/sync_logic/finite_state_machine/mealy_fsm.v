
// (C) 2021-2025 Flavio Cappelli
// Released under the MIT License.
//
// Example of simple Mealy state machine.
// See state diagram in "doc/mealy_fsm_diagram.png"
// See also https://en.wikipedia.org/wiki/Mealy_machine
//
// NOTE: Mealy machine's output depends on both the current state and
//       the input. In the state diagram, the output value is described
//       on the transition edges, after the input and the '/' separator.
//
// NOTE: If Yosys detects a FSM, under appropriate conditions it recodes
//       the FSM register and the FSM states to the One-hot encoding (see
//       below). The recoded FSM always needs a reset (possibly asynchronous
//       otherwise Yosys has to guess the reset state from the code). If we
//       don't provide a sync/async reset, the recoded FSM will start NOT
//       INITIALIZED (i.e. with illegal state) and the FSM will not work.
//
// NOTE: Due to the recoding, the behavioural and post-synthesis simulations
//       will be different (at least in the state register size and values).
//
//
// ABOUT THE FSM STATE ENCODING
// ----------------------------
// Finite State Machines (FSMs) can be written in various ways. The encoding
// of the FSM states affects FSM performance in terms of speed, resource usage
// (registers, logic) and power consumption. Common encodings for FSMs are:
//
//  * Gray encoding: successive states only differ by one bit
//    ("0000", "0001", "0011", "0010", "0110", ...).
//
//  * Binary encoding: states are enumerated as binary numbers
//    ("0000", "0001", "0010", "0011", "0100", ...);
//
//  * One-hot encoding: states are represented by patterns with exactly
//    one '1' ("000001", "000010", "000100", "001000", "010000", ...);
//
//  * SEC-DED encoding: codes that, with additional logic, are capable
//    of correcting single-bit errors and detecting double-bit errors
//    (see https://en.wikipedia.org/wiki/Hamming_code)
//
// The choosed encoding depends on the nature of the design. Gray encoding
// minimize power dissipation and reduces glitches in FSMs with LIMITED OR
// NO BRANCHES. Binary encoding minimizes the length of the state vector,
// which is good for CPLDs. One-hot encoding uses more registers and less
// logic to optimize speed and reduce power dissipation. SEC-DED codes are
// used to improve tolerance to noise and spurious events (like cosmic rays)
// that can flip bits in state registers (there are also other methods not
// described here). Many synthesis tools (including Yosys) use one-hot as the
// default encoding scheme for state machine up to 32 states. Note that, for
// clarity and ease of maintenance, designers usually don't use the above
// encodings directly, but rather enumerated values, that are translated to
// the desidered encoding by the synthesis tool. Of course, if the desidered
// encoding scheme is not supported by the tool it must be implemented by
// hand (and the automatic conversion must be prevented on that FSM).
//
// When Yosys detects a FSM (via "synth_ice40" or similar command), by default
// it converts the FSM register and FSM states to one-hot (if some conditions
// apply, for example if we don't directly access any individual state bits).
// THE ONE-HOT ENCODING IS ALWAYS ZERO INITIALIZED AT FPGA CONFIGURATION:
//
//      000000  BOOT (FPGA configuration)
//      000001  S0
//      000010  S1
//      000100  S2
//      ......  ..
//
// A one-hot encoded FSM, with N encoding bits, has "2^N − N" illegal states
// and THE INITIAL STATE AT THE FPGA CONFIGURATION IS ONE OF THEM: this explain
// why we need a reset. Without a reset the FSM cannot know its true starting
// state (so it cannot work). Note that an initial value provided by hand on
// the state register will break the recoding (at least in Yosys). If needed,
// we can tell Yosys to ignore the FSM or encode it in a different way, using
// the "fsm_encoding" attribute (for more infos see the Yosys user's manual):
//
//  (* fsm_encoding = "auto" *)    - This is the default: can be used to force
//                                   Yosys to detect registers that should be
//                                   considered FSM state registers (and Yosys
//                                   will select the best encoding for the FSM).
//                                   Note that marking registers that are not
//                                   suitable for FSM recoding can cause the
//                                   synthesis to fail or produce wrong logic.
//
//  (* fsm_encoding = "binary" *)  - Tell Yosys to use the binary encodings
//                                   for the marked state register.
//
//  (* fsm_encoding = "one-hot" *) - Tell Yosys to use the one-hot encodings
//                                   for the marked state register.
//
//  (* fsm_encoding = "gray" *)    - Currently unimplemented and ignored in
//                                   Yosys (gray encoding).
//
//  (* fsm_encoding = "user" *)    - Tell Yosys to detects the FSM, but to not
//                                   recode the states.
//
//  (* fsm_encoding = "none" *)    - Tell Yosys to completely ignore the FSM.
//                                   This can be used on registers that Yosys
//                                   detects as FSM registers but shouldn't be
//                                   considered as such (or we don't want to).
//
// As stated above, Yosys and many other synthesis tools recode FSM states to
// one-hot because it favors speed and optimizations. Theoretically, it is easy
// to detect invalid states in the one-hot encoding; practically, such detection
// prevents the synthesis tool to perform the above optimizations: the only way a
// one-hot encoding can be faster (and possibly smaller) than a binary encoding
// is that the illegal states are ignored. Indeed, if we put a "recover state"
// in the "default" statement of the "case/endcase" state transition, Yosys
// detect it and disable the FSM recoding (and the FSM optimizations).
//
// At least in Yosys, if we need to protect the FSM from deadlocks, irrecoverable
// loops and other functional issues (due to illegal states), we must implement
// the desidered encoding (and the protection mechanisms) by ourselves. See
// https://www.eetimes.com/using-fpgas-in-mission-critical-systems/

`default_nettype none                       // Do not allow undeclared signals.
`include "timescale.vh"                     // Timescale defined globally.


module mealy_fsm
(
    input  wire       i__clk,
    input  wire       i__rst,
    input  wire       i__din,
    output reg  [1:0] o__dout
);

    // FSM states. We can use any value here (unless we
    // don't want the state recoding performed by Yosys).
    localparam S0 = 50,
               S1 = 51,
               S2 = 52,
               S3 = 53;

    // State register: "integer" is preferred, to ensure the FSM is detected by
    // Yosys, so the state register and the FSM states can be recoded to one-hot.
    // DO NOT SET ANY INITIAL VALUE HERE, OTHERWISE THE FSM RECODE IS SKIPPED.
    integer state, nextstate;

    // Synchronous logic: state transition.
    always @ (posedge i__clk, posedge i__rst) begin
        if (i__rst)
            state <= S0;                    // Initial state.
        else
            state <= nextstate;             // State transition.
        end

    // Combinational logic: next state.
    always @ (*) begin
        nextstate = state;                  // Default state: unchanged (also prevents latches, DO NOT REMOVE).
        case (state)                        // Just an example. We can also use "case ({state, i__din})"

            S0: begin
                if (i__din == 1)
                    nextstate = S2;
            end

            S1: begin
                if (i__din == 1)
                    nextstate = S0;
                else
                    nextstate = S2;
            end

            S2: begin
                if (i__din == 1)
                    nextstate = S1;
                else
                    nextstate = S3;
            end

            S3: begin
                if (i__din == 0)
                    nextstate = S1;
            end

            default:;                       // Makes verilator happy if the case is not full. NOTE: IF
                                            // WE SET A STATE HERE (TO HANDLE THE ILLEGAL STATES), YOSYS
                                            // DETECT IT, DISABLE THE FSM RECODING AND FSM OPTIMIZATIONS,
                                            // AND EMIT THE MESSAGE: "Circuit seems to be self-resetting".
        endcase
    end

    // Combinational logic: output (depends on both the current state and the input).
    always @ (*) begin
        o__dout = 2'b00;                    // Default output (also prevents latches, DO NOT REMOVE).
        case (state)                        // Just an example. We could also use "case ({state, i__din})"

            S0: if (i__din == 1)
                    o__dout = 2'b11;

            S1: if (i__din == 1)
                    o__dout = 2'b11;

            S2: if (i__din == 1)
                    o__dout = 2'b10;
                else
                    o__dout = 2'b01;

            S3: if (i__din == 1)
                    o__dout = 2'b10;
                else
                    o__dout = 2'b01;

            default:;                       // Makes verilator happy if the case is not full.
        endcase
    end
endmodule
