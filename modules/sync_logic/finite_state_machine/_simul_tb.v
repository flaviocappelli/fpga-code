
// (C) 2021-2025 Flavio Cappelli
// Released under the MIT License.
//
// See https://www.fpgatutorial.com/how-to-write-a-basic-verilog-testbench/

`default_nettype none                       // Do not allow undeclared signals.
`define DUMPSTR(x) `"x.vcd`"                // Required for GTKWave.
`define SIMULATION_TIME 40us                // Simulation time (in s, ms, us or ns).
`include "timescale.vh"                     // Timescale defined globally.

module testbench();
    initial begin
        $dumpfile(`DUMPSTR(`VCD_OUTPUT));   // Required for GTKWave.
        $dumpvars(0, testbench);            // Dump all variables in all instantiated modules.
        $printtimescale(testbench);         // Display the time scale and resolution (see above).
        #`SIMULATION_TIME                   // Wait until the simulation time is elapsed.
        $timeformat(-6, 2, "us", 1);        // Display the time elapsed in "us".
        $display("Simulation terminated after %t", $realtime);
        $finish;                            // Terminate the simulation.
    end


    // ---- Clock simulation ----

    reg clk;
    initial forever
        #0.5us clk = (clk === 1'b0);        // f = 1 / 2*0.5us = 1MHz


    // ---- Reset simulation ----

    reg reset;                              // Reset, active high.
    initial begin
        #0.5us
        reset = 1;
        #5.0us
        reset = 0;
    end


    // ---- Moore / Mealy FSM simulation ----

    reg        fsm_din;                     // FSM input.
    wire [1:0] moore_dout;                  // Moore FSM output.
    wire [1:0] mealy_dout;                  // Mealy FSM output.

    _simul_top TOP (                        // Top module.
        .clk(clk),
        .reset(reset),
        .fsm_din(fsm_din),
        .moore_dout(moore_dout),
        .mealy_dout(mealy_dout)
    );

    integer i;
    initial begin
        #0.5us
        fsm_din = 0;
        #6.0us
        fsm_din = 1;
        #7.0us
        fsm_din = 0;
        #7.0us
        fsm_din = 1;
        #3.0us
        fsm_din = 0;
        #5.0us
        fsm_din = 1;
        #5.0us
        fsm_din = 0;
    end

endmodule
