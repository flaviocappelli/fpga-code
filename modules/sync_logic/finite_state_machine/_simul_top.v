
// (C) 2021-2025 Flavio Cappelli
// Released under the MIT License.
//
// This file is used to pass config parameters (if any) to the
// modules under test, ensuring the modules are configured the
// same way in both behavioral and post-synthesis simulation.
// It also makes "apio lint" happy because it guarantees that
// only one "top" module exists (Verilator requires it).

`default_nettype none           // Do not allow undeclared signals.
`include "timescale.vh"         // Timescale defined globally.

(* top *)                       // Mark this as top module, because
module _simul_top (             // sometimes Yosys doesn't detect it.
    input  wire       clk,
    input  wire       reset,
    input  wire       fsm_din,
    output wire [1:0] moore_dout,
    output wire [1:0] mealy_dout
);

    moore_fsm MOORE (
        .i__clk(clk),
        .i__rst(reset),
        .i__din(fsm_din),
        .o__dout(moore_dout)
    );

    mealy_fsm MEALY (
        .i__clk(clk),
        .i__rst(reset),
        .i__din(fsm_din),
        .o__dout(mealy_dout)
    );

endmodule
