
// (C) 2021-2025 Flavio Cappelli
// Released under the MIT License.
//
// See https://www.fpgatutorial.com/how-to-write-a-basic-verilog-testbench/

`default_nettype none                       // Do not allow undeclared signals.
`define DUMPSTR(x) `"x.vcd`"                // Required for GTKWave.
`define SIMULATION_TIME 25us                // Simulation time (in s, ms, us or ns).
`include "timescale.vh"                     // Timescale defined globally.

module testbench();
    initial begin
        $dumpfile(`DUMPSTR(`VCD_OUTPUT));   // Required for GTKWave.
        $dumpvars(0, testbench);            // Dump all variables in all instantiated modules.
        $printtimescale(testbench);         // Display the time scale and resolution (see above).
        #`SIMULATION_TIME                   // Wait until the simulation time is elapsed.
        $timeformat(-6, 2, "us", 1);        // Display the time elapsed in "us".
        $display("Simulation terminated after %t", $realtime);
        $finish;                            // Terminate the simulation.
    end


    // ---- Clock simulation ----

    reg clk;
    initial forever
        #0.5us clk = (clk === 1'b0);        // f = 1 / 2*0.5us = 1MHz


    // ---- RegisterFile simulation ----

    reg        wen;                         // Write enable.
    reg  [2:0] sel_ri;                      // Input register select address.
    reg  [2:0] sel_ro1;                     // Output register 1 select address.
    reg  [2:0] sel_ro2;                     // Output register 2 select address.
    reg  [3:0] data_in;                     // Data input to selected input register.
    wire [3:0] data_out1;                   // Data output from selected output register 1.
    wire [3:0] data_out2;                   // Data output from selected output register 2.

    _simul_top TOP (                        // Top module.
        .clk(clk),
        .wen(wen),
        .sel_ri(sel_ri),
        .sel_ro1(sel_ro1),
        .sel_ro2(sel_ro2),
        .data_in(data_in),
        .data_out1(data_out1),
        .data_out2(data_out2)
    );

    integer i;
    initial begin
        #0.5us
        wen = 0;
        sel_ri = 0;
        sel_ro1 = 0;
        sel_ro2 = 7;
        data_in = 0;
        #3.4us

        // Write.
        for (i = 0; i < 8; ++i) begin
            sel_ri = i;
            data_in = 8+i;
            wen = 1;
            #0.7us
            wen = 0;
            #0.3us
            ;
        end
        #3.0us

        // Read.
        for (i = 0; i < 8; ++i) begin
            sel_ro1 = i;
            sel_ro2 = 7-i;
            #1.0us
            ;
        end
    end

endmodule
