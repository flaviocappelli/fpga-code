
// (C) 2021-2025 Flavio Cappelli
// Released under the MIT License.
//
// This file is used to pass config parameters (if any) to the
// modules under test, ensuring the modules are configured the
// same way in both behavioral and post-synthesis simulation.
// It also makes "apio lint" happy because it guarantees that
// only one "top" module exists (Verilator requires it).

`default_nettype none           // Do not allow undeclared signals.
`include "timescale.vh"         // Timescale defined globally.

(* top *)                       // Mark this as top module, because
module _simul_top (             // sometimes Yosys doesn't detect it.
    input  wire       clk,
    input  wire       wen,
    input  wire [2:0] sel_ri,
    input  wire [2:0] sel_ro1,
    input  wire [2:0] sel_ro2,
    input  wire [3:0] data_in,
    output wire [3:0] data_out1,
    output wire [3:0] data_out2
);

    registerfile #(
        .N_REGS(8),
        .DATA_WIDTH(4),
        .CLK_FALLING_EDGE(0)
    ) REGFILE (
        .i__clk(clk),
        .i__wen(wen),
        .i__sri(sel_ri),
        .i__sro1(sel_ro1),
        .i__sro2(sel_ro2),
        .i__din(data_in),
        .o__dout1(data_out1),
        .o__dout2(data_out2)
    );

endmodule
