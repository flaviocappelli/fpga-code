
// (C) 2021-2025 Flavio Cappelli
// Released under the MIT License.
//
// Array of registers for softcores.
//
// NOTE: The readings of output registers are continuous; the writing
//       of the input register is instead performed by default on the
//       positive clock edges (when "i__wen" is high). Negative clock
//       edges can be selected by setting CLK_FALLING_EDGE = 1.
//
// NOTE: The reset is not required.

`default_nettype none                               // Do not allow undeclared signals.
`include "timescale.vh"                             // Timescale defined globally.


module registerfile #(
    parameter N_REGS = 8,                           // Number of registers (default 8).
    parameter DATA_WIDTH = 8,                       // Size of data bus (default 8 bits).
    parameter CLK_FALLING_EDGE = 0,                 // If 1 capture data input on the falling clock edge.
    parameter RSEL_WIDTH = $clog2(N_REGS)           // Size of register select (default depends on N_REGS).
) (
    input  wire                  i__clk,            // Write clock, by default active on positive edge.
    input  wire                  i__wen,            // Write enable input, always active high.
    input  wire [RSEL_WIDTH-1:0] i__sri,            // Input register select address.
    input  wire [RSEL_WIDTH-1:0] i__sro1,           // Output register 1 select address.
    input  wire [RSEL_WIDTH-1:0] i__sro2,           // Output register 2 select address.
    input  wire [DATA_WIDTH-1:0] i__din,            // Data input to selected input register (see i__sri).
    output wire [DATA_WIDTH-1:0] o__dout1,          // Data output from selected output register 1 (see i__sro1).
    output wire [DATA_WIDTH-1:0] o__dout2           // Data output from selected output register 2 (see i__sro2).
);

    wire clock;
    reg [DATA_WIDTH-1:0] regs [N_REGS-1:0];         // Registers.

    assign clock = i__clk ^ CLK_FALLING_EDGE;       // Determine active clock edge (gate optimized away at compile time).

    always @ (posedge clock) begin                  // Register input.
        if (i__wen)
            regs[i__sri] <= i__din;
    end

    assign o__dout1 = regs[i__sro1];                // Registers output.
    assign o__dout2 = regs[i__sro2];

endmodule
