
// (C) 2021-2025 Flavio Cappelli
// Released under the MIT License.
//
// See https://www.fpgatutorial.com/how-to-write-a-basic-verilog-testbench/

`default_nettype none                       // Do not allow undeclared signals.
`define DUMPSTR(x) `"x.vcd`"                // Required for GTKWave.
`define SIMULATION_TIME 500us               // Simulation time (in s, ms, us or ns).
`include "timescale.vh"                     // Timescale defined globally.

module testbench();
    initial begin
        $dumpfile(`DUMPSTR(`VCD_OUTPUT));   // Required for GTKWave.
        $dumpvars(0, testbench);            // Dump all variables in all instantiated modules.
        $printtimescale(testbench);         // Display the time scale and resolution (see above).
        #`SIMULATION_TIME                   // Wait until the simulation time is elapsed.
        $timeformat(-6, 2, "us", 1);        // Display the time elapsed in "us".
        $display("Simulation terminated after %t", $realtime);
        $finish;                            // Terminate the simulation.
    end


    // ---- Clock simulation ----

    // With f = 100MHz the timings of the data bitstream are very precise, but the WS2812B
    // should also work with lower clock frequencies (up to 5MHz). NOTE: If you modify the
    // frequency below, you must also adjust the .CLK_MHz() parameter in "_simul_top.v".

    reg clk;
    initial forever
        #5ns clk = (clk === 1'b0);          // f = 1 / 2*50ns = 100MHz


    // ---- Reset simulation ----

    reg reset;                              // Reset, active high.
    initial begin
        #0.5us
        reset = 1;
        #1.5us
        reset = 0;
    end


    // ---- WS2812B RGB LED simulation ----

    reg  [1:0] led_address = 0;             // Address of the LED to which data is being sent.
    reg [23:0] grb_data;                    // Color data (GRB format) read from a simple ROM table.
    wire       led_ready;                   // When asserted, the module accept data on the next rising clock edge.
    wire       ws2812b_din;                 // Output to the interface of WS2812B RGB LED (data input).

    always @(posedge clk) begin
        if (led_ready) begin
            case (led_address)
                2'b00 : grb_data <= 24'b11111111_00000000_00000000;
                2'b01 : grb_data <= 24'b00000000_11111111_00000000;
                2'b10 : grb_data <= 24'b00000000_00000000_11111111;
                2'b11 : grb_data <= 24'b11110000_11110000_11110000;
            endcase
            led_address <= led_address + 1;
        end
    end

    _simul_top TOP (
        .clk(clk),
        .reset(reset),
        .grb_data(grb_data),
        .ws2812b_din(ws2812b_din),
        .led_ready(led_ready)
    );

endmodule
