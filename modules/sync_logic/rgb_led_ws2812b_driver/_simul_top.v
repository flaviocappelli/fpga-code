
// (C) 2021-2025 Flavio Cappelli
// Released under the MIT License.
//
// This file is used to pass config parameters (if any) to the
// modules under test, ensuring the modules are configured the
// same way in both behavioral and post-synthesis simulation.
// It also makes "apio lint" happy because it guarantees that
// only one "top" module exists (Verilator requires it).

`default_nettype none                   // Do not allow undeclared signals.
`include "timescale.vh"                 // Timescale defined globally.

(* top *)                               // Mark this as top module, because
module _simul_top (                     // sometimes Yosys doesn't detect it.
    input  wire        clk,             // Input clock source.
    input  wire        reset,           // Reset chain control (restart from LED0).
    input  wire [23:0] grb_data,        // Input GRB data (|G7|..|G0|R7|..|R0|B7|..|B0|).
    output wire        ws2812b_din,     // Output to the interface of WS2812B RGB LED (data input).
    output wire        led_ready        // When asserted, the module accept data on the next rising clock edge.
);

    rgb_led_ws2812b_driver #(
        .CLK_MHz(100)                   // Clock frequency in MHz, see the testbench.
    ) WS2812B_DRV (
        .i__clk(clk),
        .i__reset(reset),
        .i__grb_data(grb_data),
        .o__ws2812b_din(ws2812b_din),
        .o__led_ready(led_ready)
    );

endmodule
