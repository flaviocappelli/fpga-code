
// (C) 2021-2025 Flavio Cappelli
// Released under the MIT License.
//
// Control driver for the WS2812B RGB LED.
//
// The WS2812B is an addressable RGB LED commonly used in lighting applications.
// These LEDs are "intelligent" because they have an integrated control chip that
// allows each LED to be individually controlled in terms of color and brightness,
// making them ideal for creating dynamic lighting effects. The main characteristic
// of the WS2812B is its ability to receive data through a single-wire communication
// protocol. Data is sent serially from one LED to the next, allowing a chain of LEDs
// to be controlled by sending a continuous stream of data bit to the first LED of the
// chain: each LED retains the first 24-bits it receives, and forwards the remaining bits
// to the next LEDs in the chain; a "reset" condition restart the addressing logic. Each
// color (red, green, blue) is represented by 8-bits, for a total color depth of 24-bits
// (i.e. 16 million of colors). For further information see the WS2812B datasheet.
//
// NOTE: The bit stream must continue after all LEDs in the chain have been initialized,
//       since the reset condition is precisely represented by the absence of such stream
//       for a certain time. The data sent is however ignored (so it can be dummy data).
//
// NOTE: This code should work with WS2812B-2020 (2x2mm) and WS2812B-5050 (5x5mm). Note
//       that some old datasheets from "WorldSemi" report wrong bit transfert timings.

`default_nettype none                       // Do not allow undeclared signals.
`include "timescale.vh"                     // Timescale defined globally.


module rgb_led_ws2812b_driver #(
    parameter CLK_MHz = -1                  // Clock frequency (mandatory, used to calculate the timings).
) (
    input  wire        i__clk,              // Input clock source.
    input  wire        i__reset,            // Reset chain control (restart from LED0).
    input  wire [23:0] i__grb_data,         // Input GRB data (|G7|..|G0|R7|..|R0|B7|..|B0|).
    output wire        o__ws2812b_din,      // Output to the interface of WS2812B RGB LED (data input).
    output wire        o__led_ready         // When asserted, the module accept data on the next rising clock edge.
);

    if (CLK_MHz < 0) begin
        $fatal(1, "ERROR: *** Clock frequency not assigned ***");
    end
    if (CLK_MHz < 5) begin
        $fatal(1, "ERROR: *** system clock too slow for accurate timing ***");
    end

    // WS2812B timing (see the datasheet):
    //  - Bit Transfer Time (length of 1 bit) -> 1.25us +/- 600ns
    //      T0H -> 0.40us +/- 150ns
    //      T1H -> 0.80us +/- 150ns
    //      T0L -> 0.85us +/- 150ns
    //      T1L -> 0.45us +/- 150ns
    //  - Reset time >= 300us (this is the longest interval
    //    time: the "clk_cnt" register must be sized for it).
    localparam BTT_NCLKS = $rtoi(CLK_MHz * 1.25 + 0.5);
    localparam T1H_NCLKS = $rtoi(CLK_MHz * 0.8 + 0.5);
    localparam T0H_NCLKS = $rtoi(CLK_MHz * 0.4 + 0.5);
    localparam RES_NCLKS = $rtoi(CLK_MHz * 300 + 0.5);
    localparam T1L_NCLKS = BTT_NCLKS - T1H_NCLKS;
    localparam T0L_NCLKS = BTT_NCLKS - T0H_NCLKS;
    localparam CNT_SIZE = $clog2(RES_NCLKS);

    // FSM states.
    localparam S_WAIT_RES  = 0,
               S_CHECK_CNT = 1,
               S_SEND_HIGH = 2,
               S_SEND_LOW  = 3;

    // FSM register. Note that, by default, Yosys recodes FSM state register into one-hot which
    // is zero initialized during boot (FPGA configuration). We must ensure the initial state is
    // not "S_SEND_LOW" (otherwise "o__led_ready" might be asserted before reset) and to do that
    // we simply disable the Yosys recoding. See https://stackoverflow.com/questions/65061562.
    (* fsm_encoding = "none" *) reg [1:0] state;

    // Registers.
    reg [CNT_SIZE-1:0] clk_cnt;             // Counter used to count clock cycles.
    reg          [4:0] bit_cnt;             // Number of bits that must be sent to complete the data transmission (max 24).
    reg         [23:0] bit_stream;          // Stream of data bit under transmission.
    reg                ws2812b;             // Output to the WS2812B data pin.

    // Output signals.
    assign o__ws2812b_din = ws2812b;
    assign o__led_ready = (bit_cnt == 0) && (clk_cnt == 0) && (state == S_SEND_LOW);

    /* verilator lint_off WIDTHTRUNC */     // Suppress some annoying Verilator's warnings (safe).
    /* verilator lint_off WIDTHEXPAND */

    always @(posedge i__clk, posedge i__reset) begin
        if (i__reset) begin

            bit_cnt <= 0;
            ws2812b <= 1'b0;
            clk_cnt <= RES_NCLKS - 2;
            state <= S_SEND_LOW;

        end else case (state)

            // Check bit counter.
            S_CHECK_CNT:
                begin
                    if (bit_cnt > 0) begin
                        bit_cnt <= bit_cnt - 1;
                        clk_cnt <= bit_stream[23] ? T1H_NCLKS - 1 : T0H_NCLKS - 1;
                        state <= S_SEND_HIGH;
                    end else begin
                        bit_cnt <= 24;
                        bit_stream <= i__grb_data;
                    end
                end

            // Send high level part of the bit.
            S_SEND_HIGH:
                begin
                    ws2812b <= 1'b1;
                    if (clk_cnt > 'b0)
                        clk_cnt <= clk_cnt - 1;
                    else begin
                        clk_cnt <= bit_stream[23] ? T1L_NCLKS - 2 : T0L_NCLKS - 2;
                        state <= S_SEND_LOW;
                    end
                end

            // Send low level part of the bit (and reset).
            S_SEND_LOW:
                begin
                    ws2812b <= 1'b0;
                    if (clk_cnt > 'b0)
                        clk_cnt <= clk_cnt - 1;
                    else begin
                        bit_stream <= { bit_stream[22:0], 1'b0 };
                        state <= S_CHECK_CNT;
                    end
                end

        endcase
    end

    /* verilator lint_on WIDTHEXPAND */
    /* verilator lint_on WIDTHTRUNC */

endmodule
