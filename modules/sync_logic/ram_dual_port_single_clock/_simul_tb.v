
// (C) 2021-2025 Flavio Cappelli
// Released under the MIT License.
//
// See https://www.fpgatutorial.com/how-to-write-a-basic-verilog-testbench/

`default_nettype none                       // Do not allow undeclared signals.
`define DUMPSTR(x) `"x.vcd`"                // Required for GTKWave.
`define SIMULATION_TIME 40us                // Simulation time (in s, ms, us or ns).
`include "timescale.vh"                     // Timescale defined globally.
`include "config.vh"                        // Shared parameters.

module testbench();
    initial begin
        $dumpfile(`DUMPSTR(`VCD_OUTPUT));   // Required for GTKWave.
        $dumpvars(0, testbench);            // Dump all variables in all instantiated modules.
        $printtimescale(testbench);         // Display the time scale and resolution (see above).
        #`SIMULATION_TIME                   // Wait until the simulation time is elapsed.
        $timeformat(-6, 2, "us", 1);        // Display the time elapsed in "us".
        $display("Simulation terminated after %t", $realtime);
        $finish;                            // Terminate the simulation.
    end


    // ---- Clock simulation ----

    reg clk;
    initial forever
        #0.5us clk = (clk === 1'b0);        // f = 1 / 2*0.5us = 1MHz


    // ---- Dual Port Single Clock Synchronous RAM simulation ----

    reg                    wen;             // Write enable.
    reg  [`ADDR_WIDTH-1:0] waddr;           // Write address.
    reg  [`ADDR_WIDTH-1:0] raddr;           // Read address.
    reg  [`DATA_WIDTH-1:0] data_in;         // Data input.
    wire [`DATA_WIDTH-1:0] data_out;        // Data output.

    _simul_top TOP (                        // Top module.
        .clk(clk),
        .wen(wen),
        .waddr(waddr),
        .raddr(raddr),
        .data_in(data_in),
        .data_out(data_out)
    );

    integer i;
    initial begin
        #0.5us
        wen = 0;
        waddr = 0;
        raddr = 0;
        #1.3us

        // Write.
        for (i = 0; i < `MEMORY_DEPTH; ++i) begin
            waddr = i;
            data_in = $random;
            wen = 1;
            #0.7us
            wen = 0;
            #0.3us
            ;
        end
        #3.0us

        // Read.
        for (i = 0; i < `MEMORY_DEPTH; ++i) begin
            raddr = i;
            #1.0us
            ;
        end
    end

endmodule
