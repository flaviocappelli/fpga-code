
// (C) 2021-2025 Flavio Cappelli
// Released under the MIT License.
//
// 4-Bit Synchronous Binary Up Counter.

`default_nettype none                               // Do not allow undeclared signals.
`include "timescale.vh"                             // Timescale defined globally.


module counter_4bin_up (
    input  wire       i__clk,                       // Clock, active on positive edge.
    input  wire       i__rst,                       // Reset, active high, asynchronous.
    input  wire       i__en,                        // Count enable, active high.
    output reg  [3:0] o__counter                    // Output counter value.
);

    always @(posedge i__clk, posedge i__rst) begin
        if (i__rst)
            o__counter <= 0;
        else if (i__en)
            o__counter <= o__counter + 1;
    end

endmodule
