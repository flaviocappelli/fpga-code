
// (C) 2021-2025 Flavio Cappelli
// Released under the MIT License.
//
// Dual Port Dual Clock Synchronous RAM.
//
// NOTE: This configuration has two separate clocks for read and write operations.
//       This allows the user to perform read and write operations asynchronously.
//
// NOTE: In a dual-clock design, read and write operations usually don't happen at
//       the same time (we can't even know if the read and write addresses are both
//       stable), so talking about "read-during-write behavior" makes no sense. The
//       synthesis tools also cannot infer the "read-during-write behavior" because
//       it depends on the timing of the two clocks within the target device. Thus,
//       the "read-during-write behavior" of the synthesized design is undefined.
//       See "Intel Quartus Prime Pro Edition User Guide Design Recommendations",
//       section 1.4.1.7 (Simple Dual-Port, Dual-Clock Synchronous RAM).

`default_nettype none                               // Do not allow undeclared signals.
`include "timescale.vh"                             // Timescale defined globally.


module ram_dual_port_dual_clock #(
    parameter INIT_FILE = "",                       // RAM initialization file (optional).
    parameter DATA_WIDTH = 8,                       // Size of data bus (default 8 bits).
    parameter MEMORY_DEPTH = 64,                    // Memory size (default 64 locations).
    parameter ADDR_WIDTH = $clog2(MEMORY_DEPTH)     // Size of address bus (default depends on MEMORY_DEPTH).
) (
    input  wire                  i__wclk,           // Write clock, active on positive edge.
    input  wire                  i__rclk,           // Read clock, active on positive edge.
    input  wire                  i__wen,            // Write enable input, active high.
    input  wire [ADDR_WIDTH-1:0] i__waddr,          // Write address input.
    input  wire [ADDR_WIDTH-1:0] i__raddr,          // Read address input.
    input  wire [DATA_WIDTH-1:0] i__wdata,          // Data input (write).
    output wire [DATA_WIDTH-1:0] o__rdata           // Data output (read).
);

    reg [DATA_WIDTH-1:0] out;                       // Used to store data output.
    reg [DATA_WIDTH-1:0] mem [0:MEMORY_DEPTH-1];    // Allocated memory.

    initial begin
        if (INIT_FILE != 0) begin
            $readmemh(INIT_FILE, mem);              // INIT (if initialization file is provided).
            `ifndef YOSYS
            $display("Initializing RAM from hex file '%s'.", INIT_FILE);    // CRASH YOSYS (*).
            `endif
        end
    end

    // --- WRITE ---

    always @(posedge i__wclk) begin
        if (i__wen)                                 // WRITE to RAM if allowed.
            mem[i__waddr] <= i__wdata;
    end

    // --- READ ---

    always @(posedge i__rclk) begin
        out <= mem[i__raddr];                       // READ from RAM and store data.
    end

    assign o__rdata = out;                          // Output stored data.

endmodule

// NOTE (*): Since Yosys-37, the $display() statement causes a segmentation fault in
// the command "yosys -q -p 'read_blif -wideports hw.blif; write_verilog hw.synth'".
// To avoid this issue, we must exclude such statement from the code parsed by Yosys.
// Sadly, this will make the given message visible only in the behavioral simulation.
