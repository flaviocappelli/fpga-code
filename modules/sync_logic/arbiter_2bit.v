
// (C) 2021-2025 Flavio Cappelli
// Released under the MIT License.
//
// 2-bit Arbiter.
//
// See:
// http://www.asic-world.com/verilog/verilog_one_day1.html

`default_nettype none                       // Do not allow undeclared signals.
`include "timescale.vh"                     // Timescale defined globally.


module arbiter_2bit (
    input  wire       i__clk,               // Clock, active on positive edge.
    input  wire       i__rst,               // Reset, active high, asynchronous.
    input  wire [1:0] i__req,               // Request for grant, synchronous.
    output reg  [1:0] o__grant              // Corresponding grant signals.
);

    always @(posedge i__clk, posedge i__rst) begin
        if (i__rst)
            o__grant <= 0;                  // Reset o__grant signals.
        else
            case (i__req)
                2'b00 : o__grant <= 2'b00;
                2'b01 : o__grant <= 2'b01;
                2'b10 : o__grant <= 2'b10;
                2'b11 : o__grant <= 2'b01;  // i__req[0] has priority over i__req[1].
            endcase
    end

endmodule
