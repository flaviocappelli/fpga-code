
// (C) 2021-2025 Flavio Cappelli
// Released under the MIT License.
//
// Synchronous FIFO (same clock domain for R/W operations).
//
// A synchronous FIFO is a First-In-First-Out queue in which there is
// a common clock pulse for both data write and data read; in addition
// to the data itself, some control signals are required, because such
// data could be produced and consumed at different rates (in bursts):
//
// +--------+                    +----+                     +--------+
// |        |  <-- in_ready <--  |    |  <-- out_ready <--  |        |
// |PRODUCER|  ==> in_data  ==>  |FIFO|  ==> out_data  ==>  |CONSUMER|
// |        |  --> in_valid -->  |    |  --> out_valid -->  |        |
// +--------+                    +----+                     +--------+
//    |           clk/rst          |                           |
// >--+----------------------------+---------------------------+
//
// Pipeline control signals:
//  * clk/rst:   clock/reset to all entities;
//  * in_ready:  FIFO is ready to accept new data (i.e. FIFO is not full);
//  * in_valid:  input data (in_data) is ready to be written into the FIFO;
//  * out_ready: CONSUMER is ready to accept data (out_data) from the FIFO;
//  * out_valid: output data (out_data) is valid (i.e. FIFO is not empty).
//
// NOTE: For optimization reasons, "FIFO_DEPTH" must be a power of 2 (if
//       it isn't, then FIFO_DEPTH is rounded to the next higher power of
//       2). Also FIFO_DEPTH must be >= 2 (otherwise it is forced to 2).
//
// NOTE: If FIFO_DEPTH <= 8, Yosys might implements the FIFO memory using
//       logic cells, instead of inferred RAM (at least on iCE40 FPGAs).
//       Indeed, for small depths, logic cells are more efficient than
//       inferred RAM. To force the RAM, simply specify a bigger depth.
//
// For practical examples of FIFO depth calculation, see:
//
//      https://www.youtube.com/watch?v=fk-lznkZN5Q
//      https://www.youtube.com/watch?v=oPWhL5uDk1c
//
// See also the Asynchronous FIFO in cross_domain_logic/async_fifo/.

`default_nettype none                                   // Do not allow undeclared signals.
`include "timescale.vh"                                 // Timescale defined globally.


module sync_fifo #(
    parameter DATA_WIDTH = 8,
    parameter FIFO_DEPTH = 4
) (
    input  wire                  i__clk,                // Read/Write clock, active on positive edge.
    input  wire                  i__rst,                // Reset, active high, synchronous (see below).

    // Pipeline in (into the module).
    input  wire [DATA_WIDTH-1:0] i__in_data,            // Input data.
    input  wire                  i__in_valid,           // If high, on clock edge store input data and increment write pointer.
    output wire                  o__in_ready,           // High when ready to accept new input data (i.e. FIFO not full).

    // Pipeline out (out of the module).
    output wire [DATA_WIDTH-1:0] o__out_data,           // Output data, valid on clock edge if o__out_valid == 1.
    output wire                  o__out_valid,          // High when output data is valid (i.e. FIFO not empty).
    input  wire                  i__out_ready           // If high, on clock edge increment read pointer.
);

    localparam FIFO_DEPTH_AUX = FIFO_DEPTH >= 2 ? FIFO_DEPTH : 2;
    localparam ADDR_WIDTH = $clog2(FIFO_DEPTH_AUX);

    reg [DATA_WIDTH-1:0] mem [0:2**ADDR_WIDTH-1];       // FIFO buffer.
    reg [ADDR_WIDTH:0] wp;                              // Write pointer (extra bit used for full/empty detection).
    reg [ADDR_WIDTH:0] rp;                              // Read pointer (extra bit used full/empty detection).

    always @(posedge i__clk) begin                      // Write logic. NEED SYNC RESET, OTHERWISE RAM IS NOT INFERRED.
        if (i__rst)
            wp <= 0;
        else if (o__in_ready && i__in_valid) begin      // If FIFO is not full and producer input data is valid,
            mem[wp[ADDR_WIDTH-1:0]] <= i__in_data;      // then store data and increment write pointer. Note that
            wp <= wp + 1;                               // the extra bit of write pointer must be stripped out.
        end
    end

    always @(posedge i__clk) begin                      // Read logic. NEED SYNC RESET, OTHERWISE RAM IS NOT INFERRED.
        if (i__rst)
            rp <= 0;
        else if (o__out_valid && i__out_ready)          // If FIFO is not empty and consumer is ready to accept new data,
            rp <= rp + 1;                               // then increment read pointer (data is automatically outputted).
    end

    assign o__out_data  = mem[rp[ADDR_WIDTH-1:0]];                      // Extra bit of read pointer must be stripped out.
    assign o__out_valid = (rp != wp);                                   // See below.
    assign o__in_ready  = (rp[ADDR_WIDTH] == wp[ADDR_WIDTH]) ||         // See below.
                          (rp[ADDR_WIDTH-1:0] != wp[ADDR_WIDTH-1:0]);
endmodule

/*
How the logic for "out_valid" and "in_ready" works? Here I'll explain that.

At first note that the wp/rp registers have ADDR_WIDTH + 1 bits (while the
FIFO memory is addressed with ADDR_WIDTH bits): such extra bit in the wp/rp
registers is used to obtain the above signals. Of course, when we access the
FIFO memory, the extra bit must be stripped out. We use this method because
it is more efficient than counting the number of items stored in the FIFO.

For example, with ADDR_WIDTH = 4, the wp/rp registers have 5 bits, so they
can count from 0 to 31, while the FIFO has space for 16 elements (i.e. 2^4).
We start with wp = 0, rp = 0 (reset) and we increment the wp register each
time a new element is inserted into the FIFO:

        +----- bit ADDR_WIDTH
        |+---- bit ADDR_WIDTH-1
        ||  +- bit 0
        ||  |

    wp  00000       rp  00000       empty       (wptr == rptr)
        00001           00000       1 elem      (wptr == rptr + 1)
        00010           00000       2 elem      (wptr == rptr + 2)
        .....           .....
        01111           00000       15 elem     (wptr == rptr + 15)
        10000           00000       full        (wptr == rptr + 16)

(note: with only 4 bits for wp/rp we would not be able to distinguish the
"empty" condition from the "full" condition without an additional counter).
Now we see what happens when we remove elements from the queue; in this
case the rp register is incremented:

    wp  10000       rp  00001       15 elem     (wptr == rptr + 15)
        10000           00010       14 elem     (wptr == rptr + 14)
        .....           .....
        10000           01111       1 elem      (wptr == rptr + 1)
        10000           10000       empty       (wptr == rptr)

What we saw above, can be repeated starting from any possible "empty"
condition:

    wp  00001       rp  00001       empty       (wptr == rptr)
        .....           .....
        10001           00001       full        (wptr == rptr + 16)
        .....           .....
        10001           10001       empty       (wptr == rptr)

    wp  00010       rp  00010       empty       (wptr == rptr)
        .....           .....
        10010           00010       full        (wptr == rptr + 16)
        .....           .....
        10010           10010       empty       (wptr == rptr)

        -----           -----

    wp  11110       rp  11110       empty       (wptr == rptr)
        .....           .....
        01110           11110       full        (wptr == rptr + 16)
        .....           .....
        01110           01110       empty       (wptr == rptr)

    wp  11111       rp  11111       empty       (wptr == rptr)
        .....           .....
        01111           11111       full        (wptr == rptr + 16)
        .....           .....
        01111           01111       empty       (wptr == rptr)

As we can see, the wrap-around of the wp/rp registers works in our favor!
The FIFO will be empty each time the wp/rp registers contain the same value:

    wr == rp  -->  fifo empty
    wr != rp  -->  out_valid (fifo not empty )

The FIFO will be full each time the lower ADDR_WIDTH bits of wp/rp registers
(in this example bits 0,1,2,3) will have the same value, while the upper bit
will have opposite value:

    (wp[ADDR_WIDTH] != rp[ADDR_WIDTH]) && (wp[ADDR_WIDTH-1:0] == rp[ADDR_WIDTH-1:0])  -->  fifo full
    (wp[ADDR_WIDTH] == rp[ADDR_WIDTH]) || (wp[ADDR_WIDTH-1:0] != rp[ADDR_WIDTH-1:0])  -->  in_ready (fifo not full)
*/
