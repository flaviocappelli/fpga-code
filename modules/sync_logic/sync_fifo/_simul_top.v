
// (C) 2021-2025 Flavio Cappelli
// Released under the MIT License.
//
// This file is used to pass config parameters (if any) to the
// modules under test, ensuring the modules are configured the
// same way in both behavioral and post-synthesis simulation.
// It also makes "apio lint" happy because it guarantees that
// only one "top" module exists (Verilator requires it).

`default_nettype none           // Do not allow undeclared signals.
`include "timescale.vh"         // Timescale defined globally.

(* top *)                       // Mark this as top module, because
module _simul_top (             // sometimes Yosys doesn't detect it.
    input  wire       clk,
    input  wire       reset,
    // Pipeline in.
    input  wire [7:0] in_data,
    input  wire       in_valid,
    output wire       in_ready,
    // Pipeline out.
    output wire [7:0] out_data,
    output wire       out_valid,
    input  wire       out_ready
);

    sync_fifo #(
        .FIFO_DEPTH(8)          // Depth should be 2^n with n >= 1
    ) FIFO (                    // (see notes in "sync_fifo.v").
        .i__clk(clk),
        .i__rst(reset),
        // ---
        .i__in_data(in_data),
        .i__in_valid(in_valid),
        .o__in_ready(in_ready),
        // ---
        .o__out_data(out_data),
        .o__out_valid(out_valid),
        .i__out_ready(out_ready)
    );

endmodule
