
// (C) 2021-2025 Flavio Cappelli
// Released under the MIT License.
//
// See https://www.fpgatutorial.com/how-to-write-a-basic-verilog-testbench/

`default_nettype none                       // Do not allow undeclared signals.
`define DUMPSTR(x) `"x.vcd`"                // Required for GTKWave.
`define SIMULATION_TIME 220us               // Simulation time (in s, ms, us or ns).
`include "timescale.vh"                     // Timescale defined globally.

module testbench();
    initial begin
        $dumpfile(`DUMPSTR(`VCD_OUTPUT));   // Required for GTKWave.
        $dumpvars(0, testbench);            // Dump all variables in all instantiated modules.
        $printtimescale(testbench);         // Display the time scale and resolution (see above).
        #`SIMULATION_TIME                   // Wait until the simulation time is elapsed.
        $timeformat(-6, 2, "us", 1);        // Display the time elapsed in "us".
        $display("Simulation terminated after %t", $realtime);
        $finish;                            // Terminate the simulation.
    end


    // ---- Clock simulation ----

    reg clk;
    initial forever
        #0.5us clk = (clk === 1'b0);        // f = 1 / 2*0.5us = 1MHz


    // ---- Reset simulation ----

    reg reset;                              // Reset, active high.
    initial begin
        #0.5us
        reset = 1;
        #4.5us
        reset = 0;
    end


    // ---- Sync FIFO simulation ----

    reg  [7:0] in_data;                     // Pipeline in.
    reg        in_valid;
    wire       in_ready;
    wire [7:0] out_data;                    // Pipeline out.
    wire       out_valid;
    reg        out_ready;

    _simul_top TOP (                        // Top module.
        .clk(clk),
        .reset(reset),
        // Pipeline in.
        .in_data(in_data),
        .in_valid(in_valid),
        .in_ready(in_ready),
        // Pipeline out.
        .out_data(out_data),
        .out_valid(out_valid),
        .out_ready(out_ready)
    );

    // Debug signals. Note that "out_data" return obsoleted stored values
    // when not valid; to better see in GtkWave only the valid output data,
    // we define array "out_data_val" (unknown when "out_data" is not valid).
    wire [7:0] out_data_val = out_valid ? out_data : 8'dx;
    wire fifo_empty = ~out_valid;
    wire fifo_full = ~in_ready;

    // +--------+                    +----+                     +--------+
    // |        |  <-- in_ready <--  |    |  <-- out_ready <--  |        |
    // |PRODUCER|  ==> in_data  ==>  |FIFO|  ==> out_data  ==>  |CONSUMER|
    // |        |  --> in_valid -->  |    |  --> out_valid -->  |        |
    // +--------+                    +----+                     +--------+
    //    |           clk/rst          |                           |
    // ---+----------------------------+---------------------------+
    //
    // Pipeline control signals:
    //  * clk/rst:   clock/reset to all entities;
    //  * in_ready:  FIFO is ready to accept new data (i.e. FIFO is not full);
    //  * in_valid:  input data (in_data) is ready to be written into the FIFO;
    //  * out_ready: CONSUMER is ready to accept data (out_data) from the FIFO;
    //  * out_valid: output data (out_data) is valid (i.e. FIFO is not empty).
    //
    // Fifo under test has depth 8. Test steps:
    //   * for each clock cycle after the reset, generate incremental numbers (used as input data);
    //   * at the beginning "in_ready" should be high (the FIFO is empty so it can accepts new data);
    //   * at the beginning "out_valid" should be low (the FIFO is empty so it cannot return valid data);
    //   * perform writes for 3 clock cycles -> "out_valid" should become true at the first write;
    //   * disable "in_valid" for just one clock cycle -> the FIFO should ignore the last generated number;
    //   * perform other writes until the fifo is full -> "in_ready" should become false;
    //   * try to perform another write -> the FIFO should ignore the last generated number (because it is full);
    //   * do nothing for one clock cycle;
    //   * perform read for 3 clock cycle -> "in_ready" should become true at the first read (FIFO is not full now);
    //   * disable "out_ready" for one clock cycle -> the fifo output should not change;
    //   * perform other reads until the fifo is empty -> "out_valid" should become false;
    //   * try to perform another read -> the FIFO output should not change (because the FIFO is empty);
    //   * do nothing for one clock cycle;
    //   * perform just one write (one clock);
    //   * do nothing for one clock cycle;
    //   * perform just one read (one clock);
    //   * do nothing for one clock cycle;
    //   * Repeat the whole process 8 times (to test write/read internal pointers on all positions).

    integer i, j, k;
    initial begin
        k = 0;
        #0.5us
        in_data   = 0;
        in_valid  = 1'b0;
        out_ready = 1'b0;
        #5.20us
        for(i=0; i<8; i=i+1) begin
            in_valid = 1'b1;                    // Enable writing.
            for(j=0; in_ready; j=j+1) begin     // Store incremental bytes until the fifo is full, but ...
                k = k + 1;
                in_data = k;
                if (j == 3) begin               // ...suspend writing for one clock cycle after 3 writes (to test "in_valid" signal).
                    in_valid = 1'b0;
                    #1us
                    in_valid = 1'b1;
                end else begin
                    #1us
                    ;
                end
            end                                 // The cycle exit when the fifo is full ("in_ready" become false).
            k = k + 1;                          // Try to store another byte (to ensure nothing is written).
            in_data = k;
            #1us
            in_valid = 1'b0;                    // Disable writing.
            #1us                                // Do nothing for one cycle.
            out_ready = 1'b1;                   // Enable reading.
            for(j=0; out_valid; j=j+1) begin    // Read bytes until the fifo is empty, but...
                if (j == 3) begin               // ...suspend reading for one clock cycle after 3 reads (to test "out_ready" signal).
                    out_ready = 1'b0;
                    #1us
                    out_ready = 1'b1;
                end else begin
                    #1us
                    ;
                end
            end                                 // The cycle exit when the fifo is empty ("out_valid" become false).
            #1us                                // Try to read for another clock cycle (to ensure output does not changr).
            out_ready = 1'b0;                   // Disable reading.
            #1us                                // Do nothing for one cycle.
            k = k + 1;                          // Now write just one byte (one cycle).
            in_data = k;
            in_valid = 1'b1;
            #1us
            in_valid = 1'b0;
            #1us                                // Do nothing for one cycle.
            out_ready = 1'b1;                   // Now read for just 1 cycle.
            #1us
            out_ready = 1'b0;
            #1us                                // Do nothing for one cycle.
            ;
        end                                     // Repeat all above 8 times.
     end

endmodule
