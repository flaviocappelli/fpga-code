
// (C) 2021-2025 Flavio Cappelli
// Released under the MIT License.
//
// N-Bit Gray Counter.
//
// See https://en.wikipedia.org/wiki/Gray_code

`default_nettype none                               // Do not allow undeclared signals.
`include "timescale.vh"                             // Timescale defined globally.


module counter_gray #(
    parameter COUNTER_WIDTH = 4                     // Number of bits in the counter.
) (
    input  wire                     i__clk,         // Clock, active on positive edge.
    input  wire                     i__rst,         // Reset, active high, asynchronous.
    input  wire                     i__en,          // Count enable, active high.
    output wire [COUNTER_WIDTH-1:0] o__gray         // Output gray counter value.
);

    reg [COUNTER_WIDTH-1:0] counter;

    always @(posedge i__clk, posedge i__rst) begin
        if (i__rst)
            counter <= 0;
        else if (i__en)
            counter <= counter + 1;
    end

    assign o__gray = counter ^ (counter >> 1);      // Binary to Gray conversion.

endmodule
