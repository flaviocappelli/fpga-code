
// USB "host reset" detection.
// See https://github.com/davidthings/tinyfpga_bx_usbserial/issues/19

`default_nettype none              // Do not allow undeclared signals.
`include "timescale.vh"            // Timescale defined globally.


module usb_reset_det (
  input  wire clk_48mhz,
  input  wire reset_in,
  input  wire usb_p_rx,
  input  wire usb_n_rx,
  output reg  host_reset
);


  reg [18:0] reset_tick = 0;                    // 2^19  => max 524287.
  wire timer_expired = reset_tick >= 479999;    // 48E6/100 - 1  =>  10ms.

  always@(posedge clk_48mhz) begin
    if (reset_in) begin
      reset_tick <= 'd0;
      host_reset <= 1'b0;
    end else begin
      reset_tick <= (usb_p_rx | usb_n_rx | timer_expired) ? 'd0 : reset_tick + 'd1;
      host_reset <= timer_expired;
    end
  end

endmodule
