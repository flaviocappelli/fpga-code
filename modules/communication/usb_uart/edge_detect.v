
`default_nettype none              // Do not allow undeclared signals.
`include "timescale.vh"            // Timescale defined globally.


module rising_edge_detector (
  input  wire clk,
  input  wire in,
  output wire out
);
  reg in_q;

  always @(posedge clk) begin
    in_q <= in;
  end

  assign out = !in_q && in;
endmodule

module falling_edge_detector (
  input  wire clk,
  input  wire in,
  output wire out
);
  reg in_q;

  always @(posedge clk) begin
    in_q <= in;
  end

  assign out = in_q && !in;
endmodule
