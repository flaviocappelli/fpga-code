
// (C) 2021-2025 Flavio Cappelli
// Released under the MIT License.
//
// Helper for the "gw2ar_pll_54MHz_from_27MHz.v" module:
// simulate the 54MHz clock output and the lock signal.
//
// The simulation of the Gowin PLL module is not implemented
// in Yosys simulation models, so we must do it by ourselves.
// The following code will simulate a PLL generated clock at
// 54MHz with a 55% dutycycle. The PLL lock time (about 30us)
// is an average value, based on observations with a scope.
// For further details, see file "GOWIN PLL NOTES.txt".
//
// NOTE: The simulation is very coarse. It does not fully
//       emulate the behavior of a PLL, it only simulates
//       the clock with constant frequency and lock signal.
//       Also, it assumes that the input clock is running.
//
// NOTE: This module is intended for simulation only (both
//       behavioral and post-synthesis), not true synthesis.
//       See, for example, the project "03_blink_using_PLL"
//       to understand how to use this PLL helper.

`default_nettype none                           // Do not allow undeclared signals.
`include "clkgen_sim_helper.vh"                 // Must be included before timescale.vh
`include "timescale.vh"                         // Timescale defined globally.

/* verilator lint_off MULTITOP */               // Do not complain when this module is not used.

(* blackbox *)                                  // Tell Yosys to instantiate an external module (Yosys
module gw2ar_pll_54MHz_sim_helper (             // (see this module only in post-synthesis simulation).
    // verilator lint_off UNUSED
    input  wire i__clk_in,                      // Disable Verilator warning ("i__clk_in" unused).
    // verilator lint_on UNUSED
    output reg  o__clk_out,
    output reg  o__async_locked
);

    `ifndef VERILATOR                           // Parsing with Verilator is handled separately.
    `ifndef SYNTHESIS                           // Yosys must see only the module's interface.

    // --- Simulation with Icarus Verilog ---

    // The Gowin datasheet does not say anything about whether the PLL "LOCK" signal
    // is synchronous or not with the input clock and the generated clock, so IT MUST
    // BE CONSIDERED ASYNCHRONOUS to both (this is probably true for all FPGAs). As
    // stated above, the simulation is very coarse: the PLL lock signal is asserted
    // approximately 30us after the start of the simulation, and the PLL clock simply
    // begins about 4us before the assertion of the lock signal. Note: to simulate
    // asynchronous signals, we must use non-integer delay values in the code below.

    clkgen_sim_helper #(
        .DELAY_us(29.961 - 4.163), .FREQ_MHz(54), .DUTYCYCLE(55)
    ) PLLCLKGEN (.o__clk(o__clk_out));

    initial begin
        o__async_locked = 1'b0;
        #29.961us o__async_locked = 1'b1;
    end

    `endif
    `else

    // --- Simulation with Verilator (interface to C++) ---

    // See the file "gw2ar_pll/gw2ar_pll_54MHz_sim_sched.cxx"
    // in the "verilator" top directory and the chapter "Direct
    // Programming Interface (DPI)" in Verilator's documentation.

    // Verilog function called from C++ to drive the PLL clock.
    export "DPI-C" function _gw2ar_pll_54MHz_sim_clk_driver;
    function void _gw2ar_pll_54MHz_sim_clk_driver(bit s);
        o__clk_out = s;
    endfunction

    // Verilog function called from C++ to set the PLL lock line.
    export "DPI-C" function _gw2ar_pll_54MHz_sim_lock_driver;
    function void _gw2ar_pll_54MHz_sim_lock_driver(bit s);
        o__async_locked = s;
    endfunction

    // C++ function called from Verilog to schedule the PLL simulation.
    import "DPI-C" context function void _gw2ar_pll_54MHz_sim_sched();
    initial begin
        _gw2ar_pll_54MHz_sim_sched();
    end

    `endif

endmodule

/* verilator lint_on MULTITOP */
