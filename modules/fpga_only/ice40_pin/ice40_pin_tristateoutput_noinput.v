
// (C) 2021-2025 Flavio Cappelli
// Released under the MIT License.
//
// Configure a iCE40 I/O PIN as tristate output (input ignored).
// Optionally enable the pullup resistor on the I/O PIN.
//
// NOTE: Tested only on the iCE40 LP FPGA family.

`default_nettype none                       // Do not allow undeclared signals.
`include "timescale.vh"                     // Timescale defined globally.


module ice40_pin_tristateoutput_noinput #(
    parameter PULLUP = 1'b0
) (
    inout wire p__ice40_pin,                // iCE40 package PIN name.
    input wire i__output_signal,            // Output signal (FPGA logic to PIN).
    input wire i__output_en                 // Output HiZ/Enable (from FPGA logic).
);

// Unfortunately all tested version of Verilator reports errors on SB_IO,
// so don't parse it (Verilator is used just to "lint", so no problem).
`ifdef VERILATOR
    assign p__ice40_pin = i__output_en ? i__output_signal : 1'bz;
`else
    SB_IO #(
        .PIN_TYPE(6'b1010_01),              // PIN_OUTPUT_TRISTATE + PIN_INPUT (input ignored).
        .PULLUP(PULLUP)                     // PULLUP ENABLE (ignored on Bank 3).
    ) IOBUF (
        .PACKAGE_PIN(p__ice40_pin),
        .OUTPUT_ENABLE(i__output_en),
        .D_OUT_0(i__output_signal)
    );
`endif

endmodule
