
// (C) 2021-2025 Flavio Cappelli
// Released under the MIT License.
//
// Configure a iCE40 I/O PIN as simple input.
// Optionally enable the pullup resistor on the I/O PIN.
//
// NOTE: Tested only on the iCE40 LP FPGA family.

`default_nettype none                       // Do not allow undeclared signals.
`include "timescale.vh"                     // Timescale defined globally.


module ice40_pin_nooutput_simpleinput #(
    parameter PULLUP = 1'b0
) (
    input  wire p__ice40_pin,               // iCE40 package PIN name.
    output wire o__input_signal             // Input signal (PIN to FPGA logic).
);

// Unfortunately all tested version of Verilator reports errors on SB_IO,
// so don't parse it (Verilator is used just to "lint", so no problem).
`ifdef VERILATOR
    assign o__input_signal = p__ice40_pin;
`else
    SB_IO #(
        .PIN_TYPE(6'b0000_01),              // PIN_NO_OUTPUT + PIN_INPUT.
        .PULLUP(PULLUP)                     // PULLUP ENABLE (ignored on Bank 3).
    ) IOBUF (
        .PACKAGE_PIN(p__ice40_pin),
        .D_IN_0(o__input_signal)
    );
`endif

endmodule
