
// (C) 2021-2025 Flavio Cappelli
// Released under the MIT License.
//
// See https://www.fpgatutorial.com/how-to-write-a-basic-verilog-testbench/

`default_nettype none                       // Do not allow undeclared signals.
`define DUMPSTR(x) `"x.vcd`"                // Required for GTKWave.
`define SIMULATION_TIME 80us                // Simulation time (in s, ms, us or ns).
`include "timescale.vh"                     // Timescale defined globally.

module testbench();
    initial begin
        $dumpfile(`DUMPSTR(`VCD_OUTPUT));   // Required for GTKWave.
        $dumpvars(0, testbench);            // Dump all variables in all instantiated modules.
        $printtimescale(testbench);         // Display the time scale and resolution (see above).
        #`SIMULATION_TIME                   // Wait until the simulation time is elapsed.
        $timeformat(-6, 2, "us", 1);        // Display the time elapsed in "us".
        $display("Simulation terminated after %t", $realtime);
        $finish;                            // Terminate the simulation.
    end


    // ---- Clock simulation ----

    reg clk;
    initial begin
        #2us                                // Simulate undefined clock for 2us.
        forever
            #0.5us clk = (clk === 1'b0);    // f = 1 / 2*0.5us = 1MHz
    end


    // ---- Clock Enable simulation ----

    reg clke;
    initial begin
        #1us                                // Simulate undefined clock enable for 1us.
        clke = 1'b0;
        #9us
        clke = 1'b1;
    end


    // ---- FPGA Reset Generator simulation ----

    wire reset1, reset2;                    // Generated active high resets.

    _simul_top TOP (                        // Top module.
        .clk(clk),
        .clke(clke),
        .reset1(reset1),                    // Reset without clock enabled.
        .reset2(reset2)                     // Reset with clock enabled.
    );

endmodule
