
// (C) 2021-2025 Flavio Cappelli
// Released under the MIT License.
//
// This file is used to pass config parameters (if any) to the
// modules under test, ensuring the modules are configured the
// same way in both behavioral and post-synthesis simulation.
// It also makes "apio lint" happy because it guarantees that
// only one "top" module exists (Verilator requires it).

`default_nettype none           // Do not allow undeclared signals.
`include "timescale.vh"         // Timescale defined globally.

(* top *)                       // Mark this as top module, because
module _simul_top (             // sometimes Yosys doesn't detect it.
    input  wire clk,
    input  wire clke,
    output wire reset1,
    output wire reset2
);

    localparam RESET_COUNTER_WIDTH = 5;

    fpga_reset_generator #(
        .RESET_COUNTER_WIDTH(RESET_COUNTER_WIDTH)
    ) FRG1 (
        .i__clk(clk),
        .i__clk_en(1'b1),
        .o__gen_rst(reset1)
    );

    fpga_reset_generator #(
        .RESET_COUNTER_WIDTH(RESET_COUNTER_WIDTH)
    ) FRG2 (
        .i__clk(clk),
        .i__clk_en(clke),
        .o__gen_rst(reset2)
    );

endmodule
