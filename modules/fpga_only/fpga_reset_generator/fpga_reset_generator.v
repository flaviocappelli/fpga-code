
// (C) 2021-2025 Flavio Cappelli
// Released under the MIT License.
//
// Generate an active high reset signal for
// (2^RESET_COUNTER_WIDTH)-2 clock cycles.
//
// NOTE: RESET_COUNTER_WIDTH must be >= 2 (default 3).
//
// NOTE: If "i__clk_en" (clock enable) is not used, set to 1'b1.
//
// NOTE: Reset is activated even if the clock is still not active.
//
// NOTE: Works only on FPGA devices (ASICs needs an external reset, see
//       the folder cross_domain_logic/reset_generator_with_async_reset/).

`default_nettype none                               // Do not allow undeclared signals.
`include "timescale.vh"                             // Timescale defined globally.


module fpga_reset_generator #(
    parameter RESET_COUNTER_WIDTH = 3               // Number of bits in the reset counter.
) (
    input  wire i__clk,                             // Clock, active on positive edge.
    input  wire i__clk_en,                          // Clock enable, active high.
    output wire o__gen_rst                          // Active high generated reset.
);

    reg [RESET_COUNTER_WIDTH-1:0] rst_state = 0;    // Initialization works only on FPGAs.

    // Reset is high until the counter reach the max value (NAND of state bits).
    assign o__gen_rst = ~&rst_state;

    // Increment the counter. Note that Verilator might complain about the sum below
    // for two reasons (it depends on the version of Verilator and enabled options):
    // 1# the usage of 'o__gen_rst' is detected as sync operation, but almost all my
    // other modules use 'o__gen_rst' as asynchronous reset input, so a SYNCASYNCNET
    // warning is generated (usage of sync and async operations on the same net); 2#
    // the sum has operands with different sizes (WIDTH warning).  Both warnings are
    // harmless, the logic is still simulated and synthesized correctly if we simply
    // disable them. See https://verilator.org/guide/latest/warnings.html
    always @(posedge i__clk)
        if (i__clk_en) begin                        // Increments only if enabled.
            // verilator lint_off SYNCASYNCNET
            // verilator lint_off WIDTH
            rst_state <= rst_state + o__gen_rst;    // Increment stops when reset is deasserted.
            // verilator lint_on WIDTH
            // verilator lint_on SYNCASYNCNET
        end

endmodule
