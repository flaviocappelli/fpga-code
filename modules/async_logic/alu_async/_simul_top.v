
// (C) 2021-2025 Flavio Cappelli
// Released under the MIT License.
//
// This file is used to pass config parameters (if any) to the
// modules under test, ensuring the modules are configured the
// same way in both behavioral and post-synthesis simulation.
// It also makes "apio lint" happy because it guarantees that
// only one "top" module exists (Verilator requires it).

`default_nettype none           // Do not allow undeclared signals.
`include "timescale.vh"         // Timescale defined globally.

(* top *)                       // Mark this as top module, because
module _simul_top (             // sometimes Yosys doesn't detect it.
    input  wire [4:0] op,
    input  wire [3:0] din1,
    input  wire [3:0] din2,
    input  wire       flag_cin,
    output wire [3:0] dout,
    output wire       flag_cout,
    output wire       flag_o,
    output wire       flag_s,
    output wire       flag_z,
    output wire       flag_p
);

    alu_async #(
        .DATA_WIDTH(4)
    ) ALU (
        .i__op(op),
        .i__din1(din1),
        .i__din2(din2),
        .i__flag_cin(flag_cin),
        .o__dout(dout),
        .o__flag_cout(flag_cout),
        .o__flag_o(flag_o),
        .o__flag_s(flag_s),
        .o__flag_z(flag_z),
        .o__flag_p(flag_p)
    );

endmodule
