
// (C) 2021-2025 Flavio Cappelli
// Released under the MIT License.
//
// See https://www.fpgatutorial.com/how-to-write-a-basic-verilog-testbench/

`default_nettype none                       // Do not allow undeclared signals.
`define DUMPSTR(x) `"x.vcd`"                // Required for GTKWave.
`define SIMULATION_TIME 2590us              // Simulation time (in s, ms, us or ns).
`include "timescale.vh"                     // Timescale defined globally.
`include "alu_defs.vh"                      // ALU definitions.

module testbench();
    initial begin
        $dumpfile(`DUMPSTR(`VCD_OUTPUT));   // Required for GTKWave.
        $dumpvars(0, testbench);            // Dump all variables in all instantiated modules.
        $printtimescale(testbench);         // Display the time scale and resolution (see above).
        #`SIMULATION_TIME                   // Wait until the simulation time is elapsed.
        $timeformat(-6, 2, "us", 1);        // Display the time elapsed in "us".
        $display("Simulation terminated after %t", $realtime);
        $finish;                            // Terminate the simulation.
    end


    // ---- Clock simulation ----

    // NONE REQUIRED.


    // ---- Asynchronous (4-bit) ALU simulation ----

    reg  [4:0] op;
    reg  [3:0] din1;
    reg  [3:0] din2;
    reg        flag_cin;
    wire [3:0] dout;
    wire       flag_cout;
    wire       flag_o;
    wire       flag_s;
    wire       flag_z;
    wire       flag_p;
    reg        extra;
    reg  [3:0] result;

    _simul_top TOP (                        // Top module.
        .op(op),
        .din1(din1),
        .din2(din2),
        .flag_cin(flag_cin),
        .dout(dout),
        .flag_cout(flag_cout),
        .flag_o(flag_o),
        .flag_s(flag_s),
        .flag_z(flag_z),
        .flag_p(flag_p)
    );

    integer i, j, ones;
    initial begin                           // Generate exhaustive test data: YOU HAVE TO USE THE
        #0.5us                              // HORIZONTAL SCROLL IN GTKWAVE TO SEE ALL TEST DATA.
        din1 = 'hx;
        din2 = 'hx;
        flag_cin = 1'bx;

        // Test ALU_NOT.
        op = `ALU_NOT;
        for (i = 0; i < 16; ++i) begin
            din1 = i[3:0];
            #1.0us
            if (dout != ~din1)
                $fatal(1, "ERROR: wrong ALU output! (ALU_NOT)");
        end
        din1 = 'hx;
        op = 'hx;
        #2.0us

        // Test ALU_RL.
        op = `ALU_RL;
        for (i = 0; i < 32; ++i) begin
            din1 = i[3:0];
            flag_cin = i[4];
            #1.0us
            if (dout != {din1[2:0], flag_cin} || flag_cout != din1[3])
                $fatal(1, "ERROR: wrong ALU output! (ALU_RL)");
        end
        din1 = 'hx;
        flag_cin = 1'bx;
        op = 'hx;
        #2.0us

        // Test ALU_RR.
        op = `ALU_RR;
        for (i = 0; i < 32; ++i) begin
            din1 = i[3:0];
            flag_cin = i[4];
            #1.0us
            if (dout != {flag_cin, din1[3:1]} || flag_cout != din1[0])
                $fatal(1, "ERROR: wrong ALU output! (ALU_RR)");
        end
        din1 = 'hx;
        flag_cin = 1'bx;
        op = 'hx;
        #2.0us

        // Test ALU_RLC.
        op = `ALU_RLC;
        for (i = 0; i < 16; ++i) begin
            din1 = i[3:0];
            #1.0us
            if (dout != {din1[2:0], din1[3]} || flag_cout != din1[3])
                $fatal(1, "ERROR: wrong ALU output! (ALU_RLC)");
        end
        din1 = 'hx;
        op = 'hx;
        #2.0us

        // Test ALU_RRC.
        op = `ALU_RRC;
        for (i = 0; i < 16; ++i) begin
            din1 = i[3:0];
            #1.0us
            if (dout != {din1[0], din1[3:1]} || flag_cout != din1[0])
                $fatal(1, "ERROR: wrong ALU output! (ALU_RRC)");
        end
        din1 = 'hx;
        op = 'hx;
        #2.0us

        // Test ALU_SL.
        op = `ALU_SL;
        for (i = 0; i < 16; ++i) begin
            din1 = i[3:0];
            #1.0us
            if (dout != {din1[2:0], 1'b0} || flag_cout != din1[3])
                $fatal(1, "ERROR: wrong ALU output! (ALU_SL)");
        end
        din1 = 'hx;
        op = 'hx;
        #2.0us

        // Test ALU_SRA.
        op = `ALU_SRA;
        for (i = 0; i < 16; ++i) begin
            din1 = i[3:0];
            #1.0us
            if (dout != {din1[3], din1[3:1]} || flag_cout != din1[0])
                $fatal(1, "ERROR: wrong ALU output! (ALU_SRA)");
        end
        din1 = 'hx;
        op = 'hx;
        #2.0us

        // Test ALU_SRL.
        op = `ALU_SRL;
        for (i = 0; i < 16; ++i) begin
            din1 = i[3:0];
            #1.0us
            if (dout != {1'b0, din1[3:1]} || flag_cout != din1[0])
                $fatal(1, "ERROR: wrong ALU output! (ALU_SRL)");
        end
        din1 = 'hx;
        op = 'hx;
        #2.0us

        // Test ALU_INC.
        op = `ALU_INC;
        for (i = 0; i < 16; ++i) begin
            din1 = i[3:0];
            #1.0us
            if ({flag_cout, dout} != ({din1} + 1'b1))
                $fatal(1, "ERROR: wrong ALU output! (ALU_INC)");
        end
        din1 = 'hx;
        op = 'hx;
        #2.0us

        // Test ALU_DEC.
        op = `ALU_DEC;
        for (i = 0; i < 16; ++i) begin
            din1 = i[3:0];
            #1.0us
            if ({flag_cout, dout} != ({din1} - 1'b1))
                $fatal(1, "ERROR: wrong ALU output! (ALU_DEC)");
        end
        din1 = 'hx;
        op = 'hx;
        #2.0us

        // Test ALU_NEG.
        op = `ALU_NEG;
        for (i = 0; i < 16; ++i) begin
            din1 = i[3:0];
            #1.0us
            if (dout != -din1)
                $fatal(1, "ERROR: wrong ALU output! (ALU_NEG)");
        end
        din1 = 'hx;
        op = 'hx;
        #2.0us

        // Test ALU_OR.
        op = `ALU_OR;
        for (i = 0; i < 16; ++i) begin
            for (j = 0; j < 16; ++j) begin
                din1 = i[3:0];
                din2 = j[3:0];
                #1.0us
                if (dout != (din1 | din2))
                    $fatal(1, "ERROR: wrong ALU output! (ALU_OR)");
            end
        end
        din1 = 'hx;
        din2 = 'hx;
        op = 'hx;
        #2.0us

        // Test ALU_AND.
        op = `ALU_AND;
        for (i = 0; i < 16; ++i) begin
            for (j = 0; j < 16; ++j) begin
                din1 = i[3:0];
                din2 = j[3:0];
                #1.0us
                if (dout != (din1 & din2))
                    $fatal(1, "ERROR: wrong ALU output! (ALU_AND)");
            end
        end
        din1 = 'hx;
        din2 = 'hx;
        op = 'hx;
        #2.0us

        // Test ALU_XOR.
        op = `ALU_XOR;
        for (i = 0; i < 16; ++i) begin
            for (j = 0; j < 16; ++j) begin
                din1 = i[3:0];
                din2 = j[3:0];
                #1.0us
                if (dout != (din1 ^ din2))
                    $fatal(1, "ERROR: wrong ALU output! (ALU_XOR)");
            end
        end
        din1 = 'hx;
        din2 = 'hx;
        op = 'hx;
        #2.0us

        // Test ALU_ADD and flag O.
        op = `ALU_ADC;
        for (i = 0; i < 32; ++i) begin
            for (j = 0; j < 16; ++j) begin
                din1 = i[3:0];
                din2 = j[3:0];
                flag_cin = i[4];
                #1.0us
                if ({flag_cout, dout} != (din1 + din2 + {3'b0, flag_cin}))
                    $fatal(1, "ERROR: wrong ALU output! (ALU_ADC)");

                // Check overflow flag (use another method).
                // See https://stackoverflow.com/questions/24586842
                {extra, result} = {din1[3], din1} + {din2[3], din2} + flag_cin;
                if (({extra, result[3]} == 2'b01 || {extra, result[3]} == 2'b10) && flag_o != 1'b1)
                    $fatal(1, "ERROR: wrong ALU flag O! (ALU_ADC)");
            end
        end
        din1 = 'hx;
        din2 = 'hx;
        op = 'hx;
        #2.0us

        // Test ALU_ADD and flag O.
        op = `ALU_ADD;
        for (i = 0; i < 16; ++i) begin
            for (j = 0; j < 16; ++j) begin
                din1 = i[3:0];
                din2 = j[3:0];
                #1.0us
                if ({flag_cout, dout} != (din1 + din2))
                    $fatal(1, "ERROR: wrong ALU output! (ALU_ADD)");

                // Check overflow flag (use another method).
                // See https://stackoverflow.com/questions/24586842
                {extra, result} = {din1[3], din1} + {din2[3], din2};
                if (({extra, result[3]} == 2'b01 || {extra, result[3]} == 2'b10) && flag_o != 1'b1)
                    $fatal(1, "ERROR: wrong ALU flag O! (ALU_ADD)");
            end
        end
        din1 = 'hx;
        din2 = 'hx;
        op = 'hx;
        #2.0us

        // Test ALU_SBC and flag O.
        op = `ALU_SBC;
        for (i = 0; i < 32; ++i) begin
            for (j = 0; j < 16; ++j) begin
                din1 = i[3:0];
                din2 = j[3:0];
                flag_cin = i[4];
                #1.0us
                if ({flag_cout, dout} != (din1 - din2 - {3'b0, flag_cin}))
                    $fatal(1, "ERROR: wrong ALU output! (ALU_SBC)");

                // Check overflow flag (use another method).
                // See https://stackoverflow.com/questions/24586842
                {extra, result} = {din1[3], din1} - {din2[3], din2} - flag_cin;
                if (({extra, result[3]} == 2'b01 || {extra, result[3]} == 2'b10) && flag_o != 1'b1)
                    $fatal(1, "ERROR: wrong ALU flag O! (ALU_SBC)");
            end
        end
        din1 = 'hx;
        din2 = 'hx;
        op = 'hx;
        #2.0us

        // Test ALU_SUB and flag O.
        op = `ALU_SUB;
        for (i = 0; i < 16; ++i) begin
            for (j = 0; j < 16; ++j) begin
                din1 = i[3:0];
                din2 = j[3:0];
                #1.0us
                if ({flag_cout, dout} != (din1 - din2))
                    $fatal(1, "ERROR: wrong ALU output! (ALU_SUB)");

                // Check overflow flag (use another method).
                // See https://stackoverflow.com/questions/24586842
                {extra, result} = {din1[3], din1} - {din2[3], din2};
                if (({extra, result[3]} == 2'b01 || {extra, result[3]} == 2'b10) && flag_o != 1'b1)
                    $fatal(1, "ERROR: wrong ALU flag O! (ALU_SUB)");
            end
        end
        din1 = 'hx;
        din2 = 'hx;
        op = 'hx;
        #2.0us

        // Test ALU_MOV1 and flags Z, S, P.
        op = `ALU_MOV1;
        for (i = 0; i < 16; ++i) begin
            din1 = i[3:0];
            #1.0us
            if (dout != din1)
                $fatal(1, "ERROR: wrong ALU output1 (ALU_MOV1)");
            if ((dout == 0 && flag_z == 1'b0) || (dout != 0 && flag_z == 1'b1))
                $fatal(1, "ERROR: wrong ALU flag Z! (ALU_MOV1)");
            if (dout[3] != flag_s)
                $fatal(1, "ERROR: wrong ALU flag S! (ALU_MOV1)");
            ones = 0;                       // Count number of bit 1.
            for(j = 0; j < 4; ++j)
                ones = ones + dout[j];
            if (ones[0] != ~flag_p)         // Check parity flag.
                $fatal(1, "ERROR: wrong ALU flag P! (ALU_MOV1)");
        end
        din1 = 'hx;
        op = 'hx;
        #2.0us

        // Test ALU_MOV2 and flags Z, S, P.
        op = `ALU_MOV2;
        for (i = 0; i < 16; ++i) begin
            din2 = i[3:0];
            #1.0us
            if (dout != din2)
                $fatal(1, "ERROR: wrong ALU output! (ALU_MOV2)");
            if ((dout == 0 && flag_z == 1'b0) || (dout != 0 && flag_z == 1'b1))
                $fatal(1, "ERROR: wrong ALU flag Z! (ALU_MOV2)");
            if (dout[3] != flag_s)
                $fatal(1, "ERROR: wrong ALU flag S! (ALU_MOV2)");
            ones = 0;                       // Count number of bit 1.
            for(j = 0; j < 4; ++j)
                ones = ones + dout[j];
            if (ones[0] != ~flag_p)         // Check parity flag.
                $fatal(1, "ERROR: wrong ALU flag P! (ALU_MOV2)");
        end
        din1 = 'hx;
        op = 'hx;
        #2.0us
        ;

        // All ok.
        $display("All tests successfully completed!");
    end

endmodule
