
// (C) 2021-2025 Flavio Cappelli
// Released under the MIT License.
//
// Defines for the Arithmetic Logic Unit.

// Unary operations (on 1st operand).
`define ALU_NOT   5'h00   // Invert (one's complement).
`define ALU_RL    5'h01   // Rotate left through carry.
`define ALU_RR    5'h02   // Rotate right through carry.
`define ALU_RLC   5'h03   // Rotate left circular.
`define ALU_RRC   5'h04   // Rotate right circular.
`define ALU_SL    5'h05   // Shift left.
`define ALU_SRA   5'h06   // Shift right aritmetic.
`define ALU_SRL   5'h07   // Shift right logic.
`define ALU_INC   5'h08   // Increment.
`define ALU_DEC   5'h09   // Decrement.
`define ALU_NEG   5'h0A   // Negate (two's complement).

// Binary operations (on both operands).
`define ALU_OR    5'h10   // Bitwise OR.
`define ALU_AND   5'h11   // Bitwise AND.
`define ALU_XOR   5'h12   // Bitwise EX-OR.
`define ALU_ADC   5'h13   // Add with input carry.
`define ALU_ADD   5'h14   // Add (without input carry).
`define ALU_SBC   5'h15   // Subtract with input carry.
`define ALU_SUB   5'h16   // Subtract (without input carry).

// Just send operands out.
`define ALU_MOV1  5'h1E   // Output 1st operand.
`define ALU_MOV2  5'h1F   // Output 2nd operand.


// The following line forces KDE's Kate editor to recognize
// this file as "Verilog" source (for syntax highlighting).
// kate: hl verilog;
