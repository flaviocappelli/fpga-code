
// (C) 2021-2025 Flavio Cappelli
// Released under the MIT License.
//
// Asynchronous Arithmetic Logic Unit for softcores.
//
// NOTE: Moltiplication and division are not included.
//
// NOTE: Flags are always affected, so the softcore code must
//       determine which flags are stored for each operation.
//
// NOTE: All shifts are only one bit at a time (multi-bit shifts
//       require a Barrel Shifter, which is resource-consuming and
//       can be easily implemented using the multiplication logic).

`default_nettype none                               // Do not allow undeclared signals.
`include "timescale.vh"                             // Timescale defined globally.
`include "alu_defs.vh"


module alu_async #(
    parameter DATA_WIDTH = 8                        // Size of data (default 8 bits).
) (
    input  wire            [4:0] i__op,             // Operation.
    input  wire [DATA_WIDTH-1:0] i__din1,           // 1st operand.
    input  wire [DATA_WIDTH-1:0] i__din2,           // 2nd operand.
    input  wire                  i__flag_cin,       // Carry flag (input).
    output reg  [DATA_WIDTH-1:0] o__dout,           // Data output.
    output reg                   o__flag_cout,      // Carry flag (output).
    output reg                   o__flag_o,         // Overflow flag.
    output wire                  o__flag_s,         // Sign flag.
    output wire                  o__flag_z,         // Zero flag.
    output wire                  o__flag_p          // Parity flag.
);

    always @(*) begin
        o__dout = {DATA_WIDTH{1'bz}};               // Default output values (avoid latches).
        o__flag_cout = 1'b0;

        case (i__op)
            // Unary operations: dout = op(din1) (din2 ignored).
            `ALU_NOT : o__dout = ~i__din1;
            `ALU_RL  : {o__flag_cout, o__dout} = {i__din1, i__flag_cin};
            `ALU_RR  : {o__dout, o__flag_cout} = {i__flag_cin, i__din1};
            `ALU_RLC : {o__flag_cout, o__dout} = {i__din1, i__din1[DATA_WIDTH-1] };
            `ALU_RRC : {o__dout, o__flag_cout} = {i__din1[0], i__din1};
            `ALU_SL  : {o__flag_cout, o__dout} = {i__din1, 1'b0};
            `ALU_SRA : {o__dout, o__flag_cout} = {i__din1[DATA_WIDTH-1], i__din1};
            `ALU_SRL : {o__dout, o__flag_cout} = {1'b0, i__din1};
            `ALU_INC : {o__flag_cout, o__dout} = i__din1 + 1'b1;
            `ALU_DEC : {o__flag_cout, o__dout} = i__din1 - 1'b1;
            `ALU_NEG : o__dout = ~i__din1 + 1'b1;

            // Binary operations: dout = op(din1,din2).
            `ALU_OR  : o__dout = i__din1 | i__din2;
            `ALU_AND : o__dout = i__din1 & i__din2;
            `ALU_XOR : o__dout = i__din1 ^ i__din2;
            `ALU_ADC : {o__flag_cout, o__dout} = i__din1 + i__din2 + {{DATA_WIDTH-1{1'b0}}, i__flag_cin};
            `ALU_ADD : {o__flag_cout, o__dout} = i__din1 + i__din2;
            `ALU_SBC : {o__flag_cout, o__dout} = i__din1 - i__din2 - {{DATA_WIDTH-1{1'b0}}, i__flag_cin};
            `ALU_SUB : {o__flag_cout, o__dout} = i__din1 - i__din2;

            // Just send operands out.
            `ALU_MOV1 : o__dout = i__din1;
            `ALU_MOV2 : o__dout = i__din2;

            // Avoid verilator's complains.
            default :;
        endcase
    end

    // Set the zero flag.
    assign o__flag_z = ~|o__dout;

    // Set the sign flag.
    assign o__flag_s = o__dout[DATA_WIDTH-1];

    // Set the overflow flag (defined only when both operands are set).
    // Note: the overflow flag is not the same as the carry bit: overflow
    // condition represents "DATA LOSS" in the result, while the carry bit
    // is the amount carried over for the next stage of calculation. See:
    // http://teaching.idallen.com/dat2343/10f/notes/040_overflow.txt
    always @(*) begin
        case ({i__din1[DATA_WIDTH-1], i__din2[DATA_WIDTH-1], o__dout[DATA_WIDTH-1]})
            3'b001  : o__flag_o = (i__op == `ALU_ADC || i__op == `ALU_ADD) ? 1'b1 : 1'b0;
            3'b011  : o__flag_o = (i__op == `ALU_SBC || i__op == `ALU_SUB) ? 1'b1 : 1'b0;
            3'b100  : o__flag_o = (i__op == `ALU_SBC || i__op == `ALU_SUB) ? 1'b1 : 1'b0;
            3'b110  : o__flag_o = (i__op == `ALU_ADC || i__op == `ALU_ADD) ? 1'b1 : 1'b0;
            default : o__flag_o = 1'b0;
        endcase
    end

    // Set the parity flag (1 if even parity).
    assign o__flag_p = ~^o__dout;

endmodule
