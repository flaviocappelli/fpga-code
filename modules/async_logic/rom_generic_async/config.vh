
// File included by testbench and top module: set shared parameters.

`ifndef _config_vh_
`define _config_vh_

`define DATA_WIDTH      8
`define MEMORY_DEPTH    32
`define INIT_FILE       "rom_init.hex"

// Don't change this.
`define ADDR_WIDTH      $clog2(`MEMORY_DEPTH)

`endif  //_config_vh_


// The following line forces KDE's Kate editor to recognize
// this file as "Verilog" source (for syntax highlighting).
// kate: hl verilog;
