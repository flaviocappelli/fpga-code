
// (C) 2021-2025 Flavio Cappelli
// Released under the MIT License.
//
// 2-Inputs Multiplexer.

`default_nettype none                               // Do not allow undeclared signals.
`include "timescale.vh"                             // Timescale defined globally.


module mux_2_inputs #(
    parameter DATA_WIDTH = 8                        // Size of data (default 8 bits).
) (
    input  wire                  i__s,              // Input selection.
    input  wire [DATA_WIDTH-1:0] i__a,              // Data inputs.
    input  wire [DATA_WIDTH-1:0] i__b,
    output reg  [DATA_WIDTH-1:0] o__y               // Data output.
);

    always @(*) begin
        case (i__s)
            1'b0 : o__y = i__a;
            1'b1 : o__y = i__b;
        endcase
    end

endmodule
