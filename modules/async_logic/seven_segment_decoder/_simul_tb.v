
// (C) 2021-2025 Flavio Cappelli
// Released under the MIT License.
//
// See https://www.fpgatutorial.com/how-to-write-a-basic-verilog-testbench/

`default_nettype none                       // Do not allow undeclared signals.
`define DUMPSTR(x) `"x.vcd`"                // Required for GTKWave.
`define SIMULATION_TIME 40us                // Simulation time (in s, ms, us or ns).
`include "timescale.vh"                     // Timescale defined globally.

module testbench();
    initial begin
        $dumpfile(`DUMPSTR(`VCD_OUTPUT));   // Required for GTKWave.
        $dumpvars(0, testbench);            // Dump all variables in all instantiated modules.
        $printtimescale(testbench);         // Display the time scale and resolution (see above).
        #`SIMULATION_TIME                   // Wait until the simulation time is elapsed.
        $timeformat(-6, 2, "us", 1);        // Display the time elapsed in "us".
        $display("Simulation terminated after %t", $realtime);
        $finish;                            // Terminate the simulation.
    end


    // ---- Clock simulation ----

    // NONE REQUIRED.


    // ---- 7-Segment Decoder simulation ----

    reg        on;                          // LCD on/off.
    reg        dp;                          // Decimal point on/off.
    reg  [3:0] value;                       // Hexadecimal input value.
    wire [7:0] segments;                    // 7-Segment outputs.

    _simul_top TOP (                        // Top module.
        .on(on),
        .dp(dp),
        .value(value),
        .segments(segments)
    );

    integer i;
    initial begin
        #0.5us
        on = 0;
        #2.5us
        on = 1;
        dp = 0;
        for (i = 0; i < 16; ++i) begin
            value = i;
            #1.0us
            ;
        end
        on = 0;
        #2.5us
        on = 1;
        dp = 1;
        for (i = 0; i < 16; ++i) begin
            value = i;
            #1.0us
            ;
        end
        on = 0;
    end

endmodule
