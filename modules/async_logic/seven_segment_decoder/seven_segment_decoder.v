
// (C) 2021-2025 Flavio Cappelli
// Released under the MIT License.
//
// 7-Segment LED display decoder (asynchronous).
//
// NOTE: There are two types of 7-Segment LED displays:
//
//        * Common Cathode: all cathodes of the 7-Segment LED are connected
//          together; such connection must be grounded, while power is applied
//          to the appropriate segments in order to illuminate them.
//
//        * Common Anode: all the anodes of the 7-Segment LED are connected
//          together; power must be applied to such connection, while ground
//          is applied to the appropriate segments to light up.
//
//       In both cases a resistors must be added to EACH segment (and point)
//       connection to limit the amount of current flowing through the LED.
//
// NOTE: Most FPGAs don't have the capability to drive the LEDs directly.
//       Read the datasheet very carefully to avoid damage to the FPGA I/O.
//
// By default the decoder uses positive logic (high outputs) to light up
// the 7-Segment LED display:
//
//            a               input   segments         input   segments
//         -------            value   abcdefgp         value   abcdefgp
//        |       |           -----------------        -----------------
//      f |       | b          '0' => 1111110x          '8' => 1111111x
//        |       |            '1' => 0110000x          '9' => 1111011x
//         -------             '2' => 1101101x          'A' => 1110111x
//        |   g   |            '3' => 1111001x          'b' => 0011111x
//      e |       | c          '4' => 0110011x          'C' => 1001110x
//        |       |            '5' => 1011011x          'd' => 0111101x
//         -------   .         '6' => 1011111x          'E' => 1001111x
//            d      p         '7' => 1110000x          'F' => 1000111x
//
// To use negative logic (low outputs) set NEG_OUTPUT_LOGIC to 1.

`default_nettype none                   // Do not allow undeclared signals.
`include "timescale.vh"                 // Timescale defined globally.


module seven_segment_decoder #(
    parameter NEG_OUTPUT_LOGIC = 0
) (
    input  wire       i__on,            // LCD on(1)/off(0).
    input  wire       i__dp,            // Decimal point on(1)/off(0).
    input  wire [3:0] i__value,         // Hexadecimal input value.
    output wire [7:0] o__segments       // LCD 7-Segment output.
);

    reg [6:0] seg;

    always @(i__value) begin
        case (i__value)  // abcdefg
            4'h0 : seg = 7'b1111110;
            4'h1 : seg = 7'b0110000;
            4'h2 : seg = 7'b1101101;
            4'h3 : seg = 7'b1111001;
            4'h4 : seg = 7'b0110011;
            4'h5 : seg = 7'b1011011;
            4'h6 : seg = 7'b1011111;
            4'h7 : seg = 7'b1110000;
            4'h8 : seg = 7'b1111111;
            4'h9 : seg = 7'b1111011;
            4'hA : seg = 7'b1110111;
            4'hB : seg = 7'b0011111;
            4'hC : seg = 7'b1001110;
            4'hD : seg = 7'b0111101;
            4'hE : seg = 7'b1001111;
            4'hF : seg = 7'b1000111;
        endcase
    end

    //generate (optional in Verilog-2005)
    if (!NEG_OUTPUT_LOGIC) begin : gen_POUT
        assign o__segments = i__on ? {seg, i__dp} : 8'h00;
    end else begin : gen_NOUT
        assign o__segments = i__on ? ~{seg, i__dp} : 8'hff;
    end
    //endgenerate

endmodule
