
// (C) 2021-2025 Flavio Cappelli
// Released under the MIT License.
//
// Unsigned asynchronous 6x6 multiplier.
//
// NOTE: three implementations are provided:
//
//        * MUL6X6_METHOD = 0  -->  use tolchain default
//        * MUL6X6_METHOD = 1  -->  use four 3x3 multipliers
//        * MUL6X6_METHOD = 2  -->  use "flat" Vedic 6x6 logic
//
// NOTE: the table methods is not implemented because it uses
//       too much FPGA resources and is anyway slower than the
//       above methods (see the 3x3 multiplier).
//
// The FPGA I am currently using (iCE40 LP8K) does not have
// a DSP core (or a dedicated hardware multiplier) so, it is
// interesting to see which method provides the fastest logic.
// These are the resources used by Yosys on the iCE40 FPGA:
//
//   MUL6X6_METHOD  MUL3X3_METHOD  Cells  SB_LUT4  SB_CARRY  EPD(*)
//   --------------------------------------------------------------
//         0           ignored       81      74        7     11.5ns
//         1              0          94      83       11     15.4ns
//         1              1          89      70       19     12.0ns
//         1              2         262     255        7     15.0ns
//         2           ignored       93      79       14     10.7ns
//
// (*) Estimated propagation delay of the multiplier
//     on the TinyFPGA BX (excluding the I/O cells).
//
// For the 6x6 multiplication, the implementation provided by Yosys
// seems optimal in terms of FPGA resource utilization, but it isn't
// the fastest. The fastest seems the "flat" Vedic 6x6 method.
//
// NOTE: these results may vary among different synthesis
//       tools, and even among different versions of the
//       same tool. Furthermore, the optimizations carried
//       out by the synthesis tool depend on the used FPGA
//       and the overall circuit, so it is recommended to
//       rerun such tests on the circuit under development.

`default_nettype none               // Do not allow undeclared signals.
`include "timescale.vh"             // Timescale defined globally.


module unsigned_multiplier_6x6_async #(
    parameter MUL6X6_METHOD = 1,    // Fastest method measured.
    parameter MUL3X3_METHOD = 1
) (
    input  wire  [5:0] i__a,
    input  wire  [5:0] i__b,
    output reg  [11:0] o__y
);

    //generate (optional in Verilog-2005)
    if (MUL6X6_METHOD == 0) begin : gen_6X6_M0

        // Toolchain implementation.
        always @(*) begin
            o__y = i__a * i__b;
        end

    end else if (MUL6X6_METHOD == 1) begin : gen_6X6_M1

        // If we set: X1 = a5*2^2 + a4*2^1 + a3*2^0, X0 = a2*2^2 + a1*2^1 + a0*2^0,
        //            Y1 = b5*2^2 + b4*2^1 + b3*2^0, Y0 = b2*2^2 + b1*2^1 + b0*2^0,
        //
        // then we can write:
        //
        // A * B = (a5*2^5 + a4*2^4 + a3*2^3 + a2*2^2 + a1*2^1 + a0*2^0) *
        //         (b5*2^5 + b4*2^4 + b3*2^3 + b2*2^2 + b1*2^1 + b0*2^0) =
        //         (X1*2^3 + X0*2^0) * (Y1*2^3 + Y0*2^0) =
        //         X1*Y1*2^6 + (X0*Y1 + X1*Y0)*2^3 + X0*Y0*2^0
        //
        // So, the 6x6 multiplication can be decomposed in four 3x3 multipliers, plus
        // some adders (and some shifts, but those are all trivials):
        //
        //        ------  X0*Y0  (p0)   Note that each p term can have a maximum value of 49
        //     ------     X0*Y1  (p1)   (i.e. the result of 7*7). We will proceed adding the
        //     ------     X1*Y0  (p2)   middle terms p1 and p2 first: the result will be at
        //  ------        X1*Y1  (p3)   most 98 (49 + 49), so 7 bits are required for such sum.
        //                              Then we will add this sum to {p3, p0[5:3]} (max value
        // 110001_110, i.e. 398), so a 9-bit word is enough for the last sum (98 + 398 = 496).

        wire [5:0] p0, p1, p2, p3;
        reg  [6:0] middlesum;       // 7 bits required (see above).
        reg  [8:0] lastsum;         // 9 bits required (see above).

        unsigned_multiplier_3x3_async #(
            .MUL3X3_METHOD(MUL3X3_METHOD)
        ) MUL3x3_0 (
            .i__a(i__a[2:0]),       // X0.
            .i__b(i__b[2:0]),       // Y0.
            .o__y(p0)
        );

        unsigned_multiplier_3x3_async #(
            .MUL3X3_METHOD(MUL3X3_METHOD)
        ) MUL3x3_1 (
            .i__a(i__a[2:0]),       // X0.
            .i__b(i__b[5:3]),       // Y1.
            .o__y(p1)
        );

        unsigned_multiplier_3x3_async #(
            .MUL3X3_METHOD(MUL3X3_METHOD)
        ) MUL3x3_2 (
            .i__a(i__a[5:3]),       // X1.
            .i__b(i__b[2:0]),       // Y0.
            .o__y(p2)
        );

        unsigned_multiplier_3x3_async #(
            .MUL3X3_METHOD(MUL3X3_METHOD)
        ) MUL3x3_3 (
            .i__a(i__a[5:3]),       // X1.
            .i__b(i__b[5:3]),       // Y1.
            .o__y(p3)
        );

        always @(*) begin
            middlesum = p1 + p2;
            lastsum = {p3, p0[5:3]} + {2'b00, middlesum};
            o__y = {lastsum, p0[2:0]};
        end

    end else begin : gen_6X6_M2

        // Vedic method from "Urdhva Triyakbhyam Sutra".
        //
        // (a5*2^5 + a4*2^4 + a3*2^3 + a2*2^2 + a1*2^1 + a0*2^0) * (b5*2^5 + b4*2^4 + b3*2^3 + b2*2^2 + b1*2^1 + b0*2^0) =
        // a5*b5*2^10 + (a4*b5 + a5*b4)*2^9 + (a3*b5 + a4*b4 + a5*b3)*2^8 + (a2*b5 + a3*b4 + a4*b3 + a5*b2)*2^7 +
        // (a1*b5 + a2*b4 + a3*b3 + a4*b2 + a5*b1)*2^6 + (a0*b5 + a1*b4 + a2*b3 + a3*b2 + a4*b1 + a5*b0)*2^5 +
        // (a0*b4 + a1*b3 + a2*b2 + a3*b1 + a4*b0)*2^4 + (a0*b3 + a1*b2 + a2*b1+ a3*b0)*2^3 +
        // (a0*b2 + a1*b1 + a2*b0)*2^2 + (a0*b1 + a1*b0)*2^1 + a0*b0*2^0
        //
        // y11 y10 y9  y8  y7  y6  y5  y4  y3  y2  y1  y0
        // ==============================================
        //                                             p0 = a0 * b0
        //
        //                                     c1  s1     = carry & sum of (p1,p2) : half adder
        //                                                  where p1 = a0*b1, p2 = a1*b0
        //
        //                                 c2  s2         = carry & sum of (p3,p4,p5) : full adder
        //                                                  where p3 = a0*b2, p4 = a1*b1, p5 = a2*b0
        //
        //                         t32 t31 t30            = 3 bits from p6 + p7 + p8 + p9 : table generated (LUT4)
        //                                                  where p6 = a0*b3, p7 = a1*b2, p8 = a2*b1, p9 = a3*b0
        //
        //                     t42 t41 t40                = 3 bits from p10 + p11 + p12 + p13 + p14 : toolchain default
        //                                                  where p10 = a0*b4, p11 = a1*b3, p12 = a2*b2, p13 = a3*b1, p14 = a4*b0
        //
        //                 t52 t51 t50                    = 3 bits from p15 + p16 + p17 + p18 + p19 + p20 : toolchain default
        //                                                  where p15=a0*b5, p16=a1*b4, p17=a2*b3, p18=a3*b2, p19=a4*b1, p20=a5*b0
        //
        //             t62 t61 t60                        = 3 bits from p21 + p22 + p23 + p24 + p25 : toolchain default
        //                                                  where p21 = a1*b5, p22 = a2*b4, p23 = a3*b3, p24 = a4*b2, p25 = a5*b1
        //
        //         t72 t71 t70                            = 3 bits from p26 + p27 + p28 + p29 : table generated (LUT4)
        //                                                  where p26 = a2*b5, p27 = a3*b4, p28 = a4*b3, p29 = a5*b2
        //
        //         c8  s8                                 = carry & sum of (p30,p31,p32) : full adder
        //                                                  where p30 = a3*b5, p31 = a4*b4, p32 = a5*b3
        //
        //     c9  s9                                     = carry & sum of (p33,p34) : half adder
        //                                                  where p33 = a4*b5, p34 = a5*b4
        //
        //     p35                                        = a5*b5
        // ----------------------------------------------
        // S11 S10 S9  S8  S7  S6  S5  S4  S3  S2         = carry & sum of ({p35,  s9,  s8, t70, t60, t50, t40, t30, s2},
        //                                                                  { c9,  c8, t71, t61, t51, t41, t31,  c2, c1},
        //                                                                  {  0, t72, t62, t52, t42, t32,  0,    0,  0}) : toolchain default
        //
        // NOTE: I initially used tasks instead of the macros below, but I discovered that some
        // versions of Icarus Verilog return wrong results with tasks, while macros just work.

        `define half_adder(a,b,sum,carry)    {carry, sum} = a + b
        `define full_adder(cin,a,b,sum,cout) {cout, sum} = a + b + cin

        reg p0, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, p12, p13, p14, p15, p16, p17, p18;
        reg p19, p20, p21, p22, p23, p24, p25, p26, p27, p28, p29, p30, p31, p32, p33, p34, p35;
        reg s1, c1, s2, c2, s8, c8, s9, c9;
        reg [2:0] t3, t4, t5, t6, t7;
        reg [9:0] lastsum;

        always @(*) begin
            // 2^0
            p0 = i__a[0] * i__b[0];

            // 2^1
            p1 = i__a[0] * i__b[1];
            p2 = i__a[1] * i__b[0];
            `half_adder(p1, p2, s1, c1);

            // 2^2
            p3 = i__a[0] * i__b[2];
            p4 = i__a[1] * i__b[1];
            p5 = i__a[2] * i__b[0];
            `full_adder(p3, p4, p5, s2, c2);

            // 2^3
            p6 = i__a[0] * i__b[3];
            p7 = i__a[1] * i__b[2];
            p8 = i__a[2] * i__b[1];
            p9 = i__a[3] * i__b[0];
            case ({p9,p8,p7,p6})
                4'b0000 : t3 = 3'd0;
                4'b0001 : t3 = 3'd1;
                4'b0010 : t3 = 3'd1;
                4'b0011 : t3 = 3'd2;
                4'b0100 : t3 = 3'd1;
                4'b0101 : t3 = 3'd2;
                4'b0110 : t3 = 3'd2;
                4'b0111 : t3 = 3'd3;
                4'b1000 : t3 = 3'd1;
                4'b1001 : t3 = 3'd2;
                4'b1010 : t3 = 3'd2;
                4'b1011 : t3 = 3'd3;
                4'b1100 : t3 = 3'd2;
                4'b1101 : t3 = 3'd3;
                4'b1110 : t3 = 3'd3;
                4'b1111 : t3 = 3'd4;
            endcase

            // Following sums are perfectly working but Verilator generates multiple %Warning-WIDTH on
            // them. I prefer to disable such warning, rather than writing verbose and less clear code!
            // verilator lint_off WIDTH

            // 2^4
            p10 = i__a[0] * i__b[4];
            p11 = i__a[1] * i__b[3];
            p12 = i__a[2] * i__b[2];
            p13 = i__a[3] * i__b[1];
            p14 = i__a[4] * i__b[0];
            t4 = p10 + p11 + p12 + p13 + p14;

            // 2^5
            p15 = i__a[0] * i__b[5];
            p16 = i__a[1] * i__b[4];
            p17 = i__a[2] * i__b[3];
            p18 = i__a[3] * i__b[2];
            p19 = i__a[4] * i__b[1];
            p20 = i__a[5] * i__b[0];
            t5 = p15 + p16 + p17 + p18 + p19 + p20;

            // 2^6
            p21 = i__a[1] * i__b[5];
            p22 = i__a[2] * i__b[4];
            p23 = i__a[3] * i__b[3];
            p24 = i__a[4] * i__b[2];
            p25 = i__a[5] * i__b[1];
            t6 = p21 + p22 + p23 + p24 + p25;

            // Reenable the Verilator %Warning-WIDTH previously disabled.
            // verilator lint_on WIDTH

            // 2^7
            p26 = i__a[2] * i__b[5];
            p27 = i__a[3] * i__b[4];
            p28 = i__a[4] * i__b[3];
            p29 = i__a[5] * i__b[2];
            case ({p29,p28,p27,p26})
                4'b0000 : t7 = 3'd0;
                4'b0001 : t7 = 3'd1;
                4'b0010 : t7 = 3'd1;
                4'b0011 : t7 = 3'd2;
                4'b0100 : t7 = 3'd1;
                4'b0101 : t7 = 3'd2;
                4'b0110 : t7 = 3'd2;
                4'b0111 : t7 = 3'd3;
                4'b1000 : t7 = 3'd1;
                4'b1001 : t7 = 3'd2;
                4'b1010 : t7 = 3'd2;
                4'b1011 : t7 = 3'd3;
                4'b1100 : t7 = 3'd2;
                4'b1101 : t7 = 3'd3;
                4'b1110 : t7 = 3'd3;
                4'b1111 : t7 = 3'd4;
            endcase

            // 2^8
            p30 = i__a[3] * i__b[5];
            p31 = i__a[4] * i__b[4];
            p32 = i__a[5] * i__b[3];
            `full_adder(p30, p31, p32, s8, c8);

            // 2^9
            p33 = i__a[4] * i__b[5];
            p34 = i__a[5] * i__b[4];
            `half_adder(p33, p34, s9, c9);

            // 2^10
            p35 = i__a[5] * i__b[5];

            // Ultimate sum and output.
            lastsum = { p35,    s9,    s8, t7[0], t6[0], t5[0], t4[0], t3[0],   s2} +
                      {  c9,    c8, t7[1], t6[1], t5[1], t4[1], t3[1],    c2,   c1} +
                      {1'b0, t7[2], t6[2], t5[2], t4[2], t3[2],  1'b0,  1'b0, 1'b0};
            o__y = {lastsum, s1, p0};
        end

        `undef half_adder
        `undef full_adder

    end
    //endgenerate

endmodule
