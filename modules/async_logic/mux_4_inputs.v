
// (C) 2021-2025 Flavio Cappelli
// Released under the MIT License.
//
// 4-Inputs Multiplexer.

`default_nettype none                               // Do not allow undeclared signals.
`include "timescale.vh"                             // Timescale defined globally.


module mux_4_inputs #(
    parameter DATA_WIDTH = 8                        // Size of data (default 8 bits).
) (
    input  wire            [1:0] i__s,              // Input selection.
    input  wire [DATA_WIDTH-1:0] i__a,              // Data inputs.
    input  wire [DATA_WIDTH-1:0] i__b,
    input  wire [DATA_WIDTH-1:0] i__c,
    input  wire [DATA_WIDTH-1:0] i__d,
    output reg  [DATA_WIDTH-1:0] o__y               // Data output.
);

    always @(*) begin
        case (i__s)
            2'b00 : o__y = i__a;
            2'b01 : o__y = i__b;
            2'b10 : o__y = i__c;
            2'b11 : o__y = i__d;
        endcase
    end

endmodule
