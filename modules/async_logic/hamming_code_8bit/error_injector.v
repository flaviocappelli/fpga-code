
// (C) 2021-2025 Flavio Cappelli
// Released under the MIT License.
//
// This module is used to inject errors into the Hamming Code.

`default_nettype none                           // Do not allow undeclared signals.
`include "timescale.vh"                         // Timescale defined globally.


module error_injector(
    input  wire [11:0] hamming_in,
    input  wire [11:0] error_pattern,
    output wire [11:0] hamming_out
);

    assign hamming_out = hamming_in ^ error_pattern;

endmodule
