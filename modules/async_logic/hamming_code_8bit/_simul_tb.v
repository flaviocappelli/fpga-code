
// (C) 2021-2025 Flavio Cappelli
// Released under the MIT License.
//
// See https://www.fpgatutorial.com/how-to-write-a-basic-verilog-testbench/

`default_nettype none                       // Do not allow undeclared signals.
`define DUMPSTR(x) `"x.vcd`"                // Required for GTKWave.
`define SIMULATION_TIME 3330us              // Simulation time (in s, ms, us or ns).
`include "timescale.vh"                     // Timescale defined globally.

module testbench();
    initial begin
        $dumpfile(`DUMPSTR(`VCD_OUTPUT));   // Required for GTKWave.
        $dumpvars(0, testbench);            // Dump all variables in all instantiated modules.
        $printtimescale(testbench);         // Display the time scale and resolution (see above).
        #`SIMULATION_TIME                   // Wait until the simulation time is elapsed.
        $timeformat(-6, 2, "us", 1);        // Display the time elapsed in "us".
        $display("Simulation terminated after %t", $realtime);
        $finish;                            // Terminate the simulation.
    end


    // ---- Clock simulation ----

    // NONE REQUIRED.


    // ---- Hamming Code Encoder/Decoder simulation ----

    reg   [7:0] data;                       // Input data.
    reg  [11:0] error_inj;                  // Error injected in the generated Hamming Code.
    wire [11:0] hamming_code;               // Generated Hamming Code.
    wire [11:0] altered_code;               // Hamming Code altered with injected error.
    wire  [7:0] corrected_data;             // Data corrected after the injected error.
    wire        flip_alert;                 // Bit flip detected and corrected.

    _simul_top TOP (                        // Top module.
        .data(data),
        .error_inj(error_inj),
        .hamming_code(hamming_code),
        .altered_code(altered_code),
        .corrected_data(corrected_data),
        .flip_alert(flip_alert)
    );

    initial begin                           // Generate exhaustive test data: YOU HAVE TO USE THE
        #0.5us                              // HORIZONTAL SCROLL IN GTKWAVE TO SEE ALL TEST DATA.
        data = 0;
        repeat (256) begin
            error_inj = 12'b000000000000;   // Start with no error.
            #1us
            error_inj = 12'b000000000001;   // Flip bit 0 of the generated Hamming Code.
            #1us
            repeat (11) begin               // Move flip bit on the next position (and so on).
                error_inj = { error_inj[10:0], 1'b0};
                if (data != corrected_data)
                    $fatal(1, "ERROR: altered Hamming Code not corrected!");
                #1us
                ;
            end
            data = data + 1;                // Increment input data word and repeat all test.
        end
    end

endmodule
