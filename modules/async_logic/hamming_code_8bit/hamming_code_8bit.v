
// (C) 2021-2025 Flavio Cappelli
// Released under the MIT License.
//
// Hamming encoder/decoder for 8-bit data word
// (with single error detection and correction).
// See https://en.wikipedia.org/wiki/Hamming_code

`default_nettype none                       // Do not allow undeclared signals.
`include "timescale.vh"                     // Timescale defined globally.


module hamming_encoder_8to12(
    input  wire  [8:1] i__din,              // Data input.
    output wire [12:1] o__hout              // Hamming code output.
);

    // Map data bits on the Hamming Code and calculate parity bits. Note
    // that in Hamming Codes, bits are always enumerated starting from 1.
    assign o__hout[1]  = i__din[1] ^ i__din[2] ^ i__din[4] ^ i__din[5] ^ i__din[7];
    assign o__hout[2]  = i__din[1] ^ i__din[3] ^ i__din[4] ^ i__din[6] ^ i__din[7];
    assign o__hout[3]  = i__din[1];
    assign o__hout[4]  = i__din[2] ^ i__din[3] ^ i__din[4] ^ i__din[8];
    assign o__hout[5]  = i__din[2];
    assign o__hout[6]  = i__din[3];
    assign o__hout[7]  = i__din[4];
    assign o__hout[8]  = i__din[5] ^ i__din[6] ^ i__din[7] ^ i__din[8];
    assign o__hout[9]  = i__din[5];
    assign o__hout[10] = i__din[6];
    assign o__hout[11] = i__din[7];
    assign o__hout[12] = i__din[8];

endmodule


module hamming_decoder_12to8(
    input  wire [12:1] i__hin,              // Hamming code input.
    output wire  [8:1] o__dout,             // Corrected output data.
    output wire        o__flip              // Bit flip alert.
);

    wire c1,c2,c4,c8;

    // Calculate old parity bits ^ new parity bits.
    assign c1 = i__hin[1] ^ i__hin[3] ^ i__hin[5]  ^ i__hin[7]  ^ i__hin[9]  ^ i__hin[11];
    assign c2 = i__hin[2] ^ i__hin[3] ^ i__hin[6]  ^ i__hin[7]  ^ i__hin[10] ^ i__hin[11];
    assign c4 = i__hin[4] ^ i__hin[5] ^ i__hin[6]  ^ i__hin[7]  ^ i__hin[12];
    assign c8 = i__hin[8] ^ i__hin[9] ^ i__hin[10] ^ i__hin[11] ^ i__hin[12];

    // Return corrected data bits.
    assign o__dout[1] = ({c8,c4,c2,c1} == 4'b0011 ? ~i__hin[3]  : i__hin[3]);
    assign o__dout[2] = ({c8,c4,c2,c1} == 4'b0101 ? ~i__hin[5]  : i__hin[5]);
    assign o__dout[3] = ({c8,c4,c2,c1} == 4'b0110 ? ~i__hin[6]  : i__hin[6]);
    assign o__dout[4] = ({c8,c4,c2,c1} == 4'b0111 ? ~i__hin[7]  : i__hin[7]);
    assign o__dout[5] = ({c8,c4,c2,c1} == 4'b1001 ? ~i__hin[9]  : i__hin[9]);
    assign o__dout[6] = ({c8,c4,c2,c1} == 4'b1010 ? ~i__hin[10] : i__hin[10]);
    assign o__dout[7] = ({c8,c4,c2,c1} == 4'b1011 ? ~i__hin[11] : i__hin[11]);
    assign o__dout[8] = ({c8,c4,c2,c1} == 4'b1100 ? ~i__hin[12] : i__hin[12]);

    // Notify error detection and correction (i.e. bit flip), if any. This
    // can be useful, for example, to implement an ECC RAM: when a bit flip
    // is detected, the corrected data must be immediately written back to the
    // RAM, otherwise subsequent errors will not be detected (nor corrected).
    assign o__flip = c8 | c4 | c2 | c1;

endmodule
