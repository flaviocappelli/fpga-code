
// (C) 2021-2025 Flavio Cappelli
// Released under the MIT License.
//
// This file is used to pass config parameters (if any) to the
// modules under test, ensuring the modules are configured the
// same way in both behavioral and post-synthesis simulation.
// It also makes "apio lint" happy because it guarantees that
// only one "top" module exists (Verilator requires it).

`default_nettype none           // Do not allow undeclared signals.
`include "timescale.vh"         // Timescale defined globally.

(* top *)                       // Mark this as top module, because
module _simul_top (             // sometimes Yosys doesn't detect it.
    input  wire  [7:0] data,
    input  wire [11:0] error_inj,
    output wire [11:0] hamming_code,
    output wire [11:0] altered_code,
    output wire  [7:0] corrected_data,
    output wire        flip_alert
);

    hamming_encoder_8to12 HENC (
        .i__din(data),
        .o__hout(hamming_code)
    );

    error_injector EINJ (
        .hamming_in(hamming_code),
        .error_pattern(error_inj),
        .hamming_out(altered_code)
    );

    hamming_decoder_12to8 HDEC(
        .i__hin(altered_code),
        .o__dout(corrected_data),
        .o__flip(flip_alert)
    );

endmodule
