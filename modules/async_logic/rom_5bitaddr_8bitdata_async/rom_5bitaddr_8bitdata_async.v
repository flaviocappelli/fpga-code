
// (C) 2021-2025 Flavio Cappelli
// Released under the MIT License.
//
// Example of Asynchronous ROM with 8-Bit Data and 5-Bit Address.
//
// NOTE: FPGAs and ASICs do not have tri-state drivers inside the core,
//       only at I/O pads. So we don't need inputs like "cs" or "oe" here.

`default_nettype none                               // Do not allow undeclared signals.
`include "timescale.vh"                             // Timescale defined globally.


module rom_5bitaddr_8bitdata_async (
    input  wire [4:0] i__addr,                      // Address input.
    output reg  [7:0] o__data                       // Data output.
);

    always @(i__addr) begin
        case (i__addr)
            5'h00 : o__data = 8'b10000000;
            5'h01 : o__data = 8'b10000001;
            5'h02 : o__data = 8'b10000010;
            5'h03 : o__data = 8'b10000011;
            5'h04 : o__data = 8'b10000100;
            5'h05 : o__data = 8'b10000101;
            5'h06 : o__data = 8'b10000110;
            5'h07 : o__data = 8'b10000111;
            5'h08 : o__data = 8'b10001000;
            5'h09 : o__data = 8'b10001001;
            5'h0A : o__data = 8'b10001010;
            5'h0B : o__data = 8'b10001011;
            5'h0C : o__data = 8'b10001100;
            5'h0D : o__data = 8'b10001101;
            5'h0E : o__data = 8'b10001110;
            5'h0F : o__data = 8'b10001111;
            5'h10 : o__data = 8'b10010000;
            5'h11 : o__data = 8'b10010001;
            5'h12 : o__data = 8'b10010010;
            5'h13 : o__data = 8'b10010011;
            5'h14 : o__data = 8'b10010100;
            5'h15 : o__data = 8'b10010101;
            5'h16 : o__data = 8'b10010110;
            5'h17 : o__data = 8'b10010111;
            5'h18 : o__data = 8'b10011000;
            5'h19 : o__data = 8'b10011001;
            5'h1A : o__data = 8'b10011010;
            5'h1B : o__data = 8'b10011011;
            5'h1C : o__data = 8'b10011100;
            5'h1D : o__data = 8'b10011101;
            5'h1E : o__data = 8'b10011110;
            5'h1F : o__data = 8'b10011111;

            // Add the line below if not all address are defined.
            // default: o__data = 8'bXXXXXXXX
        endcase
    end

endmodule
