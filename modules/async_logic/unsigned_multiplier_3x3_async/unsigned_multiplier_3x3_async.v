
// (C) 2021-2025 Flavio Cappelli
// Released under the MIT License.
//
// Unsigned asynchronous 3x3 multiplier.
//
// NOTE: three implementations are provided:
//
//        * MUL3X3_METHOD = 0  -->  use tolchain default
//        * MUL3X3_METHOD = 1  -->  use Vedic 3x3 logic
//        * MUL3X3_METHOD = 2  -->  use a table
//
// The FPGA I am currently using (iCE40 LP8K) does not have
// a DSP core (or a dedicated hardware multiplier) so, it is
// interesting to see which method provides the fastest logic.
// These are the resources used by Yosys on the iCE40 FPGA:
//
//   MUL3X3_METHOD  Cells  SB_LUT4  SB_CARRY  EPD(*)
//   -----------------------------------------------
//         0          16      13       3       7.4ns
//         1          16      13       3       7.4ns
//         2          57      57       0      10.0ns
//
// (*) Estimated propagation delay of the multiplier
//     on the TinyFPGA BX (excluding the I/O cells).
//
// For the 3x3 moltiplication, the implementation provided
// by Yosys and by the Vedic method are practically equal.
//
// NOTE: these results may vary among different synthesis
//       tools, and even among different versions of the
//       same tool. Furthermore, the optimizations carried
//       out by the synthesis tool depend on the used FPGA
//       and the overall circuit, so it is recommended to
//       rerun such tests on the circuit under development.

`default_nettype none               // Do not allow undeclared signals.
`include "timescale.vh"             // Timescale defined globally.


module unsigned_multiplier_3x3_async #(
    parameter MUL3X3_METHOD = 1    // Fastest method measured.
) (
    input  wire [2:0] i__a,
    input  wire [2:0] i__b,
    output reg  [5:0] o__y
);

    //generate (optional in Verilog-2005)
    if (MUL3X3_METHOD == 0) begin : gen_3X3_M0

        // Toolchain implementation.
        always @(*) begin
            o__y = i__a * i__b;
        end

    end else if (MUL3X3_METHOD == 1) begin : gen_3X3_M1

        // Vedic method from "Urdhva Triyakbhyam Sutra".
        //
        // (a2*2^2 + a1*2^1 + a0*2^0) * (b2*2^2 + b1*2^1 + b0*2^0) =
        // a2*b2*2^4 + (a1*b2 + a2*b1)*2^3 + (a0*b2 + a1*b1 + a2*b0)*2^2 +
        // (a0*b1 + a1*b0)*2^1 +a0*b0*2^0
        //
        // y5  y4  y3  y2  y1  y0
        // ======================
        //                     p0 = a0 * b0
        //
        //             c1  s1     = carry & sum of (p1,p2) : half adder
        //                          where p1 = a0*b1, p2 = a1*b0
        //
        //         c2  s2         = carry & sum of (p3,p4,p5) : full adder
        //                          where p3 = a0*b2, p4 = a1*b1, p5 = a2*b0
        //
        //     c3  s3             = carry & sum of (p6,p7) : half adder
        //                          where p6 = a1*b2, p7 = a2*b1
        //
        //     p8                 = a2*b2
        // ----------------------
        // S5  S4  S3  S2         = carry & sum of ({c3,c2,c1}, {p8,s3,s2}) : toolchain default
        //
        // NOTE: I initially used tasks instead of the macros below,
        // but I discovered that some versions of Icarus Verilog return
        // wrong results with tasks, while macros just work.

        `define half_adder(a,b,sum,carry)    {carry, sum} = a + b
        `define full_adder(cin,a,b,sum,cout) {cout, sum} = a + b + cin

        reg p0, p1, p2, p3, p4, p5, p6, p7, p8;
        reg s1, c1, s2, c2, s3, c3;
        reg [3:0] lastsum;

        always @(*) begin
            // 2^0
            p0 = i__a[0] * i__b[0];

            // 2^1
            p1 = i__a[0] * i__b[1];
            p2 = i__a[1] * i__b[0];
            `half_adder(p1, p2, s1, c1);

            // 2^2
            p3 = i__a[0] * i__b[2];
            p4 = i__a[1] * i__b[1];
            p5 = i__a[2] * i__b[0];
            `full_adder(p3, p4, p5, s2, c2);

            // 2^3
            p6 = i__a[1] * i__b[2];
            p7 = i__a[2] * i__b[1];
            p8 = i__a[2] * i__b[2];
            `half_adder(p6, p7, s3, c3);

            // Ultimate sum and output.
            lastsum = {p8,s3,s2} + {c3,c2,c1};
            o__y = {lastsum, s1, p0};
        end

        `undef half_adder
        `undef full_adder

    end else begin : gen_3X3_M2

        // Use a table.
        always @(*) begin
            case({i__a, i__b})
                6'b000_000: o__y = 6'b000000;
                6'b000_001: o__y = 6'b000000;
                6'b000_010: o__y = 6'b000000;
                6'b000_011: o__y = 6'b000000;
                6'b000_100: o__y = 6'b000000;
                6'b000_101: o__y = 6'b000000;
                6'b000_110: o__y = 6'b000000;
                6'b000_111: o__y = 6'b000000;
                6'b001_000: o__y = 6'b000000;
                6'b001_001: o__y = 6'b000001;
                6'b001_010: o__y = 6'b000010;
                6'b001_011: o__y = 6'b000011;
                6'b001_100: o__y = 6'b000100;
                6'b001_101: o__y = 6'b000101;
                6'b001_110: o__y = 6'b000110;
                6'b001_111: o__y = 6'b000111;
                6'b010_000: o__y = 6'b000000;
                6'b010_001: o__y = 6'b000010;
                6'b010_010: o__y = 6'b000100;
                6'b010_011: o__y = 6'b000110;
                6'b010_100: o__y = 6'b001000;
                6'b010_101: o__y = 6'b001010;
                6'b010_110: o__y = 6'b001100;
                6'b010_111: o__y = 6'b001110;
                6'b011_000: o__y = 6'b000000;
                6'b011_001: o__y = 6'b000011;
                6'b011_010: o__y = 6'b000110;
                6'b011_011: o__y = 6'b001001;
                6'b011_100: o__y = 6'b001100;
                6'b011_101: o__y = 6'b001111;
                6'b011_110: o__y = 6'b010010;
                6'b011_111: o__y = 6'b010101;
                6'b100_000: o__y = 6'b000000;
                6'b100_001: o__y = 6'b000100;
                6'b100_010: o__y = 6'b001000;
                6'b100_011: o__y = 6'b001100;
                6'b100_100: o__y = 6'b010000;
                6'b100_101: o__y = 6'b010100;
                6'b100_110: o__y = 6'b011000;
                6'b100_111: o__y = 6'b011100;
                6'b101_000: o__y = 6'b000000;
                6'b101_001: o__y = 6'b000101;
                6'b101_010: o__y = 6'b001010;
                6'b101_011: o__y = 6'b001111;
                6'b101_100: o__y = 6'b010100;
                6'b101_101: o__y = 6'b011001;
                6'b101_110: o__y = 6'b011110;
                6'b101_111: o__y = 6'b100011;
                6'b110_000: o__y = 6'b000000;
                6'b110_001: o__y = 6'b000110;
                6'b110_010: o__y = 6'b001100;
                6'b110_011: o__y = 6'b010010;
                6'b110_100: o__y = 6'b011000;
                6'b110_101: o__y = 6'b011110;
                6'b110_110: o__y = 6'b100100;
                6'b110_111: o__y = 6'b101010;
                6'b111_000: o__y = 6'b000000;
                6'b111_001: o__y = 6'b000111;
                6'b111_010: o__y = 6'b001110;
                6'b111_011: o__y = 6'b010101;
                6'b111_100: o__y = 6'b011100;
                6'b111_101: o__y = 6'b100011;
                6'b111_110: o__y = 6'b101010;
                6'b111_111: o__y = 6'b110001;
            endcase
        end

    end
    //endgenerate

endmodule
