
// (C) 2021-2025 Flavio Cappelli
// Released under the MIT License.
//
// Unsigned asynchronous 4x4 multiplier.
//
// NOTE: three implementations are provided:
//
//        * MUL4X4_METHOD = 0  -->  use tolchain default
//        * MUL4X4_METHOD = 1  -->  use four 2x2 multipliers
//        * MUL4X4_METHOD = 2  -->  use "flat" Vedic 4x4 logic
//
// NOTE: the table methods is not implemented because it uses
//       too much FPGA resources and is anyway slower than the
//       above methods (see the 3x3 multiplier).
//
// The FPGA I am currently using (iCE40 LP8K) does not have
// a DSP core (or a dedicated hardware multiplier) so, it is
// interesting to see which method provides the fastest logic.
// These are the resources used by Yosys on the iCE40 FPGA:
//
//   MUL4X4_METHOD  MUL2X2_METHOD  Cells  SB_LUT4  SB_CARRY  EPD(*)
//   --------------------------------------------------------------
//         0           ignored       30      26       4       8.2ns
//         1              0          44      37       7      12.3ns
//         1              1          31      27       4       8.6ns
//         1              2          31      27       4       7.6ns
//         2           ignored       37      33       4       9.9ns
//
// (*) Estimated propagation delay of the multiplier
//     on the TinyFPGA BX (excluding the I/O cells).
//
// For the 4x4 multiplication, the implementation provided by Yosys
// seems optimal in terms of FPGA resource utilization, but it isn't
// the fastest. The fastest seems the decomposition into four 2x2
// products, each one of them implemented with a table.
//
// NOTE: these results may vary among different synthesis
//       tools, and even among different versions of the
//       same tool. Furthermore, the optimizations carried
//       out by the synthesis tool depend on the used FPGA
//       and the overall circuit, so it is recommended to
//       rerun such tests on the circuit under development.

`default_nettype none               // Do not allow undeclared signals.
`include "timescale.vh"             // Timescale defined globally.


module unsigned_multiplier_4x4_async #(
    parameter MUL4X4_METHOD = 1,    // Fastest method measured.
    parameter MUL2X2_METHOD = 2
) (
    input  wire [3:0] i__a,
    input  wire [3:0] i__b,
    output reg  [7:0] o__y
);

    //generate (optional in Verilog-2005)
    if (MUL4X4_METHOD == 0) begin : gen_4X4_M0

        // Toolchain implementation.
        always @(*) begin
            o__y = i__a * i__b;
        end

    end else if (MUL4X4_METHOD == 1) begin : gen_4X4_M1

        // If we set: X1 = a3*2^1 + a2*2^0, X0 = a1*2^1 + a0*2^0,
        //            Y1 = b3*2^1 + b2*2^0, Y0 = b1*2^1 + b0*2^0,
        //
        // then we can write:
        //
        // A * B = (a3*2^3 + a2*2^2 + a1*2^1 + a0*2^0) *
        //         (b3*2^3 + b2*2^2 + b1*2^1 + b0*2^0) =
        //         (X1*2^2 + X0*2^0) * (Y1*2^2 + Y0*2^0) =
        //         X1*Y1*2^4 + (X0*Y1 + X1*Y0)*2^2 + X0*Y0*2^0
        //
        // So, the 4x4 multiplication can be decomposed in four 2x2 multipliers, plus
        // some adders (and some shifts, but those are all trivials):
        //
        //      ----  X0*Y0  (p0)   Note that each p term can have a maximum value of 9
        //    ----    X0*Y1  (p1)   (i.e. the result of 3*3). We will proceed adding the
        //    ----    X1*Y0  (p2)   middle terms p1 and p2 first: the result will be at
        //  ----      X1*Y1  (p3)   most 18 (9 + 9), so 5 bits are required for such sum.
        //                          Then we will add this sum to {p3, p0[3:2]} (max value
        // 1001_10, i.e. 38), so a 6-bit word is enough for the last sum (18 + 38 = 56).

        wire [3:0] p0, p1, p2, p3;
        reg  [4:0] middlesum;       // 5 bits required (see above).
        reg  [5:0] lastsum;         // 6 bits required (see above).

        unsigned_multiplier_2x2_async #(
            .MUL2X2_METHOD(MUL2X2_METHOD)
        ) MUL2x2_0 (
            .i__a(i__a[1:0]),       // X0.
            .i__b(i__b[1:0]),       // Y0.
            .o__y(p0)
        );

        unsigned_multiplier_2x2_async #(
            .MUL2X2_METHOD(MUL2X2_METHOD)
        ) MUL2x2_1 (
            .i__a(i__a[1:0]),       // X0.
            .i__b(i__b[3:2]),       // Y1.
            .o__y(p1)
        );

        unsigned_multiplier_2x2_async #(
            .MUL2X2_METHOD(MUL2X2_METHOD)
        ) MUL2x2_2 (
            .i__a(i__a[3:2]),       // X1.
            .i__b(i__b[1:0]),       // Y0.
            .o__y(p2)
        );

        unsigned_multiplier_2x2_async #(
            .MUL2X2_METHOD(MUL2X2_METHOD)
        ) MUL2x2_3 (
            .i__a(i__a[3:2]),       // X1.
            .i__b(i__b[3:2]),       // Y1.
            .o__y(p3)
        );

        always @(*) begin
            middlesum = p1 + p2;
            lastsum = {p3, p0[3:2]} + {1'b0, middlesum};
            o__y = {lastsum, p0[1:0]};
        end

    end else begin : gen_4X4_M2

        // Vedic method from "Urdhva Triyakbhyam Sutra".
        //
        // (a3*2^3 + a2*2^2 + a1*2^1 + a0*2^0) * (b3*2^3 + b2*2^2 + b1*2^1 + b0*2^0) =
        // a3*b3*2^6 + (a2*b3 + a3*b2)*2^5 + (a1*b3 + a2*b2 + a3*b1)*2^4 +
        // (a0*b3 + a1*b2 + a2*b1 + a3*b0)*2^3 + (a0*b2 + a1*b1 + a2*b0)*2^2 +
        // (a0*b1 + a1*b0)*2^1 + a0*b0*2^0
        //
        // y7  y6  y5  y4  y3  y2  y1  y0
        // ==============================
        //                             p0 = a0 * b0
        //
        //                     c1  s1     = carry & sum of (p1,p2) : half adder
        //                                  where p1 = a0*b1, p2 = a1*b0
        //
        //                 c2  s2         = carry & sum of (p3,p4,p5) : full adder
        //                                  where p3 = a0*b2, p4 = a1*b1, p5 = a2*b0
        //
        //         t32 t31 t30            = 3 bits from p6 + p7 + p8 + p9 : table generated (LUT4)
        //                                  where p6 = a0*b3, p7 = a1*b2, p8 = a2*b1, p9 = a3*b0
        //
        //         c4  s4                 = carry & sum of (p10,p11,p12) : full adder
        //                                  where p10 = a1*b3, p11 = a2*b2, p12 = a3*b1
        //
        //     c5  s5                     = carry & sum of (p13,p14) : half adder
        //                                  where p13 = a2*b3, p14 = a3*b2
        //
        //     p15                        = a3*b3
        // ------------------------------
        // S7  S6  S5  S4  S3  S2         = carry & sum of ({p15,  s5,  s4, t30, s2},
        //                                                  { c5,  c4, t31,  c2, c1},
        //                                                  {  0, t32,   0,   0,  0}) : toolchain default
        //
        // NOTE: I initially used tasks instead of the macros below, but I discovered that some
        // versions of Icarus Verilog return wrong results with tasks, while macros just work.

        `define half_adder(a,b,sum,carry)    {carry, sum} = a + b
        `define full_adder(cin,a,b,sum,cout) {cout, sum} = a + b + cin

        reg p0, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, p12, p13, p14, p15;
        reg s1, c1, s2, c2, s4, c4, s5, c5;
        reg [2:0] t3;
        reg [5:0] lastsum;

        always @(*) begin
            // 2^0
            p0 = i__a[0] * i__b[0];

            // 2^1
            p1 = i__a[0] * i__b[1];
            p2 = i__a[1] * i__b[0];
            `half_adder(p1, p2, s1, c1);

            // 2^2
            p3 = i__a[0] * i__b[2];
            p4 = i__a[1] * i__b[1];
            p5 = i__a[2] * i__b[0];
            `full_adder(p3, p4, p5, s2, c2);

            // 2^3
            p6 = i__a[0] * i__b[3];
            p7 = i__a[1] * i__b[2];
            p8 = i__a[2] * i__b[1];
            p9 = i__a[3] * i__b[0];
            case ({p9,p8,p7,p6})
                4'b0000 : t3 = 3'd0;
                4'b0001 : t3 = 3'd1;
                4'b0010 : t3 = 3'd1;
                4'b0011 : t3 = 3'd2;
                4'b0100 : t3 = 3'd1;
                4'b0101 : t3 = 3'd2;
                4'b0110 : t3 = 3'd2;
                4'b0111 : t3 = 3'd3;
                4'b1000 : t3 = 3'd1;
                4'b1001 : t3 = 3'd2;
                4'b1010 : t3 = 3'd2;
                4'b1011 : t3 = 3'd3;
                4'b1100 : t3 = 3'd2;
                4'b1101 : t3 = 3'd3;
                4'b1110 : t3 = 3'd3;
                4'b1111 : t3 = 3'd4;
            endcase

            // 2^4
            p10 = i__a[1] * i__b[3];
            p11 = i__a[2] * i__b[2];
            p12 = i__a[3] * i__b[1];
            `full_adder(p10, p11, p12, s4, c4);

            // 2^5
            p13 = i__a[2] * i__b[3];
            p14 = i__a[3] * i__b[2];
            `half_adder(p13, p14, s5, c5);

            // 2^6
            p15 =  i__a[3] * i__b[3];

            // Ultimate sum and output.
            lastsum = { p15,    s5,    s4, t3[0],   s2} +
                      {  c5,    c4, t3[1],    c2,   c1} +
                      {1'b0, t3[2],  1'b0,  1'b0, 1'b0};
            o__y = {lastsum, s1, p0};
        end

        `undef half_adder
        `undef full_adder

    end
    //endgenerate

endmodule
