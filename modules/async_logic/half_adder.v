
// (C) 2021-2025 Flavio Cappelli
// Released under the MIT License.
//
// Half Adder.

`default_nettype none                               // Do not allow undeclared signals.
`include "timescale.vh"                             // Timescale defined globally.


module half_adder #(
    parameter DATA_WIDTH = 8                        // Size of data (default 8 bits).
) (
    input  wire [DATA_WIDTH-1:0] i__a,              // Data inputs.
    input  wire [DATA_WIDTH-1:0] i__b,
    output wire [DATA_WIDTH-1:0] o__sum,            // Sum.
    output wire                  o__cout            // Carry.
);

    assign {o__cout, o__sum} = i__a + i__b;

endmodule
