
// (C) 2021-2025 Flavio Cappelli
// Released under the MIT License.
//
// Unsigned asynchronous 8x8 multiplier.
//
// NOTE: three implementations are provided:
//
//        * MUL8X8_METHOD = 0  -->  use tolchain default
//        * MUL8X8_METHOD = 1  -->  use four 4x4 multipliers
//        * MUL8X8_METHOD = 2  -->  use "flat" Vedic 8x8 logic
//
// NOTE: the table methods is not implemented because it uses
//       too much FPGA resources and is anyway slower than the
//       above methods (see the 3x3 multiplier).
//
// The FPGA I am currently using (iCE40 LP8K) does not have
// a DSP core (or a dedicated hardware multiplier) so, it is
// interesting to see which method provides the fastest logic.
// These are the resources used by Yosys on the iCE40 FPGA:
//
//   MUL8X8_METHOD  MUL4X4_METHOD  MUL2X2_METHOD  Cells  SB_LUT4  SB_CARRY  EPD(*)
//   -----------------------------------------------------------------------------
//         0           ignored        ignored      169      159      10     12.5ns
//         1              0           ignored      162      146      16     18.6ns
//         1              1              0         206      166      40     15.9ns
//         1              1              1         159      133      26     10.4ns
//         1              1              2         159      133      26     13.3ns
//         1              2            ignored     181      155      26     12.4ns
//         2           ignored         ignored     184      158      26     14.8ns
//
// (*) Estimated propagation delay of the multiplier
//     on the TinyFPGA BX (excluding the I/O cells).
//
// For the 8x8 moltiplication, the implementation provided by
// Yosys does not seem the best.  A better result is obtained
// with the decomposition into four 4x4 multiplications, each
// of which further decomposed into four 2x2 multiplications
// (each implemented using the Vedic 2x2 logic).
//
// NOTE: these results may vary among different synthesis
//       tools, and even among different versions of the
//       same tool. Furthermore, the optimizations carried
//       out by the synthesis tool depend on the used FPGA
//       and the overall circuit, so it is recommended to
//       rerun such tests on the circuit under development.

`default_nettype none               // Do not allow undeclared signals.
`include "timescale.vh"             // Timescale defined globally.


module unsigned_multiplier_8x8_async #(
    parameter MUL8X8_METHOD = 1,    // Fastest method measured.
    parameter MUL4X4_METHOD = 1,
    parameter MUL2X2_METHOD = 1
) (
    input  wire  [7:0] i__a,
    input  wire  [7:0] i__b,
    output reg  [15:0] o__y
);

    //generate (optional in Verilog-2005)
    if (MUL8X8_METHOD == 0) begin : gen_8X8_M0

        // Toolchain implementation.
        always @(*) begin
            o__y = i__a * i__b;
        end

    end else if (MUL8X8_METHOD == 1) begin : gen_8X8_M1

        // If we set: X1 = a7*2^3 + a6*2^2 + a5*2^1 + a4*2^0,
        //            X0 = a3*2^3 + a2*2^2 + a1*2^1 + a0*2^0,
        //            Y1 = b7*2^3 + b6*2^2 + b5*2^1 + b4*2^0,
        //            Y0 = b3*2^3 + b2*2^2 + b1*2^1 + b0*2^0,
        //
        // then we can write:
        //
        // A * B = (a7*2^7 + a6*2^6 + a5*2^5 + a4*2^4 + a3*2^3 + a2*2^2 + a1*2^1 + a0*2^0) *
        //         (b7*2^7 + b6*2^6 + b5*2^5 + b4*2^4 + b3*2^3 + b2*2^2 + b1*2^1 + b0*2^0) =
        //         (X1*2^4 + X0*2^0) * (Y1*2^4 + Y0*2^0) =
        //         X1*Y1*2^8 + (X0*Y1 + X1*Y0)*2^4 + X0*Y0*2^0
        //
        // So, the 8x8 multiplication can be decomposed in four 4x4 multipliers, plus
        // some adders (and some shifts, but those are all trivials):
        //
        //         -------- X0*Y0  (p0)  Note that each p term can have a maximum value of 225
        //     --------     X0*Y1  (p1)  (i.e. the result of 15*15). We will proceed adding the
        //     --------     X1*Y0  (p2)  middle terms p1 and p2 first: the result will be at most
        // --------         X1*Y1  (p3)  450 (225 + 225), so 9 bits are required for the sum. Then
        //                               we will add such sum to {p3, p0[7:4]} (max value 3614, i.e.
        // 11100001_1110), so a 12-bit word is enough for the last sum (450 + 3614 = 4064).

        wire [7:0] p0, p1, p2, p3;
        reg  [8:0] middlesum;       // 9 bits required (see above).
        reg  [11:0] lastsum;        // 12 bits required (see above).

        unsigned_multiplier_4x4_async #(
            .MUL4X4_METHOD(MUL4X4_METHOD),
            .MUL2X2_METHOD(MUL2X2_METHOD)
        ) MUL4x4_0 (
            .i__a(i__a[3:0]),       // X0.
            .i__b(i__b[3:0]),       // Y0.
            .o__y(p0)
        );

        unsigned_multiplier_4x4_async #(
            .MUL4X4_METHOD(MUL4X4_METHOD),
            .MUL2X2_METHOD(MUL2X2_METHOD)
        ) MUL4x4_1 (
            .i__a(i__a[3:0]),       // X0.
            .i__b(i__b[7:4]),       // Y1.
            .o__y(p1)
        );

        unsigned_multiplier_4x4_async #(
            .MUL4X4_METHOD(MUL4X4_METHOD),
            .MUL2X2_METHOD(MUL2X2_METHOD)
        ) MUL4x4_2 (
            .i__a(i__a[7:4]),       // X1.
            .i__b(i__b[3:0]),       // Y0.
            .o__y(p2)
        );

        unsigned_multiplier_4x4_async #(
            .MUL4X4_METHOD(MUL4X4_METHOD),
            .MUL2X2_METHOD(MUL2X2_METHOD)
        ) MUL4x4_3 (
            .i__a(i__a[7:4]),       // X1.
            .i__b(i__b[7:4]),       // Y1.
            .o__y(p3)
        );

        always @(*) begin
            middlesum = p1 + p2;
            lastsum = {p3, p0[7:4]} + {3'b000, middlesum};
            o__y = {lastsum, p0[3:0]};
        end

    end else begin : gen_8X8_M2

        // Vedic method from "Urdhva Triyakbhyam Sutra".
        //
        // (a7*2^7 + a6*2^6 + a5*2^5 + a4*2^4 + a3*2^3 + a2*2^2 + a1*2^1 + a0*2^0) *
        // (b7*2^7 + b6*2^6 + b5*2^5 + b4*2^4 + b3*2^3 + b2*2^2 + b1*2^1 + b0*2^0) =
        // a7*b7*2^14 + (a6*b7 + a7*b6)*2^13 + (a5*b7 + a6*b6 + a7*b5)*2^12 +
        // (a4*b7 + a5*b6 + a6*b5 + a7*b4)*2^11 + (a3*b7 + a4*b6 + a5*b5 + a6*b4 + a7*b3)*2^10 +
        // (a2*b7 + a3*b6 + a4*b5 + a5*b4 + a6*b3 + a7*b2)*2^9 + (a1*b7 + a2*b6 + a3*b5 + a4*b4 + a5*b3 + a6*b2 + a7*b1)*2^8 +
        // (a0*b7 + a1*b6 + a2*b5 + a3*b4 + a4*b3 + a5*b2 + a6*b1 + a7*b0)*2^7 + (a0*b6 + a1*b5 + a2*b4 + a3*b3 + a4*b2 + a5*b1 + a6*b0)*2^6 +
        // (a0*b5 + a1*b4 + a2*b3 + a3*b2 + a4*b1 + a5*b0)*2^5 + (a0*b4 + a1*b3 + a2*b2 + a3*b1 + a4*b0)*2^4 +
        // (a0*b3 + a1*b2 + a2*b1 + a3*b0)*2^3 + (a0*b2 + a1*b1 + a2*b0)*2^2 + (a0*b1 + a1*b0)*2^1 + a0*b0*2^0
        //
        // y15 y14 y13 y12 y11 y10 y9  y8  y7  y6  y5  y4  y3  y2  y1  y0
        // ==============================================================
        //                                                             p0 = a0 * b0
        //
        //                                                     c1  s1     = carry & sum of (p1,p2) : half adder
        //                                                                  where p1 = a0*b1, p2 = a1*b0
        //
        //                                                 c2  s2         = carry & sum of (p3,p4,p5) : full adder
        //                                                                  where p3 = a0*b2, p4 = a1*b1, p5 = a2*b0
        //
        //                                         t32 t31 t30            = 3 bits from p6 + p7 + p8 + p9 : table generated (LUT4)
        //                                                                  where p6 = a0*b3, p7 = a1*b2, p8 = a2*b1, p9 = a3*b0
        //
        //                                     t42 t41 t40                = 3 bits from p10 + p11 + p12 + p13 + p14 : toolchain default
        //                                                                  where p10=a0*b4, p11=a1*b3, p12=a2*b2, p13=a3*b1, p14=a4*b0
        //
        //                                 t52 t51 t50                    = 3 bits from p15 + p16 + p17 + p18 + p19 + p20 : toolchain default
        //                                                                    where p15=a0*b5, p16=a1*b4, p17=a2*b3,p18=a3*b2, p19=a4*b1, p20=a5*b0
        //
        //                             t62 t61 t60                        = 3 bits from p21 + p22 + p23 + p24 + p25 + p26 + p27 : toolchain default
        //                                                                    where p21=a0*b6, p22=a1*b5, p23=a2*b4, p24=a3*b3, p25=a4*b2, p26=a5*b1, p27=a6*b0
        //
        //                     t73 t72 t71 t70                            = 4 bits from p28 + p29 + p30 + p31 + p32 + p33 + p34 + p35 : toolchain default
        //                                                                    where p28=a0*b7, p29=a1*b6, p30=a2*b5, p31=a3*b4, p32=a4*b3, p33=a5*b2, p34=a6*b1, p35=a7*b0
        //
        //                     t82 t81 t80                                = 3 bits from p36 + p37 + p38 + p39 + p40 + p41 + p42 : toolchain default
        //                                                                    where p36=a1*b7, p37=a2*b6, p38=a3*b5, p39=a4*b4, p40=a5*b3, p41=a6*b2, p42=a7*b1
        //
        //                 t92 t91 t90                                    = 3 bits from p43 + p44 + p45 + p46 + p47 + p48 : toolchain default
        //                                                                    where p43=a2*b7, p44=a3*b6, p45=a4*b5, p46=a5*b4, p47=a6*b3, p48=a7*b2
        //
        //             ta2 ta1 ta0                                        = 3 bits from p49 + p50 + p51 + p52 + p53 : toolchain default
        //                                                                    where p49=a3*b7, p50=a4*b6, p51=a5*b5, p52=a6*b4, p53=a7*b3
        //
        //         tb2 tb1 tb0                                            = 3 bits from p54 + p55 + p56 + p57 : table generated (LUT4)
        //                                                                    where p54 = a4*b7, p55 = a5*b6, p56 = a6*b5, p57 = a7*b4
        //
        //         cc  sc                                                 = carry & sum of (p58,p59,p60) : full adder
        //                                                                  where p58 = a5*b7, p59 = a6*b6, p60 = a7*b5
        //
        //     cd  sd                                                     = carry & sum of (p61,p62) : half adder
        //                                                                  where p61 = a6*b7, p62 = a7*b6
        //
        //     p63                                                        = a7*b7
        // --------------------------------------------------------------
        // S16 S15 S14 S13 S11 S10 S9  S8  S7  S6  S5  S4  S3  S2         = carry & sum of (
        //                                                                  {p63,  sd,  sc, tb0, ta0, t90, t80, t70, t60, t50, t40, t30, s2},
        //                                                                  { cd,  cc, tb1, ta1, t91, t81, t71, t61, t51, t41, t31,  c2, c1},
        //                                                                  {  0, tb2, ta2, t92, t82, t72, t62, t52, t42, t32,   0,   0,  0},
        //                                                                  {  0,   0,   0,   0, t73,   0,   0,   0,   0,   0,   0,   0,  0}
        //                                                                  ) : toolchain default
        //
        // NOTE: I initially used tasks instead of the macros below, but I discovered that some
        // versions of Icarus Verilog return wrong results with tasks, while macros just work.

        `define half_adder(a,b,sum,carry)    {carry, sum} = a + b
        `define full_adder(cin,a,b,sum,cout) {cout, sum} = a + b + cin

        reg p0, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, p12, p13, p14, p15, p16, p17, p18, p19;
        reg p20, p21, p22, p23, p24, p25, p26, p27, p28, p29, p30, p31, p32, p33, p34, p35, p36, p37;
        reg p38, p39, p40, p41, p42, p43, p44, p45, p46, p47, p48, p49, p50, p51, p52, p53, p54, p55;
        reg p56, p57, p58, p59, p60, p61, p62, p63, s1, c1, s2, c2, sc, cc, sd, cd;
        reg [2:0] t3, t4, t5, t6, t8, t9, ta, tb;
        reg [3:0] t7;
        reg [13:0] lastsum;

        always @(*) begin
            // 2^0
            p0 = i__a[0] * i__b[0];

            // 2^1
            p1 = i__a[0] * i__b[1];
            p2 = i__a[1] * i__b[0];
            `half_adder(p1, p2, s1, c1);

            // 2^2
            p3 = i__a[0] * i__b[2];
            p4 = i__a[1] * i__b[1];
            p5 = i__a[2] * i__b[0];
            `full_adder(p3, p4, p5, s2, c2);

            // 2^3
            p6 = i__a[0] * i__b[3];
            p7 = i__a[1] * i__b[2];
            p8 = i__a[2] * i__b[1];
            p9 = i__a[3] * i__b[0];
            case ({p9,p8,p7,p6})
                4'b0000 : t3 = 3'd0;
                4'b0001 : t3 = 3'd1;
                4'b0010 : t3 = 3'd1;
                4'b0011 : t3 = 3'd2;
                4'b0100 : t3 = 3'd1;
                4'b0101 : t3 = 3'd2;
                4'b0110 : t3 = 3'd2;
                4'b0111 : t3 = 3'd3;
                4'b1000 : t3 = 3'd1;
                4'b1001 : t3 = 3'd2;
                4'b1010 : t3 = 3'd2;
                4'b1011 : t3 = 3'd3;
                4'b1100 : t3 = 3'd2;
                4'b1101 : t3 = 3'd3;
                4'b1110 : t3 = 3'd3;
                4'b1111 : t3 = 3'd4;
            endcase

            // Following sums are perfectly working but Verilator generates multiple %Warning-WIDTH on
            // them. I prefer to disable such warning, rather than writing verbose and less clear code!
            // verilator lint_off WIDTH

            // 2^4
            p10 = i__a[0] * i__b[4];
            p11 = i__a[1] * i__b[3];
            p12 = i__a[2] * i__b[2];
            p13 = i__a[3] * i__b[1];
            p14 = i__a[4] * i__b[0];
            t4 = p10 + p11 + p12 + p13 + p14;

            // 2^5
            p15 = i__a[0] * i__b[5];
            p16 = i__a[1] * i__b[4];
            p17 = i__a[2] * i__b[3];
            p18 = i__a[3] * i__b[2];
            p19 = i__a[4] * i__b[1];
            p20 = i__a[5] * i__b[0];
            t5 = p15 + p16 + p17 + p18 + p19 + p20;

            // 2^6
            p21 = i__a[0] * i__b[6];
            p22 = i__a[1] * i__b[5];
            p23 = i__a[2] * i__b[4];
            p24 = i__a[3] * i__b[3];
            p25 = i__a[4] * i__b[2];
            p26 = i__a[5] * i__b[1];
            p27 = i__a[6] * i__b[0];
            t6 = p21 + p22 + p23 + p24 + p25 + p26 + p27;

            // 2^7
            p28 = i__a[0] * i__b[7];
            p29 = i__a[1] * i__b[6];
            p30 = i__a[2] * i__b[5];
            p31 = i__a[3] * i__b[4];
            p32 = i__a[4] * i__b[3];
            p33 = i__a[5] * i__b[2];
            p34 = i__a[6] * i__b[1];
            p35 = i__a[7] * i__b[0];
            t7 = p28 + p29 + p30 + p31 + p32 + p33 + p34 + p35;

            // 2^8
            p36 = i__a[1] * i__b[7];
            p37 = i__a[2] * i__b[6];
            p38 = i__a[3] * i__b[5];
            p39 = i__a[4] * i__b[4];
            p40 = i__a[5] * i__b[3];
            p41 = i__a[6] * i__b[2];
            p42 = i__a[7] * i__b[1];
            t8 = p36 + p37 + p38 + p39 + p40 + p41 + p42;

            // 2^9
            p43 = i__a[2] * i__b[7];
            p44 = i__a[3] * i__b[6];
            p45 = i__a[4] * i__b[5];
            p46 = i__a[5] * i__b[4];
            p47 = i__a[6] * i__b[3];
            p48 = i__a[7] * i__b[2];
            t9 = p43 + p44 + p45 + p46 + p47 + p48;

            // 2^10
            p49 = i__a[3] * i__b[7];
            p50 = i__a[4] * i__b[6];
            p51 = i__a[5] * i__b[5];
            p52 = i__a[6] * i__b[4];
            p53 = i__a[7] * i__b[3];
            ta = p49 + p50 + p51 + p52 + p53;

            // Reenable the Verilator %Warning-WIDTH previously disabled.
            // verilator lint_on WIDTH

            // 2^11
            p54 = i__a[4] * i__b[7];
            p55 = i__a[5] * i__b[6];
            p56 = i__a[6] * i__b[5];
            p57 = i__a[7] * i__b[4];
            case ({p54,p55,p56,p57})
                4'b0000 : tb = 3'd0;
                4'b0001 : tb = 3'd1;
                4'b0010 : tb = 3'd1;
                4'b0011 : tb = 3'd2;
                4'b0100 : tb = 3'd1;
                4'b0101 : tb = 3'd2;
                4'b0110 : tb = 3'd2;
                4'b0111 : tb = 3'd3;
                4'b1000 : tb = 3'd1;
                4'b1001 : tb = 3'd2;
                4'b1010 : tb = 3'd2;
                4'b1011 : tb = 3'd3;
                4'b1100 : tb = 3'd2;
                4'b1101 : tb = 3'd3;
                4'b1110 : tb = 3'd3;
                4'b1111 : tb = 3'd4;
            endcase

            // 2^12
            p58 = i__a[5] * i__b[7];
            p59 = i__a[6] * i__b[6];
            p60 = i__a[7] * i__b[5];
            `full_adder(p58, p59, p60, sc, cc);

            // 2^13
            p61 = i__a[6] * i__b[7];
            p62 = i__a[7] * i__b[6];
            `half_adder(p61, p62, sd, cd);

            // 2^14
            p63 = i__a[7] * i__b[7];

            // Ultimate sum and output.
            lastsum = { p63,    sd,    sc, tb[0], ta[0], t9[0], t8[0], t7[0], t6[0], t5[0], t4[0], t3[0],   s2} +
                      {  cd,    cc, tb[1], ta[1], t9[1], t8[1], t7[1], t6[1], t5[1], t4[1], t3[1],    c2,   c1} +
                      {1'b0, tb[2], ta[2], t9[2], t8[2], t7[2], t6[2], t5[2], t4[2], t3[2],  1'b0,  1'b0, 1'b0} +
                      {1'b0,  1'b0,  1'b0,  1'b0, t7[3],  1'b0,  1'b0,  1'b0,  1'b0,  1'b0,  1'b0,  1'b0, 1'b0};
            o__y = {lastsum, s1, p0};
        end

        `undef half_adder
        `undef full_adder

    end
    //endgenerate

endmodule
