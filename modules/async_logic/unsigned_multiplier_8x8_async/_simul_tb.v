
// (C) 2021-2025 Flavio Cappelli
// Released under the MIT License.
//
// See https://www.fpgatutorial.com/how-to-write-a-basic-verilog-testbench/

`default_nettype none                       // Do not allow undeclared signals.
`define DUMPSTR(x) `"x.vcd`"                // Required for GTKWave.
`define SIMULATION_TIME 65540us             // Simulation time (in s, ms, us or ns).
`include "timescale.vh"                     // Timescale defined globally.

module testbench();
    initial begin
        $dumpfile(`DUMPSTR(`VCD_OUTPUT));   // Required for GTKWave.
        $dumpvars(0, testbench);            // Dump all variables in all instantiated modules.
        $printtimescale(testbench);         // Display the time scale and resolution (see above).
        #`SIMULATION_TIME                   // Wait until the simulation time is elapsed.
        $timeformat(-6, 2, "us", 1);        // Display the time elapsed in "us".
        $display("Simulation terminated after %t", $realtime);
        $finish;                            // Terminate the simulation.
    end


    // ---- Clock simulation ----

    // NONE REQUIRED.


    // ---- Unsigned asynchronous 8x8 multiplier simulation ----

    reg   [7:0] a;                          // Operands.
    reg   [7:0] b;
    wire [15:0] y;                          // Result.

    _simul_top TOP (                        // Top module.
        .a(a),
        .b(b),
        .y(y)
    );

    integer i, j, res;
    initial begin                           // Generate exhaustive test data: YOU HAVE TO USE THE
        #0.5us                              // HORIZONTAL SCROLL IN GTKWAVE TO SEE ALL TEST DATA.
        for (i = 0; i < 256; ++i) begin
            for (j = 0; j < 256; ++j) begin
                a = i[7:0];
                b = j[7:0];
                #1.0us
                res = i*j;
                if (y != res[15:0])
                    $fatal(1, "ERROR: wrong result!");
            end
        end
        $display("Test successfully completed!");
    end

endmodule
