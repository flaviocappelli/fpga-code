
Simulation of module "unsigned_multiplier_16x16_async"
------------------------------------------------------

The exhaustive simulation of this module with Icarus Verilog
is not feasible, because it would generate a VERY HUGE data file
(several hundreds of GB, especially for post-synthesis simulation)
and would take a very long time (more than sixteen hours on a Core
i7). This is why the "_simul_tb.v" file is not provided here.

We use another approach: Verilator can convert a Verilog module
hierarchy into a C++ class. By instantiating this class into an
appropriate C++ driver file (see _simul_tb.cxx) we can build an
executable capable of simulating the logic circuit much faster
than Icarus Verilog.

To run such simulation use my makefile with the argument "vsimto"
(behavioral simulation) or "vpssimto" (post-synthesis simulation).
Note that the latter takes much more time than the former.

NOTE: No graphical output is generated, just some console messages.
