
// (C) 2021-2025 Flavio Cappelli
// Released under the MIT License.
//
// See https://verilator.org/guide/latest/
//     https://projectf.io/posts/verilog-sim-verilator-sdl
//     https://zipcpu.com/blog/2017/06/21/looking-at-verilator.html
//
// NOTE: Verilator 4.222 or above is required.

#include <cstdlib>          // For EXIT_SUCCESS, EXIT_FAILURE
#include <iostream>         // For std::cout
#include <iomanip>          // For std::setw
#include <memory>           // For std::unique_ptr
#include "Vtop.h"           // Verilated model


int main(int argc, char **argv)
{
    // Construct a VerilatedContext to hold simulation time, etc.
    const std::unique_ptr<VerilatedContext> contextp{new VerilatedContext};

    // Pass program arguments to Verilated code (must be done before a model is created).
    contextp->commandArgs(argc, argv);

    // Create the Verilated model of the module under test (i.e. the top module). The
    // I/O interface of the Verilated model is what the testbench sees (highest level).
    const std::unique_ptr<Vtop> top{new Vtop{contextp.get(), "testbench"}};

    // Notify the start of the test.
    std::cout << "\nTesting module: unsigned_multiplier_16x16_async\n\n";
    std::cout.flush();

    // Test the module with all possible inputs and show a progress indicator.
    for (int a = 0; a < 65536; ++a)
    {
        if (a % 256 == 0) {
            std::cout << "testing [" << std::setw(5) << a << " .. " << std::setw(5) << a+255 << "] * [0 .. 65535]";
            std::cout.flush();
        }

        for (int b = 0; b < 65536; ++b)
        {
            top->a = a;
            top->b = b;
            top->eval();

            int y = top->y;
            if (y != a*b) {
                std::cout << "\nSorry, test failed: a = " << a << ", b = " << b << ", y = " << y << " (should be " << a*b << ")\n\n";
                std::cout.flush();

                // Model cleanup.
                top->final();

                // Call destructors and return failure.
                return EXIT_FAILURE;
            }
        }

        if (a % 256 == 255) {
            std::cout << " done\n";
            std::cout.flush();
        }
    }

    // Notify all tests are ok.
    std::cout << "\nAll ok\n\n";
    std::cout.flush();

    // Model cleanup.
    top->final();

    // Call destructors and return good completion status.
    return EXIT_SUCCESS;
}
