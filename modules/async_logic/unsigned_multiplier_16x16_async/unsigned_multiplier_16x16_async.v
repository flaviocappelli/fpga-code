
// (C) 2021-2025 Flavio Cappelli
// Released under the MIT License.
//
// Unsigned asynchronous 16x16 multiplier.
//
// NOTE: two implementations are provided:
//
//        * MUL16X16_METHOD = 0  -->  use tolchain default
//        * MUL16X16_METHOD = 1  -->  use four 8x8 multipliers
//
// The FPGA I am currently using (iCE40 LP8K) does not have
// a DSP core (or a dedicated hardware multiplier) so, it is
// interesting to see which method provides the fastest logic.
// These are the resources used by Yosys on the iCE40 FPGA:
//
//   MUL16X16_METHOD  MUL8X8_METHOD  MUL4X4_METHOD  MUL2X2_METHOD  Cells  SB_LUT4  SB_CARRY  EPD(*)
//   ----------------------------------------------------------------------------------------------
//         0             ignored        ignored        ignored      683      659      24     21.9ns
//         1                0           ignored        ignored      709      669      40     32.8ns
//         1                1              0           ignored      732      643      89     28.9ns
//         1                1              1              0         915      725     190     27.0ns
//         1                1              1              1         718      592     126     23.4ns
//         1                1              1              2         725      599     126     23.2ns
//         1                1              2           ignored      805      679     126     21.9ns
//         1                2           ignored        ignored      793      667     126     26.4ns
//
// (*) Estimated propagation delay of the multiplier
//     on the TinyFPGA BX (excluding the I/O cells).
//
// For the 16x16 multiplication, the implementation provided
// by Yosys seems the best.
//
// NOTE: these results may vary among different synthesis
//       tools, and even among different versions of the
//       same tool. Furthermore, the optimizations carried
//       out by the synthesis tool depend on the used FPGA
//       and the overall circuit, so it is recommended to
//       rerun such tests on the circuit under development.

`default_nettype none               // Do not allow undeclared signals.
`include "timescale.vh"             // Timescale defined globally.


module unsigned_multiplier_16x16_async #(
    parameter MUL16X16_METHOD = 0,  // Fastest method measured.
    parameter MUL8X8_METHOD = 0,
    parameter MUL4X4_METHOD = 0,
    parameter MUL2X2_METHOD = 0
) (
    input  wire [15:0] i__a,
    input  wire [15:0] i__b,
    output reg  [31:0] o__y
);

    //generate (optional in Verilog-2005)
    if (MUL16X16_METHOD == 0) begin : gen_16X16_M0

        // Toolchain implementation.
        always @(*) begin
            o__y = i__a * i__b;
        end

    end else begin : gen_16X16_M1

        // If we set: X1 = a15*2^7 + a14*2^6 + a13*2^5 + a12*2^4 + a11*2^3 + a10*2^2 + a9*2^1 + a8*2^0,
        //            X0 =  a7*2^7 +  a6*2^6 +  a5*2^5 +  a4*2^4 +  a3*2^3 +  a2*2^2 + a1*2^1 + a0*2^0,
        //            Y1 = b15*2^7 + b14*2^6 + b13*2^5 + b12*2^4 + b11*2^3 + b10*2^2 + b9*2^1 + b8*2^0,
        //            Y0 =  b7*2^7 +  b6*2^6 +  b5*2^5 +  b4*2^4 +  b3*2^3 +  b2*2^2 + b1*2^1 + b0*2^0,
        //
        // then we can write:
        //
        // A * B = (a15*2^15 + a14*2^14 + a13*2^13 + a12*2^12 + a11*2^11 + a10*2^10 + a9*2^9 + a8*2^8 +
        //           a7*2^7  +  a6*2^6  +  a5*2^5  +  a4*2^4  +  a3*2^3  +  a2*2^2  + a1*2^1 + a0*2^0) *
        //         (b15*2^15 + b14*2^14 + b13*2^13 + b12*2^12 + b11*2^11 + b10*2^10 + b9*2^9 + b8*2^8 +
        //           b7*2^7  +  b6*2^6  +  b5*2^5  +  b4*2^4  +  b3*2^3  +  b2*2^2  + b1*2^1 + b0*2^0) =
        //         (X1*2^8 + X0*2^0) * (Y1*2^8 + Y0*2^0) =
        //         X1*Y1*2^16 + (X0*Y1 + X1*Y0)*2^8 + X0*Y0*2^0
        //
        // So, the 16x16 multiplication can be decomposed in four 8x8 multipliers, plus some adders (and
        // some shifts, but those are all trivials):
        //
        //                 ---------------- X0*Y0  (p0)  Note that each p term will have a maximum value
        //         ----------------         X0*Y1  (p1)  of 65025 (i.e. the result of 255*255). We will
        //         ----------------         X1*Y0  (p2)  proceed adding the middle terms p1 and p2 first:
        // ----------------                 X1*Y1  (p3)  such sum will be at most 130050 (65025 + 65025),
        //                                               so 17 bits are required for it. Then we will add
        // this sum to {p3, p0[15:8]} (max value 16646654) so a 24-bit word is required for the last sum
        // (130050 + 16646654 = 16776704).

        wire [15:0] p0, p1, p2, p3;     // Partial products.
        reg  [16:0] middlesum;          // 17 bits required (see above).
        reg  [23:0] lastsum;            // 24 bits required (see above).

        unsigned_multiplier_8x8_async #(
            .MUL8X8_METHOD(MUL8X8_METHOD),
            .MUL4X4_METHOD(MUL4X4_METHOD),
            .MUL2X2_METHOD(MUL2X2_METHOD)
        ) MUL8x8_0 (
            .i__a(i__a[7:0]),           // X0.
            .i__b(i__b[7:0]),           // Y0.
            .o__y(p0)
        );

        unsigned_multiplier_8x8_async #(
            .MUL8X8_METHOD(MUL8X8_METHOD),
            .MUL4X4_METHOD(MUL4X4_METHOD),
            .MUL2X2_METHOD(MUL2X2_METHOD)
        ) MUL8x8_1 (
            .i__a(i__a[7:0]),           // X0.
            .i__b(i__b[15:8]),          // Y1.
            .o__y(p1)
        );

        unsigned_multiplier_8x8_async #(
            .MUL8X8_METHOD(MUL8X8_METHOD),
            .MUL4X4_METHOD(MUL4X4_METHOD),
            .MUL2X2_METHOD(MUL2X2_METHOD)
        ) MUL8x8_2 (
            .i__a(i__a[15:8]),          // X1.
            .i__b(i__b[7:0]),           // Y0.
            .o__y(p2)
        );

        unsigned_multiplier_8x8_async #(
            .MUL8X8_METHOD(MUL8X8_METHOD),
            .MUL4X4_METHOD(MUL4X4_METHOD),
            .MUL2X2_METHOD(MUL2X2_METHOD)
        ) MUL8x8_3 (
            .i__a(i__a[15:8]),          // X1.
            .i__b(i__b[15:8]),          // Y1.
            .o__y(p3)
        );

        always @(*) begin
            middlesum = p1 + p2;
            lastsum = {p3, p0[15:8]} + {7'b0000000, middlesum};
            o__y = {lastsum, p0[7:0]};
        end

    end
    //endgenerate

endmodule
