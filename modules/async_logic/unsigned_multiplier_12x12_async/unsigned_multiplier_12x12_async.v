
// (C) 2021-2025 Flavio Cappelli
// Released under the MIT License.
//
// Unsigned asynchronous 12x12 multiplier.
//
// NOTE: three implementations are provided:
//
//        * MUL12X12_METHOD = 0  -->  use tolchain default
//        * MUL12X12_METHOD = 1  -->  use four 6x6 multipliers
//        * MUL12X12_METHOD = 2  -->  use nine 4x4 multipliers
//
// The FPGA I am currently using (iCE40 LP8K) does not have
// a DSP core (or a dedicated hardware multiplier) so, it is
// interesting to see which method provides the fastest logic.
// These are the resources used by Yosys on the iCE40 FPGA:
//
//   MUL12X12_METHOD  MUL6X6_METHOD  MUL3X3_METHOD  MUL4X4_METHOD  MUL2X2_METHOD  Cells  SB_LUT4  SB_CARRY  EPD(*)
//   -------------------------------------------------------------------------------------------------------------
//         0             ignored        ignored        ignored        ignored      379      362      17     15.3ns
//         1                0           ignored        ignored        ignored      389      361      28     27.3ns
//         1                1              0           ignored        ignored      423      360      63     24.3ns
//         1                1              1           ignored        ignored      413      321      92     17.6ns
//         1                1              2           ignored        ignored     1143     1099      44     24.9ns
//         1                2           ignored        ignored        ignored      427      355      72     20.4ns
//         2             ignored        ignored           0           ignored      375      325      50     21.9ns
//         2             ignored        ignored           1              0         490      393      97     21.6ns
//         2             ignored        ignored           1              1         385      324      61     18.3ns
//         2             ignored        ignored           1              2         385      324      61     16.9ns
//         2             ignored        ignored           2           ignored      436      375      61     20.5ns
//
// (*) Estimated propagation delay of the multiplier
//     on the TinyFPGA BX (excluding the I/O cells).
//
// For the 12x12 multiplication, the implementation provided
// by Yosys seems the best.
//
// NOTE: these results may vary among different synthesis
//       tools, and even among different versions of the
//       same tool. Furthermore, the optimizations carried
//       out by the synthesis tool depend on the used FPGA
//       and the overall circuit, so it is recommended to
//       rerun such tests on the circuit under development.

`default_nettype none               // Do not allow undeclared signals.
`include "timescale.vh"             // Timescale defined globally.


module unsigned_multiplier_12x12_async #(
    parameter MUL12X12_METHOD = 0,  // Fastest method measured.
    parameter MUL6X6_METHOD = 0,
    parameter MUL4X4_METHOD = 0,
    parameter MUL3X3_METHOD = 0,
    parameter MUL2X2_METHOD = 0
) (
    input  wire [11:0] i__a,
    input  wire [11:0] i__b,
    output reg  [23:0] o__y
);

    //generate (optional in Verilog-2005)
    if (MUL12X12_METHOD == 0) begin : gen_12X12_M0

        // Toolchain implementation.
        always @(*) begin
            o__y = i__a * i__b;
        end

    end else if (MUL12X12_METHOD == 1) begin : gen_12X12_M1

        // If we set: X1 = a11*2^5 + a10*2^4 + a9*2^3 + a8*2^2 + a7*2^1 + a6*2^0,
        //            X0 =  a5*2^5 +  a4*2^4 + a3*2^3 + a2*2^2 + a1*2^1 + a0*2^0,
        //            Y1 = b11*2^5 + b10*2^4 + b9*2^3 + b8*2^2 + b7*2^1 + b6*2^0,
        //            Y0 =  b5*2^5 +  b4*2^4 + b3*2^3 + b2*2^2 + b1*2^1 + b0*2^0,
        //
        // then we can write:
        //
        // A * B = (a11*2^11 + a10*2^10 + a9*2^9 + a8*2^8 + a7*2^7 + a6*2^6 +
        //           a5*2^5  +  a4*2^4  + a3*2^3 + a2*2^2 + a1*2^1 + a0*2^0) *
        //         (b11*2^11 + b10*2^10 + b9*2^9 + b8*2^8 + b7*2^7 + b6*2^6  +
        //           b5*2^5  +  b4*2^4  + b3*2^3 + b2*2^2 + b1*2^1 + b0*2^0) =
        //         (X1*2^6 + X0*2^0) * (Y1*2^6 + Y0*2^0) =
        //         X1*Y1*2^12 + (X0*Y1 + X1*Y0)*2^6 + X0*Y0*2^0
        //
        // So, the 12x12 multiplication can be decomposed in four 6x6 multipliers, plus
        // some adders (and some shifts, but those are all trivials):
        //
        //             ------------ X0*Y0  (p0)  Note that each p term will have a maximum
        //       ------------       X0*Y1  (p1)  value of 3969 (i.e. the result of 63*63).
        //       ------------       X1*Y0  (p2)  We will proceed adding the middle terms p1
        // ------------             X1*Y1  (p3)  and p2 first: such sum will be at most 7938
        //                                       (3069 + 3069), so 13 bits are required for
        // it. Then we will add this sum to {p3, p0[11:6]} (max value 254078) so a 18-bit
        // word is required for the last sum (7938 + 254078 = 262016).

        wire [11:0] p0, p1, p2, p3;     // Partial products.
        reg  [12:0] middlesum;          // 13 bits required (see above).
        reg  [17:0] lastsum;            // 18 bits required (see above).

        unsigned_multiplier_6x6_async #(
            .MUL6X6_METHOD(MUL6X6_METHOD),
            .MUL3X3_METHOD(MUL3X3_METHOD)
        ) MUL6x6_0 (
            .i__a(i__a[5:0]),           // X0.
            .i__b(i__b[5:0]),           // Y0.
            .o__y(p0)
        );

        unsigned_multiplier_6x6_async #(
            .MUL6X6_METHOD(MUL6X6_METHOD),
            .MUL3X3_METHOD(MUL3X3_METHOD)
        ) MUL6x6_1 (
            .i__a(i__a[5:0]),           // X0.
            .i__b(i__b[11:6]),          // Y1.
            .o__y(p1)
        );

        unsigned_multiplier_6x6_async #(
            .MUL6X6_METHOD(MUL6X6_METHOD),
            .MUL3X3_METHOD(MUL3X3_METHOD)
        ) MUL6x6_2 (
            .i__a(i__a[11:6]),          // X1.
            .i__b(i__b[5:0]),           // Y0.
            .o__y(p2)
        );

        unsigned_multiplier_6x6_async #(
            .MUL6X6_METHOD(MUL6X6_METHOD),
            .MUL3X3_METHOD(MUL3X3_METHOD)
        ) MUL6x6_3 (
            .i__a(i__a[11:6]),          // X1.
            .i__b(i__b[11:6]),          // Y1.
            .o__y(p3)
        );

        always @(*) begin
            middlesum = p1 + p2;
            lastsum = {p3, p0[11:6]} + {5'b00000, middlesum};
            o__y = {lastsum, p0[5:0]};
        end

    end else begin : gen_12X12_M2

        // If we set: X2 = a11*2^3 + a10*2^2 + a9*2^1 + a8*2^0,
        //            X1 =  a7*2^3 +  a6*2^2 + a5*2^1 + a4*2^0,
        //            X0 =  a3*2^3 +  a2*2^2 + a1*2^1 + a0*2^0,
        //            Y2 = b11*2^3 + b10*2^2 + b9*2^1 + b8*2^0,
        //            Y1 =  b7*2^3 +  b6*2^2 + b5*2^1 + b4*2^0,
        //            Y0 =  b3*2^3 +  b2*2^2 + b1*2^1 + b0*2^0,
        //
        // then we can write:
        //
        // A * B = (a11*2^11 + a10*2^10 + a9*2^9 + a8*2^8 + a7*2^7 + a6*2^6 +
        //           a5*2^5  +  a4*2^4  + a3*2^3 + a2*2^2 + a1*2^1 + a0*2^0) *
        //         (b11*2^11 + b10*2^10 + b9*2^9 + b8*2^8 + b7*2^7 + b6*2^6  +
        //           b5*2^5  +  b4*2^4  + b3*2^3 + b2*2^2 + b1*2^1 + b0*2^0) =
        //         (X2*2^8 + X1*2^4 + X0*2^0) * (Y2*2^8 + Y1*2^4 + Y0*2^0) =
        //         X2*Y2*2^16 + (X1*Y2 + X2*Y1)*2^12 + (X0*Y2 + X1*Y1 + X2*Y0)*2^8 +
        //         (X0*Y1 + X1*Y0)*2^4 + X0*Y0
        //
        // So, the 12x12 multiplication can be also decomposed in nine 4x4 multipliers,
        // plus some adders (and some shifts, but those are all trivials):
        //
        //                 -------- X0*Y0  (p0)  Note that each p term can have a maximum value
        //             --------     X0*Y1  (p1)  value of 225 (i.e. the result of 15*15). We will
        //             --------     X1*Y0  (p2)  proceed first grouping the p terms in this way:
        //         --------         X0*Y2  (p3)
        //         --------         X1*Y1  (p4)               --------     {p3}
        //         --------         X2*Y0  (p5)               --------     {p5}
        //     --------             X1*Y2  (p6)           ---------------- {p6,p1}
        //     --------             X2*Y1  (p7)           ---------------- {p7,p2}
        // --------                 X2*Y2  (p8)       -------------------- {p8,p4,p0[7:4]}
        //
        // Then we will add the terms p3 + p5: the result will be at most 450 (225 + 225), so 9 bits
        // are required for this sum. Then we will add the terms {p6,p1} + {p7,p2}: the result will
        // be at most 115650 (57825 + 57825), so 17 bits are required for this sum. Then we will add
        // the previous sums (with p3+p5 left shifted of four bits) to {p8,p4,p0[7:4]}: the result
        // will be at most 1048064 (450*16 + 115650 + 925214), so 20 bits are required for this sum.

        wire [7:0] p0, p1, p2, p3, p4, p5, p6, p7, p8;  // Partial products.
        reg  [8:0] firstsum;                            // 9 bits required (see above).
        reg  [16:0] secondsum;                          // 17 bits required (see above).
        reg  [19:0] lastsum;                            // 20 bits required (see above).

        unsigned_multiplier_4x4_async #(
            .MUL4X4_METHOD(MUL4X4_METHOD),
            .MUL2X2_METHOD(MUL2X2_METHOD)
        ) MUL4x4_0 (
            .i__a(i__a[3:0]),                           // X0.
            .i__b(i__b[3:0]),                           // Y0.
            .o__y(p0)
        );

        unsigned_multiplier_4x4_async #(
            .MUL4X4_METHOD(MUL4X4_METHOD),
            .MUL2X2_METHOD(MUL2X2_METHOD)
        ) MUL4x4_1 (
            .i__a(i__a[3:0]),                           // X0.
            .i__b(i__b[7:4]),                           // Y1.
            .o__y(p1)
        );

        unsigned_multiplier_4x4_async #(
            .MUL4X4_METHOD(MUL4X4_METHOD),
            .MUL2X2_METHOD(MUL2X2_METHOD)
        ) MUL4x4_2 (
            .i__a(i__a[7:4]),                           // X1.
            .i__b(i__b[3:0]),                           // Y0.
            .o__y(p2)
        );

        unsigned_multiplier_4x4_async #(
            .MUL4X4_METHOD(MUL4X4_METHOD),
            .MUL2X2_METHOD(MUL2X2_METHOD)
        ) MUL4x4_3 (
            .i__a(i__a[3:0]),                           // X0.
            .i__b(i__b[11:8]),                          // Y2.
            .o__y(p3)
        );

        unsigned_multiplier_4x4_async #(
            .MUL4X4_METHOD(MUL4X4_METHOD),
            .MUL2X2_METHOD(MUL2X2_METHOD)
        ) MUL4x4_4 (
            .i__a(i__a[7:4]),                           // X1.
            .i__b(i__b[7:4]),                           // Y1.
            .o__y(p4)
        );

        unsigned_multiplier_4x4_async #(
            .MUL4X4_METHOD(MUL4X4_METHOD),
            .MUL2X2_METHOD(MUL2X2_METHOD)
        ) MUL4x4_5 (
            .i__a(i__a[11:8]),                          // X2.
            .i__b(i__b[3:0]),                           // Y0.
            .o__y(p5)
        );

        unsigned_multiplier_4x4_async #(
            .MUL4X4_METHOD(MUL4X4_METHOD),
            .MUL2X2_METHOD(MUL2X2_METHOD)
        ) MUL4x4_6 (
            .i__a(i__a[7:4]),                           // X1.
            .i__b(i__b[11:8]),                          // Y2.
            .o__y(p6)
        );

        unsigned_multiplier_4x4_async #(
            .MUL4X4_METHOD(MUL4X4_METHOD),
            .MUL2X2_METHOD(MUL2X2_METHOD)
        ) MUL4x4_7 (
            .i__a(i__a[11:8]),                          // X2.
            .i__b(i__b[7:4]),                           // Y1.
            .o__y(p7)
        );

        unsigned_multiplier_4x4_async #(
            .MUL4X4_METHOD(MUL4X4_METHOD),
            .MUL2X2_METHOD(MUL2X2_METHOD)
        ) MUL4x4_8 (
            .i__a(i__a[11:8]),                          // X2.
            .i__b(i__b[11:8]),                          // Y2.
            .o__y(p8)
        );

        always @(*) begin
            firstsum = p3 + p5;
            secondsum = {p6, p1} + {p7, p2};
            lastsum = {p8, p4, p0[7:4]} + {3'b000, secondsum} + {7'b0000000, firstsum, 4'b0000};
            o__y = {lastsum, p0[3:0]};
        end

    end
    //endgenerate

endmodule
