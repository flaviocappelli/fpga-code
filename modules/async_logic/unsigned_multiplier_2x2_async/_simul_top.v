
// (C) 2021-2025 Flavio Cappelli
// Released under the MIT License.
//
// This file is used to pass config parameters (if any) to the
// modules under test, ensuring the modules are configured the
// same way in both behavioral and post-synthesis simulation.
// It also makes "apio lint" happy because it guarantees that
// only one "top" module exists (Verilator requires it).

`default_nettype none           // Do not allow undeclared signals.
`include "timescale.vh"         // Timescale defined globally.

(* top *)                       // Mark this as top module, because
module _simul_top (             // sometimes Yosys doesn't detect it.
    input  wire [1:0] a,
    input  wire [1:0] b,
    output wire [3:0] y
);

    unsigned_multiplier_2x2_async #(
        .MUL2X2_METHOD(0)
    ) MUL2x2 (
        .i__a(a),
        .i__b(b),
        .o__y(y)
    );

endmodule
