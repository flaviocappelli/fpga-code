
// (C) 2021-2025 Flavio Cappelli
// Released under the MIT License.
//
// Unsigned asynchronous 2x2 multiplier.
//
// NOTE: three implementations are provided:
//
//        * MUL2X2_METHOD = 0  -->  use tolchain default
//        * MUL2X2_METHOD = 1  -->  use Vedic 2x2 logic
//        * MUL2X2_METHOD = 2  -->  use a table
//
// The FPGA I am currently using (iCE40 LP8K) does not have
// a DSP core (or a dedicated hardware multiplier) so, it is
// interesting to see which method provides the fastest logic.
// These are the resources used by Yosys on the iCE40 FPGA:
//
//   MUL2X2_METHOD  Cells  SB_LUT4  SB_CARRY  EPD(*)
//   -----------------------------------------------
//         0          8       6        2      6.9ns
//         1          4       4        0      3.4ns
//         2          4       4        0      3.4ns
//
// (*) Estimated propagation delay of the multiplier
//     on the TinyFPGA BX (excluding the I/O cells).
//
// For the 2x2 moltiplication, the implementation provided
// by Yosys for the iCE40 LP8K FPGA seem to be the worst!
//
// NOTE: these results may vary among different synthesis
//       tools, and even among different versions of the
//       same tool. Furthermore, the optimizations carried
//       out by the synthesis tool depend on the used FPGA
//       and the overall circuit, so it is recommended to
//       rerun such tests on the circuit under development.

`default_nettype none               // Do not allow undeclared signals.
`include "timescale.vh"             // Timescale defined globally.


module unsigned_multiplier_2x2_async #(
    parameter MUL2X2_METHOD = 2    // Fastest method measured.
) (
    input  wire [1:0] i__a,
    input  wire [1:0] i__b,
    output reg  [3:0] o__y
);

    //generate (optional in Verilog-2005)
    if (MUL2X2_METHOD == 0) begin : gen_2X2_M0

        // Toolchain implementation.
        always @(*) begin
            o__y = i__a * i__b;
        end

    end else if (MUL2X2_METHOD == 1) begin : gen_2X2_M1

        // Vedic method from "Urdhva Triyakbhyam Sutra".
        //
        // (a1*2^1 + a0*2^0) * (b1*2^1 + b0*2^0) =
        // a1*b1*2^2 + (a1*b0 + a0*b1)*2^1 + a0*b0*2^0
        //
        // y2  y1  y1  y0
        // ==============
        //             p0 = a0 * b0
        //
        //     c1, s1     = carry & sum of (p1,p2) : half adder
        //                  where p1 = a0*b1, p2 = a1*b0
        //
        //     p3         = a1*b1
        // --------------
        // c2, s2         = carry & sum of (p3,c1) : half adder
        //                  where p3 = a1*b1
        //
        // NOTE: I initially used a task instead of the macro below,
        // but I discovered that some versions of Icarus Verilog return
        // wrong results with tasks, while macros just work.

        `define half_adder(a,b,sum,carry) {carry, sum} = a + b

        reg p0, p1, p2, p3;
        reg s1, c1, s2, c2;

        always @(*) begin
            // 2^0
            p0 = i__a[0] * i__b[0];

            // 2^1
            p1 = i__a[0] * i__b[1];
            p2 = i__a[1] * i__b[0];
            `half_adder(p1, p2, s1, c1);

            // 2^2
            p3 = i__a[1] * i__b[1];

            // Ultimate sum and output.
            `half_adder(c1, p3, s2, c2);
            o__y = {c2, s2, s1, p0};
        end

        `undef half_adder

    end else begin : gen_2X2_M2

        // Use a table.
        always @(*) begin
            case({i__a, i__b})
                4'b00_00: o__y = 4'b0000;
                4'b00_01: o__y = 4'b0000;
                4'b00_10: o__y = 4'b0000;
                4'b00_11: o__y = 4'b0000;
                4'b01_00: o__y = 4'b0000;
                4'b01_01: o__y = 4'b0001;
                4'b01_10: o__y = 4'b0010;
                4'b01_11: o__y = 4'b0011;
                4'b10_00: o__y = 4'b0000;
                4'b10_01: o__y = 4'b0010;
                4'b10_10: o__y = 4'b0100;
                4'b10_11: o__y = 4'b0110;
                4'b11_00: o__y = 4'b0000;
                4'b11_01: o__y = 4'b0011;
                4'b11_10: o__y = 4'b0110;
                4'b11_11: o__y = 4'b1001;
            endcase
        end

    end
    //endgenerate

endmodule
