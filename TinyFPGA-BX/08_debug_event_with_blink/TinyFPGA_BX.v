
// (C) 2021-2025 Flavio Cappelli
// Released under the MIT License.
//
// Show how to use the user LED to debug a slow internal event.
//
// INPUT: none.
//
// OUPUT: LED.
//
// For the pin names on the TinyFPGA BX board see TinyFPGA-BX-pins.pcf

`default_nettype none       // Do not allow undeclared signals.
`include "timescale.vh"     // Timescale defined globally.

(* top *)                   // Mark this as top module, because
module TinyFPGA_BX (        // sometimes Yosys doesn't detect it.
    input  wire CLK_16MHz,  // 16MHz on-board clock.
    output wire LED,        // User/boot LED next to power LED.
    output wire USBPU       // USB pull-up resistor.
);
    // USB not used (drive USB pull-up resistor to '0'
    // to avoid the USB device enumeration by the OS).
    assign USBPU = 0;


    ////////////////////////////////////////
    // Create a simple active high reset. //
    ////////////////////////////////////////

    wire rst_master;

    fpga_reset_generator RESET_GEN (
        .i__clk(CLK_16MHz),
        .i__clk_en(1'b1),
        .o__gen_rst(rst_master)
    );


    ///////////////////////////////////////////////////////////////
    // Generates a slow periodic signal dividing the 16MHz clock //
    ///////////////////////////////////////////////////////////////

    `ifdef SIMULATION
      `define SIGNAL_FREQ_HZ    500     // Simulation.
    `else
      `define SIGNAL_FREQ_HZ    0.5     // Build.
    `endif

    localparam MAXCOUNT = $rtoi(16000000/(`SIGNAL_FREQ_HZ*2)-1 + 0.5);
    localparam CNTSIZE  = $clog2(MAXCOUNT + 1);

    reg [CNTSIZE-1:0] counter;
    reg periodic_signal;

    always @(posedge CLK_16MHz, posedge rst_master) begin
        if (rst_master) begin
            periodic_signal <= 0;
            counter <= 0;
        end else begin
            if (counter == MAXCOUNT[CNTSIZE-1:0])
                begin
                    counter <= 0;
                    periodic_signal <= ~periodic_signal;
                end
            else
                counter <= counter + 1;
        end
    end


    ///////////////////////////////////////////////////////////////////////////
    // Uses the LED to show the positive edges of the above periodic signal: //
    // first generates a short one-clock pulse at each positive edge of the  //
    // signal, then uses it to trigger a monostable that drives the LED.     //
    ///////////////////////////////////////////////////////////////////////////

    `ifdef SIMULATION
      `define BLINK_TIME_S      0.00010     // Simulation.
    `else
      `define BLINK_TIME_S      0.10        // Build
    `endif

    wire posedge_detected;

    slow_activehigh_signal_posedgedetector PEDETECT (
        .i__clk(CLK_16MHz),
        .i__rst(rst_master),
        .i__sigin(periodic_signal),
        .o__posedge(posedge_detected)
    );

    activehigh_monostable_highlevel_triggered #(
        .PULSE_CLOCK_CYCLES($rtoi(16000000*`BLINK_TIME_S-1 + 0.5))
    ) BLINKGEN (
        .i__clk(CLK_16MHz),
        .i__rst(rst_master),
        .i__sigin(posedge_detected),
        .o__pulse(LED)
    );

endmodule
