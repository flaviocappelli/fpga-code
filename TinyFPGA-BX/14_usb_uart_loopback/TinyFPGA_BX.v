
// (C) 2021-2025 Flavio Cappelli
// Released under the MIT License.
//
// Creates a full speed (12 Mbit/s) CDC-ACM USB communication device
// and establishes a data loopback. The CDC-ACM class allows the USB
// device to appear as a simple virtual serial device; the loopback
// (which sends back all the data received) allows us to verify that
// the TX/RX logic is fully functional. Also, the user led is set to
// ON when some data is received (and retransmitted), OFF otherwise.
//
// Note that Windows 10 provides a built-in driver (Usbser.sys) and
// USB CDC devices are automatically recognized as virtual COM ports.
// MacOS/Linux also provide built-in drivers for USB CDC ACM devices.
// On MacOS, the virtual COM gets a name like /dev/cu.usbmodem14601,
// while on Linux, it gets a name like /dev/ttyACM0. Linux requires
// that the user account belongs to the "dialout" (or "uucp") group
// (it depends on the distro) to grant permissions for COM access.
//
// INPUT/OUTPUT: USB communication device class. See:
//               https://www.usbmadesimple.co.uk/index.html
//
// For the pin names on the TinyFPGA BX board see TinyFPGA-BX-pins.pcf

//`define DEBUG_USB_CORE    // Uncommen to enable debug of usb_uart_core module.
//`define DEBUG_USB_CTRL    // Uncommen to enable debug of USB control lines.

`default_nettype none       // Do not allow undeclared signals.
`include "timescale.vh"     // Timescale defined globally.

(* top *)                   // Mark this as top module, because
module TinyFPGA_BX (        // sometimes Yosys doesn't detect it.
    input  wire CLK_16MHz,  // 16MHz on-board clock.

    `ifdef DEBUG_USB_CORE
    output wire PIN_1,      // debug[0].
    output wire PIN_2,      // debug[1].
    output wire PIN_3,      // debug[2].
    output wire PIN_4,      // debug[3].
    output wire PIN_5,      // debug[4].
    output wire PIN_6,      // debug[5].
    output wire PIN_7,      // debug[6].
    output wire PIN_8,      // debug[7].
    output wire PIN_9,      // debug[8].
    output wire PIN_10,     // debug[9].
    output wire PIN_11,     // debug[10].
    output wire PIN_12,     // debug[11].
    `endif

    `ifdef DEBUG_USB_CTRL
    output wire PIN_19,     // uart_out_ready.
    output wire PIN_20,     // uart_out_valid.
    output wire PIN_21,     // uart_in_ready.
    output wire PIN_22,     // uart_in_valid.
    output wire PIN_23,     // rst_master.
    output wire PIN_24,     // clk_48MHz.
    `endif

    output wire LED,        // User/boot LED next to power LED.
    inout  wire USBP,       // USB+
    inout  wire USBN,       // USB-
    output wire USBPU       // USB pull-up resistor.
);

    // USB used (drive USB pull-up resistor to '1' to allow
    // the USB device detection and enumeration by the OS).
    assign USBPU = 1'b1;


    /////////////////////////////////////////////////////
    // Generate a 192MHz clock using the internal PLL. //
    /////////////////////////////////////////////////////

    wire clk_192MHz, pll_locked;

    ice40_pll_192MHz_from_16MHz PLL192 (
        .i__clk_in (CLK_16MHz),
        .o__clk_out(clk_192MHz),
        .o__async_locked(pll_locked)
    );


    //////////////////////////////////////
    // Divide the clock by 4 to obtain  //
    // the 48MHz clock required by USB. //
    //////////////////////////////////////

    wire clk_96MHz, clk_48MHz;
    reg [1:0] prescaler = 0;

    always @(posedge clk_192MHz) begin
        prescaler <= prescaler + 1'b1;
    end

    assign clk_96MHz = prescaler[0];
    assign clk_48MHz = prescaler[1];


    ////////////////////////////////////////
    // Create a simple active high reset. //
    ////////////////////////////////////////

    wire rst_master;

    fpga_reset_generator #(
        .RESET_COUNTER_WIDTH(4)
    ) RESET_GEN (
        .i__clk(clk_48MHz),
        .i__clk_en(pll_locked),
        .o__gen_rst(rst_master)
    );


    //////////////////////////////////////
    // Instanciate the root USB module. //
    //////////////////////////////////////

    wire [7:0] uart_in_data;
    wire [7:0] uart_out_data;
    wire uart_in_valid, uart_in_ready;
    wire uart_out_valid, uart_out_ready;
    wire usb_p_tx, usb_n_tx, usb_p_rx, usb_n_rx, usb_tx_en;
    wire usb_host_rst;

    `ifdef DEBUG_USB_CORE
    wire [11:0] debug;
    `endif

    // verilator lint_off PINMISSING
    usb_uart_core USB_UART (
        .clk_48mhz(clk_48MHz),
        .reset(rst_master | usb_host_rst),      // NOTE: USB host reset required, see below!

        // Only for debugging.
        `ifdef DEBUG_USB_CORE
        .debug(debug),
        `endif

        // USB I/O, must be properly connected to the FPGA I/O, see below.
        .usb_p_tx(usb_p_tx),
        .usb_n_tx(usb_n_tx),
        .usb_p_rx(usb_p_rx),
        .usb_n_rx(usb_n_rx),
        .usb_tx_en(usb_tx_en),

        // Uart pipeline in (into this module -> out of the device -> into the host).
        .uart_in_data(uart_in_data),
        .uart_in_valid(uart_in_valid),
        .uart_in_ready(uart_in_ready),

        // Uart pipeline out (out of the host -> into the device -> out of this module).
        .uart_out_data(uart_out_data),
        .uart_out_valid(uart_out_valid),
        .uart_out_ready(uart_out_ready)
    );
    // verilator lint_on PINMISSING

    // Detects the USB "reset condition" from the host. This is needed after an
    // un-plug/plug cycle of the USB cable, when the TinyFPGA BX is not powered
    // by the host (i.e. powered from an USB HUB, or through the Vin power line).
    // Otherwise, after an un-plug/plug cycle, the board will not be recognized.
    // See https://github.com/davidthings/tinyfpga_bx_usbserial/issues/19
    usb_reset_det USB_RESET (
        .clk_48mhz(clk_48MHz),
        .reset_in(rst_master),
        .usb_p_rx(usb_p_rx),
        .usb_n_rx(usb_n_rx),
        .host_reset(usb_host_rst)
    );


    ///////////////////////////////////////
    // Simple USB application: loopback. //
    ///////////////////////////////////////

    // NOTE: The USB full-speed protocol caps data throughput to 1.5MB/s.
    // See https://github.com/davidthings/tinyfpga_bx_usbserial/issues/16
    //     https://github.com/davidthings/tinyfpga_bx_usbserial/issues/12

    assign uart_in_data = uart_out_data;
    assign uart_in_valid = uart_out_valid;
    assign uart_out_ready = uart_in_ready;


    /////////////////////
    // Assign USB I/O. //
    /////////////////////

    wire usb_p_in;
    wire usb_n_in;

    assign usb_p_rx = usb_tx_en ? 1'b1 : usb_p_in;      // RX lines kept constant during transmission
    assign usb_n_rx = usb_tx_en ? 1'b0 : usb_n_in;      // (otherwise TX data would have been returned).

    ice40_pin_tristateoutput_simpleinput IOBUF_USBP (
        .p__ice40_pin(USBP),
        .o__input_signal(usb_p_in),
        .i__output_signal(usb_p_tx),
        .i__output_en(usb_tx_en)
    );

    ice40_pin_tristateoutput_simpleinput IOBUF_USBN (
        .p__ice40_pin(USBN),
        .o__input_signal(usb_n_in),
        .i__output_signal(usb_n_tx),
        .i__output_en(usb_tx_en)
    );


    ///////////////////////////
    // Export debug signals. //
    ///////////////////////////

    `ifdef DEBUG_USB_CORE
    assign PIN_1  = debug[0];
    assign PIN_2  = debug[1];
    assign PIN_3  = debug[2];
    assign PIN_4  = debug[3];
    assign PIN_5  = debug[4];
    assign PIN_6  = debug[5];
    assign PIN_7  = debug[6];
    assign PIN_8  = debug[7];
    assign PIN_9  = debug[8];
    assign PIN_10 = debug[9];
    assign PIN_11 = debug[10];
    assign PIN_12 = debug[11];
    `endif

    `ifdef DEBUG_USB_CTRL
    assign PIN_19 = uart_out_ready;
    assign PIN_20 = uart_out_valid;
    assign PIN_21 = uart_in_ready;
    assign PIN_22 = uart_in_valid;
    assign PIN_23 = rst_master;
    assign PIN_24 = clk_48MHz;
    `endif


    //////////////////////////////////////////////////////////////////////
    // Uses the LED to show TX/RX operations: defines event to show,    //
    // generates a short one-clock pulse at each positive edge of the   //
    // event, then uses it to trigger a monostable that drives the LED. //
    //////////////////////////////////////////////////////////////////////

    wire event_to_show = uart_in_valid || uart_out_valid;

    `ifdef SIMULATION
      `define BLINK_TIME_S      0.00010     // Simulation.
    `else
      `define BLINK_TIME_S      0.10        // Build
    `endif

    wire posedge_detected;

    slow_activehigh_signal_posedgedetector PEDETECT (
        .i__clk(CLK_16MHz),
        .i__rst(rst_master),
        .i__sigin(event_to_show),
        .o__posedge(posedge_detected)
    );

    activehigh_monostable_highlevel_triggered #(
        .PULSE_CLOCK_CYCLES($rtoi(16000000*`BLINK_TIME_S-1 + 0.5))
    ) BLINKGEN (
        .i__clk(CLK_16MHz),
        .i__rst(rst_master),
        .i__sigin(posedge_detected),
        .o__pulse(LED)
    );

endmodule
