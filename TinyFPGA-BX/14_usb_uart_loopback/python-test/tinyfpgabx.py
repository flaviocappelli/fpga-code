
import serial.tools.list_ports

def port():
    VID = 0x1D50
    PID = 0x6130
    device_list = serial.tools.list_ports.comports()
    portValue = None
    for device in device_list:
        if (device.vid != None or device.pid != None):
            if (device.vid == VID and device.pid == PID):
                    portValue = device.device
                    print(f"TinyFPGA is on {portValue}")
                    break
    return portValue


def boot():
    portValue = port()
    if (portValue != None):
        ser = serial.Serial(portValue)
        ser.timeout = 1            #non-block read
        ser.write(b'\x00')
        ser.close()


def open():
    portValue = port()
    if (portValue != None):
        ser = serial.Serial()
        ser.port = portValue
        ser.timeout = 1            #non-block read
        ser.writeTimeout = 2       #timeout for write
        ser.open()
        return ser
    else:
        return None
