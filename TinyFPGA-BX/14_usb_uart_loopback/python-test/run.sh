#!/bin/sh

if [ -z "${PYTHONPYCACHEPREFIX}" ]; then
    if [ -d /tmp ]; then
        PYTHONPYCACHEPREFIX="/tmp/cache/cpython/"
    else
        PYTHONPYCACHEPREFIX="$HOME/.cache/cpython/"
    fi
    export PYTHONPYCACHEPREFIX;
fi

cd $(dirname $(readlink -f "$0"))
python3 testcode.py
