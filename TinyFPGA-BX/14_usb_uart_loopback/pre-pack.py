# Clock constraints for NextPNR (clock name, frequency in MHz). See:
# https://github.com/YosysHQ/nextpnr/blob/master/docs/constraints.md

ctx.addClock("CLK_16MHz",  16)
ctx.addClock("clk_192MHz", 192)
ctx.addClock("clk_96MHz",  96)
ctx.addClock("clk_48MHz",  48)
