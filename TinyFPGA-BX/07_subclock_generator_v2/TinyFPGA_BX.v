
// (C) 2021-2025 Flavio Cappelli
// Released under the MIT License.
//
// Generate the following clocks, all with a 50% dutycycle:
//   50MHz, 20MHz, 10MHz, 5MHz, 2MHz, 1MHz, 500KHz, 200KHz,
//   100KHz, 50KHz, 20KHz, 10KHz, 5KHz, 2KHz, 1KHz, 500Hz,
//   200Hz, 100Hz, 50Hz, 20Hz, 10Hz, 5Hz, 2Hz, 1Hz (LED).
//
// Version 2, see the _README.1st file.
//
// INPUT: none.
//
// OUPUT: PIN_1 (highest clock) to PIN_24 (lowest clock) + LED (1Hz).
//
// For the pin names on the TinyFPGA BX board see TinyFPGA-BX-pins.pcf

`default_nettype none       // Do not allow undeclared signals.
`include "timescale.vh"     // Timescale defined globally.

(* top *)                   // Mark this as top module, because
module TinyFPGA_BX (        // sometimes Yosys doesn't detect it.
    input  wire CLK_16MHz,  // 16MHz clock.
    output wire PIN_1,      // Output -> clock  50MHz
    output wire PIN_2,      // Output -> clock  20MHz
    output wire PIN_3,      // Output -> clock  10MHz
    output wire PIN_4,      // Output -> clock   5MHz
    output wire PIN_5,      // Output -> clock   2MHz
    output wire PIN_6,      // Output -> clock   1MHz
    output wire PIN_7,      // Output -> clock 500KHz
    output wire PIN_8,      // Output -> clock 200KHz
    output wire PIN_9,      // Output -> clock 100KHz
    output wire PIN_10,     // Output -> clock  50KHz
    output wire PIN_11,     // Output -> clock  20KHz
    output wire PIN_12,     // Output -> clock  10KHz
    output wire PIN_13,     // Output -> clock   5KHz
    output wire PIN_14,     // Output -> clock   2KHz
    output wire PIN_15,     // Output -> clock   1KHz
    output wire PIN_16,     // Output -> clock  500Hz
    output wire PIN_17,     // Output -> clock  200Hz
    output wire PIN_18,     // Output -> clock  100Hz
    output wire PIN_19,     // Output -> clock   50Hz
    output wire PIN_20,     // Output -> clock   20Hz
    output wire PIN_21,     // Output -> clock   10Hz
    output wire PIN_22,     // Output -> clock    5Hz
    output wire PIN_23,     // Output -> clock    2Hz
    output wire PIN_24,     // Output -> clock    1Hz
    output wire LED,        // User/boot LED (blink at 1Hz)
    output wire USBPU       // USB pull-up resistor.
);
    // USB not used (drive USB pull-up resistor to '0'
    // to avoid the USB device enumeration by the OS).
    assign USBPU = 0;


    ////////////////////////////////////////////////////
    // Generate a 200MHz clock using the internal PLL //
    // The clock is supplied when clean (PLL locked). //
    ////////////////////////////////////////////////////

    wire clk_200MHz;
    wire pll_clk, pll_locked;           // PLL outputs.

    ice40_pll_200MHz_from_16MHz PLL (
        .i__clk_in( CLK_16MHz ),
        .o__clk_out( pll_clk ),
        .o__async_locked( pll_locked )
    );

    assign clk_200MHz = pll_clk & pll_locked;


    //////////////////////////////////////////////////
    // Obtain a perfectly square 100MHz clock. This //
    // will be used as input for all dividers.      //
    //////////////////////////////////////////////////

    reg clk_100MHz = 0;
    always @(posedge clk_200MHz)
        clk_100MHz = ~clk_100MHz;


    /////////////////////////
    // Generate subclocks. //
    /////////////////////////

    wire clk_50MHz,  clk_20MHz,  clk_10MHz;
    wire clk_5MHz,   clk_2MHz,   clk_1MHz;
    wire clk_500KHz, clk_200KHz, clk_100KHz;
    wire clk_50KHz,  clk_20KHz,  clk_10KHz;
    wire clk_5KHz,   clk_2KHz,   clk_1KHz;
    wire clk_500Hz,  clk_200Hz,  clk_100Hz;
    wire clk_50Hz,   clk_20Hz,   clk_10Hz;
    wire clk_5Hz,    clk_2Hz,    clk_1Hz;

    clock_divider #(.DIVISOR(        2)) GEN_50MHz  (.i__clk(clk_100MHz), .o__div_by(clk_50MHz ));
    clock_divider #(.DIVISOR(        5)) GEN_20MHz  (.i__clk(clk_100MHz), .o__div_by(clk_20MHz ));
    clock_divider #(.DIVISOR(       10)) GEN_10MHz  (.i__clk(clk_100MHz), .o__div_by(clk_10MHz ));
    clock_divider #(.DIVISOR(       20)) GEN_5MHz   (.i__clk(clk_100MHz), .o__div_by(clk_5MHz  ));
    clock_divider #(.DIVISOR(       50)) GEN_2MHz   (.i__clk(clk_100MHz), .o__div_by(clk_2MHz  ));
    clock_divider #(.DIVISOR(      100)) GEN_1MHz   (.i__clk(clk_100MHz), .o__div_by(clk_1MHz  ));
    clock_divider #(.DIVISOR(      200)) GEN_500KHz (.i__clk(clk_100MHz), .o__div_by(clk_500KHz));
    clock_divider #(.DIVISOR(      500)) GEN_200KHz (.i__clk(clk_100MHz), .o__div_by(clk_200KHz));
    clock_divider #(.DIVISOR(     1000)) GEN_100KHz (.i__clk(clk_100MHz), .o__div_by(clk_100KHz));
    clock_divider #(.DIVISOR(     2000)) GEN_50KHz  (.i__clk(clk_100MHz), .o__div_by(clk_50KHz ));
    clock_divider #(.DIVISOR(     5000)) GEN_20KHz  (.i__clk(clk_100MHz), .o__div_by(clk_20KHz ));
    clock_divider #(.DIVISOR(    10000)) GEN_10KHz  (.i__clk(clk_100MHz), .o__div_by(clk_10KHz ));
    clock_divider #(.DIVISOR(    20000)) GEN_5KHz   (.i__clk(clk_100MHz), .o__div_by(clk_5KHz  ));
    clock_divider #(.DIVISOR(    50000)) GEN_2KHz   (.i__clk(clk_100MHz), .o__div_by(clk_2KHz  ));
    clock_divider #(.DIVISOR(   100000)) GEN_1KHz   (.i__clk(clk_100MHz), .o__div_by(clk_1KHz  ));
    clock_divider #(.DIVISOR(   200000)) GEN_500Hz  (.i__clk(clk_100MHz), .o__div_by(clk_500Hz ));
    clock_divider #(.DIVISOR(   500000)) GEN_200Hz  (.i__clk(clk_100MHz), .o__div_by(clk_200Hz ));
    clock_divider #(.DIVISOR(  1000000)) GEN_100Hz  (.i__clk(clk_100MHz), .o__div_by(clk_100Hz ));
    clock_divider #(.DIVISOR(  2000000)) GEN_50Hz   (.i__clk(clk_100MHz), .o__div_by(clk_50Hz  ));
    clock_divider #(.DIVISOR(  5000000)) GEN_20Hz   (.i__clk(clk_100MHz), .o__div_by(clk_20Hz  ));
    clock_divider #(.DIVISOR( 10000000)) GEN_10Hz   (.i__clk(clk_100MHz), .o__div_by(clk_10Hz  ));
    clock_divider #(.DIVISOR( 20000000)) GEN_5Hz    (.i__clk(clk_100MHz), .o__div_by(clk_5Hz   ));
    clock_divider #(.DIVISOR( 50000000)) GEN_2Hz    (.i__clk(clk_100MHz), .o__div_by(clk_2Hz   ));
    clock_divider #(.DIVISOR(100000000)) GEN_1Hz    (.i__clk(clk_100MHz), .o__div_by(clk_1Hz   ));


    /////////////////////
    // Assign outputs. //
    /////////////////////

    assign PIN_1 = clk_50MHz;
    assign PIN_2 = clk_20MHz;
    assign PIN_3 = clk_10MHz;
    assign PIN_4 = clk_5MHz;
    assign PIN_5 = clk_2MHz;
    assign PIN_6 = clk_1MHz;
    assign PIN_7 = clk_500KHz;
    assign PIN_8 = clk_200KHz;
    assign PIN_9 = clk_100KHz;
    assign PIN_10 = clk_50KHz;
    assign PIN_11 = clk_20KHz;
    assign PIN_12 = clk_10KHz;
    assign PIN_13 = clk_5KHz;
    assign PIN_14 = clk_2KHz;
    assign PIN_15 = clk_1KHz;
    assign PIN_16 = clk_500Hz;
    assign PIN_17 = clk_200Hz;
    assign PIN_18 = clk_100Hz;
    assign PIN_19 = clk_50Hz;
    assign PIN_20 = clk_20Hz;
    assign PIN_21 = clk_10Hz;
    assign PIN_22 = clk_5Hz;
    assign PIN_23 = clk_2Hz;
    assign PIN_24 = clk_1Hz;

    // Led blink.
    assign LED = clk_1Hz;

endmodule
