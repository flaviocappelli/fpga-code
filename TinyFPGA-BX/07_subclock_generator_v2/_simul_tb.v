
// (C) 2021-2025 Flavio Cappelli
// Released under the MIT License.
//
// See https://www.fpgatutorial.com/how-to-write-a-basic-verilog-testbench/

`default_nettype none                       // Do not allow undeclared signals.
`define DUMPSTR(x) `"x.vcd`"                // Required for GTKWave.
`define SIMULATION_TIME 2ms                 // Simulation time (in s, ms, us or ns).
`include "clkgen_sim_helper.vh"             // Must be included before timescale.vh
`include "timescale.vh"                     // Timescale defined globally.

module testbench();
    initial begin
        $dumpfile(`DUMPSTR(`VCD_OUTPUT));   // Required for GTKWave.
        $dumpvars(0, testbench);            // Dump all variables in all instantiated modules.
        $printtimescale(testbench);         // Display the time scale and resolution (see above).
        #`SIMULATION_TIME                   // Wait until the simulation time is elapsed.
        $timeformat(-6, 2, "us", 1);        // Display the time elapsed in "us".
        $display("Simulation terminated after %t", $realtime);
        $finish;                            // Terminate the simulation.
    end


    // ---- Board 16MHz Clock Simulation ----

    wire CLK_16MHz;
    clkgen_sim_helper #(.FREQ_MHz(16)) CLKGEN (.o__clk(CLK_16MHz));


    // ---- Board Simulation ----

    wire LED, USBPU;                        // Simulated outputs.
    wire PIN_1, PIN_2, PIN_3, PIN_4, PIN_5, PIN_6, PIN_7, PIN_8, PIN_9;
    wire PIN_10, PIN_11, PIN_12, PIN_13, PIN_14, PIN_15, PIN_16, PIN_17;
    wire PIN_18, PIN_19, PIN_20, PIN_21, PIN_22, PIN_23, PIN_24;

    TinyFPGA_BX BOARD (
        .CLK_16MHz(CLK_16MHz),
        .PIN_1(PIN_1),
        .PIN_2(PIN_2),
        .PIN_3(PIN_3),
        .PIN_4(PIN_4),
        .PIN_5(PIN_5),
        .PIN_6(PIN_6),
        .PIN_7(PIN_7),
        .PIN_8(PIN_8),
        .PIN_9(PIN_9),
        .PIN_10(PIN_10),
        .PIN_11(PIN_11),
        .PIN_12(PIN_12),
        .PIN_13(PIN_13),
        .PIN_14(PIN_14),
        .PIN_15(PIN_15),
        .PIN_16(PIN_16),
        .PIN_17(PIN_17),
        .PIN_18(PIN_18),
        .PIN_19(PIN_19),
        .PIN_20(PIN_20),
        .PIN_21(PIN_21),
        .PIN_22(PIN_22),
        .PIN_23(PIN_23),
        .PIN_24(PIN_24),
        .LED(LED),
        .USBPU(USBPU)
    );

endmodule

// In post-synthesis simulation Yosys sees only the interface of the
// following PLL simulation helper, so we have to include it here.

`ifdef POST_SYNTH_SIM
`include "ice40_pll_200MHz_sim_helper.v"
`endif
