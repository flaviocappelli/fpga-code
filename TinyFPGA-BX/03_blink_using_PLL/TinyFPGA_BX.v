
// (C) 2021-2025 Flavio Cappelli
// Released under the MIT License.
//
// Blinking LED (test the iCE40 PLL).
//
// INPUT: none.
//
// OUPUT: LED, PIN_1, PIN_2 (see below).
//
// For the pin names on the TinyFPGA BX board see TinyFPGA-BX-pins.pcf

`default_nettype none                   // Do not allow undeclared signals.
`include "timescale.vh"                 // Timescale defined globally.

(* top *)                               // Mark this as top module, because
module TinyFPGA_BX (                    // sometimes Yosys doesn't detect it.
    input  wire CLK_16MHz,              // 16MHz on-board clock.
    output wire PIN_1,                  // Output -> clock 16MHz.
    output wire PIN_2,                  // Output -> clock 50MHz.
    output wire LED,                    // User/boot LED next to power LED.
    output wire USBPU                   // USB pull-up resistor.
);
    // USB not used (drive USB pull-up resistor to '0'
    // to avoid the USB device enumeration by the OS).
    assign USBPU = 0;


    /////////////////////////////////////////////////////////
    // Generate a 50MHz clock using the iCE40 internal PLL //
    /////////////////////////////////////////////////////////

    wire clk_out, clk_ok;               // PLL output.

    ice40_pll_50MHz_from_16MHz PLL (
        .i__clk_in(CLK_16MHz),          // Input clock.
        .o__clk_out(clk_out),           // Output clock.
        .o__async_locked(clk_ok)        // Lock signal.
    );

    assign PIN_1 = CLK_16MHz;           // 16MHz clock ouput.
    assign PIN_2 = clk_out & clk_ok;    // 50MHz clock ouput (when stable).


    ///////////////////////////////////////////
    // Blink using the 50MHz generated clock //
    ///////////////////////////////////////////

    `ifdef SIMULATION
      `define BLINK_FREQ_HZ     1000    // Simulation.
    `else
      `define BLINK_FREQ_HZ     1       // Build.
    `endif

    reg status = 0;
    reg [26:0] counter = 0;
    localparam MAXCOUNT = 50000000/(`BLINK_FREQ_HZ*2)-1;

    always @(posedge clk_out) begin
        if (clk_ok)
            begin
                if (counter == MAXCOUNT)
                    begin
                        counter <= 0;
                        status <= ~status;
                    end
                else
                    counter <= counter + 1;
            end
    end

    assign LED = status;

endmodule
