
// (C) 2021-2025 Flavio Cappelli
// Released under the MIT License.
//
// Generate the following clocks, all with a 50% dutycycle:
//   50MHz, 20MHz, 10MHz, 5MHz, 2MHz, 1MHz, 500KHz, 200KHz,
//   100KHz, 50KHz, 20KHz, 10KHz, 5KHz, 2KHz, 1KHz, 500Hz,
//   200Hz, 100Hz, 50Hz, 20Hz, 10Hz, 5Hz, 2Hz, 1Hz (LED).
//
// Version 1, see the _README.1st file.
//
// INPUT: none.
//
// OUPUT: PIN_1 (highest clock) to PIN_24 (lowest clock) + LED (1Hz).
//
// For the pin names on the TinyFPGA BX board see TinyFPGA-BX-pins.pcf

`default_nettype none       // Do not allow undeclared signals.
`include "timescale.vh"     // Timescale defined globally.

(* top *)                   // Mark this as top module, because
module TinyFPGA_BX (        // sometimes Yosys doesn't detect it.
    input  wire CLK_16MHz,  // 16MHz clock.
    output wire PIN_1,      // Output -> clock  50MHz
    output wire PIN_2,      // Output -> clock  20MHz
    output wire PIN_3,      // Output -> clock  10MHz
    output wire PIN_4,      // Output -> clock   5MHz
    output wire PIN_5,      // Output -> clock   2MHz
    output wire PIN_6,      // Output -> clock   1MHz
    output wire PIN_7,      // Output -> clock 500KHz
    output wire PIN_8,      // Output -> clock 200KHz
    output wire PIN_9,      // Output -> clock 100KHz
    output wire PIN_10,     // Output -> clock  50KHz
    output wire PIN_11,     // Output -> clock  20KHz
    output wire PIN_12,     // Output -> clock  10KHz
    output wire PIN_13,     // Output -> clock   5KHz
    output wire PIN_14,     // Output -> clock   2KHz
    output wire PIN_15,     // Output -> clock   1KHz
    output wire PIN_16,     // Output -> clock  500Hz
    output wire PIN_17,     // Output -> clock  200Hz
    output wire PIN_18,     // Output -> clock  100Hz
    output wire PIN_19,     // Output -> clock   50Hz
    output wire PIN_20,     // Output -> clock   20Hz
    output wire PIN_21,     // Output -> clock   10Hz
    output wire PIN_22,     // Output -> clock    5Hz
    output wire PIN_23,     // Output -> clock    2Hz
    output wire PIN_24,     // Output -> clock    1Hz
    output wire LED,        // User/boot LED (blink at 1Hz)
    output wire USBPU       // USB pull-up resistor.
);
    // USB not used (drive USB pull-up resistor to '0'
    // to avoid the USB device enumeration by the OS).
    assign USBPU = 0;


    ////////////////////////////////////////////////////
    // Generate a 200MHz clock using the internal PLL //
    // The clock is supplied when clean (PLL locked). //
    ////////////////////////////////////////////////////

    wire clk_200MHz;
    wire pll_clk, pll_locked;           // PLL outputs.

    ice40_pll_200MHz_from_16MHz PLL (
        .i__clk_in( CLK_16MHz ),
        .o__clk_out( pll_clk ),
        .o__async_locked( pll_locked )
    );

    assign clk_200MHz = pll_clk & pll_locked;


    //////////////////////////////////////////////////
    // Obtain a perfectly square 100MHz clock. This //
    // will be used as input for the first divider. //
    //////////////////////////////////////////////////

    reg clk_100MHz = 0;
    always @(posedge clk_200MHz)
        clk_100MHz = ~clk_100MHz;


    //////////////////////////////////
    // Generate 50MHz, 20MHz, 10MHz //
    //////////////////////////////////

    wire clk_50MHz, clk_20MHz, clk_10MHz;

    divider_by_2_5_10 DIV01 (
        .i__clk( clk_100MHz ),
        .o__div_by2( clk_50MHz ),
        .o__div_by5( clk_20MHz ),
        .o__div_by10( clk_10MHz )
    );

    assign PIN_1 = clk_50MHz;
    assign PIN_2 = clk_20MHz;
    assign PIN_3 = clk_10MHz;


    ///////////////////////////////
    // Generate 5MHz, 2MHz, 1MHz //
    ///////////////////////////////

    wire clk_5MHz, clk_2MHz, clk_1MHz;

    divider_by_2_5_10 DIV02 (
        .i__clk( clk_10MHz ),
        .o__div_by2( clk_5MHz ),
        .o__div_by5( clk_2MHz ),
        .o__div_by10( clk_1MHz )
    );

    assign PIN_4 = clk_5MHz;
    assign PIN_5 = clk_2MHz;
    assign PIN_6 = clk_1MHz;


    /////////////////////////////////////
    // Generate 500KHz, 200KHz, 100KHz //
    /////////////////////////////////////

    wire clk_500KHz, clk_200KHz, clk_100KHz;

    divider_by_2_5_10 DIV03 (
        .i__clk( clk_1MHz ),
        .o__div_by2( clk_500KHz ),
        .o__div_by5( clk_200KHz ),
        .o__div_by10( clk_100KHz )
    );

    assign PIN_7 = clk_500KHz;
    assign PIN_8 = clk_200KHz;
    assign PIN_9 = clk_100KHz;


    //////////////////////////////////
    // Generate 50KHz, 20KHz, 10KHz //
    //////////////////////////////////

    wire clk_50KHz, clk_20KHz, clk_10KHz;

    divider_by_2_5_10 DIV04 (
        .i__clk( clk_100KHz ),
        .o__div_by2( clk_50KHz ),
        .o__div_by5( clk_20KHz ),
        .o__div_by10( clk_10KHz )
    );

    assign PIN_10 = clk_50KHz;
    assign PIN_11 = clk_20KHz;
    assign PIN_12 = clk_10KHz;


    ///////////////////////////////
    // Generate 5KHz, 2KHz, 1KHz //
    ///////////////////////////////

    wire clk_5KHz, clk_2KHz, clk_1KHz;

    divider_by_2_5_10 DIV05 (
        .i__clk( clk_10KHz ),
        .o__div_by2( clk_5KHz ),
        .o__div_by5( clk_2KHz ),
        .o__div_by10( clk_1KHz )
    );

    assign PIN_13 = clk_5KHz;
    assign PIN_14 = clk_2KHz;
    assign PIN_15 = clk_1KHz;


    //////////////////////////////////
    // Generate 500Hz, 200Hz, 100Hz //
    //////////////////////////////////

    wire clk_500Hz, clk_200Hz, clk_100Hz;

    divider_by_2_5_10 DIV06 (
        .i__clk( clk_1KHz ),
        .o__div_by2( clk_500Hz ),
        .o__div_by5( clk_200Hz ),
        .o__div_by10( clk_100Hz )
    );

    assign PIN_16 = clk_500Hz;
    assign PIN_17 = clk_200Hz;
    assign PIN_18 = clk_100Hz;


    ///////////////////////////////
    // Generate 50Hz, 20Hz, 10Hz //
    ///////////////////////////////

    wire clk_50Hz, clk_20Hz, clk_10Hz;

    divider_by_2_5_10 DIV07 (
        .i__clk( clk_100Hz ),
        .o__div_by2( clk_50Hz ),
        .o__div_by5( clk_20Hz ),
        .o__div_by10( clk_10Hz )
    );

    assign PIN_19 = clk_50Hz;
    assign PIN_20 = clk_20Hz;
    assign PIN_21 = clk_10Hz;


    ////////////////////////////
    // Generate 5Hz, 2Hz, 1Hz //
    ////////////////////////////

    wire clk_5Hz, clk_2Hz, clk_1Hz;

    divider_by_2_5_10 DIV08 (
        .i__clk( clk_10Hz ),
        .o__div_by2( clk_5Hz ),
        .o__div_by5( clk_2Hz ),
        .o__div_by10( clk_1Hz )
    );

    assign PIN_22 = clk_5Hz;
    assign PIN_23 = clk_2Hz;
    assign PIN_24 = clk_1Hz;

    // Led blink.
    assign LED = clk_1Hz;

endmodule
