
// (C) 2021-2025 Flavio Cappelli
// Released under the MIT License.
//
// Divide the input clock by 2, 5 and 10.
//
// NOTE: The module output signals "o__div_by2" and "o__div_by10" will
//       always have a 50% dutycycle, regardless of the dutycycle of the
//       input clock "i__clk". Instead, the output signal "o__div_by5"
//       will have a 50% dutycycle only with a perfect square input clock.

`default_nettype none               // Do not allow undeclared signals.
`include "timescale.vh"             // Timescale defined globally.


module divider_by_2_5_10 (
    input  wire i__clk,             // Clock input.
    output wire o__div_by2,         // Clock divided by 2.
    output wire o__div_by5,         // Clock divided by 5.
    output wire o__div_by10         // Clock divided by 10.
);

    clock_divider #(.DIVISOR(2)) CLK_DIV_BY2 (
        .i__clk(i__clk),
        .o__div_by(o__div_by2)
    );

    clock_divider #(.DIVISOR(5)) CLK_DIV_BY5 (
        .i__clk(i__clk),
        .o__div_by(o__div_by5)
    );

    clock_divider #(.DIVISOR(10)) CLK_DIV_BY10 (
        .i__clk(i__clk),
        .o__div_by(o__div_by10)
    );

endmodule
