
PROJECT DESCRIPTION
-------------------
Generate the following clocks, all with a 50% dutycycle:

   50MHz -> PIN_1
   20MHz -> PIN_2
   10MHz -> PIN_3
    5MHz -> PIN_4
    2MHz -> PIN_5
    1MHz -> PIN_6
  500KHz -> PIN_7
  200KHz -> PIN_8
  100KHz -> PIN_9
   50KHz -> PIN_10
   20KHz -> PIN_11
   10KHz -> PIN_12
    5KHz -> PIN_13
    2KHz -> PIN_14
    1KHz -> PIN_15
   500Hz -> PIN_16
   200Hz -> PIN_17
   100Hz -> PIN_18
    50Hz -> PIN_19
    20Hz -> PIN_20
    10Hz -> PIN_21
     5Hz -> PIN_22
     2Hz -> PIN_23
     1Hz -> PIN_24

Also the LED is made to blink at 1Hz.



NOTES
-----
This was my first attempt to generate subclocks. This version uses a
recurring module to divide the input clock by 2, 5 and 10 (the obtained
lowest clock is routed to another instance of the same module, and so on).
Note that with such approach most generated clocks are routed through the
normal switching fabric (the iCE40 LP8K FPGA has only eight global buffer)
and so they will have higher "propagation delay", higher "skew" and lower
"fan-out" compared to dedicated clock paths. Anyway, in this example, the
load on the internal clock lines is very low, because almost all generated
clocks are just routed to the output pins of the FPGA (one-third of them is
also used to drive the input of the next divisor, that anyway has only few
flip flops). Counters are small, thus all compare operations need a tiny
number of LUT levels and this has the benefit of low power usage and fast
propagation. The cons is that the generated clocks are not synchronized
with the source clock (this can be solved using a simple synchronizer
before routing the generated clocks to the FPGA output pins).

For a different implementation see the example "07_subclock_generator_v2".

Yosys doesn't include a model of the PLL in its simulation library (see
https://discourse.tinyfpga.com/t/pll-not-simulating-properly-with-apio)
so I had to implement it myself (the code is very coarse, do not expect
too much). Both the behavioral and post-synthesis simulations work, but
the latter takes a longer time to simulate the logic (also it cannot be
started from APIO, only from my makefile, see the "make" directory).



INSTRUCTIONS
------------
  - code check
     * type "make -f ../../make/Makefile verify"
     * type "make -f ../../make/Makefile lint"

  - simulation
     * type "make -f ../../make/Makefile sim"
     * type "make -f ../../make/Makefile pssim"

  - programming
     * type "make -f ../../make/Makefile clean"
     * type "make -f ../../make/Makefile build"
     * press the reset button on the TinyFPGA-BX
     * type "../upload_TinyFPGA_BX.sh"
     * after few seconds the LED will start to blink
       and the output pins will emit their subclocks
