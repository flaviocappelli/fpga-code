
// (C) 2021-2025 Flavio Cappelli
// Released under the MIT License.
//
// Blinking LED (test the on-board MEMS oscillator).
//
// INPUT: none.
//
// OUPUT: LED.
//
// For the pin names on the TinyFPGA BX board see TinyFPGA-BX-pins.pcf

`default_nettype none       // Do not allow undeclared signals.
`include "timescale.vh"     // Timescale defined globally.

(* top *)                   // Mark this as top module, because
module TinyFPGA_BX (        // sometimes Yosys doesn't detect it.
    input  wire CLK_16MHz,  // 16MHz on-board clock.
    output wire LED,        // User/boot LED next to power LED.
    output wire USBPU       // USB pull-up resistor.
);
    // USB not used (drive USB pull-up resistor to '0'
    // to avoid the USB device enumeration by the OS).
    assign USBPU = 0;


    ////////////////////////////////////
    // Blink dividing the 16MHz clock //
    ////////////////////////////////////

    `ifdef SIMULATION
      `define BLINK_FREQ_HZ     1000    // Simulation.
    `else
      `define BLINK_FREQ_HZ     1       // Build.
    `endif

    reg status = 0;
    reg [23:0] counter = 0;
    localparam MAXCOUNT = 16000000/(`BLINK_FREQ_HZ*2)-1;

    always @(posedge CLK_16MHz) begin
        if (counter == MAXCOUNT)
            begin
                counter <= 0;
                status <= ~status;
            end
        else
            counter <= counter + 1;
    end

    assign LED = status;

endmodule
