
// (C) 2021-2025 Flavio Cappelli
// Released under the MIT License.
//
// Set the user LED to ON, the pin labeled PIN_1 to
// 3.3V and the pin labeled PIN_2 to 0V (ground).
//
// INPUT: none.
//
// OUPUT: LED, PIN_1, PIN_2.
//
// For the pin names on the TinyFPGA BX board see TinyFPGA-BX-pins.pcf

`default_nettype none       // Do not allow undeclared signals.
`include "timescale.vh"     // Timescale defined globally.

(* top *)                   // Mark this as top module, because
module TinyFPGA_BX (        // sometimes Yosys doesn't detect it.
    output wire PIN_1,      // Output.
    output wire PIN_2,      // Output.
    output wire LED,        // User/boot LED next to power LED.
    output wire USBPU       // USB pull-up resistor.
);
    // USB not used (drive USB pull-up resistor to '0'
    // to avoid the USB device enumeration by the OS).
    assign USBPU = 0;

    // Set outputs and LED.
    assign PIN_1 = 1'b1;
    assign PIN_2 = 1'b0;
    assign LED   = 1'b1;

endmodule
