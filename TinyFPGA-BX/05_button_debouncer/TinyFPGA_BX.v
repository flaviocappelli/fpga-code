
// (C) 2021-2025 Flavio Cappelli
// Released under the MIT License.
//
// Debounce a button and count button presses.
//
// INPUT: button connected to PIN_1 and GND.
//        Note that a pull-up resistor is not required,
//        because the FPGA internal one is used.
//
// OUPUT: button press count: PIN_2 (LSb), PIN_3, PIN_4, PIN_5 (MSb)
//        LED shown the logic level on PIN_2.
//
// For the pin names on the TinyFPGA BX board see TinyFPGA-BX-pins.pcf

`default_nettype none       // Do not allow undeclared signals.
`include "timescale.vh"     // Timescale defined globally.

(* top *)                   // Mark this as top module, because
module TinyFPGA_BX (        // sometimes Yosys doesn't detect it.
    input  wire CLK_16MHz,  // 16MHz clock.
    input  wire PIN_1,      // Button input.
    output wire PIN_2,      // Button press count output 0.
    output wire PIN_3,      // Button press count output 1.
    output wire PIN_4,      // Button press count output 2.
    output wire PIN_5,      // Button press count output 3.
    output wire LED,        // User/boot LED next to power LED.
    output wire USBPU       // USB pull-up resistor.
);
    // USB not used (drive USB pull-up resistor to '0'
    // to avoid the USB device enumeration by the OS).
    assign USBPU = 0;


    //////////////////////////////////////////
    // Enable the pull-up resistor on PIN_1 //
    //////////////////////////////////////////

    wire button_in;

    ice40_pin_nooutput_simpleinput #(
        .PULLUP(1'b1)
    ) PULLUP_INP (
        .p__ice40_pin(PIN_1),
        .o__input_signal(button_in)
    );


    ////////////////////////////////////////
    // Create a simple active high reset. //
    ////////////////////////////////////////

    wire rst_master;

    fpga_reset_generator RESET_GEN (
        .i__clk(CLK_16MHz),
        .i__clk_en(1'b1),
        .o__gen_rst(rst_master)
    );


    /////////////////////////////////////////////
    // Apply the 2FF Synchronizer to button_in //
    /////////////////////////////////////////////

    wire button_sync;

    slow_activelow_signal_synchronizer BUTT_SYNC (
        .i__clk(CLK_16MHz),
        .i__rst(rst_master),
        .i__async_sig_n(button_in),
        .o__sync_sig_n(button_sync)
    );


    //////////////////////////////////////////////
    // Filter button_sync with a debounce timer //
    //////////////////////////////////////////////

    wire button_debounced;

    slow_activelow_signal_deglitcher #(
        .N_SAMPLES(240000)                      // 240000 * 1/16E6 (CLK_16MHz) => 15 ms.
    ) BUTT_DEB (
        .i__clk(CLK_16MHz),
        .i__rst(rst_master),
        .i__sigin_n(button_sync),
        .o__sigout_n(button_debounced)
    );


    ////////////////////////////
    // Negative edge detector //
    ////////////////////////////

    wire button_pressed_pulse;

    slow_activelow_signal_negedgedetector NE_DETECT (
        .i__clk(CLK_16MHz),
        .i__rst(rst_master),
        .i__sigin_n(button_debounced),
        .o__negedge(button_pressed_pulse)
    );


    //////////////////////////////////////
    // Count and output button presses. //
    //////////////////////////////////////

    reg [3:0] pressed_count;

    always @(posedge CLK_16MHz, posedge rst_master) begin
        if (rst_master) begin
            pressed_count <= 4'b0;
        end else begin
            // Use clock enable to count.
            if (button_pressed_pulse)
                pressed_count <= pressed_count + 1'b1;
        end
    end

    assign LED = pressed_count[0];
    assign {PIN_5, PIN_4, PIN_3, PIN_2} = pressed_count;

endmodule
