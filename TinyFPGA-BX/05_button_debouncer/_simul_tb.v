
// (C) 2021-2025 Flavio Cappelli
// Released under the MIT License.
//
// See https://www.fpgatutorial.com/how-to-write-a-basic-verilog-testbench/

`default_nettype none                       // Do not allow undeclared signals.
`define DUMPSTR(x) `"x.vcd`"                // Required for GTKWave.
`define SIMULATION_TIME 150ms               // Simulation time (in s, ms, us or ns).
`include "clkgen_sim_helper.vh"             // Must be included before timescale.vh
`include "timescale.vh"                     // Timescale defined globally.

module testbench();
    initial begin
        $dumpfile(`DUMPSTR(`VCD_OUTPUT));   // Required for GTKWave.
        $dumpvars(0, testbench);            // Dump all variables in all instantiated modules.
        $printtimescale(testbench);         // Display the time scale and resolution (see above).
        #`SIMULATION_TIME                   // Wait until the simulation time is elapsed.
        $timeformat(-6, 2, "us", 1);        // Display the time elapsed in "us".
        $display("Simulation terminated after %t", $realtime);
        $finish;                            // Terminate the simulation.
    end


    // ---- Board 16MHz Clock Simulation ----

    wire CLK_16MHz;
    clkgen_sim_helper #(.FREQ_MHz(16)) CLKGEN (.o__clk(CLK_16MHz));


    // ---- Board Simulation ----

    reg PIN_1;                              // Simulated input.
    wire PIN_2, PIN_3;                      // Simulated outputs.
    wire PIN_4, PIN_5;
    wire LED, USBPU;

    TinyFPGA_BX BOARD (
        .CLK_16MHz(CLK_16MHz),
        .PIN_1(PIN_1),
        .PIN_2(PIN_2),
        .PIN_3(PIN_3),
        .PIN_4(PIN_4),
        .PIN_5(PIN_5),
        .LED(LED),
        .USBPU(USBPU)
    );

    integer i;
    initial begin
        PIN_1 = 1;                          // Initial button input (with pull-up).
        #10ms

        for (i=0; i<2; ++i) begin
            PIN_1 = 0;                      // Simulate button press (to GND).
            #3.210us
            PIN_1 = 1;
            #1.550us
            PIN_1 = 0;
            #2.511us
            PIN_1 = 1;
            #25.92us
            PIN_1 = 0;
            #169.6us
            PIN_1 = 1;
            #200.1us
            PIN_1 = 0;
            #188.7us
            PIN_1 = 1;
            #143.2us
            PIN_1 = 0;
            #100.5ns
            PIN_1 = 1;
            #222.0ns
            PIN_1 = 0;
            #140.3us
            PIN_1 = 1;
            #992.3us
            PIN_1 = 0;
            #220.6us
            PIN_1 = 1;
            #400.1us
            PIN_1 = 0;
            #200.7us
            PIN_1 = 1;
            #995.6us
            PIN_1 = 0;
            #26ms

            PIN_1 = 1;                      // Simulate button release (with pull-up).
            #191.20us
            PIN_1 = 0;
            #400.7us
            PIN_1 = 1;
            #199.1us
            PIN_1 = 0;
            #25.13ns
            PIN_1 = 1;
            #142.5ns
            PIN_1 = 0;
            #12.13ns
            PIN_1 = 1;
            #155.1ns
            PIN_1 = 0;
            #192.4us
            PIN_1 = 1;
            #992.1us
            PIN_1 = 0;
            #150.9us
            PIN_1 = 1;
            #600.1us
            PIN_1 = 0;
            #799.8us
            PIN_1 = 1;
            #200.3us
            PIN_1 = 0;
            #330.2ns
            PIN_1 = 1;
            #1.030us
            PIN_1 = 0;
            #200.4us
            PIN_1 = 1;
            #10.39ns
            PIN_1 = 0;
            #12.42ns
            PIN_1 = 1;
            #36ms
            ;
        end
    end

endmodule
