#!/bin/bash

# COM/serial port.
SERIALPORT=/dev/ttyACM0

if [ -f hw.bin ]; then
    FIRMWAREFILE=hw.bin
elif [ -f hardware.bin ]; then
    FIRMWAREFILE=hardware.bin
else
    echo "Firmware file not found, aborting"
    exit 1
fi

if type -P tinyprog &>/dev/null; then
    TINYPROG=tinyprog
elif [ -n "$OSSCADSUITE_ROOT" ]; then
    TINYPROG="$OSSCADSUITE_ROOT/bin/tinyprog"
else
    echo "Command 'tinyprog' not found"
    exit 1
fi

"$TINYPROG" -l
if [ $? -ne 0 ]; then
    exit 1
fi

"$TINYPROG" --pyserial -c $SERIALPORT --program $FIRMWAREFILE
