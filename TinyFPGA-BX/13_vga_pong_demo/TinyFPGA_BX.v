
// (C) 2021-2025 Flavio Cappelli
// Released under the MIT License.
//
// Show a demo of the Pong Game on a VGA display (using video
// mode 1280x1024 @ 60Hz). Also the user LED is made to blink
// (to show that the logic is working). Note: this is a demo
// and not a game because the two paddles move by themselves,
// emulating human players (more or less).
//
// It was a challenge running this demo with such video mode:
// the iCE40 LP8K on the TinyFPGA BX board is not fast enough
// to execute complex boolean/math expressions at the required
// pixel clock rate (108 MHz). I had to make heavy use of the
// pipelining technique (i.e. decompose complex combinational
// expressions into elementary sequential operations, that
// can be performed within the clock cycle).
//
// INPUT: none.
//
// OUPUT: see below.
//
// For the pin names on the TinyFPGA BX board see TinyFPGA-BX-pins.pcf

`default_nettype none       // Do not allow undeclared signals.
`include "timescale.vh"     // Timescale defined globally.

(* top *)                   // Mark this as top module, because
module TinyFPGA_BX (        // sometimes Yosys doesn't detect it.
    input  wire CLK_16MHz,  // 16MHz clock.
    output wire PIN_1,      // Output -> VGA Red signal.
    output wire PIN_2,      // Output -> VGA Green signal.
    output wire PIN_3,      // Output -> VGA Blue signal.
    output wire PIN_4,      // Output -> VGA HSync signal.
    output wire PIN_5,      // Output -> VGA VSync signal.
    output wire PIN_6,      // Output -> Pixel clock (required for SDL2 C++ simulation).
    output wire LED,        // User/boot LED (blink).
    output wire USBPU       // USB pull-up resistor.
);
    // USB not used (drive USB pull-up resistor to '0'
    // to avoid the USB device enumeration by the OS).
    assign USBPU = 0;


    /////////////////////////////////////////////////////
    // Generate a 216MHz clock using the internal PLL. //
    /////////////////////////////////////////////////////

    wire clk_216MHz, pll_locked;        // PLL outputs.

    ice40_pll_216MHz_from_16MHz PLL (
        .i__clk_in( CLK_16MHz ),
        .o__clk_out( clk_216MHz ),
        .o__async_locked( pll_locked )
    );


    ///////////////////////////////////////////////////
    // Obtain a perfectly square 108MHz clock. It is //
    // used as input for the video timing generator. //
    ///////////////////////////////////////////////////

    reg clk_108MHz = 0;

    always @(posedge clk_216MHz)
        clk_108MHz <= ~clk_108MHz;


    ////////////////////////////////////////
    // Create a simple active high reset. //
    ////////////////////////////////////////

    wire rst_master;

    fpga_reset_generator RESET_GEN (
        .i__clk(clk_108MHz),
        .i__clk_en(pll_locked),
        .o__gen_rst(rst_master)
    );


    ////////////////////////////////////
    // Generate the video signals     //
    // (video mode 1280x1024 @ 60Hz). //
    ////////////////////////////////////

    wire [10:0] y_pt;
    wire [10:0] x_pt;
    wire h_sync, v_sync, data_enable;

    video_generator_1280x1024_60hz VIDEO_1024P (
        .i__clk(clk_108MHz),
        .i__rst(rst_master),
        .o__xp(x_pt),
        .o__yp(y_pt),
        .o__hs(h_sync),
        .o__vs(v_sync),
        .o__de(data_enable)
    );


    ///////////////////////////////////////////////////////
    // Generate two pseudo random numbers. They are used //
    // to randomly reverse the direction of the paddles. //
    ///////////////////////////////////////////////////////

    // Ignore unused bits and ports.
    // verilator lint_off UNUSED
    // verilator lint_off PINMISSING

    wire [15:0] random_l, random_r;

    random_generator_16bit #(
        .SEED(16'h1D1B)
    ) RNDGEN1 (
        .i__clk(clk_108MHz),
        .i__rst(rst_master),
        .o__data(random_l)
    );

    random_generator_16bit #(
        .SEED(16'h210E)
    ) RNDGEN2 (
        .i__clk(clk_108MHz),
        .i__rst(rst_master),
        .o__data(random_r)
    );

    // verilator lint_on PINMISSING
    // verilator lint_on UNUSED


    ////////////////////////////////////////////////////////
    // Animate the Pong Game demo.                        //
    //                                                    //
    // As stated above, we must use pipelines to execute  //
    // complex boolean/math expressions. Lot of registers //
    // are required to store partial (and final) results. //
    ////////////////////////////////////////////////////////

    // Registers used to calculate the next Y position of the paddles.
    reg [10:0] paddleL_next_y_stage1_a;
    reg [10:0] paddleL_next_y_stage1_b;
    reg [10:0] paddleR_next_y_stage1_a;
    reg [10:0] paddleR_next_y_stage1_b;
    reg        paddleL_next_y_stage2_a;
    reg        paddleL_next_y_stage2_b;
    reg        paddleR_next_y_stage2_a;
    reg        paddleR_next_y_stage2_b;
    reg [10:0] paddleL_next_y_stage3;
    reg [10:0] paddleR_next_y_stage3;

    //  Registers used to calculate the planned next X,Y position of the ball.
    reg [10:0] ball_planned_next_x_stage1_a;
    reg [10:0] ball_planned_next_x_stage1_b;
    reg [10:0] ball_planned_next_y_stage1_a;
    reg [10:0] ball_planned_next_y_stage1_b;
    reg [10:0] ball_planned_next_x_stage2;
    reg [10:0] ball_planned_next_y_stage2;

    // Registers used to detect collisions between the ball and the borders.
    reg ball_y_out_paddle_area_stage1_a;
    reg ball_y_out_paddle_area_stage1_b;
    reg ball_y_out_paddle_area_stage2;
    reg ball_x_in_borderL;
    reg ball_x_in_borderR;
    reg coll_borderL;
    reg coll_borderR;
    reg coll_borderT;
    reg coll_borderB;

    //  Registers used to detect collisions between the ball and the paddles.
    reg [10:0] ball_y_in_paddle_stage1;
    reg [10:0] ball_y_in_paddleL_stage1;
    reg [10:0] ball_y_in_paddleR_stage1;
    reg        ball_y_in_paddleL_stage2_a;
    reg        ball_y_in_paddleR_stage2_a;
    reg        ball_y_in_paddleL_stage2_b;
    reg        ball_y_in_paddleR_stage2_b;
    reg        ball_y_in_paddleL_stage3;
    reg        ball_y_in_paddleR_stage3;
    reg        coll_paddleL;
    reg        coll_paddleR;

    // Registers used to detect collisions between the ball and the lateral holes.
    reg coll_holeL;
    reg coll_holeR;

    // Registers used to calculate the final next position and direction of the ball.
    reg [10:0] ball_next_x;
    reg [10:0] ball_next_y;
    reg        ball_next_xdir;
    reg        ball_next_ydir;

    // Registers used to make the movement of the paddles "pseudo-human".
    reg paddleL_receiving_ball_stage1;
    reg paddleR_receiving_ball_stage1;
    reg paddleL_receiving_ball_stage2;
    reg paddleR_receiving_ball_stage2;

    // Registers used to calculate the movement in pixels of the paddles.
    reg [10:0] paddleL_movement_stage1;
    reg [10:0] paddleR_movement_stage1;
    reg [10:0] paddleL_movement_stage2_a;
    reg [10:0] paddleR_movement_stage2_a;
    reg [10:0] paddleL_movement_stage2_b;
    reg [10:0] paddleR_movement_stage2_b;
    reg [10:0] paddleL_movement_stage3;
    reg [10:0] paddleR_movement_stage3;

    // Registers used to calculate the absolute values of the above movement.
    reg [10:0] abs_paddleL_movement;
    reg [10:0] abs_paddleR_movement;

    // Registers used to calculate the next speed of the paddles.
    reg        paddleL_next_speed_stage1;
    reg        paddleR_next_speed_stage1;
    reg [10:0] paddleL_next_speed_stage2;
    reg [10:0] paddleR_next_speed_stage2;
    reg [10:0] paddleL_next_speed_stage3;
    reg [10:0] paddleR_next_speed_stage3;

    // Registers used to calculate the next direction of the paddles.
    reg reverse_l_dir;
    reg reverse_r_dir;
    reg paddleL_next_dir;
    reg paddleR_next_dir;

    // Registers used to calculate the next scores.
    reg [3:0] scoreL_next_val_stage1;
    reg [3:0] scoreR_next_val_stage1;
    reg [3:0] scoreL_next_val_stage2;
    reg [3:0] scoreR_next_val_stage2;

    // Main variables, updated only at the
    // beginning of the vertical blanking period.
    reg  [3:0] scoreL_val;                                                  // Score of left simulated players (0 to 9 max).
    reg  [3:0] scoreR_val;                                                  // Score of right simulated players (0 to 9 max).
    reg [10:0] ball_x;                                                      // Ball origin: X position.
    reg [10:0] ball_y;                                                      // Ball origin: Y position.
    reg        ball_move_left;                                              // Ball movement: X direction (0 right, 1 left).
    reg        ball_move_up;                                                // Ball movement: Y direction (0 down, 1 up).
    reg [10:0] paddleL_y;                                                   // Left paddle origin: Y position.
    reg [10:0] paddleR_y;                                                   // Right paddle origin: Y position.
    reg [10:0] paddleL_speed;                                               // Left paddle Y movement in pixels.
    reg [10:0] paddleR_speed;                                               // Right paddle Y movement in pixels.
    reg        paddleL_move_up;                                             // Left paddle movement Y direction(0 down, 1 up).
    reg        paddleR_move_up;                                             // Right paddle movement Y direction (0 down, 1 up).

    // Game field parameters.
    localparam H_RES        = 1280;                                         // Horizontal and vertical screen resolution
    localparam V_RES        = 1024;                                         // (must match the above video generator).
    localparam X_MAX        = H_RES - 1;
    localparam Y_MAX        = V_RES - 1;
    localparam BORDER_W     = 20;                                           // Total width of screen border.
    localparam BORDER_T     = BORDER_W;                                     // Border top.
    localparam BORDER_L     = BORDER_W;                                     // Border left.
    localparam BORDER_R     = X_MAX - BORDER_W;                             // Border right.
    localparam BORDER_B     = Y_MAX - BORDER_W;                             // Border bottom.

    // Paddle parameters. Origin (0,0) of paddles is top left.
    localparam PADDLE_H     = 100;                                          // Paddle height.
    localparam PADDLE_Y_MIN = 100;                                          // Paddle min Y.
    localparam PADDLE_Y_MAX = Y_MAX - PADDLE_Y_MIN;                         // Paddle max Y (including height).
    localparam PADDLE_L_POS = 180;                                          // Paddle initial position. Must be > PADDLE_Y_MIN and < PADDLE_Y_MAX - PADDLE_H.
    localparam PADDLE_R_POS = 600;
    localparam PADDLE_MAX_S = 7;                                            // Paddle max speed in pixels.

    // Ball parameters. Origin (0,0) of ball is top left.
    localparam BALL_SZ      = 10;                                           // Ball horizontal/vertical size in pixels.
    localparam BALL_X_POS   = H_RES/2 - BALL_SZ/2;                          // Ball X starting position.
    localparam BALL_Y_POS   = 200;                                          // Ball Y starting position.
    localparam BALL_X_SPEED = 11;                                           // Ball initial X movement in pixels. Must be < min(BORDER_W).
    localparam BALL_Y_SPEED = 6;                                            // Ball initial Y movement in pixels. Must be < min(BORDER_W).

    // Signal high for one pixel clock tick at the begin of vertical blanking:
    //
    // * start_vertical_blanking = (y_pt == V_RES) && (x_pt == 0);
    //
    // On the TinyFPGA BX (with 108MHz pixel clock) THIS IS AL CRITICAL, DUE TO THE
    // SIZE OF COMPARES. The update of object's properties must occur once per frame:
    // usually this update coincide with the beginning of the vertical blanking period,
    // but it does not need to be the 1st line. It's fine even if it happens on the 2nd
    // or 3rd line, the important thing is that it happens within the blanking period.
    // This allows us to pipeline the expression (to meet the iCE40 LP timings).
    reg begin_vbp_stage1_a;
    reg begin_vbp_stage1_b;
    reg begin_vbp_stage1_c;
    reg begin_vbp_stage1_d;
    reg begin_vbp_stage2;
    always @(posedge clk_108MHz, posedge rst_master) begin
        if (rst_master) begin

            begin_vbp_stage1_a <= 1'b0;
            begin_vbp_stage1_b <= 1'b0;
            begin_vbp_stage1_c <= 1'b0;
            begin_vbp_stage1_d <= 1'b0;
            begin_vbp_stage2   <= 1'b0;

        end else begin

            begin_vbp_stage1_a <= (y_pt[10:6] == 5'b10000);     // y_pt == VRES (i.e. y_pt == 1024).
            begin_vbp_stage1_b <= (y_pt[5:0]  == 6'd0);
            begin_vbp_stage1_c <= (x_pt[10:6] == 5'd0);         // x_pt == 0.
            begin_vbp_stage1_d <= (x_pt[5:0]  == 6'd0);
            begin_vbp_stage2   <= begin_vbp_stage1_a & begin_vbp_stage1_b & begin_vbp_stage1_c & begin_vbp_stage1_d;

        end
    end
    wire start_vertical_blanking = begin_vbp_stage2;

    // Initialize and update paddles/ball positions and movements.
    always @(posedge clk_108MHz, posedge rst_master) begin
        if (rst_master) begin

            // Registers used to calculate the next Y position of the paddles.
            paddleL_next_y_stage1_a         <= 0;
            paddleL_next_y_stage1_b         <= 0;
            paddleR_next_y_stage1_a         <= 0;
            paddleR_next_y_stage1_b         <= 0;
            paddleL_next_y_stage2_a         <= 1'b0;
            paddleL_next_y_stage2_b         <= 1'b0;
            paddleR_next_y_stage2_a         <= 1'b0;
            paddleR_next_y_stage2_b         <= 1'b0;
            paddleL_next_y_stage3           <= 0;
            paddleR_next_y_stage3           <= 0;

            // Registers used to calculate the planned next X,Y position of the ball.
            ball_planned_next_x_stage1_a    <= 0;
            ball_planned_next_x_stage1_b    <= 0;
            ball_planned_next_y_stage1_a    <= 0;
            ball_planned_next_y_stage1_b    <= 0;
            ball_planned_next_x_stage2      <= 0;
            ball_planned_next_y_stage2      <= 0;

            // Registers used to detect collisions between the ball and the borders.
            ball_y_out_paddle_area_stage1_a <= 1'b0;
            ball_y_out_paddle_area_stage1_b <= 1'b0;
            ball_y_out_paddle_area_stage2   <= 1'b0;
            ball_x_in_borderL               <= 1'b0;
            ball_x_in_borderR               <= 1'b0;
            coll_borderL                    <= 1'b0;
            coll_borderR                    <= 1'b0;
            coll_borderT                    <= 1'b0;
            coll_borderB                    <= 1'b0;

            // Registers used to detect collisions between the ball and the paddles.
            ball_y_in_paddle_stage1         <= 0;
            ball_y_in_paddleL_stage1        <= 0;
            ball_y_in_paddleR_stage1        <= 0;
            ball_y_in_paddleL_stage2_a      <= 1'b0;
            ball_y_in_paddleR_stage2_a      <= 1'b0;
            ball_y_in_paddleL_stage2_b      <= 1'b0;
            ball_y_in_paddleR_stage2_b      <= 1'b0;
            ball_y_in_paddleL_stage3        <= 1'b0;
            ball_y_in_paddleR_stage3        <= 1'b0;
            coll_paddleL                    <= 1'b0;
            coll_paddleR                    <= 1'b0;

            // Registers used to detect collisions between the ball and the lateral holes.
            coll_holeL                      <= 1'b0;
            coll_holeR                      <= 1'b0;

            // Registers used to calculate the final next position and direction of the ball.
            ball_next_x                     <= 0;
            ball_next_y                     <= 0;
            ball_next_xdir                  <= 1'b0;
            ball_next_ydir                  <= 1'b0;

            // Registers used to make the movement of the paddles "pseudo-human".
            paddleL_receiving_ball_stage1   <= 1'b0;
            paddleR_receiving_ball_stage1   <= 1'b0;
            paddleL_receiving_ball_stage2   <= 1'b0;
            paddleR_receiving_ball_stage2   <= 1'b0;

            // Registers used to calculate the movement in pixels of the paddles.
            paddleL_movement_stage1         <= 0;
            paddleR_movement_stage1         <= 0;
            paddleL_movement_stage2_a       <= 0;
            paddleR_movement_stage2_a       <= 0;
            paddleL_movement_stage2_b       <= 0;
            paddleR_movement_stage2_b       <= 0;
            paddleL_movement_stage3         <= 0;
            paddleR_movement_stage3         <= 0;

            // Registers used to calculate the absolute values of the above movement.
            abs_paddleL_movement            <= 0;
            abs_paddleR_movement            <= 0;

            // Register used to calculate the next speed of the paddles.
            paddleL_next_speed_stage1       <= 1'b0;
            paddleR_next_speed_stage1       <= 1'b0;
            paddleL_next_speed_stage2       <= 0;
            paddleR_next_speed_stage2       <= 0;
            paddleL_next_speed_stage3       <= 0;
            paddleR_next_speed_stage3       <= 0;

            // Registers used to calculate the next directions of the paddles.
            reverse_l_dir                   <= 1'b0;
            reverse_r_dir                   <= 1'b0;
            paddleL_next_dir                <= 1'b0;
            paddleR_next_dir                <= 1'b0;

            // Registers used to calculate the next scores.
            scoreL_next_val_stage1          <= 0;
            scoreR_next_val_stage1          <= 0;
            scoreL_next_val_stage2          <= 0;
            scoreR_next_val_stage2          <= 0;

            // Main variables, updated only at the
            // beginning of the vertical blanking period.
            scoreL_val                      <= 0;                           // Reset scores.
            scoreR_val                      <= 0;
            ball_x                          <= BALL_X_POS;                  // Initial ball position.
            ball_y                          <= BALL_Y_POS;
            ball_move_left                  <= 1'b1;                        // Initial ball moving direction.
            ball_move_up                    <= 1'b0;
            paddleL_y                       <= PADDLE_L_POS;                // Initial paddles positions.
            paddleR_y                       <= PADDLE_R_POS;
            paddleL_speed                   <= 0;                           // Initial paddles movements in pixels.
            paddleR_speed                   <= 0;
            paddleL_move_up                 <= PADDLE_L_POS > V_RES/2;      // Paddles start moving up/down depending on the initial position.
            paddleR_move_up                 <= PADDLE_R_POS > V_RES/2;

        end else begin

            // THE PIPELINES USED IN THIS SECTION HAVE DIFFERENT LENGTHS (AND SOME ARE
            // EVEN CONCATENATED) BUT IT DOESN'T MATTER, BECAUSE THE PRIMARY SOURCES OF
            // THESE PIPELINES ARE THE ABOVE MAIN VARIABLES, THAT ARE UPDATED ONLY AT
            // THE BEGINNING OF THE VERTICAL BLANKING PERIOD (1 CLOCK CYCLE), WHILE FOR
            // THE REMAINING FRAME SUCH VARIABLES ARE CONSTANTS (1799408 CLOCK CYCLES).
            // SO, THE PIPELINES WILL HAVE LOT OF TIME TO SETTLE TO THEIR FINAL VALUES
            // (AND SUCH FINAL VALUES WILL BE USED TO UPDATE AGAIN THE MAIN VARIABLES
            // AT THE BEGINNING OF THE NEXT VERTICAL BLANKING PERIOD).

            // Get next Y position of the left paddle (must be limited to paddle area).
            // Needs a 3-stage pipeline. Calculate:
            //
            // * paddleL_next_y = paddleL_move_up ?
            //     (paddleL_y - paddleL_speed >= PADDLE_Y_MIN ? paddleL_y - paddleL_speed : PADDLE_Y_MIN) :
            //     (paddleL_y + paddleL_speed <= PADDLE_Y_MAX - PADDLE_H + 1 ? paddleL_y + paddleL_speed : PADDLE_Y_MAX - PADDLE_H + 1);
            //
            paddleL_next_y_stage1_a <= (paddleL_y - paddleL_speed);
            paddleL_next_y_stage1_b <= (paddleL_y + paddleL_speed);
            paddleL_next_y_stage2_a <= (paddleL_next_y_stage1_a >= PADDLE_Y_MIN);
            paddleL_next_y_stage2_b <= (paddleL_next_y_stage1_b <= (PADDLE_Y_MAX - PADDLE_H + 1));
            paddleL_next_y_stage3   <= paddleL_move_up ?
                (paddleL_next_y_stage2_a ? paddleL_next_y_stage1_a : PADDLE_Y_MIN) :
                (paddleL_next_y_stage2_b ? paddleL_next_y_stage1_b : (PADDLE_Y_MAX - PADDLE_H + 1));

            // Get next Y position of the right paddle (must be limited to paddle area).
            // Needs a 3-stage pipeline. Calculate:
            //
            // * paddleR_next_y = paddleR_move_up ?
            //     (paddleR_y - paddleR_speed >= PADDLE_Y_MIN ? paddleR_y - paddleR_speed : PADDLE_Y_MIN) :
            //     (paddleR_y + paddleR_speed <= PADDLE_Y_MAX - PADDLE_H + 1 ? paddleR_y + paddleR_speed : PADDLE_Y_MAX - PADDLE_H + 1);
            //
            paddleR_next_y_stage1_a <= (paddleR_y - paddleR_speed);
            paddleR_next_y_stage1_b <= (paddleR_y + paddleR_speed);
            paddleR_next_y_stage2_a <= (paddleR_next_y_stage1_a >= PADDLE_Y_MIN);
            paddleR_next_y_stage2_b <= (paddleR_next_y_stage1_b <= (PADDLE_Y_MAX - PADDLE_H + 1));
            paddleR_next_y_stage3   <= paddleR_move_up ?
                (paddleR_next_y_stage2_a ? paddleR_next_y_stage1_a : PADDLE_Y_MIN) :
                (paddleR_next_y_stage2_b ? paddleR_next_y_stage1_b : (PADDLE_Y_MAX - PADDLE_H + 1));

            // Get planned next X,Y position of the ball.
            // Needs two 2-stage pipelines. Calculate:
            //
            // * ball_planned_next_x = ball_move_left ? ball_x - BALL_X_SPEED : ball_x + BALL_X_SPEED;
            // * ball_planned_next_y = ball_move_up   ? ball_y - BALL_Y_SPEED : ball_y + BALL_Y_SPEED;
            //
            ball_planned_next_x_stage1_a <= ball_x - BALL_X_SPEED;
            ball_planned_next_x_stage1_b <= ball_x + BALL_X_SPEED;
            ball_planned_next_y_stage1_a <= ball_y - BALL_Y_SPEED;
            ball_planned_next_y_stage1_b <= ball_y + BALL_Y_SPEED;
            ball_planned_next_x_stage2   <= ball_move_left ? ball_planned_next_x_stage1_a : ball_planned_next_x_stage1_b;
            ball_planned_next_y_stage2   <= ball_move_up ? ball_planned_next_y_stage1_a : ball_planned_next_y_stage1_b;

            // Get collisions between the ball and the borders. Needs multiple 3-stage pipelines.
            // Note: collisions are triggered on adjacent pixels (not overlapping pixels). Calculate:
            //
            // * ball_y_out_paddle_area = (ball_planned_next_y < PADDLE_Y_MIN) || (ball_planned_next_y > PADDLE_Y_MAX - (BALL_SZ-1));
            // * ball_x_in_borderL = ball_planned_next_x <= BORDER_L;
            // * ball_x_in_borderR = ball_planned_next_x >= BORDER_R - (BALL_SZ-1);
            // * coll_borderL = ball_x_in_borderL && ball_y_out_paddle_area;                            // Collision with left border.
            // * coll_borderR = ball_x_in_borderR && ball_y_out_paddle_area;                            // Collision with right border.
            // * coll_borderT = ball_planned_next_y <= BORDER_T;                                        // Collision with top border.
            // * coll_borderB = ball_planned_next_y >= BORDER_B - (BALL_SZ-1);                          // Collision with bottom border.
            //
            ball_y_out_paddle_area_stage1_a <= (ball_planned_next_y_stage2 < PADDLE_Y_MIN);
            ball_y_out_paddle_area_stage1_b <= (ball_planned_next_y_stage2 > PADDLE_Y_MAX - (BALL_SZ-1));
            ball_y_out_paddle_area_stage2   <= ball_y_out_paddle_area_stage1_a || ball_y_out_paddle_area_stage1_b;
            ball_x_in_borderL               <= (ball_planned_next_x_stage2 <= BORDER_L);
            ball_x_in_borderR               <= (ball_planned_next_x_stage2 >= BORDER_R - (BALL_SZ-1));
            coll_borderL                    <= ball_x_in_borderL && ball_y_out_paddle_area_stage2;
            coll_borderR                    <= ball_x_in_borderR && ball_y_out_paddle_area_stage2;
            coll_borderT                    <= (ball_planned_next_y_stage2 <= BORDER_T);
            coll_borderB                    <= (ball_planned_next_y_stage2 >= BORDER_B - (BALL_SZ-1));

            // Get collisions between the ball and the paddles.
            // Needs two 4-stage pipelines. Calculate:
            //
            // * ball_y_in_paddleL =  (ball_planned_next_y + BALL_SZ >= paddleL_next_y) && (ball_planned_next_y <= paddleL_next_y + PADDLE_H);
            // * ball_y_in_paddleR =  (ball_planned_next_y + BALL_SZ >= paddleR_next_y) && (ball_planned_next_y <= paddleR_next_y + PADDLE_H);
            // * coll_paddleL = ball_x_in_borderL && ball_y_in_paddleL;
            // * coll_paddleR = ball_x_in_borderR && ball_y_in_paddleR;
            //
            ball_y_in_paddle_stage1    <= ball_planned_next_y_stage2 + BALL_SZ;
            ball_y_in_paddleL_stage1   <= paddleL_next_y_stage3 + PADDLE_H;
            ball_y_in_paddleR_stage1   <= paddleR_next_y_stage3 + PADDLE_H;
            ball_y_in_paddleL_stage2_a <= (ball_y_in_paddle_stage1 >= paddleL_next_y_stage3);
            ball_y_in_paddleR_stage2_a <= (ball_y_in_paddle_stage1 >= paddleR_next_y_stage3);
            ball_y_in_paddleL_stage2_b <= (ball_planned_next_y_stage2 <= ball_y_in_paddleL_stage1);
            ball_y_in_paddleR_stage2_b <= (ball_planned_next_y_stage2 <= ball_y_in_paddleR_stage1);
            ball_y_in_paddleL_stage3   <= ball_y_in_paddleL_stage2_a && ball_y_in_paddleL_stage2_b;
            ball_y_in_paddleR_stage3   <= ball_y_in_paddleR_stage2_a && ball_y_in_paddleR_stage2_b;
            coll_paddleL               <= ball_x_in_borderL && ball_y_in_paddleL_stage3;
            coll_paddleR               <= ball_x_in_borderR && ball_y_in_paddleR_stage3;

            // Get collisions between the ball and the lateral holes. No pipeline required.
            coll_holeL <= ball_x_in_borderL && !ball_y_out_paddle_area_stage2 && !ball_y_in_paddleL_stage3;
            coll_holeR <= ball_x_in_borderR && !ball_y_out_paddle_area_stage2 && !ball_y_in_paddleR_stage3;

            // Get next X position of the ball. No pipeline required.
            ball_next_x <= (coll_holeL | coll_holeR) ? BALL_X_POS :                                     // When the ball collide with the holes start a new match.
                           (coll_paddleL | coll_borderL) ? BORDER_L :                                   // On left collision (with paddle or border) move left as far as we can.
                           (coll_paddleR | coll_borderR) ? BORDER_R - (BALL_SZ-1) :                     // On right collision (with paddle or border) move right as far as we can.
                           ball_planned_next_x_stage2;                                                  // Else update as planned.

            // Get next Y position of the ball (keep the last Y when a new match
            // occours, to randomize the starting Y position). No pipeline required.
            ball_next_y <= coll_borderT ? BORDER_T :                                                    // On top collision (with border) move top as far as we can.
                           coll_borderB ? BORDER_B - (BALL_SZ-1) :                                      // On bottom collision (with border) move bottom as far as we can.
                           ball_planned_next_y_stage2;                                                  // Else update as planned.

            // Get next X direction of the ball. No pipeline required.
            ball_next_xdir <= (coll_holeL | coll_holeR) ? ~ball_move_left :                             // Invert ball direction on new match.
                              (coll_paddleL | coll_borderL) ? 1'b0 :                                    // Left collision, next frame move right.
                              (coll_paddleR | coll_borderR) ? 1'b1 :                                    // Right collision, next frame move left.
                              ball_move_left;                                                           // Unchanged.

            // Get next Y direction of the ball. No pipeline required.
            ball_next_ydir <= coll_borderT ? 1'b0 :                                                     // Top collision, next frame move down.
                              coll_borderB ? 1'b1 :                                                     // Bottom collision, next frame move up.
                              ball_move_up;                                                             // Unchanged.

            // Get flags used to make the movement of the paddles "pseudo-human". Needs two 2-stage pipelines.
            //
            // To simulate pseudo-human movements and erros we will use this strategy: each paddle moves
            // toward the ball when it receives the ball and the ball is in the player area; vice versa,
            // the paddle moves toward the center of the screen when the ball moves away from the paddle
            // but it is still in the player area; the paddle stops moving when the ball goes outside the
            // player area; also, below we randomize a little the paddle direction. At first calculate:
            //
            // * paddleL_receiving_ball = ball_move_left && (ball_x < H_RES/2);
            // * paddleR_receiving_ball = !ball_move_left && (ball_x > H_RES/2);
            //
            paddleL_receiving_ball_stage1 <= (ball_x < H_RES/2 - BORDER_W/2 - BALL_SZ + 1);
            paddleR_receiving_ball_stage1 <= (ball_x > H_RES/2 + BORDER_W/2);
            paddleL_receiving_ball_stage2 <= ball_move_left && paddleL_receiving_ball_stage1;
            paddleR_receiving_ball_stage2 <= !ball_move_left && paddleR_receiving_ball_stage1;

            // Get the paddle's movement in pixels. Needs two 3-stage pipelines. Calculate:
            //
            // Y(ball) - Y(paddle) OR Y(screen center) - Y(paddle), depending on the above flags (see above description), i.e.:
            // * paddleL_movement = paddleL_receiving_ball ? ball_y - paddleL_y - PADDLE_H/2 + BALL_SZ/2 : V_RES/2 - PADDLE_H/2 - paddleL_y;
            // * paddleR_movement = paddleR_receiving_ball ? ball_y - paddleR_y - PADDLE_H/2 + BALL_SZ/2 : V_RES/2 - PADDLE_H/2 - paddleR_y;
            //
            // Note: with Y in range [0.1023], 11 bits are enough to perform a two's complement subtraction.
            paddleL_movement_stage1   <= ball_y - paddleL_y;
            paddleR_movement_stage1   <= ball_y - paddleR_y;
            paddleL_movement_stage2_a <= paddleL_movement_stage1 - PADDLE_H/2 + BALL_SZ/2;
            paddleR_movement_stage2_a <= paddleR_movement_stage1 - PADDLE_H/2 + BALL_SZ/2;
            paddleL_movement_stage2_b <= V_RES/2 - PADDLE_H/2 - paddleL_y;
            paddleR_movement_stage2_b <= V_RES/2 - PADDLE_H/2 - paddleR_y;
            paddleL_movement_stage3   <= paddleL_receiving_ball_stage2 ? paddleL_movement_stage2_a : paddleL_movement_stage2_b;
            paddleR_movement_stage3   <= paddleR_receiving_ball_stage2 ? paddleR_movement_stage2_a : paddleR_movement_stage2_b;

            // Get absolute values of the paddle's movement in pixels. No pipeline required.
            abs_paddleL_movement <= paddleL_movement_stage3[10] ? -paddleL_movement_stage3 : paddleL_movement_stage3;
            abs_paddleR_movement <= paddleR_movement_stage3[10] ? -paddleR_movement_stage3 : paddleR_movement_stage3;

            // Get paddle next speed. Needs two 3-stage pipelines. Calculate:
            //
            // * paddleL_next_speed = ball_x < H_RES/2 ? (abs_paddleL_movement < PADDLE_MAX_S ? abs_paddleL_movement : PADDLE_MAX_S) : 0;
            // * paddleR_next_speed = ball_x > H_RES/2 ? (abs_paddleR_movement < PADDLE_MAX_S ? abs_paddleR_movement : PADDLE_MAX_S) : 0;
            //
            paddleL_next_speed_stage1 <= (abs_paddleL_movement < PADDLE_MAX_S);
            paddleR_next_speed_stage1 <= (abs_paddleR_movement < PADDLE_MAX_S);
            paddleL_next_speed_stage2 <= (paddleL_next_speed_stage1 ? abs_paddleL_movement : PADDLE_MAX_S);
            paddleR_next_speed_stage2 <= (paddleR_next_speed_stage1 ? abs_paddleR_movement : PADDLE_MAX_S);
            paddleL_next_speed_stage3 <= (paddleL_receiving_ball_stage1 ? paddleL_next_speed_stage2 : 0);
            paddleR_next_speed_stage3 <= (paddleR_receiving_ball_stage1 ? paddleR_next_speed_stage2 : 0);

            // Get paddle next direction and randomize it a little. No pipeline required.
            reverse_l_dir    <= ~|(random_l[15:12]);                                                    // True if random_l < 2^12.
            reverse_r_dir    <= ~|(random_r[15:12]);                                                    // True if random_r < 2^12.
            paddleL_next_dir <= paddleL_movement_stage3[10] ^ reverse_l_dir;
            paddleR_next_dir <= paddleR_movement_stage3[10] ^ reverse_r_dir;

            // Get next scores. Needs a 2-staged pipeline.
            // Increment the scores when the ball collide with the holes (after 9 go back to 0), i.e.:
            //
            // * scoreR_next_val = coll_holeL ? ((scoreR_val + 1) > 9 ? 0 : (scoreR_val + 1)) : scoreR_val;
            // * scoreL_next_val = coll_holeR ? ((scoreL_val + 1) > 9 ? 0 : (scoreL_val + 1)) : scoreL_val;
            //
            scoreL_next_val_stage1 <= (scoreL_val >= 9 ? 0 : (scoreL_val + 1));
            scoreR_next_val_stage1 <= (scoreR_val >= 9 ? 0 : (scoreR_val + 1));
            scoreL_next_val_stage2 <= coll_holeR ? scoreL_next_val_stage1 : scoreL_val;
            scoreR_next_val_stage2 <= coll_holeL ? scoreR_next_val_stage1 : scoreR_val;

            // Update the scores, the position/direction of the ball and the position/direction/speed
            // of the paddles. As stated above, this update occurs only at the beginning of the vertical
            // blanking period, for a single clock cycle (i.e. once per frame). The above pipelines (which
            // use these variables as primary inputs) stabilize quickly, so for all pixels in the active
            // display area, both these variables and the outputs of the above pipelines are constant.
            //
            // You may be wondering: if there's a lot of time for signal propagation why have I implemented
            // the above logic using pipelines and not simple combinational logic? I did it at the beginning,
            // but it didn't work. I got several artifacts on the display, unexpected ball jumps and other
            // weird effects, simply modifying the horizontal ball speed (or other parameters) of +/- 1px!
            // I think the reason is that Yosys globally optimize the logic based on the expected max timing,
            // and an excessive propagation time in the above logic had negatively affected the synthesis of
            // the hardware for objects visualization (see next section) that is critical (it must perform
            // many compares at 108Mhz). Pipelining the above logic, the estimated propagation time dropped
            // from about 37ns to about 10.2ns (Fmax ~98MHz, still < 108MHz, but it seems to work anyway).
            if (start_vertical_blanking) begin

                // Update the scores.
                scoreL_val <= scoreL_next_val_stage2;
                scoreR_val <= scoreR_next_val_stage2;

                // Update the X,Y position of the ball.
                ball_x <= ball_next_x;
                ball_y <= ball_next_y;

                // Update the X,Y direction of the ball.
                ball_move_left <= ball_next_xdir;
                ball_move_up   <= ball_next_ydir;

                // Update the Y position of the paddles.
                paddleL_y <= paddleL_next_y_stage3;
                paddleR_y <= paddleR_next_y_stage3;

                // Update the speed of the paddles.
                paddleL_speed <= paddleL_next_speed_stage3;
                paddleR_speed <= paddleR_next_speed_stage3;

                // Update the Y direction of the paddles.
                paddleL_move_up <= paddleL_next_dir;
                paddleR_move_up <= paddleR_next_dir;
            end
        end
    end


    ////////////////////////////////////////////////////////////////////////
    // Visualize the objects on the screen, drawing a double border, the  //
    // ball, the paddles and the scores. Note: the external border is     //
    // useful to check if the image is aligned to the synchronisms.       //
    //                                                                    //
    //          ****** TIMING OF THIS SECTION IS CRITICAL ******          //
    //                                                                    //
    // The iCE40 LP8K on the TinyFPGA BX is not fast enough to perform    //
    // the comparisons required to display a generic object on the screen //
    // (i.e. x>=obj_x && x<obj_x+OBJ_XSZ && y>=obj_y && y<obj_y+OBJ_YSZ)  //
    // at such pixel clock rate. The combinational delay will take longer //
    // than the pixel clock period, probably causing visual artifacts. To //
    // solve this, we must split the above test, and pipeline the result  //
    // over several cycles (and delay opportunely h/v syncs & data enable //
    // signal). Note that issues like this (due to the propagation time)  //
    // cannot be detected with a simulation, but only on real hardware.   //
    ////////////////////////////////////////////////////////////////////////

    // UNLIKE THE PREVIOUS SECTION, PIPELINES USED HERE **MUST HAVE**
    // THE SAME LENGTH: THE INPUTS OF THESE PIPELINES CHANGE WITH EACH
    // PIXEL, WHILE THEIR OUTPUTS ARE CONSTANTLY USED TO DETERMINE WHICH
    // OBJECT HAS TO BE DRAWN ON SCREEN, SO SUCH OUTPUTS MUST BE IN SYNC.

    // Registers and wires used for visualization of the scores (see below).
    // NOTE: bits [10:7,3:0] in the 'digit*' registers are not used other than
    // as recipient for the result of a calculus. Verilator may complains about
    // the unused bits, so we must disable such warning around these variables.
    // verilator lint_off UNUSED
    reg [10:0] digitY_stage1;
    reg [10:0] digitL_stage1;
    reg [10:0] digitR_stage1;
    // verilator lint_on UNUSED
    reg        scoreY_stage1_a;
    reg        scoreY_stage1_b;
    reg        scoreL_stage1_a;
    reg        scoreL_stage1_b;
    reg        scoreR_stage1_a;
    reg        scoreR_stage1_b;
    wire       scoreL_stage2_dispbit;
    wire       scoreR_stage2_dispbit;
    reg        scoreL_stage2;
    reg        scoreR_stage2;
    reg        scoreL_stage3;
    reg        scoreR_stage3;

    // Registers used for visualization of borders and midline (see below).
    reg paddlezone_stage1_a;
    reg paddlezone_stage1_b;
    reg border1_stage1_a;
    reg border1_stage1_b;
    reg border1_stage1_c;
    reg border1_stage1_d;
    reg border2_stage1_a;
    reg border2_stage1_b;
    reg border2_stage1_c;
    reg border2_stage1_d;
    reg midline_stage1_a;
    reg midline_stage1_b;
    reg midline_stage1_c;
    reg border1_stage2;
    reg border2_stage2;
    reg midline_stage2;
    reg border1_stage3;
    reg border2_stage3;
    reg midline_stage3;

    // Registers used for visualization of the paddles (see below).
    reg paddleL_stage1_a;
    reg paddleL_stage1_b;
    reg paddleR_stage1_a;
    reg paddleR_stage1_b;
    reg paddleL_stage2;
    reg paddleR_stage2;
    reg paddleL_stage3;
    reg paddleR_stage3;

    // Registers used for visualization of the ball (see below).
    reg ball_stage1_a;
    reg ball_stage1_b;
    reg ball_stage1_c;
    reg ball_stage1_d;
    reg ball_stage2;
    reg ball_stage3;

    // Registers used to delay the "data_enabled" signal (see below).
    reg dataenable_stage1;
    reg dataenable_stage2;
    reg dataenable_stage3;

    // Registers used to assign the pixel's color.
    reg [2:0] vga_RGB;

    // Positions of the scores's digits.
    localparam SCORE_Y = 80;
    localparam SCORE_L_X = 480;
    localparam SCORE_R_X = 720;

    // These modules map a numeric digit to display bits, based on x,y offsets.
    // Note1: digits have top left origin, mappend to x_offet = 0, y_offset = 0.
    getbit_bitmap5x5_digit09 DIGITL (
        .i__clk(clk_108MHz),
        .i__rst(rst_master),
        .i__digit(scoreL_val),                                      // Value costant in the active display area (see previous section).
        .i__y_offset(digitY_stage1[6:4]),                           // 16x zoom.
        .i__x_offset(digitL_stage1[6:4]),                           // 16x zoom.
        .o__bit(scoreL_stage2_dispbit)
    );
    getbit_bitmap5x5_digit09 DIGITR (
        .i__clk(clk_108MHz),
        .i__rst(rst_master),
        .i__digit(scoreR_val),                                      // Value costant in the active display area (see previous section).
        .i__y_offset(digitY_stage1[6:4]),                           // 16x zoom.
        .i__x_offset(digitR_stage1[6:4]),                           // 16x zoom.
        .o__bit(scoreR_stage2_dispbit)
    );

    // Draw the objects on the screen.
    always @(posedge clk_108MHz, posedge rst_master) begin
        if (rst_master) begin

            // Registers and wires used for visualization of the scores.
            digitY_stage1       <= 0;
            digitL_stage1       <= 0;
            digitR_stage1       <= 0;
            scoreY_stage1_a     <= 1'b0;
            scoreY_stage1_b     <= 1'b0;
            scoreL_stage1_a     <= 1'b0;
            scoreL_stage1_b     <= 1'b0;
            scoreR_stage1_a     <= 1'b0;
            scoreR_stage1_b     <= 1'b0;
            scoreL_stage2       <= 1'b0;
            scoreR_stage2       <= 1'b0;
            scoreL_stage3       <= 1'b0;
            scoreR_stage3       <= 1'b0;

            // Registers used for visualization of borders and midline.
            paddlezone_stage1_a <= 1'b0;
            paddlezone_stage1_b <= 1'b0;
            border1_stage1_a    <= 1'b0;
            border1_stage1_b    <= 1'b0;
            border1_stage1_c    <= 1'b0;
            border1_stage1_d    <= 1'b0;
            border2_stage1_a    <= 1'b0;
            border2_stage1_b    <= 1'b0;
            border2_stage1_c    <= 1'b0;
            border2_stage1_d    <= 1'b0;
            midline_stage1_a    <= 1'b0;
            midline_stage1_b    <= 1'b0;
            midline_stage1_c    <= 1'b0;
            border1_stage2      <= 1'b0;
            border2_stage2      <= 1'b0;
            midline_stage2      <= 1'b0;
            border1_stage3      <= 1'b0;
            border2_stage3      <= 1'b0;
            midline_stage3      <= 1'b0;

            // Registers used for visualization of the paddles.
            paddleL_stage1_a    <= 1'b0;
            paddleL_stage1_b    <= 1'b0;
            paddleR_stage1_a    <= 1'b0;
            paddleR_stage1_b    <= 1'b0;
            paddleL_stage2      <= 1'b0;
            paddleR_stage2      <= 1'b0;
            paddleL_stage3      <= 1'b0;
            paddleR_stage3      <= 1'b0;

            // Registers used for visualization of the ball.
            ball_stage1_a       <= 1'b0;
            ball_stage1_b       <= 1'b0;
            ball_stage1_c       <= 1'b0;
            ball_stage1_d       <= 1'b0;
            ball_stage2         <= 1'b0;
            ball_stage3         <= 1'b0;

            // Registers used to delay the "data_enabled" signal.
            dataenable_stage1   <= 1'b0;
            dataenable_stage2   <= 1'b0;
            dataenable_stage3   <= 1'b0;

        end else begin

            // Scores on-screen flags. Use pipelines to:
            //
            // * extract bits 'dispbit' from the digits of the scores, corresponding to the current pixel (if valid), see module 'getbit_bitmap5x5_digit09()'.
            // * get flag scoreL = (x_pt >= SCORE_L_X) && (x_pt < (SCORE_L_X+8*(1<<4))) && (y_pt >= SCORE_Y) && (y_pt < (SCORE_Y+8*(1<<4))) && scoreL_stage2_dispbit;
            // * get flag scoreR = (x_pt >= SCORE_R_X) && (x_pt < (SCORE_R_X+8*(1<<4))) && (y_pt >= SCORE_Y) && (y_pt < (SCORE_Y+8*(1<<4))) && scoreR_stage2_dispbit;
            //
            // To obtain the final flags a 3-stage pipeline is required. This is the longest
            // pipeline (other pipelines in this section must have the same length of this one).
            digitY_stage1   <= (y_pt - SCORE_Y);
            digitL_stage1   <= (x_pt - SCORE_L_X);
            digitR_stage1   <= (x_pt - SCORE_R_X);
            scoreY_stage1_a <= (y_pt >= SCORE_Y);
            scoreY_stage1_b <= (y_pt < (SCORE_Y + 8*(1<<4)));       // 16x zoom.
            scoreL_stage1_a <= (x_pt >= SCORE_L_X);
            scoreL_stage1_b <= (x_pt < (SCORE_L_X + 8*(1<<4)));     // 16x zoom.
            scoreR_stage1_a <= (x_pt >= SCORE_R_X);
            scoreR_stage1_b <= (x_pt < (SCORE_R_X + 8*(1<<4)));     // 16x zoom.
            scoreL_stage2   <= scoreL_stage1_a && scoreL_stage1_b && scoreY_stage1_a && scoreY_stage1_b;
            scoreR_stage2   <= scoreR_stage1_a && scoreR_stage1_b && scoreY_stage1_a && scoreY_stage1_b;
            scoreL_stage3   <= scoreL_stage2 && scoreL_stage2_dispbit;
            scoreR_stage3   <= scoreR_stage2 && scoreR_stage2_dispbit;

            // Borders on-screen flags. Use pipelines to calculate:
            //
            // * paddlezone = (y_pt >= PADDLE_Y_MIN) && (y_pt <= PADDLE_Y_MAX);
            // * border1 = (((x_pt == 0) || (x_pt == X_MAX-0)) && !paddlezone) || (y_pt == 0) || (y_pt == Y_MAX-0);
            // * border2 = (((x_pt < BORDER_L) || (x_pt > BORDER_R)) && !paddlezone) || (y_pt < BORDER_T) || (y_pt > BORDER_B);
            // * midline = (x_pt >= H_RES/2 - BORDER_W/2) && (x_pt <= H_RES/2 + BORDER_W/2) && (y_pt[2] == 0);
            //
            // These tests require a 2-stage pipeline, but we need another stage, to be aligned with the above pipeline.
            paddlezone_stage1_a <= (y_pt >= PADDLE_Y_MIN);
            paddlezone_stage1_b <= (y_pt <= PADDLE_Y_MAX);
            border1_stage1_a    <= (x_pt == 0);
            border1_stage1_b    <= (x_pt == X_MAX-0);
            border1_stage1_c    <= (y_pt == 0);
            border1_stage1_d    <= (y_pt == Y_MAX-0);
            border2_stage1_a    <= (x_pt < BORDER_L);
            border2_stage1_b    <= (x_pt > BORDER_R);
            border2_stage1_c    <= (y_pt < BORDER_T);
            border2_stage1_d    <= (y_pt > BORDER_B);
            midline_stage1_a    <= (x_pt >= H_RES/2 - BORDER_W/2);
            midline_stage1_b    <= (x_pt <= H_RES/2 + BORDER_W/2);
            midline_stage1_c    <= (((y_pt + 2) & 4) == 0); //(y_pt[2] == 0);
            border1_stage2      <= ((border1_stage1_a || border1_stage1_b) && !(paddlezone_stage1_a && paddlezone_stage1_b)) || border1_stage1_c || border1_stage1_d;
            border2_stage2      <= ((border2_stage1_a || border2_stage1_b) && !(paddlezone_stage1_a && paddlezone_stage1_b)) || border2_stage1_c || border2_stage1_d;
            midline_stage2      <= midline_stage1_a && midline_stage1_b && midline_stage1_c;
            border1_stage3      <= border1_stage2;
            border2_stage3      <= border2_stage2;
            midline_stage3      <= midline_stage2;

            // Paddles on-screen flags. Use pipelines to calculate:
            //
            // * paddleL = (x_pt < BORDER_L) && (y_pt >= paddleL_y) && (y_pt < paddleL_y + PADDLE_H);
            // * paddleR = (x_pt > BORDER_R) && (y_pt >= paddleR_y) && (y_pt < paddleR_y + PADDLE_H);
            //
            // As stated above, 'paddleL_y' and 'paddleR_y' are modified only at the beginning of the vertical blanking period,
            // so (paddleL_y + PADDLE_H) and (paddleR_y + PADDLE_H) should be settled at 'data_enabled' (active display area).
            // These tests require a 2-stage pipeline, but we need another stage, to be aligned with the above pipelines.
            paddleL_stage1_a <= (y_pt >= paddleL_y);
            paddleL_stage1_b <= (y_pt < (paddleL_y + PADDLE_H));
            paddleR_stage1_a <= (y_pt >= paddleR_y);
            paddleR_stage1_b <= (y_pt < (paddleR_y + PADDLE_H));
            paddleL_stage2   <= border2_stage1_a && paddleL_stage1_a && paddleL_stage1_b;
            paddleR_stage2   <= border2_stage1_b && paddleR_stage1_a && paddleR_stage1_b;
            paddleL_stage3   <= paddleL_stage2;
            paddleR_stage3   <= paddleR_stage2;

            // Ball on-screen flag. Use pipeline to calculate:
            //
            // * ball = (x_pt >= ball_x) && (x_pt < (ball_x + BALL_SZ)) && (y_pt >= ball_y) && (y_pt < (ball_y + BALL_SZ));
            //
            // As stated above, 'ball_x' and 'ball_y' are modified only at the beginning of the vertical blanking period,
            // so (ball_x + BALL_SZ) and (ball_y + BALL_SZ) should be settled at 'data_enabled' (active display area).
            // This test requires a 2-stage pipeline, but we need another stage, to be aligned with the above pipelines.
            ball_stage1_a <= (x_pt >= ball_x);
            ball_stage1_b <= (x_pt < (ball_x + BALL_SZ));
            ball_stage1_c <= (y_pt >= ball_y);
            ball_stage1_d <= (y_pt < (ball_y + BALL_SZ));
            ball_stage2   <= ball_stage1_a && ball_stage1_b && ball_stage1_c && ball_stage1_d;
            ball_stage3   <= ball_stage2;

            // Delay the 'data_enable' signal with a 3-stage pipeline (to be aligned with the above pipelines).
            dataenable_stage1 <= data_enable;
            dataenable_stage2 <= dataenable_stage1;
            dataenable_stage3 <= dataenable_stage2;

            // Set the pixel color.
            vga_RGB <= 3'b000;                                      // By default black (i.e. 0V).
            if (dataenable_stage3) begin                            // Set color only in active display area.
                if (ball_stage3)
                    vga_RGB <= 3'b111;                              // Ball (WHITE) - Highest priority.
                else if (scoreL_stage3 || scoreR_stage3)
                    vga_RGB <= 3'b100;                              // Score (RED).
                else if (paddleL_stage3 || paddleR_stage3)
                    vga_RGB <= 3'b101;                              // Paddles (PURPLE).
                else if (border1_stage3)
                    vga_RGB <= 3'b111;                              // Outer border (WHITE).
                else if (border2_stage3)
                    vga_RGB <= 3'b001;                              // Bouncing border (BLUE).
                else if (midline_stage3)
                    vga_RGB <= 3'b001;                              // Field game separator (BLUE) - Lowest priority.
            end
        end
    end

    assign PIN_1 = vga_RGB[2];                                      // R
    assign PIN_2 = vga_RGB[1];                                      // G
    assign PIN_3 = vga_RGB[0];                                      // B


    /////////////////////////////////////////////////////////////////////////////////////
    // Delay h/v syncs for four clock cycles, to compensate for the above pipelines    //
    // (3 stages + one additional clock required by the assignment of 'vga_RGB').      //
    //                                                                                 //
    // NOTE: syncs are sampled only when 'clk_108MHz' becomes valid: at the beginning  //
    // they are: 1# not initialized in the behavioral simulation with Icarus Verilog;  //
    // 2# randomized in behavioral simulation with Verilator; 3# set to 0 by Yosys in  //
    // both post-synthesis simulations (with Icarus Verilog / Verilator), and on real  //
    // hardware. Anyway it's not required to reset them to the inactive states (which  //
    // depend on the video mode), because the monitor is still able to sync, no matter //
    // their initial values (both in SDL2 simulation and with real synthesized logic). //
    /////////////////////////////////////////////////////////////////////////////////////

    reg [3:0] vga_HS, vga_VS;

    always @(posedge clk_108MHz) begin
        vga_HS <= {vga_HS[2:0], h_sync};
        vga_VS <= {vga_VS[2:0], v_sync};
    end

    assign PIN_4 = vga_HS[3];
    assign PIN_5 = vga_VS[3];


    ///////////////////////////////////////////////
    // This is required only for the simulation, //
    // or to check the pixel clock with a scope. //
    ///////////////////////////////////////////////

    assign PIN_6 = clk_108MHz;


    ////////////////////
    // Blink the LED. //
    ////////////////////

    `ifdef SIMULATION
      `define BLINK_FREQ_HZ     5000    // Simulation.
    `else
      `define BLINK_FREQ_HZ     5       // Build.
    `endif

    reg status = 0;
    reg [26:0] counter = 0;
    localparam MAXCOUNT = 108000000/(`BLINK_FREQ_HZ*2)-1;

    always @(posedge clk_108MHz) begin
        if (counter == MAXCOUNT)
            begin
                counter <= 0;
                status <= ~status;
            end
        else
            counter <= counter + 1;
    end

    assign LED = status;

endmodule
