
// (C) 2021-2025 Flavio Cappelli
// Released under the MIT License.
//
// Return the indexed bit of a 5x5 bitmap with digit 0-9.

`default_nettype none               // Do not allow undeclared signals.
`include "timescale.vh"             // Timescale defined globally.


module getbit_bitmap5x5_digit09 (
    input  wire       i__clk,       // Clock, active on positive edge.
    input  wire       i__rst,       // Reset, active high, asynchronous.
    input  wire [3:0] i__digit,     // Digit 0-9.
    input  wire [2:0] i__y_offset,  // Vertical offset (0-4).
    input  wire [2:0] i__x_offset,  // Horizontal offset (0-4).
    output wire       o__bit        // Data output (1 bit).
);

    // verilator lint_off LITENDIAN
    reg outb;                       // Output bit.
    reg [0:4] line;                 // Reverse bits (natural display order).
    // verilator lint_on LITENDIAN

    always @(*) begin
        case ({i__digit, i__y_offset})

            // Digit 0.
            7'o00:  line = 5'b11111;
            7'o01:  line = 5'b10001;
            7'o02:  line = 5'b10001;
            7'o03:  line = 5'b10001;
            7'o04:  line = 5'b11111;

            // Digit 1.
            7'o10:  line = 5'b01100;
            7'o11:  line = 5'b00100;
            7'o12:  line = 5'b00100;
            7'o13:  line = 5'b00100;
            7'o14:  line = 5'b11111;

            // Digit 2.
            7'o20:  line = 5'b11111;
            7'o21:  line = 5'b00001;
            7'o22:  line = 5'b11111;
            7'o23:  line = 5'b10000;
            7'o24:  line = 5'b11111;

            // Digit 3.
            7'o30:  line = 5'b11111;
            7'o31:  line = 5'b00001;
            7'o32:  line = 5'b11111;
            7'o33:  line = 5'b00001;
            7'o34:  line = 5'b11111;

            // Digit 4.
            7'o40:  line = 5'b10001;
            7'o41:  line = 5'b10001;
            7'o42:  line = 5'b11111;
            7'o43:  line = 5'b00001;
            7'o44:  line = 5'b00001;

            // Digit 5.
            7'o50:  line = 5'b11111;
            7'o51:  line = 5'b10000;
            7'o52:  line = 5'b11111;
            7'o53:  line = 5'b00001;
            7'o54:  line = 5'b11111;

            // Digit 6.
            7'o60:  line = 5'b11111;
            7'o61:  line = 5'b10000;
            7'o62:  line = 5'b11111;
            7'o63:  line = 5'b10001;
            7'o64:  line = 5'b11111;

            // Digit 7.
            7'o70:  line = 5'b11111;
            7'o71:  line = 5'b00001;
            7'o72:  line = 5'b00001;
            7'o73:  line = 5'b00001;
            7'o74:  line = 5'b00001;

            // Digit 8.
            7'o100:  line = 5'b11111;
            7'o101:  line = 5'b10001;
            7'o102:  line = 5'b11111;
            7'o103:  line = 5'b10001;
            7'o104:  line = 5'b11111;

            // Digit 9.
            7'o110:  line = 5'b11111;
            7'o111:  line = 5'b10001;
            7'o112:  line = 5'b11111;
            7'o113:  line = 5'b00001;
            7'o114:  line = 5'b11111;

            // Invalid digits.
            default: line = 5'b00000;
        endcase
    end

    always @(posedge i__clk, posedge i__rst) begin
        if (i__rst)
            outb <= 1'b0;
        else
            outb <= (i__x_offset <= 4) ? line[i__x_offset] : 1'b0;
    end

    assign o__bit = outb;

endmodule
