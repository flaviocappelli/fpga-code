# Clock constraints for NextPNR (clock name, frequency in MHz). See:
# https://github.com/YosysHQ/nextpnr/blob/master/docs/constraints.md

ctx.addClock("CLK_16MHz", 16)
ctx.addClock("clk_216MHz", 216)

# 108Mhz always fails but 105MHz works and seems to be enough.
ctx.addClock("clk_108MHz", 105)
