
PROJECT DESCRIPTION
-------------------
Show a demo of the Pong Game on a VGA display (using video
mode 1280x1024 @ 60Hz). Also the user LED is made to blink
(to show that the logic is working). Note: this is a demo
and not a game because the two paddles move by themselves,
emulating human players (more or less).

It was a challenge running this demo with such video mode:
the iCE40 LP8K on the TinyFPGA BX board is not fast enough
to execute complex boolean/math expressions at the required
pixel clock rate (108 MHz). I had to make heavy use of the
pipelining technique (i.e. decompose complex combinational
expressions into elementary sequential operations, that
can be performed within the clock cycle).

NOTE: due to the strict timings involved in the design, the
success of the Place and Route stage depends on the version
of the synthesis tools. Unfortunately, the latest releases
of "OSS CAD Suite" fails to meet the timing requirements. I
have verified that "OSS CAD Suite" tagged "2022-10-02" works
(if the first attempt fails, try the build more times).



REQUIRED HARDWARE
-----------------
You need a VGA display and some components, see the project
"10_vga_vertical_stripes". Also, make sure your VGA display
supports the video mode 1280x1024 @ 60Hz.



SIMULATION
----------
The following simulations are provided:

 * Behavioral and post-synthesis Verilog simulations, performed
   with Icarus Verilog. The generated output signals (on the PINs
   of the TinyFPGA BX) are merely displayed in GtkWave.

 * Behavioral and post-synthesis C++ simulations, performed with
   Verilator. The generated output signals can still be displayed
   in GtkWave. Also we can emulate the VGA output on the PC screen
   (this simulation uses the SDL2 library, that must be installed).

Note that the C++ simulation is faster than the Verilog simulation
but not fast enough to run in real-time, so don't expect too many
FPS (frames per seconds) on the emulated VGA output.

WARNIGS: This example uses lot of logic, so the 'pssim' simulation
         will take some minutes (on modern computers). BE PATIENT.
         All other simulations are faster than this one.



INSTRUCTIONS
------------
  - code check
     * type "make -f ../../make/Makefile verify"
     * type "make -f ../../make/Makefile lint"

  - simulation
     * type "make -f ../../make/Makefile sim"
       (behavioral, Icarus Verilog, run GtkWave)

     * type "make -f ../../make/Makefile pssim"
       (post-synthesis, Icarus Verilog, run GtkWave)

     * type "make -f ../../make/Makefile vsim"
       (behavioral, Verilator/C++, run GtkWave)

     * type "make -f ../../make/Makefile vpssim"
       (post-synthesis, Verilator/C++, run GtkWave)

     * type "make -f ../../make/Makefile vsimto"
       (behavioral, Verilator/C++, emulate VGA with SDL2)

     * type "make -f ../../make/Makefile vpssimto"
       (post-synthesis, Verilator/C++, emulate VGA with SDL2)

  - programming
     * type "make -f ../../make/Makefile clean"
     * type "make -f ../../make/Makefile build"
     * press the reset button on the TinyFPGA-BX
     * type "../upload_TinyFPGA_BX.sh"
     * after few seconds the USER LED will start to blink
       and the output pins will emit their signals to draw
       the generated frames on the connected VGA display.



REFERENCES
----------
The "doc" folder is a link to useful documentation
and VGA specifications.
