
// (C) 2021-2025 Flavio Cappelli
// Released under the MIT License.
//
// See https://verilator.org/guide/latest/
//     https://projectf.io/posts/verilog-sim-verilator-sdl
//     https://zipcpu.com/blog/2017/06/21/looking-at-verilator.html
//
// NOTE: Verilator 4.222 or above is required.
//
// NOTE: Verilator build C++ sources with -std=gnu++14.

#include "_simul_hlp.hxx"           // Helper classes and functions.

#include <cstdlib>                  // Use: EXIT_SUCCESS, EXIT_FAILURE, exit.

#if !VM_TRACE_FST
#define SIMULATION_TIME    20*_s    // Simulation time with SDL2 (max 5h7m26s).
#else
#define SIMULATION_TIME    20*_ms   // Simulation time with trace generation (max 5h7m26s).
#endif

#if !VM_TRACE_FST
static void init_sdl2();
static bool update_sdl2();          // Must return TRUE on error.
static void show_sdl2_fps();        // Display frame per second.
static void cleanup_sdl2();

#define UPDATE_SDL2_OK     0        // Return values for update_sdl2().
#define UPDATE_SDL2_ERR    1
#endif


extern "C"                          // Required by SDL/SDL2 in Windows!
int main(int argc, char **argv)
{
    // Initialize SDL2.
    #if !VM_TRACE_FST
    init_sdl2();
    #endif

    // Initialize the Verilator's environment.
    verilator_init(argc, argv);

    // Schedule the TinyFPGA-BX 16MHz clock simulation.
    verilator_sched_clk(31.25*_ns, CLK_16MHz, 16.0*_MHz);

    // Install the user's handler (the handler is invoked after
    // all event's callbacks, associated with the same deadline,
    // have been invoked). Such user's handler must return TRUE
    // on errors, to stop the event handling loop and force exit.
    #if !VM_TRACE_FST
    verilator_install_handler(update_sdl2);
    #endif

    // Handle the scheduled events (until the simulation
    // time expires or all events have been processed).
    verilator_exec(SIMULATION_TIME);

    // Cleanup the Verilator's environment.
    verilator_cleanup();

    // Show frames/s and cleanup SDL2.
    #if !VM_TRACE_FST
    show_sdl2_fps();
    cleanup_sdl2();
    #endif

    // All ok.
    return EXIT_SUCCESS;
}



// ------------------- Screen emulation using SDL2 library -------------------

#if !VM_TRACE_FST
#include <SDL.h>
#include <iostream>
#include <iomanip>


// Parameters for video mode 1024x768 @ 60Hz.
static const int H_RES = 1024;
static const int V_RES = 768;
static const int H_BACK_PORCH = 160;
static const int V_BACK_PORCH = 29;
static const bool HSYNC_POLARITY = 0;
static const bool VSYNC_POLARITY = 0;


typedef struct pixel_t {    // For SDL texture:
    uint8_t a;              //   Transparency (alpha).
    uint8_t b;              //   Blue.
    uint8_t g;              //   Green.
    uint8_t r;              //   Red.
} pixel_t;

static pixel_t screen_buffer[H_RES*V_RES];

static SDL_Window*   sdl_window   = NULL;
static SDL_Renderer* sdl_renderer = NULL;
static SDL_Texture*  sdl_texture  = NULL;

static uint64_t frame_count = 0;
static uint64_t start_ticks = SDL_GetPerformanceCounter();


static void init_sdl2()
{
    if (SDL_Init(SDL_INIT_VIDEO) < 0) {
        std::cout << "SDL init failed" << std::endl << std::flush;
        exit(EXIT_FAILURE);
    }

    sdl_window = SDL_CreateWindow("SDL2 Screen",
                                  SDL_WINDOWPOS_CENTERED,
                                  SDL_WINDOWPOS_CENTERED,
                                  H_RES, V_RES,
                                  SDL_WINDOW_SHOWN
    );

    if (!sdl_window) {
        std::cout << "Window creation failed: " << SDL_GetError() << std::endl << std::flush;
        exit(EXIT_FAILURE);
    }

    sdl_renderer = SDL_CreateRenderer(sdl_window,
                                      -1,
                                      SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC
    );

    if (!sdl_renderer) {
        std::cout << "Renderer creation failed: " << SDL_GetError() << std::endl << std::flush;
        exit(EXIT_FAILURE);
    }

    sdl_texture = SDL_CreateTexture(sdl_renderer,
                                    SDL_PIXELFORMAT_RGBA8888,
                                    SDL_TEXTUREACCESS_TARGET,
                                    H_RES, V_RES
    );

    if (!sdl_texture) {
        std::cout << "Texture creation failed: " << SDL_GetError() << std::endl << std::flush;
        exit(EXIT_FAILURE);
    }
}



static bool update_sdl2()
{
    // FPGA I/O signals.
    const uint8_t & red   = vtop->PIN_1 ? 0xFF : 0;
    const uint8_t & green = vtop->PIN_2 ? 0xFF : 0;
    const uint8_t & blue  = vtop->PIN_3 ? 0xFF : 0;
    const uint8_t & hsync = vtop->PIN_4;
    const uint8_t & vsync = vtop->PIN_5;
    const uint8_t & pxclk = vtop->PIN_6;

    // *** NOTHING TO CHANGE BELOW THIS LINE ***

    // Variables used to get pixel position from hsync/vsync/pxclk.
    static int x_pos = 0, y_pos = 0;
    static uint8_t last_hsync = hsync;
    static uint8_t last_vsync = vsync;
    static uint8_t last_pxclk = 0;
    static bool vs_lock = false;

    // Detect changes in the pixel clock.
    if (last_pxclk != pxclk)
    {
        // Clock posedge?
        if (pxclk)
        {
            // Increment pixel position on the line.
            ++x_pos;

            // Detect changes in the hsync pulse.
            if (last_hsync != hsync)
            {
                // Detect the end of hsync pulse (active -> inactive).
                if (bool(hsync) != HSYNC_POLARITY)
                {
                    // Start the next line.
                    x_pos = -H_BACK_PORCH;
                    ++y_pos;
                }

                // Update the stored hsync.
                last_hsync = hsync;
            }

            // Detect changes in the vsync pulse.
            if (last_vsync != vsync)
            {
                // Detect the end of vsync pulse (active -> inactive).
                if (bool(vsync) != VSYNC_POLARITY)
                {
                    // Start the next frame and notify the lock of vertical sync.
                    y_pos = -V_BACK_PORCH;
                    vs_lock = true;
                }

                // Update the stored vsync.
                last_vsync = vsync;
            }

            // Perform actions only if the vertical sync is locked.
            if (vs_lock)
            {
                // Update pixel if not in blanking interval.
                if (x_pos >= 0 && x_pos < H_RES && y_pos >= 0 && y_pos < V_RES)
                {
                    pixel_t* p = &screen_buffer[y_pos * H_RES + x_pos];

                    p->a = 0xFF;    // Transparency, not used.
                    p->r = red;
                    p->g = green;
                    p->b = blue;
                }

                // Perform some checks at the beginnig of each line.
                if (x_pos == 0)
                {
                    // Look for for quit events.
                    SDL_Event e;
                    if (SDL_PollEvent(&e)) {
                        if (e.type == SDL_QUIT) {
                            return UPDATE_SDL2_ERR;     // Break the event loop.
                        }
                    }

                    // Update texture once per frame at start of blanking.
                    if (y_pos == V_RES)
                    {
                        SDL_UpdateTexture(sdl_texture, NULL, screen_buffer, H_RES*sizeof(pixel_t));
                        SDL_RenderClear(sdl_renderer);
                        SDL_RenderCopy(sdl_renderer, sdl_texture, NULL, NULL);
                        SDL_RenderPresent(sdl_renderer);
                        frame_count++;
                    }
                }
            }
        }

        // Update the stored pixel clock.
        last_pxclk = pxclk;
    }

    // No errors.
    return UPDATE_SDL2_OK;
}


static void show_sdl2_fps()
{
    uint64_t end_ticks = SDL_GetPerformanceCounter();
    double duration = ((double)(end_ticks-start_ticks))/SDL_GetPerformanceFrequency();
    double fps = (double)frame_count/duration;

    std::cout << std::fixed;
    std::cout << std::setprecision(3);
    std::cout << "Frames per second: " << fps << std::endl << std::flush;
}


static void cleanup_sdl2()
{
    SDL_DestroyTexture(sdl_texture);
    SDL_DestroyRenderer(sdl_renderer);
    SDL_DestroyWindow(sdl_window);
    SDL_Quit();
}

#endif  // !VM_TRACE_FST
