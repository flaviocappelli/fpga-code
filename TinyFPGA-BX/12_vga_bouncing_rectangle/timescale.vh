
// File included by all modules: set globally the timescale.
// Due to IEEE recommendation, Verilator 4.x require that the
// timescale is defined in all modules of the project.

`ifndef _timescale_vh_
`define _timescale_vh_

`timescale 1ns/1ps               // Time-unit / Precision.

`endif  //_timescale_vh_


// The following line forces KDE's Kate editor to recognize
// this file as "Verilog" source (for syntax highlighting).
// kate: hl verilog;
