
// (C) 2021-2025 Flavio Cappelli
// Released under the MIT License.
//
// Draw a bouncing rectangle on a VGA display (using
// video mode 1024x768 @ 60Hz). Also the user LED is
// made to blink (to show that the logic is working).
//
// INPUT: none.
//
// OUPUT: see below.
//
// For the pin names on the TinyFPGA BX board see TinyFPGA-BX-pins.pcf

`default_nettype none       // Do not allow undeclared signals.
`include "timescale.vh"     // Timescale defined globally.

(* top *)                   // Mark this as top module, because
module TinyFPGA_BX (        // sometimes Yosys doesn't detect it.
    input  wire CLK_16MHz,  // 16MHz clock.
    output wire PIN_1,      // Output -> VGA Red signal.
    output wire PIN_2,      // Output -> VGA Green signal.
    output wire PIN_3,      // Output -> VGA Blue signal.
    output wire PIN_4,      // Output -> VGA HSync signal.
    output wire PIN_5,      // Output -> VGA VSync signal.
    output wire PIN_6,      // Output -> Pixel clock (required for SDL2 C++ simulation).
    output wire LED,        // User/boot LED (blink).
    output wire USBPU       // USB pull-up resistor.
);
    // USB not used (drive USB pull-up resistor to '0'
    // to avoid the USB device enumeration by the OS).
    assign USBPU = 0;


    /////////////////////////////////////////////////////
    // Generate a 130MHz clock using the internal PLL. //
    /////////////////////////////////////////////////////

    wire clk_130MHz, pll_locked;        // PLL outputs.

    ice40_pll_130MHz_from_16MHz PLL (
        .i__clk_in( CLK_16MHz ),
        .o__clk_out( clk_130MHz ),
        .o__async_locked( pll_locked )
    );


    //////////////////////////////////////////////////
    // Obtain a perfectly square 65MHz clock. It is //
    // used as input for the video timing generator.//
    //////////////////////////////////////////////////

    reg clk_65MHz = 0;

    always @(posedge clk_130MHz)
        clk_65MHz <= ~clk_65MHz;


    ////////////////////////////////////////
    // Create a simple active high reset. //
    ////////////////////////////////////////

    wire rst_master;

    fpga_reset_generator RESET_GEN (
        .i__clk(clk_65MHz),
        .i__clk_en(pll_locked),
        .o__gen_rst(rst_master)
    );


    ///////////////////////////////////
    // Generate the video signals    //
    // (video mode 1024x768 @ 60Hz). //
    ///////////////////////////////////

    wire [9:0] y_pt;
    wire [10:0] x_pt;
    wire h_sync, v_sync, data_enable;

    video_generator_1024x768_60hz VIDEO_768P (
        .i__clk(clk_65MHz),
        .i__rst(rst_master),
        .o__xp(x_pt),
        .o__yp(y_pt),
        .o__hs(h_sync),
        .o__vs(v_sync),
        .o__de(data_enable)
    );


    /////////////////////////////////////
    // Animate the bouncing rectangle. //
    /////////////////////////////////////

    reg  [2:0] rect_c;                              // Rectangle color (changes on bounces).
    reg [10:0] rect_x;                              // Rectangle origin: X position (must have same size of x_pt).
    reg  [9:0] rect_y;                              // Rectangle origin: Y position (must have same size of y_pt).
    reg rect_move_left;                             // Rectangle movement: X direction (0 right, 1 left).
    reg rect_move_up;                               // Rectangle movement: Y direction (0 down, 1 up).

    // Parameters. Note: rectangle's origin is top left.
    localparam H_RES        = 1024;                 // Horizontal and vertical screen resolution
    localparam V_RES        = 768;                  // (must match the above video generator).
    localparam X_MAX        = H_RES - 1;
    localparam Y_MAX        = V_RES - 1;
    localparam BORDER_W     = 20;                   // Total width of screen border.
    localparam BORDER_T     = BORDER_W;             // Border top.
    localparam BORDER_L     = BORDER_W;             // Border left.
    localparam BORDER_R     = X_MAX - BORDER_W;     // Border right.
    localparam BORDER_B     = Y_MAX - BORDER_W;     // Border bottom.
    localparam RECT_X_SZ    = 200;                  // Rectangle horizontal size in pixels.
    localparam RECT_Y_SZ    = 100;                  // Rectangle vertical size in pixels.
    localparam RECT_X_POS   = 400;                  // Rectangle X starting position.
    localparam RECT_Y_POS   = 200;                  // Rectangle Y starting position.
    localparam RECT_X_SPEED = 3;                    // Rectangle X movements in pixels. Must be < min(BORDER_W).
    localparam RECT_Y_SPEED = 2;                    // Rectangle Y movements in pixels. Must be < min(BORDER_W).

    // Signal high for one pixel clock tick at the begin of vertical blanking.
    wire start_vertical_blanking = (y_pt == V_RES) && (x_pt == 0);

    // Planned next X,Y position of the rectangle.
    wire [10:0] rect_planned_next_x = rect_move_left ? rect_x - RECT_X_SPEED : rect_x + RECT_X_SPEED;
    wire  [9:0] rect_planned_next_y = rect_move_up   ? rect_y - RECT_Y_SPEED : rect_y + RECT_Y_SPEED;

    // Get collision between the rectangle and the borders. Note that such
    // collision is triggered on adjacent pixels (not overlapping pixels).
    wire coll_border_l = rect_planned_next_x <= BORDER_L;                   // Collision with left border.
    wire coll_border_r = rect_planned_next_x >= BORDER_R - (RECT_X_SZ-1);   // Collision with right border.
    wire coll_border_t = rect_planned_next_y <= BORDER_T;                   // Collision with top border.
    wire coll_border_b = rect_planned_next_y >= BORDER_B - (RECT_Y_SZ-1);   // Collision with bottom border.

    // Get next X position of the rectangle.
    wire [10:0] rect_next_x = coll_border_l ? BORDER_L :                    // On collision with left border move left as far as we can.
                              coll_border_r ? BORDER_R - (RECT_X_SZ-1) :    // On collision with right border move right as far as we can.
                              rect_planned_next_x;                          // Else update as planned.

    // Get next X direction of the rectangle.
    wire rect_next_x_dir = coll_border_l ? 1'b0 :                           // Collision with left border, next frame move right.
                           coll_border_r ? 1'b1 :                           // Collision with right border, next frame move left.
                           rect_move_left;                                  // Unchanged.

    // Get next Y position of the rectangle.
    wire [9:0] rect_next_y = coll_border_t ? BORDER_T :                     // On collision with top border move top as far as we can.
                             coll_border_b ? BORDER_B - (RECT_Y_SZ-1) :     // On collision with bottom border move bottom as far as we can.
                             rect_planned_next_y;                           // Else update as planned.

    // Get next Y direction of the rectangle.
    wire rect_next_y_dir = coll_border_t ? 1'b0 :                           // Collision with top border, next frame move down.
                           coll_border_b ? 1'b1 :                           // Collision with bottom border, next frame move up.
                           rect_move_up;                                    // Unchanged.

    // Get generic collision.
    wire collision = coll_border_l || coll_border_r | coll_border_t | coll_border_b;

    // Get next color for the rectangle.
    wire [2:0] rect_c_next = collision ? { rect_c[1:0], rect_c[2] } :       // On collision rotate colors.
                             rect_c;                                        // Unchanged.

    // Initialize and update rectangle properties.
    always @(posedge clk_65MHz, posedge rst_master) begin
        if (rst_master) begin

            rect_c <= 3'b110;                       // Initial rectangle color (YELLOW).
            rect_x <= RECT_X_POS;                   // Initial rectangle position.
            rect_y <= RECT_Y_POS;
            rect_move_left <= 0;                    // Initial rectangle moving direction.
            rect_move_up   <= 0;

        // Update properties: note that this happens only one time / frame, so the logic
        // above will have enough time to settle, despite the result of the time analisys.
        end else if (start_vertical_blanking) begin

            // Update the rectangle X,Y position.
            rect_x <= rect_next_x;
            rect_y <= rect_next_y;

            // Update the rectangle X,Y direction.
            rect_move_left <= rect_next_x_dir;
            rect_move_up   <= rect_next_y_dir;

            // Update the rectangle color.
            rect_c <= rect_c_next;

        end
    end


    //////////////////////////////////////////////////////
    // Draw a triple border and the bouncing rectangle. //
    // Note: the two outer borders are useful to check  //
    // if the image is aligned to the synchronisms.     //
    //                                                  //
    // TIMING OF THIS SECTION IS CRITICAL: compares are //
    // performed for each screen pixel, so propagation  //
    // time must be < pixel clock period. If we comment //
    // the above update code, we get for this section   //
    // (with -abc9) 11.68ns -> 85.64MHz > 65MHz -> ok!  //
    //////////////////////////////////////////////////////

    reg [2:0] vga_RGB;

    wire border_1 = (x_pt == 0) || (x_pt == X_MAX-0) || (y_pt == 0) || (y_pt == Y_MAX-0);
    wire border_2 = (x_pt == 1) || (x_pt == X_MAX-1) || (y_pt == 1) || (y_pt == Y_MAX-1);
    wire border_3 = (x_pt < BORDER_L) || (x_pt > BORDER_R) || (y_pt < BORDER_T) || (y_pt > BORDER_B);

    wire rectangle = (x_pt >= rect_x) && (x_pt < rect_x + RECT_X_SZ) && (y_pt >= rect_y) && (y_pt < rect_y + RECT_Y_SZ);

    always @(posedge clk_65MHz) begin
        vga_RGB <= 3'b000;                          // By default black (i.e. 0V).
        if (data_enable) begin
            if (border_1)
                vga_RGB <= 3'b100;                  // Outer border (RED).
            else if (border_2)
                vga_RGB <= 3'b110;                  // Middle border (YELLOW).
            else if (border_3)
                vga_RGB <= 3'b001;                  // Bouncing border (BLUE).
            else if (rectangle)
                vga_RGB <= rect_c;                  // Rectangle.
        end
    end

    assign PIN_1 = vga_RGB[2];                      // R
    assign PIN_2 = vga_RGB[1];                      // G
    assign PIN_3 = vga_RGB[0];                      // B


    /////////////////////////////////////////////////////////////////////////////////////
    // Add one clock cycle delay on the syncs to compensate for the pixel computation. //
    //                                                                                 //
    // NOTE: syncs are sampled only when 'clk_65MHz' becomes valid: at the beginning   //
    // they are: 1# not initialized in the behavioral simulation with Icarus Verilog;  //
    // 2# randomized in behavioral simulation with Verilator; 3# set to 0 by Yosys in  //
    // both post-synthesis simulations (with Icarus Verilog / Verilator), and on real  //
    // hardware. Anyway it's not required to reset them to the inactive states (which  //
    // depend on the video mode), because the monitor is still able to sync, no matter //
    // their initial values (both in SDL2 simulation and with real synthesized logic). //
    /////////////////////////////////////////////////////////////////////////////////////

    reg vga_HS, vga_VS;

    always @(posedge clk_65MHz) begin
        vga_HS <= h_sync;
        vga_VS <= v_sync;
    end

    assign PIN_4 = vga_HS;
    assign PIN_5 = vga_VS;


    ///////////////////////////////////////////////
    // This is required only for the simulation, //
    // or to check the pixel clock with a scope. //
    ///////////////////////////////////////////////

    assign PIN_6 = clk_65MHz;


    ////////////////////
    // Blink the LED. //
    ////////////////////

    `ifdef SIMULATION
      `define BLINK_FREQ_HZ     5000    // Simulation.
    `else
      `define BLINK_FREQ_HZ     5       // Build.
    `endif

    reg status = 0;
    reg [25:0] counter = 0;
    localparam MAXCOUNT = 65000000/(`BLINK_FREQ_HZ*2)-1;

    always @(posedge clk_65MHz) begin
        if (counter == MAXCOUNT)
            begin
                counter <= 0;
                status <= ~status;
            end
        else
            counter <= counter + 1;
    end

    assign LED = status;

endmodule
