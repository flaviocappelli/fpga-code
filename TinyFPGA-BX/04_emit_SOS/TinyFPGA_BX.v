
// (C) 2021-2025 Flavio Cappelli
// Released under the MIT License.
//
// Emit a continuous SOS signal.
//
// INPUT: none.
//
// OUPUT: LED.
//
// For the pin names on the TinyFPGA BX board see TinyFPGA-BX-pins.pcf

`default_nettype none       // Do not allow undeclared signals.
`include "timescale.vh"     // Timescale defined globally.

(* top *)                   // Mark this as top module, because
module TinyFPGA_BX (        // sometimes Yosys doesn't detect it.
    input  wire CLK_16MHz,  // 16MHz on-board clock.
    output wire LED,        // User/boot LED next to power LED.
    output wire USBPU       // USB pull-up resistor.
);
    // USB not used (drive USB pull-up resistor to '0'
    // to avoid the USB device enumeration by the OS).
    assign USBPU = 0;


    /////////////////////////////////////////
    // Blink with an SOS Morse Code Signal //
    /////////////////////////////////////////

    `ifdef SIMULATION
      `define PATTERN_SCAN_FREQ_HZ      2000    // Simulation.
    `else
      `define PATTERN_SCAN_FREQ_HZ      2       // Build.
    `endif

    // Reduce the frequency using a counter.
    reg [23:0] counter = 0;
    localparam MAXCOUNT = 16000000/(`PATTERN_SCAN_FREQ_HZ*2)-1;

    // Counter used as index in the blink pattern.
    reg [4:0] blink_counter = 0;

    // Pattern that will be written on the LED over time.
    wire [31:0] blink_pattern = 32'b00000000010101011101110111010101;

    // Increment the counter at every clock positive edge
    // (and the blink counter when the counter expires).
    always @(posedge CLK_16MHz) begin
        if (counter == MAXCOUNT)
            begin
                counter <= 0;
                blink_counter <= blink_counter + 1;
            end
        else
            counter <= counter + 1;
    end

    // Light up the LED according to the pattern.
    assign LED = blink_pattern[blink_counter[4:0]];

endmodule
