
// (C) 2021-2025 Flavio Cappelli
// Released under the MIT License.
//
// Emulate the light pattern of the car Kitt from
// Knight Rider TV series (see the _README.1st file).
//
// INPUT: none.
//
// OUPUT: PIN_1 to PIN_8 (light patterns) + User LED (blink).
//
// For the pin names on the TinyFPGA BX board see TinyFPGA-BX-pins.pcf

`default_nettype none       // Do not allow undeclared signals.
`include "timescale.vh"     // Timescale defined globally.

(* top *)                   // Mark this as top module, because
module TinyFPGA_BX (        // sometimes Yosys doesn't detect it.
    input  wire CLK_16MHz,  // 16MHz on-board clock.
    output wire PIN_1,      // Light pattern on LED 1.
    output wire PIN_2,      // Light pattern on LED 2.
    output wire PIN_3,      // Light pattern on LED 3.
    output wire PIN_4,      // Light pattern on LED 4.
    output wire PIN_5,      // Light pattern on LED 5.
    output wire PIN_6,      // Light pattern on LED 6.
    output wire PIN_7,      // Light pattern on LED 7.
    output wire PIN_8,      // Light pattern on LED 8.
    output wire LED,        // User/boot LED next to power LED.
    output wire USBPU       // USB pull-up resistor.
);
    // USB not used (drive USB pull-up resistor to '0'
    // to avoid the USB device enumeration by the OS).
    assign USBPU = 0;


    //////////////////////////////////////////////////
    // Create a ripetitive delay dividing the 16MHz //
    // clock. At the end of each delay generate an  //
    // enable pulse to increment the pattern index. //
    //////////////////////////////////////////////////

    `ifdef SIMULATION
      `define INDEX_INC_FREQ_HZ     5000    // Simulation.
    `else
      `define INDEX_INC_FREQ_HZ     5       // Build.
    `endif

    reg inc_enable = 0;
    reg [23:0] counter = 0;
    localparam MAXCOUNT = 16000000/(`INDEX_INC_FREQ_HZ*2)-1;

    always @(posedge CLK_16MHz) begin
        if (counter == MAXCOUNT)
            begin
                counter <= 0;
                inc_enable <= 1;
            end
        else begin
            counter <= counter + 1;
            inc_enable <= 0;
        end
    end


    ////////////////////////////////////////////////
    // When enabled, increment the pattern index. //
    ////////////////////////////////////////////////

    reg [3:0] index = 0;
    always @(posedge CLK_16MHz) begin
        if (inc_enable)
            index <= index + 1;
    end


    ////////////////////////////////////////////////////
    // Use the index as address of an asyncronous ROM //
    // to generate the patterns on the eight outputs. //
    ////////////////////////////////////////////////////

    wire [7:0] out;

    rom_generic_async #(
        .DATA_WIDTH(8),
        .MEMORY_DEPTH(16),
        .INIT_FILE("rom_init.hex")
    ) ROM (
        .i__addr(index),
        .o__data(out)
    );

    /////////////////////
    // Assign outputs. //
    /////////////////////

    assign PIN_1 = out[0];
    assign PIN_2 = out[1];
    assign PIN_3 = out[2];
    assign PIN_4 = out[3];
    assign PIN_5 = out[4];
    assign PIN_6 = out[5];
    assign PIN_7 = out[6];
    assign PIN_8 = out[7];

    // Use the index MSb to drive the User LED.
    assign LED = index[3];

endmodule
