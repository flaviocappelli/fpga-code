
// (C) 2021-2025 Flavio Cappelli
// Released under the MIT License.
//
// Draw some colored vertical stripes on a VGA display
// (using video mode 640x480 @ 60Hz). Also the user LED
// is made to blink (to show that the logic is working).
//
// INPUT: none.
//
// OUPUT: see below.
//
// For the pin names on the TinyFPGA BX board see TinyFPGA-BX-pins.pcf

`default_nettype none       // Do not allow undeclared signals.
`include "timescale.vh"     // Timescale defined globally.

(* top *)                   // Mark this as top module, because
module TinyFPGA_BX (        // sometimes Yosys doesn't detect it.
    input  wire CLK_16MHz,  // 16MHz clock.
    output wire PIN_1,      // Output -> VGA Red signal.
    output wire PIN_2,      // Output -> VGA Green signal.
    output wire PIN_3,      // Output -> VGA Blue signal.
    output wire PIN_4,      // Output -> VGA HSync signal.
    output wire PIN_5,      // Output -> VGA VSync signal.
    output wire PIN_6,      // Output -> Pixel clock (required for SDL2 C++ simulation).
    output wire LED,        // User/boot LED (blink).
    output wire USBPU       // USB pull-up resistor.
);
    // USB not used (drive USB pull-up resistor to '0'
    // to avoid the USB device enumeration by the OS).
    assign USBPU = 0;


    ////////////////////////////////////////////////////
    // Generate a 50MHz clock using the internal PLL. //
    ////////////////////////////////////////////////////

    wire clk_50MHz, pll_locked;         // PLL outputs.

    ice40_pll_50MHz_from_16MHz PLL (
        .i__clk_in( CLK_16MHz ),
        .o__clk_out( clk_50MHz ),
        .o__async_locked( pll_locked )
    );


    //////////////////////////////////////////////////
    // Obtain a perfectly square 25MHz clock. It is //
    // used as input for the video timing generator.//
    //////////////////////////////////////////////////

    reg clk_25MHz = 0;

    always @(posedge clk_50MHz)
        clk_25MHz <= ~clk_25MHz;


    ////////////////////////////////////////
    // Create a simple active high reset. //
    ////////////////////////////////////////

    wire rst_master;

    fpga_reset_generator RESET_GEN (
        .i__clk(clk_25MHz),
        .i__clk_en(pll_locked),
        .o__gen_rst(rst_master)
    );


    //////////////////////////////////
    // Generate the video signals   //
    // (video mode 640x480 @ 60Hz). //
    //////////////////////////////////

    wire [9:0] y_pt;
    wire [9:0] x_pt;
    wire h_sync, v_sync, data_enable;

    video_generator_640x480_60hz VIDEO_480P (
        .i__clk(clk_25MHz),
        .i__rst(rst_master),
        .o__xp(x_pt),
        .o__yp(y_pt),
        .o__hs(h_sync),
        .o__vs(v_sync),
        .o__de(data_enable)
    );


    ///////////////////////////////////////////////////
    // Draw a double border and the vertical stripes.//
    // Note: the two borders are useful to check if  //
    // the image is aligned to the synchronisms.     //
    ///////////////////////////////////////////////////

    reg [2:0] vga_RGB;

    localparam H_RES = 640;             // Horizontal and vertical screen resolution
    localparam V_RES = 480;             // (must match the above video generator).
    localparam X_MAX = H_RES - 1;
    localparam Y_MAX = V_RES - 1;

    wire border_1 = (x_pt == 0) || (x_pt == X_MAX-0) || (y_pt == 0) || (y_pt == Y_MAX-0);
    wire border_2 = (x_pt == 1) || (x_pt == X_MAX-1) || (y_pt == 1) || (y_pt == Y_MAX-1);

    always @(posedge clk_25MHz) begin
        vga_RGB <= 3'b000;              // By default black (i.e. 0V).
        if (data_enable) begin
            if (border_1)
                vga_RGB <= 3'b100;      // External border (RED).
            else if (border_2)
                vga_RGB <= 3'b110;      // Internal border (YELLOW).
            else
                vga_RGB <= x_pt[7:5];   // Stripes (several colors).
        end
    end

    assign PIN_1 = vga_RGB[2];          // R
    assign PIN_2 = vga_RGB[1];          // G
    assign PIN_3 = vga_RGB[0];          // B


    /////////////////////////////////////////////////////////////////////////////////////
    // Add one clock cycle delay on the syncs to compensate for the pixel computation. //
    //                                                                                 //
    // NOTE: syncs are sampled only when 'clk_25MHz' becomes valid: at the beginning   //
    // they are: 1# not initialized in the behavioral simulation with Icarus Verilog;  //
    // 2# randomized in behavioral simulation with Verilator; 3# set to 0 by Yosys in  //
    // both post-synthesis simulations (with Icarus Verilog / Verilator), and on real  //
    // hardware. Anyway it's not required to reset them to the inactive states (which  //
    // depend on the video mode), because the monitor is still able to sync, no matter //
    // their initial values (both in SDL2 simulation and with real synthesized logic). //
    /////////////////////////////////////////////////////////////////////////////////////

    reg vga_HS, vga_VS;

    always @(posedge clk_25MHz) begin
        vga_HS <= h_sync;
        vga_VS <= v_sync;
    end

    assign PIN_4 = vga_HS;
    assign PIN_5 = vga_VS;


    ///////////////////////////////////////////////
    // This is required only for the simulation, //
    // or to check the pixel clock with a scope. //
    ///////////////////////////////////////////////

    assign PIN_6 = clk_25MHz;


    ////////////////////
    // Blink the LED. //
    ////////////////////

    `ifdef SIMULATION
      `define BLINK_FREQ_HZ     5000    // Simulation.
    `else
      `define BLINK_FREQ_HZ     5       // Build.
    `endif

    reg status = 0;
    reg [24:0] counter = 0;
    localparam MAXCOUNT = 25000000/(`BLINK_FREQ_HZ*2)-1;

    always @(posedge clk_25MHz) begin
        if (counter == MAXCOUNT)
            begin
                counter <= 0;
                status <= ~status;
            end
        else
            counter <= counter + 1;
    end

    assign LED = status;

endmodule
