
// (C) 2021-2025 Flavio Cappelli
// Released under the MIT License.
//
// See https://www.fpgatutorial.com/how-to-write-a-basic-verilog-testbench/

`default_nettype none                       // Do not allow undeclared signals.
`define DUMPSTR(x) `"x.vcd`"                // Required for GTKWave.
`define SIMULATION_TIME 20ms                // Simulation time (in s, ms, us or ns).
`include "clkgen_sim_helper.vh"             // Must be included before timescale.vh
`include "timescale.vh"                     // Timescale defined globally.

module testbench();
    initial begin
        $dumpfile(`DUMPSTR(`VCD_OUTPUT));   // Required for GTKWave.
        $dumpvars(0, testbench);            // Dump all variables in all instantiated modules.
        $printtimescale(testbench);         // Display the time scale and resolution (see above).
        #`SIMULATION_TIME                   // Wait until the simulation time is elapsed.
        $timeformat(-6, 2, "us", 1);        // Display the time elapsed in "us".
        $display("Simulation terminated after %t", $realtime);
        $finish;                            // Terminate the simulation.
    end


    // ---- Board 16MHz Clock Simulation ----

    wire CLK_16MHz;
    clkgen_sim_helper #(.FREQ_MHz(16)) CLKGEN (.o__clk(CLK_16MHz));


    // ---- Board Simulation ----

    wire LED, USBPU;                        // Simulated outputs.
    wire PIN_1, PIN_2, PIN_3, PIN_4, PIN_5, PIN_6;

    TinyFPGA_BX BOARD (
        .CLK_16MHz(CLK_16MHz),
        .PIN_1(PIN_1),
        .PIN_2(PIN_2),
        .PIN_3(PIN_3),
        .PIN_4(PIN_4),
        .PIN_5(PIN_5),
        .PIN_6(PIN_6),
        .LED(LED),
        .USBPU(USBPU)
    );

endmodule

// In post-synthesis simulation Yosys sees only the interface of the
// following PLL simulation helper, so we have to include it here.

`ifdef POST_SYNTH_SIM
`include "ice40_pll_50MHz_sim_helper.v"
`endif
