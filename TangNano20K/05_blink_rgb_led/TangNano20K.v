
// (C) 2021-2025 Flavio Cappelli
// Released under the MIT License.
//
// Blinking RGB LED.
//
// INPUT: none.
//
// OUPUT: WS2812B DIN (data input).
//
// For the pin names on the Tang Nano 20K see TangNano20K-pins.cst

`default_nettype none                   // Do not allow undeclared signals.
`include "timescale.vh"                 // Timescale defined globally.

(* top *)                               // Mark this as top module, because
module TangNano20K (                    // sometimes Yosys doesn't detect it.
    input  wire CLK_27MHz,              // 27.0MHz on-board clock.
    output wire WS2812B_DIN             // Output -> RGB LED WS2812B data input.
);

    ////////////////////////////////////
    // Blink dividing the 27MHz clock //
    ////////////////////////////////////

    `ifdef SIMULATION
      `define BLINK_FREQ_HZ     1000    // Simulation.
    `else
      `define BLINK_FREQ_HZ     1       // Build.
    `endif

    reg [2:0] color = 0;
    reg [24:0] counter = 0;
    localparam MAXCOUNT = 27000000/(`BLINK_FREQ_HZ*2)-1;

    /* verilator lint_off WIDTH */
    always @(posedge CLK_27MHz) begin
        if (counter == MAXCOUNT)
            begin
                counter <= 0;
                color <= color + 1;
            end
        else
            counter <= counter + 1;
    end


    /////////////////////////////////////////////////////
    // Create reset signal for the WS2812B RGB LED (it //
    // must be reset each time new data is sent to it) //
    /////////////////////////////////////////////////////

    reg [2:0] last_color = 0;
    always @(posedge CLK_27MHz) begin
        last_color <= color;
    end

    (* keep *) wire led_reset = color != last_color;


    ////////////////////////////////////////////////////
    // Create GRB data stream for the WS2812B RGB LED //
    ////////////////////////////////////////////////////

    // Assign blinking rotating colors, i.e. off,red,off,green,off,blue,off,white.
    // NOTE: The intensity of the colors is reduced to avoid discomfort to the eyes.
    (* keep *) reg [23:0] grb_data;
    always @(posedge CLK_27MHz) begin
        case (color)
            3'd0 : grb_data <= 24'h00_00_00;
            3'd1 : grb_data <= 24'h00_D0_00;  // RED:   brightness 82%
            3'd2 : grb_data <= 24'h00_00_00;
            3'd3 : grb_data <= 24'h70_00_00;  // GREEN: brightness 44%
            3'd4 : grb_data <= 24'h00_00_00;
            3'd5 : grb_data <= 24'h00_00_F0;  // BLUE:  brightness 94%
            3'd6 : grb_data <= 24'h00_00_00;
            3'd7 : grb_data <= 24'h40_40_40;  // WHITE: brightness 25%
        endcase
    end


    ///////////////////////////////
    // Drive the WS2812B RGB LED //
    ///////////////////////////////

    // Ignore output signal "o__led_ready" (we don't need it here).
    /* verilator lint_off PINMISSING */

    rgb_led_ws2812b_driver #(
        .CLK_MHz(27)                    // Tang Nano 20K onboard clock frequency.
    ) WS2812B_DRV (
        .i__clk(CLK_27MHz),
        .i__reset(led_reset),
        .i__grb_data(grb_data),
        .o__ws2812b_din(WS2812B_DIN)
    );

    /* verilator lint_on PINMISSING */

endmodule
