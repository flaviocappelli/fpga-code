
// (C) 2021-2025 Flavio Cappelli
// Released under the MIT License.
//
// See https://www.fpgatutorial.com/how-to-write-a-basic-verilog-testbench/

`default_nettype none                       // Do not allow undeclared signals.
`define DUMPSTR(x) `"x.vcd`"                // Required for GTKWave.
`define SIMULATION_TIME 4ms                 // Simulation time (in s, ms, us or ns).
`include "clkgen_sim_helper.vh"             // Must be included before timescale.vh
`include "timescale.vh"                     // Timescale defined globally.

module testbench();
    initial begin
        $dumpfile(`DUMPSTR(`VCD_OUTPUT));   // Required for GTKWave.
        $dumpvars(0, testbench);            // Dump all variables in all instantiated modules.
        $printtimescale(testbench);         // Display the time scale and resolution (see above).
        #`SIMULATION_TIME                   // Wait until the simulation time is elapsed.
        $timeformat(-6, 2, "us", 1);        // Display the time elapsed in "us".
        $display("Simulation terminated after %t", $realtime);
        $finish;                            // Terminate the simulation.
    end


    // ---- Board 27MHz Clock Simulation ----

    wire CLK_27MHz;
    clkgen_sim_helper #(.FREQ_MHz(27)) CLKGEN (.o__clk(CLK_27MHz));


    // ---- Board Simulation ----

    wire WS2812B_DIN;                       // Simulated output (RGB LED WS2812B data input).

    TangNano20K BOARD (
        .CLK_27MHz(CLK_27MHz),
        .WS2812B_DIN(WS2812B_DIN)
    );

endmodule
