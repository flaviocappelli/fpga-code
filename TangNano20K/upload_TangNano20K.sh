#!/bin/bash

usage() {
  echo "Usage: $(basename -- ${0}) [-h|-f] [<firmware.fs>]"
  echo ""
  echo "Options:"
  echo "  -f  burn flash (default SRAM)"
  echo "  -h  show usage and exit"
  echo ""
  echo "If <firmware.fs> is not specified, \"hw.fs\" is used"
  exit 1
}

if [ "x${1}" == "x-h" ]; then
  usage
fi

BURN_ARG=""
if [ "x${1}" == "x-f" ]; then
  BURN_ARG="-f"
  shift
fi

if [ -n "${1}" ]; then
  FW=${1}
else
  FW=hw.fs
  echo "Firmware not specified, use default (hw.fs)"
fi

FILENAME=$(basename -- "${FW}")
EXTENSION="${FILENAME##*.}"
if [ "x${EXTENSION}" != "xfs" ]; then
  echo "Invalid argument, aborting"
  exit 1
fi

if [ ! -e "${FW}" ]; then
  echo "Firmware file not found, aborting"
  exit 1
fi

if [ -x /usr/bin/openFPGALoader ]; then
  OPENFPGALOADER="/usr/bin/openFPGALoader"
elif [ -n "${OSSCADSUITE_ROOT}" ]; then
  OPENFPGALOADER="${OSSCADSUITE_ROOT}/bin/openFPGALoader"
else
  echo "openFPGALoader not found, aborting"
  exit 1
fi

if ${OPENFPGALOADER} --detect | grep -q "idcode 0x81b"; then
  echo "Tang Nano 20K found, programming..."
  ${OPENFPGALOADER} -b tangnano20k ${BURN_ARG} "$FW"
  exit 0
fi

echo "Tang Nano 20K not found, aborting"
exit 1
