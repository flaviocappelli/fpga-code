
// (C) 2021-2025 Flavio Cappelli
// Released under the MIT License.
//
// See https://www.fpgatutorial.com/how-to-write-a-basic-verilog-testbench/

`default_nettype none                       // Do not allow undeclared signals.
`define DUMPSTR(x) `"x.vcd`"                // Required for GTKWave.
`define SIMULATION_TIME 150ms               // Simulation time (in s, ms, us or ns).
`include "clkgen_sim_helper.vh"             // Must be included before timescale.vh
`include "timescale.vh"                     // Timescale defined globally.

module testbench();
    initial begin
        $dumpfile(`DUMPSTR(`VCD_OUTPUT));   // Required for GTKWave.
        $dumpvars(0, testbench);            // Dump all variables in all instantiated modules.
        $printtimescale(testbench);         // Display the time scale and resolution (see above).
        #`SIMULATION_TIME                   // Wait until the simulation time is elapsed.
        $timeformat(-6, 2, "us", 1);        // Display the time elapsed in "us".
        $display("Simulation terminated after %t", $realtime);
        $finish;                            // Terminate the simulation.
    end


    // ---- Board 27MHz Clock Simulation ----

    wire CLK_27MHz;
    clkgen_sim_helper #(.FREQ_MHz(27)) CLKGEN (.o__clk(CLK_27MHz));


    // ---- Board Simulation ----

    reg BTN_S1;                             // Simulated input.
    wire [5:0] LED;                         // Simulated outputs.

    TangNano20K BOARD (
        .CLK_27MHz(CLK_27MHz),
        .BTN_S1(BTN_S1),
        .LED(LED)
    );

    integer i;
    initial begin
        BTN_S1 = 1;                          // Initial button input (with pull-up).
        #10ms

        for (i=0; i<2; ++i) begin
            BTN_S1 = 0;                      // Simulate button press (to GND).
            #3.210us
            BTN_S1 = 1;
            #1.550us
            BTN_S1 = 0;
            #2.511us
            BTN_S1 = 1;
            #25.92us
            BTN_S1 = 0;
            #169.6us
            BTN_S1 = 1;
            #200.1us
            BTN_S1 = 0;
            #188.7us
            BTN_S1 = 1;
            #143.2us
            BTN_S1 = 0;
            #100.5ns
            BTN_S1 = 1;
            #222.0ns
            BTN_S1 = 0;
            #140.3us
            BTN_S1 = 1;
            #992.3us
            BTN_S1 = 0;
            #220.6us
            BTN_S1 = 1;
            #400.1us
            BTN_S1 = 0;
            #200.7us
            BTN_S1 = 1;
            #995.6us
            BTN_S1 = 0;
            #26ms

            BTN_S1 = 1;                      // Simulate button release (with pull-up).
            #191.20us
            BTN_S1 = 0;
            #400.7us
            BTN_S1 = 1;
            #199.1us
            BTN_S1 = 0;
            #25.13ns
            BTN_S1 = 1;
            #142.5ns
            BTN_S1 = 0;
            #12.13ns
            BTN_S1 = 1;
            #155.1ns
            BTN_S1 = 0;
            #192.4us
            BTN_S1 = 1;
            #992.1us
            BTN_S1 = 0;
            #150.9us
            BTN_S1 = 1;
            #600.1us
            BTN_S1 = 0;
            #799.8us
            BTN_S1 = 1;
            #200.3us
            BTN_S1 = 0;
            #330.2ns
            BTN_S1 = 1;
            #1.030us
            BTN_S1 = 0;
            #200.4us
            BTN_S1 = 1;
            #10.39ns
            BTN_S1 = 0;
            #12.42ns
            BTN_S1 = 1;
            #36ms
            ;
        end
    end

endmodule
