
// (C) 2021-2025 Flavio Cappelli
// Released under the MIT License.
//
// Debounce button S1 and count button presses.
//
// INPUT: button S1, connected to FPGA I/O pin 87.
//
// OUPUT: button press count: LED[0] (LSb) to LED[5] (MSb)
//
// For the pin names on the Tang Nano 20K see TangNano20K-pins.cst

`default_nettype none               // Do not allow undeclared signals.
`include "timescale.vh"             // Timescale defined globally.

(* top *)                           // Mark this as top module, because
module TangNano20K (                // sometimes Yosys doesn't detect it.
    input  wire CLK_27MHz,          // 27.0MHz on-board clock.
    input  wire BTN_S1,             // Button input (active high).
    output wire [5:0] LED           // Button press count (inverted (*)).
);


    // Input asyncronous signal.
    wire button_in = BTN_S1;


    ////////////////////////////////////////
    // Create a simple active high reset. //
    ////////////////////////////////////////

    wire rst_master;

    fpga_reset_generator RESET_GEN (
        .i__clk(CLK_27MHz),
        .i__clk_en(1'b1),
        .o__gen_rst(rst_master)
    );


    /////////////////////////////////////////////
    // Apply the 2FF Synchronizer to button_in //
    /////////////////////////////////////////////

    wire button_sync;

    slow_activehigh_signal_synchronizer BUTT_SYNC (
        .i__clk(CLK_27MHz),
        .i__rst(rst_master),
        .i__async_sig(button_in),
        .o__sync_sig(button_sync)
    );


    //////////////////////////////////////////////
    // Filter button_sync with a debounce timer //
    //////////////////////////////////////////////

    wire button_debounced;

    slow_activehigh_signal_deglitcher #(
        .N_SAMPLES(405000)          // 405000 * 1/27E6 (CLK_27MHz) => 15 ms.
    ) BUTT_DEB (
        .i__clk(CLK_27MHz),
        .i__rst(rst_master),
        .i__sigin(button_sync),
        .o__sigout(button_debounced)
    );


    ////////////////////////////
    // Positive edge detector //
    ////////////////////////////

    wire button_pressed_pulse;

    slow_activehigh_signal_posedgedetector PE_DETECT (
        .i__clk(CLK_27MHz),
        .i__rst(rst_master),
        .i__sigin(button_debounced),
        .o__posedge(button_pressed_pulse)
    );


    //////////////////////////////////////
    // Count and output button presses. //
    //////////////////////////////////////

    reg [5:0] pressed_count;

    always @(posedge CLK_27MHz, posedge rst_master) begin
        if (rst_master) begin
            pressed_count <= 6'b0;
        end else begin
            // Use clock enable to count.
            if (button_pressed_pulse)
                pressed_count <= pressed_count + 1'b1;
        end
    end

    assign LED = ~pressed_count;    // NOTE (*): LEDS[5:0] are switched ON when
                                    // the corresponding lines are driven LOW.
endmodule
