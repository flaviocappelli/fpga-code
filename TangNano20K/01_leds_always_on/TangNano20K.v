
// (C) 2021-2025 Flavio Cappelli
// Released under the MIT License.
//
// Switch LED[5:0] ON, and set port PIN_1 (1st pin on left of USB
// connector, marked "73" on the PCB) to 3.3V and port PIN_2 (2nd
// pin on left of USB, marked "74" on the PCB) to 0V (ground).
//
// INPUT: none.
//
// OUPUT: LED[5:0], PIN_1, PIN_2.
//
// For the pin names on the Tang Nano 20K see TangNano20K-pins.cst

`default_nettype none           // Do not allow undeclared signals.
`include "timescale.vh"         // Timescale defined globally.

(* top *)                       // Mark this as top module, because
module TangNano20K (            // sometimes Yosys doesn't detect it.
    output wire PIN_1,          // Output (driven to 3.3V).
    output wire PIN_2,          // Output (driven to GND).
    output wire [5:0] LED       // User LEDs (switched on).
);

    // Set outputs.
    assign PIN_1 = 1'b1;
    assign PIN_2 = 1'b0;
    assign LED   = {6{1'b0}};   // NOTE: LEDS[5:0] are switched ON when
                                // the corresponding lines are driven LOW.
endmodule
