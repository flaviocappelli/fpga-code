
// Board "Tang Nano 20K": Gowin GW2AR-LV18QN88C8/I7 FPGA constraint file (.cst), see
// https://github.com/YosysHQ/apicula/blob/master/examples/himbaechel/tangnano20k.cst
// and https://wiki.sipeed.com/hardware/en/tang/tang-nano-20k/nano-20k.html
//
// IMPORTANT NOTE
// --------------
// The Gowin IDE complains about unused I/O pins: to prevent such errors, all unused I/O
// pins must be either commented out or removed from the .cst file. Additionally, in the
// Gowin IDE the IO_TYPE defaults to LVCMOS18 (if not specified): this leads to conflicts
// in the Tang Nano 20K where all I/O banks are powered by 3.3V. To avoid issues, always
// specify the IO_TYPE of used pins (it should work correctly with the OSS-CAD-SUITE).
//
// If you are unsure how to configure the various pins, use the Gowin IDE to generate the
// .cst file (see https://wiki.sipeed.com/hardware/en/tang/tang-nano-20k/example/led.html),
// then remove "BANK_VCCIO=3.3" from the definitions: it is not strictly required in the
// Gowin IDE, and is not yet supported by executable "gowin_pack" in the OSS-CAD-SUITE.


// Main clock.
IO_LOC "CLK_27MHz" 4;   // Schematics: PIN04_SYS_CLK
IO_PORT "CLK_27MHz" IO_TYPE=LVCMOS33 PULL_MODE=UP;
CLOCK_LOC "CLK_27MHz" BUFG;


// LED[5:0].
IO_LOC "LED[0]" 15;     // Schematics: PIN15_SYS_LED0
IO_LOC "LED[1]" 16;     // Schematics: PIN16_SYS_LED1
IO_LOC "LED[2]" 17;     // Schematics: PIN17_SYS_LED2
IO_LOC "LED[3]" 18;     // Schematics: PIN18_SYS_LED3
IO_LOC "LED[4]" 19;     // Schematics: PIN19_SYS_LED4
IO_LOC "LED[5]" 20;     // Schematics: PIN20_SYS_LED5
IO_PORT "LED[0]" IO_TYPE=LVCMOS33 PULL_MODE=UP DRIVE=8;
IO_PORT "LED[1]" IO_TYPE=LVCMOS33 PULL_MODE=UP DRIVE=8;
IO_PORT "LED[2]" IO_TYPE=LVCMOS33 PULL_MODE=UP DRIVE=8;
IO_PORT "LED[3]" IO_TYPE=LVCMOS33 PULL_MODE=UP DRIVE=8;
IO_PORT "LED[4]" IO_TYPE=LVCMOS33 PULL_MODE=UP DRIVE=8;
IO_PORT "LED[5]" IO_TYPE=LVCMOS33 PULL_MODE=UP DRIVE=8;

// NOTE: On the Tang Nano 20K, the LED[5:0] are switched
// ON when the corresponding lines are driven LOW, so they
// should be called "INV_LED" or "LED_N" instead of "LED.


// Other output pins.
IO_LOC "PIN_1" 73;      // Schematics: PIN73_HSPI_DIN2
IO_LOC "PIN_2" 74;      // Schematics: PIN74_HSPI_DIN3
IO_PORT "PIN_1" IO_TYPE=LVCMOS33 PULL_MODE=UP DRIVE=8;
IO_PORT "PIN_2" IO_TYPE=LVCMOS33 PULL_MODE=UP DRIVE=8;
