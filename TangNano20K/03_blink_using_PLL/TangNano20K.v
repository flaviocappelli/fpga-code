
// (C) 2021-2025 Flavio Cappelli
// Released under the MIT License.
//
// Blinking LEDs (test the Gowin PLL).
//
// INPUT: none.
//
// OUPUT: LED[5:0], PIN_1, PIN_2 (see below).
//
// For the pin names on the Tang Nano 20K see TangNano20K-pins.cst

`default_nettype none                   // Do not allow undeclared signals.
`include "timescale.vh"                 // Timescale defined globally.

(* top *)                               // Mark this as top module, because
module TangNano20K (                    // sometimes Yosys doesn't detect it.
    input  wire CLK_27MHz,              // 27.0MHz on-board clock.
    output wire PIN_1,                  // Output -> clock 27MHz.
    output wire PIN_2,                  // Output -> clock 54MHz.
    output wire [5:0] LED               // User LEDs (blink).
);


    /////////////////////////////////////////////////////////
    // Generate a 54MHz clock using the Gowin internal PLL //
    /////////////////////////////////////////////////////////

    wire clk_out, clk_ok;               // PLL output.

    gw2ar_pll_54MHz_from_27MHz PLL (
        .i__clk_in(CLK_27MHz),          // Input clock.
        .o__clk_out(clk_out),           // Output clock.
        .o__async_locked(clk_ok)        // Lock signal.
    );

    assign PIN_1 = CLK_27MHz;           // 16MHz clock ouput.
    assign PIN_2 = clk_out & clk_ok;    // 50MHz clock ouput (when stable).


    ///////////////////////////////////////////
    // Blink using the 54MHz generated clock //
    ///////////////////////////////////////////

    `ifdef SIMULATION
      `define BLINK_FREQ_HZ     1000    // Simulation.
    `else
      `define BLINK_FREQ_HZ     1       // Build.
    `endif

    reg [5:0] ledreg = 6'b111110;
    reg [25:0] counter = 0;
    localparam MAXCOUNT = 54000000/(`BLINK_FREQ_HZ)-1;

    always @(posedge clk_out) begin
        if (clk_ok)
            begin
                if (counter == MAXCOUNT)
                    begin
                        counter <= 0;
                        ledreg[5:0] <= {ledreg[4:0],ledreg[5]};
                    end
                else
                    counter <= counter + 1;
            end
    end

    assign LED = ledreg;

endmodule
