
// (C) 2021-2025 Flavio Cappelli
// Released under the MIT License.
//
// Blinking LEDs (test the on-board 27MHz oscillator).
//
// INPUT: none.
//
// OUPUT: LED[5:0], PIN_1 (27MHz clock).
//
// For the pin names on the Tang Nano 20K see TangNano20K-pins.cst

`default_nettype none                   // Do not allow undeclared signals.
`include "timescale.vh"                 // Timescale defined globally.

(* top *)                               // Mark this as top module, because
module TangNano20K (                    // sometimes Yosys doesn't detect it.
    input  wire CLK_27MHz,              // 27.0MHz on-board clock.
    output wire PIN_1,                  // Output (27MHz clock).
    output wire [5:0] LED               // User LEDs (blink).
);

    ////////////////////////////////////
    // Blink dividing the 27MHz clock //
    ////////////////////////////////////

    `ifdef SIMULATION
      `define BLINK_FREQ_HZ     1000    // Simulation.
    `else
      `define BLINK_FREQ_HZ     1       // Build.
    `endif

    reg [5:0] ledreg = 6'b111110;
    reg [24:0] counter = 0;
    localparam MAXCOUNT = 27000000/(`BLINK_FREQ_HZ)-1;

    always @(posedge CLK_27MHz) begin
        if (counter == MAXCOUNT)
            begin
                counter <= 0;
                ledreg[5:0] <= {ledreg[4:0],ledreg[5]};
            end
        else
            counter <= counter + 1;
    end

    // Set outputs.
    assign PIN_1 = CLK_27MHz;
    assign LED = ledreg;

endmodule
