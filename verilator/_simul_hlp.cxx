
// (C) 2021-2025 Flavio Cappelli
// Released under the MIT License.
//
// Helper for Verilator C++ simulation (code).

#include "_simul_hlp.hxx"                   // Helper classes and functions.

#include <cassert>                          // Use assert.
#include <algorithm>                        // Use std::min.
#include <queue>                            // Use std::priority_queue.



// ------------------------------ Event's Queue ------------------------------

/*
 * Abstract class for events.
 */
class vEventBase
{
public:
    vEventBase(const uint64_t tpu):         // Constructor.
        m_tpu(tpu) {
        assert(tpu != 0);
    }

    uint64_t getTpu() const {               // Return activation time in tpu (timeprecision units).
        return m_tpu;
    }

    virtual void callback() = 0;            // Callback, pure virtual.
    virtual ~vEventBase() {}                // Destructor.

protected:
    const uint64_t m_tpu;
};


/*
 * Comparator for the below event queue. See
 * https://stackoverflow.com/questions/16111337
 */
struct ev_cmp
{
    bool operator()(const vEventBase *a, const vEventBase *b) {
        return (a->getTpu()) > (b->getTpu());
    }
};


/*
 * Priority queue, ordered by stored "tpu" time:
 * top() returns the pointer to the closest event.
 */
static std::priority_queue<vEventBase*, std::vector<vEventBase*>, ev_cmp> ev_q;



// ------------------------- 128bit fixed point math -------------------------

/*
 * Define a representation for unsigned 128bit fixed point numbers.
 * Integer and fractional parts are both stored in "uint64_t" types.
 * Note that just the required functions/operators are implemented.
 * This class is used to perfom high precision clock generation.
 */
class ufp128_t
{
public:
    // Convert a floating point value to an ufp128_t object.
    // To improve precision, we must use long double here,
    // see https://stackoverflow.com/questions/54948430.
    ufp128_t(long double v) {
        assert(v >= 0);
        assert(v < 18446744073709551616.0L);    // < 2^64 --> (uint64_t)v <= UINT64_MAX.

        // Get integer part.
        hi = static_cast<uint64_t>(v);
        assert(v - hi >= 0);
        assert(v - hi < 1.0L);

        // Get fractional part. Round the result to improve precision on
        // small values: by some tests, I detected that we have to add .4
        // (not .5) to minimize the "mean errors" and "mean square errors".
        // Due to rounding, we must ensure the result stays <= UINT64_MAX.
        v = .4 + (v - hi) * 18446744073709551616.0L;
        if (v > 18446744073709551615.0L)
            v = 18446744073709551615.0L;
        lo = static_cast<uint64_t>(v);
    }

    // Add a ufp128_t object to "this" object.
    ufp128_t &operator+=(const ufp128_t &rhs)
    {
        lo += rhs.lo;
        hi += rhs.hi + (lo < rhs.lo);           // Takes wrap around into account.
        return *this;
    }

    // Compare "this" object with an unsigned long integer.
    bool operator!=(const uint64_t v) const
    {
        return (hi != v) || (lo != 0);          // (hi,lo) != (v,0).
    }

    // Return the ROUNDED integer part of "this" object.
    uint64_t round() const {
        return hi + (static_cast<int64_t>(lo) < 0);
    }

private:
    uint64_t lo;                                // Fractional part.
    uint64_t hi;                                // Integer part.
};



// ------------------------- Verilator's Environment -------------------------

/*
 * Extend the VerilatedContext class.
 */
class VerilatedContextEx : public VerilatedContext
{
public:
    // Provide time increment in ps, ns, us, ms, sec.
    void timeInc_ps(const double t) { timeInc(ps_to_tpu(t)); }
    void timeInc_ns(const double t) { timeInc(ns_to_tpu(t)); }
    void timeInc_us(const double t) { timeInc(us_to_tpu(t)); }
    void timeInc_ms(const double t) { timeInc(ms_to_tpu(t)); }
    void timeInc_se(const double t) { timeInc(se_to_tpu(t)); }

    // Convert time (expressed in ps, ns, us, ms, sec) to timeprecision
    // units (i.e. "tpu"). The returned value depends on the timescale.
    uint64_t ps_to_tpu(const double t) { return time_to_tpu(t,-12); }
    uint64_t ns_to_tpu(const double t) { return time_to_tpu(t, -9); }
    uint64_t us_to_tpu(const double t) { return time_to_tpu(t, -6); }
    uint64_t ms_to_tpu(const double t) { return time_to_tpu(t, -3); }
    uint64_t se_to_tpu(const double t) { return time_to_tpu(t,  0); }

    // Convert time (expressed in ps, ns, us, ms, sec) to high resolution
    // timeprecision units ("tpu128" are represented using unsigned 128bit
    // fixed point numbers and are used to perfom efficient high precision
    // clock generation). The returned value depends on the timescale.
    ufp128_t ps_to_tpu128(const double t) { return time_to_tpu128(t,-12); }
    ufp128_t ns_to_tpu128(const double t) { return time_to_tpu128(t, -9); }
    ufp128_t us_to_tpu128(const double t) { return time_to_tpu128(t, -6); }
    ufp128_t ms_to_tpu128(const double t) { return time_to_tpu128(t, -3); }
    ufp128_t se_to_tpu128(const double t) { return time_to_tpu128(t,  0); }

    // Return the maximum allowed simulation time in seconds.
    double max_simulation_time_s() {
        return static_cast<double>(UINT64_MAX)/vl_time_pow10(-timeprecision());
    }

protected:
    // Helper function (used to perform "time to tpu" conversions).
    uint64_t time_to_tpu(const double t, const int8_t t_pow10) {
        const int scaling_pow10 = t_pow10 - timeprecision();

        // If this fails, the provided time interval is incompatible with
        // the timeprecision set by --timescale-override (see my Makefile).
        assert(scaling_pow10 >= 0 && scaling_pow10 <= 18);

        // Convert to timeprecision units. We use the Verilator's function
        // vl_time_pow10(n) because it's faster than pow(10,n) from math.h;
        // vl_time_pow10(n) returns an uint64_t and its argument must be an
        // integer in range 0..18 (see the function implementation, i.e. the
        // code in "verilated.cpp", from the Verilator's include directory).
        const uint64_t tpu = static_cast<uint64_t>(0.5 + t*vl_time_pow10(scaling_pow10));

        // If this fails, the provided time increment is incompatible with
        // the timeprecision set by --timescale-override (see my Makefile).
        assert(tpu != 0);

        // Return the time interval converted in timeprecision units.
        return tpu;
    }

    // Helper function (used to perform "time to tpu128" conversions;
    // very similar to the helper above, see that for any explanation).
    ufp128_t time_to_tpu128(const double t, const int8_t t_pow10) {
        const int scaling_pow10 = t_pow10 - timeprecision();
        assert(scaling_pow10 >= 0 && scaling_pow10 <= 18);

        const ufp128_t tpu128(t*vl_time_pow10(scaling_pow10));
        assert(tpu128 != 0);

        return tpu128;
    }
};


/*
 * Shared Verilator's context (used in this file only).
 */
static VerilatedContextEx *vctx = nullptr;

/*
 * Shared Verilated top module (used in this file and in the testbench (i.e.
 * main) to access Verilator's objects, like I/O pins and public variables).
 */
Vtop *vtop = nullptr;

/*
 * Shared Verilator's trace class (used in this file only).
 */
#if VM_TRACE_FST
#include "verilated_fst_c.h"
static VerilatedFstC *vtfp = nullptr;
#endif


/*
 * Initialize the verilator's environment.
 */
void verilator_init(int argc, char **argv)
{
    // Construct a VerilatedContextEx to hold simulation time, etc.
    vctx = new VerilatedContextEx;

    // Set debug level (0 off, 9 highest). May be overridden by command arguments.
    vctx->debug(0);

    // Randomization reset policy: easily discover broken design due to missing resets.
    vctx->randReset(2);

    // Pass program arguments to Verilated code (must be done before a model is created).
    vctx->commandArgs(argc, argv);

    // Create the Verilated model of the module under test (i.e. the top module). The
    // I/O interface of the Verilated model is what the testbench sees (highest level).
    vtop = new Vtop{vctx, "testbench"};

    // Start tracing (if enabled).
    #if VM_TRACE_FST
    vtfp = new VerilatedFstC;
    vctx->traceEverOn(true);
    vtop->trace(vtfp, 99);          // Trace 99 levels of hierarchy
    vtfp->open("trace.fst");
    #endif

    // Check the time precision (min 1ps).
    assert(vctx->timeprecision() <= -12);
}


/*
 * Cleanup the Verilator's environment. Must
 * be called before terminating the program.
 */
void verilator_cleanup(void)
{
    // Cleanup verilated model.
    if (vtop)
        vtop->final();

    // Close tracing if opened.
    #if VM_TRACE_FST
    if (vtfp)
        vtfp->close();
    delete vtfp;
    vtfp = nullptr;
    #endif

    // Destroy model and context.
    delete vtop;
    vtop = nullptr;
    delete vctx;
    vctx = nullptr;

    // Cleanup event queue.
    while (!ev_q.empty())
    {
        vEventBase *ev = ev_q.top();
        assert(ev != nullptr);

        ev_q.pop();
        delete ev;
    }
}



// --------------------------- Event Handling Loop ---------------------------

/*
 * Hold the user's handler (if installed).
 */
static bool (*user_handler)() = nullptr;


/*
 * Install the user's handler (the handler is invoked after
 * all event's callbacks, associated with the same deadline,
 * have been invoked). Such user's handler must return TRUE
 * on errors, to stop the event handling loop and force exit.
 */
void verilator_install_handler(bool (*callback)())
{
    user_handler = callback;
}


/*
 * Handle the scheduled events (until the simulation
 * time expires or all events have been processed).
 */
void verilator_exec(const double simul_time_s)
{
    // Check initial time.
    assert(vctx->time() == 0);

    // Check simulation time. NOTE: THE MAXIMUM ALLOWED SIMULATION TIME
    // DEPENDS ON THE TIME PRECISION. With a time precision of '1ps' (the
    // minimum allowed, see verilator_init()) we can have up to 213 days of
    // simulated time; with a time precision of '1fs' (the maximum allowed
    // by Verilator and my preferred choice, see option --timescale-override
    // in my makefile) we'll have up to 5 hours of simulated time. Note that
    // the real time required to complete the simulation will depend on the
    // host's performance and the complexity of the simulated logic, and may
    // be much larger than the values ​​given above. Also, the amount of data
    // accumulated by the simulation will likely exceed the host's storage
    // capacity well before the maximum allowed simulation time is reached!
    assert(simul_time_s < vctx->max_simulation_time_s());

    // Get simulation time in tpu.
    uint64_t simul_time = vctx->se_to_tpu(simul_time_s);

    // Dump the initial state at time 0.
    vtop->eval();
    #if VM_TRACE_FST
    vtfp->dump(vctx->time());
    #endif

    // Process the enqueued events.
    bool stop = false;
    while ((!ev_q.empty()) && (vctx->time() < simul_time) && (!vctx->gotFinish()) && !stop)
    {
        // Check the queue top object.
        assert(ev_q.top() != nullptr);
        assert(ev_q.top()->getTpu() > vctx->time());        // NOTE: '>'.

        // Increment time to just "1 tpu" before the next event. Note that we
        // may have events that differ in time only by 1 tpu; in that case such
        // increment is 0: we must ignore it and also skip the evaluation of the
        // verilated logic, because it has already been done, on this simulation
        // time, by the previous call to eval(), at the end of the while() loop.
        uint64_t tpu_increment = ev_q.top()->getTpu() - vctx->time() - 1;
        if (tpu_increment != 0) {
            vctx->timeInc(tpu_increment);

            // Evaluate logic.
            vtop->eval();
            #if VM_TRACE_FST
            vtfp->dump(vctx->time());
            #endif
        }

        // Perform "1 tpu" time increment. After such increment, we
        // definitely have vctx->time() == ev_q.top()->getTpu() (we
        // don't need to check it, it's ensured by the above code).
        vctx->timeInc(1);

        // Process all events scheduled at the same time (can be more than one).
        while (!ev_q.empty())
        {
            // Check the queue top object.
            assert(ev_q.top() != nullptr);
            assert(ev_q.top()->getTpu() >= vctx->time());   // NOTE: '>='.

            // Consider only events scheduled at the current time.
            if (ev_q.top()->getTpu() != vctx->time())
                break;

            // Get the event and remove it from the queue.
            vEventBase *ev = ev_q.top();
            ev_q.pop();

            // Call the event handler.
            (ev->callback)();

            // Free the event object.
            delete ev;
        }

        // Call the user's event handler (if defined);
        // break the loop if it returns true (error).
        if (user_handler)
            stop = user_handler();

        // Evaluate logic.
        vtop->eval();
        #if VM_TRACE_FST
        vtfp->dump(vctx->time());
        #endif
    }

    // If simulation time is not expired, complete the tracking.
    if (vctx->time() < simul_time) {
        vctx->timeInc(simul_time - vctx->time());
        vtop->eval();
        #if VM_TRACE_FST
        vtfp->dump(vctx->time());
        #endif
    }
}



// ------------------------------ Signal Toggle ------------------------------

/*
 * Event class for signal toggle.
 */
class vSigTglEvent: public vEventBase
{
public:
    vSigTglEvent(const uint64_t tpu, uint8_t * const vsignal):
        vEventBase(tpu), m_vsignal(vsignal) {}

    virtual void callback() {
        *m_vsignal = *m_vsignal ? 0 : 1;
    }

protected:
    uint8_t * const m_vsignal;
};


/*
 * Schedule toggle of Verilator signal (1 bit only).
 */
void _verilator_sched_tgl(const double sched_time_s, uint8_t * const vsignal)
{
    // Check args.
    assert(vsignal != nullptr);
    assert(sched_time_s >= 1E-12);      // Toggle at time 0 is not permitted.

    // Schedule event.
    ev_q.push(new vSigTglEvent(vctx->se_to_tpu(sched_time_s), vsignal));

}



// ---------------------------- Signal Assignment ----------------------------

/*
 * Event class for signal assignment.
 */
template <class T>
class vSigSetEvent: public vEventBase
{
public:
    vSigSetEvent(const uint64_t tpu, T * const vsignal, const T val):
        vEventBase(tpu), m_vsignal(vsignal), m_val(val) {}

    virtual void callback() {
        *m_vsignal = m_val;
    }

protected:
    T * const m_vsignal;
    const T m_val;
};


/*
 * Schedule assignment to a variable of type "uint8_t". Note that
 * Verilator type "vluint8_t" is deprecated since Verilator 4.222.
 */
void _verilator_sched_set(const double sched_time_s, uint8_t * const vsignal, const uint8_t val)
{
    // Check args.
    assert(vsignal != nullptr);
    assert(sched_time_s >= 0.0);      // Assignment at time 0 is permitted.

    // If scheduled time is less then 1ps assign immediately, else schedule event.
    if (sched_time_s < 1E-12)
        *vsignal = val;
    else
        ev_q.push(new vSigSetEvent<uint8_t>(vctx->se_to_tpu(sched_time_s), vsignal, val));
}


/*
 * Schedule assignment to a variable of type "uint16_t". Note that
 * Verilator type "vluint16_t" is deprecated since Verilator 4.222.
 */
void _verilator_sched_set(const double sched_time_s, uint16_t * const vsignal, const uint16_t val)
{
    // Check args.
    assert(vsignal != nullptr);
    assert(sched_time_s >= 0.0);      // Assignment at time 0 is permitted.

    // If scheduled time is less then 1ps assign immediately, else schedule event.
    if (sched_time_s < 1E-12)
        *vsignal = val;
    else
        ev_q.push(new vSigSetEvent<uint16_t>(vctx->se_to_tpu(sched_time_s), vsignal, val));
}


/*
 * Schedule assignment to a variable of type "uint32_t". Note that
 * Verilator type "vluint32_t" is deprecated since Verilator 4.222.
 */
void _verilator_sched_set(const double sched_time_s, uint32_t * const vsignal, const uint32_t val)
{
    // Check args.
    assert(vsignal != nullptr);
    assert(sched_time_s >= 0.0);      // Assignment at time 0 is permitted.

    // If scheduled time is less then 1ps assign immediately, else schedule event.
    if (sched_time_s < 1E-12)
        *vsignal = val;
    else
        ev_q.push(new vSigSetEvent<uint32_t>(vctx->se_to_tpu(sched_time_s), vsignal, val));
}


/*
 * Schedule assignment to a variable of type "uint64_t". Note that
 * Verilator type "vluint64_t" is deprecated since Verilator 4.222.
 */
void _verilator_sched_set(const double sched_time_s, uint64_t * const vsignal, const uint64_t val)
{
    // Check args.
    assert(vsignal != nullptr);
    assert(sched_time_s >= 0.0);      // Assignment at time 0 is permitted.

    // If scheduled time is less then 1ps assign immediately, else schedule event.
    if (sched_time_s < 1E-12)
        *vsignal = val;
    else
        ev_q.push(new vSigSetEvent<uint64_t>(vctx->se_to_tpu(sched_time_s), vsignal, val));
}



// -------------------------- Main Clock Generation --------------------------

/*
 * Event class for high precision clock generation.
 */
class vClkEdgeEvent: public vEventBase
{
public:
    vClkEdgeEvent(const uint64_t tpu, const ufp128_t time_tpu128,
                  const ufp128_t T_tpu128, uint8_t * const vsignal, const uint8_t val):
        vEventBase(tpu), m_time_tpu128(time_tpu128),
        m_T_tpu128(T_tpu128), m_vsignal(vsignal), m_val(val) {}

    virtual void callback() {
        *m_vsignal = m_val;

        // Schedule a new event (i.e. edge) after the period T.
        m_time_tpu128 += m_T_tpu128;
        ev_q.push(new vClkEdgeEvent(m_time_tpu128.round(), m_time_tpu128, m_T_tpu128, m_vsignal, m_val));
    }

protected:
          ufp128_t  m_time_tpu128;              // Elapsed time in high resolution timeprecision units.
    const ufp128_t  m_T_tpu128;                 // Clock period in high resolution timeprecision units.
    uint8_t * const m_vsignal;
    const uint8_t   m_val;
};


/*
 * Schedule the clock generation. The dutycycle "dc" is in %. Must be min(Ton,Toff) >= 1ps.
 * The clock simulation is very accurate, since the timings of clock edges are calculated
 * using 128-bit fixed-point math and then approximated to the nearest "tpu" integers.
 */
void _verilator_sched_clk(const double sched_time_s, uint8_t * const vsignal,
                          const double freq_Hz, const double dc)
{
    // Check args.
    assert(vsignal != nullptr);
    assert(sched_time_s >= 0.0);                // Start at time 0 is permitted (first edge is anyway at t>0).
    assert(freq_Hz >= 1E-6 && freq_Hz <= 10E9); // Allowed clock range: 1uHz..10GHz (should be enough!).
    assert(dc > 0.0 && dc < 100.0);

    // Initial value of clock.
    *vsignal = 0;

    // Get T, Ton and Toff.
    double T_s = 1.0 / freq_Hz;
    double T_on_s = 0.01 * dc * T_s;
    double T_off_s = 0.01 * (100.0 - dc) * T_s;

    // Check dutycycle (see above).
    assert(std::min(T_on_s, T_off_s) >= 1E-12);

    // Get the delay for the first positive/negative clock edge.
    double pos_edge_delay_s = sched_time_s + T_off_s;
    double neg_edge_delay_s = sched_time_s + T_s;

    // Convert the clock period in high resolution timeprecision units.
    ufp128_t T_tpu128 = vctx->se_to_tpu128(T_s);

    // Schedule first positive/negative edge (i.e. start clock generation).
    ev_q.push(new vClkEdgeEvent(vctx->se_to_tpu(pos_edge_delay_s), vctx->se_to_tpu128(pos_edge_delay_s), T_tpu128, vsignal, 1));
    ev_q.push(new vClkEdgeEvent(vctx->se_to_tpu(neg_edge_delay_s), vctx->se_to_tpu128(neg_edge_delay_s), T_tpu128, vsignal, 0));
}



// ----------------------------- PLL Simulation ------------------------------

#ifdef INCLUDED_SVDPI

/*
 * Event class for simulation of the PLL lock signal.
 */
class vPllLockEvent: public vEventBase
{
public:
    vPllLockEvent(const uint64_t tpu, const dpiPllF_t dpi_f):
        vEventBase(tpu), m_dpi_f(dpi_f) {}

    virtual void callback() {
        m_dpi_f(1);                         // Call the DPI function to assert the lock signal.
    }

protected:
    const dpiPllF_t m_dpi_f;                // Stored DPI function.
};


/*
 * Event class for simulation of the PLL clock.
 */
class vPllClkEdgeEvent: public vEventBase
{
public:
    vPllClkEdgeEvent(const uint64_t tpu, const ufp128_t time_tpu128,
                  const ufp128_t T_tpu128, const dpiPllF_t dpi_f, const uint8_t val):
        vEventBase(tpu), m_time_tpu128(time_tpu128),
        m_T_tpu128(T_tpu128), m_dpi_f(dpi_f), m_val(val) {}

    virtual void callback() {
        m_dpi_f(m_val);

        // Schedule a new event (i.e. edge) after the period T.
        m_time_tpu128 += m_T_tpu128;
        ev_q.push(new vPllClkEdgeEvent(m_time_tpu128.round(), m_time_tpu128, m_T_tpu128, m_dpi_f, m_val));
    }

protected:
          ufp128_t  m_time_tpu128;              // Elapsed time in high resolution timeprecision units.
    const ufp128_t  m_T_tpu128;                 // Clock period in high resolution timeprecision units.
    const dpiPllF_t m_dpi_f;
    const uint8_t   m_val;
};


/*
 * Schedule a simulation of PLL clock and lock signal.
 */
void _verilator_sched_pll(const double freq_Hz, const double dc,
                          const double clk_sched_us, const double lock_sched_us,
                          dpiPllF_t clk_dpi_f, dpiPllF_t lock_dpi_f)
{
    // ------------------------------------- PLL CLOCK SCHEDULING -------------------------------------

    // Check args.
    assert(clk_dpi_f != nullptr);
    assert(clk_sched_us >= 0.01);               // PLL clock out at min 10ns.
    assert(freq_Hz >= 1E-6 && freq_Hz <= 10E9); // Allowed clock range: 1uHz..10GHz (should be enough!).
    assert(dc > 0.0 && dc < 100.0);

    // Initial value of clock.
    clk_dpi_f(0);

    // Get T, Ton and Toff.
    double T_s = 1.0 / freq_Hz;
    double T_on_s = 0.01 * dc * T_s;
    double T_off_s = 0.01 * (100.0 - dc) * T_s;

    // Check dutycycle (see above).
    assert(std::min(T_on_s, T_off_s) >= 1E-12);

    // Get the delay for the first positive/negative clock edge.
    double clk_sched_s = clk_sched_us / 1E6;
    double pos_edge_delay_s = clk_sched_s + T_off_s;
    double neg_edge_delay_s = clk_sched_s + T_s;

    // Convert the clock period in high resolution timeprecision units.
    ufp128_t T_tpu128 = vctx->se_to_tpu128(T_s);

    // Schedule first positive/negative edge (i.e. start PLL clock generation).
    ev_q.push(new vPllClkEdgeEvent(vctx->se_to_tpu(pos_edge_delay_s), vctx->se_to_tpu128(pos_edge_delay_s), T_tpu128, clk_dpi_f, 1));
    ev_q.push(new vPllClkEdgeEvent(vctx->se_to_tpu(neg_edge_delay_s), vctx->se_to_tpu128(neg_edge_delay_s), T_tpu128, clk_dpi_f, 0));

    // ------------------------------------- PLL LOCK SCHEDULING --------------------------------------

    // Check args.
    assert(lock_dpi_f != nullptr);
    assert(lock_sched_us >= 0.01);              // PLL lock at min 10ns.

    // Initial value of lock signal.
    lock_dpi_f(0);

    // Convert lock_sched_us in tpu.
    uint64_t lock_sched_tpu = vctx->us_to_tpu(lock_sched_us);

    // Schedule the PLL lock signal generation.
    ev_q.push(new vPllLockEvent(lock_sched_tpu, lock_dpi_f));
}

#endif  // INCLUDED_SVDPI
