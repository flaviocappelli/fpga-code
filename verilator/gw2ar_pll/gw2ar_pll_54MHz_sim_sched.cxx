
// (C) 2021-2025 Flavio Cappelli
// Released under the MIT License.
//
// C++ helper for module "gw2ar_pll_54MHz_sim_helper.v":
// schedule the PLL simulation (clock and lock signal) in
// C++ testbenches (simulation with Verilator). See "Direct
// Programming Interface (DPI)" in Verilator's documentation.


// Include my helper classes and functions.
#include "_simul_hlp.hxx"

// Include the Verilator's top module DPI interface.
#include "Vtop__Dpi.h"


// Schedule the PLL simulation (automatically called by
// "initial" keyword in "gw2ar_pll_54MHz_sim_helper.v").
void _gw2ar_pll_54MHz_sim_sched(void)
{
    // Generate a 54MHz clock with 55% dutycycle
    // (like in "gw2ar_pll_54MHz_sim_helper.v").

    _verilator_sched_pll(54E6, 55,                              // PLL clock frequency and dutycycle.
                         29.961 - 4.163,                        // Scheduled time for starting PLL clock.
                         29.961,                                // Scheduted time for asserting PLL lock signal.
                         _gw2ar_pll_54MHz_sim_clk_driver,       // Verilog function called to drive the PLL clock.
                         _gw2ar_pll_54MHz_sim_lock_driver       // Verilog function called to set the PLL lock line.
    );
}
