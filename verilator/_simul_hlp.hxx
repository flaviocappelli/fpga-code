
// (C) 2021-2025 Flavio Cappelli
// Released under the MIT License.
//
// Helper for Verilator C++ simulation (header).

#ifndef _SIMUL_HLP_HXX
#define _SIMUL_HLP_HXX

// Some useful macros.
#define _s   (1E0)
#define _ms  (1E-3)
#define _us  (1E-6)
#define _ns  (1E-9)
#define _ps  (1E-12)
#define _Hz  (1E0)
#define _KHz (1E3)
#define _MHz (1E6)
#define _GHz (1E9)

// Verilated top model.
#include "Vtop.h"

// Check the Verilator version: Verilator 4.222 or higher is REQUIRED, as we
// use some APIs that are available only starting from this version. Note that
// VERILATOR_VERSION_INTEGER macro is implemented only in Verilator >= 4.219.
#if !defined(VERILATOR_VERSION_INTEGER) || VERILATOR_VERSION_INTEGER < 4222000
#error "Verilator 4.222 or later required"
#endif

// Initialize the Verilator's environment.
void verilator_init(int argc, char **argv);

// Install the user's handler (the handler is invoked after
// all event's callbacks, associated with the same deadline,
// have been invoked). Such user's handler must return TRUE
// on errors, to stop the event handling loop and force exit.
void verilator_install_handler(bool (*callback)());

// Handle the scheduled events (until the simulation
// time expires or all events have been processed).
void verilator_exec(const double simul_time_s);

// Cleanup the Verilator's environment. Must
// be called before terminating the program.
void verilator_cleanup(void);

// Schedule toggle of Verilator signal (1 bit only).
void _verilator_sched_tgl(const double sched_time_s, uint8_t * const vsignal);

// Schedule assignment to a variable of type "uint8_t".
void _verilator_sched_set(const double sched_time_s, uint8_t * const vsignal, const uint8_t val);

// Schedule assignment to a variable of type "uint16_t".
void _verilator_sched_set(const double sched_time_s, uint16_t * const vsignal, const uint16_t val);

// Schedule assignment to a variable of type "uint32_t".
void _verilator_sched_set(const double sched_time_s, uint32_t * const vsignal, const uint32_t val);

// Schedule assignment to a variable of type "uint64_t".
void _verilator_sched_set(const double sched_time_s, uint64_t * const vsignal, const uint64_t val);

// Schedule the clock generation. The dutycycle "dc" is in %. Must be min(Ton,Toff) >= 1ps.
// The clock simulation is very accurate, since the timings of clock edges are calculated
// using 128-bit fixed-point math and then approximated to the nearest "tpu" integers.
void _verilator_sched_clk(const double sched_time_s, uint8_t * const vsignal,
                          const double freq_Hz, const double dc = 50.0);

// Simplify use of Verilated I/O objects hiding the top module object and the address operator.
#define verilator_sched_clk(t,s,f,...) _verilator_sched_clk((t),&(vtop->s),(f),##__VA_ARGS__)
#define verilator_sched_set(t,s,v)     _verilator_sched_set((t),&(vtop->s),(v))
#define verilator_sched_tgl(t,s)       _verilator_sched_tgl((t),&(vtop->s))

// Schedule a simulation of PLL clock and lock signal. NOTE: macro "INCLUDED_SVDPI" and
// type "svBit" are defined by Verilator only when the DPI (Direct Programming Interface)
// is used. This function is called by *.cxx files in "./ice40_pll/" and "./gw2ar_pll/".
#ifdef INCLUDED_SVDPI
typedef void (*dpiPllF_t)(svBit);
void _verilator_sched_pll(const double freq_Hz, const double dc,
                          const double clk_sched_us, const double lock_sched_us,
                          dpiPllF_t clk_dpi_f, dpiPllF_t lock_dpi_f);
#endif  // INCLUDED_SVDPI

// Verilator >= 4.200 needs this definition in Windows, otherwise
// a linker error "undefined reference to sc_time_stamp()" will
// be thrown. See https://veripool.org/guide/latest/faq.html
#if defined(WIN32) || defined(_WIN32) || defined(__WIN32)
inline double sc_time_stamp() { return 0; }
#endif

// Shared Verilated top module (used through the
// above macros to access the Verilator's object).
extern Vtop *vtop;

#endif  // _SIMUL_HLP_HXX
