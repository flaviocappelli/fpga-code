#!/bin/bash
#
# (C) 2021-2025 Flavio Cappelli
# Released under the MIT License.
#
# Generate file fpgadata.mk from APIO.


fatalerror() {
    echo -e "\nERROR: $1"
    [[ -n "$VIRTUAL_ENV" ]] && [[ -n "$APIO_VENV" ]] && deactivate && rm -rf "$APIO_VENV"
    exit 1
}

has_duplicates() {
  {
    sort | uniq -d | grep . -qc
  } < "$1"
}

if ! command -v apio &> /dev/null; then
    echo "APIO not found, creating python virtual environment"
    APIO_VENV=$(mktemp -d)
    if [ -z "$APIO_VENV" ]; then
        fatalerror "\"mktemp -d\" failed"
    fi
    if ! python3 -m venv "$APIO_VENV/apio" ; then
        fatalerror "\"python3 -m venv $APIO_VENV/apio\" failed"
    fi
    if ! source "$APIO_VENV/apio"/bin/activate; then
        fatalerror "\"source $APIO_VENV/apio/bin/activate\" failed"
    fi
    echo "Downloading and installing APIO in virtual environment"
    if ! pip install apio; then
        fatalerror "\"pip install apio\" failed"
    fi
fi

BOARDS=$(apio boards -l 2>/dev/null | tail -n +7 | head -n -2)
if [ -z "$BOARDS" ]; then
    fatalerror "\"apio boards -l\" failed"
fi

FPGAS=$(apio boards -f 2>/dev/null | tail -n +7 | sed 's/[ ].*//')
if [ -z "$FPGAS" ]; then
    fatalerror "\"apio boards -f\" failed"
fi


OUTFILE="fpgadata.mk"
if [ -e $OUTFILE ]; then
    mv -f $OUTFILE $OUTFILE.bak
fi
echo -e "# (C) 2021-2025 Flavio Cappelli\n# Released under the MIT License.\n#\n# FPGA data for the Makefile.\n" >$OUTFILE
echo -e "\nFPGADATA_LOADED := 1" >>$OUTFILE

align=41
echo -e "\n############ BOARDS AND USED FPGAs ############\n" >>$OUTFILE
echo "$BOARDS" | while IFS= read -a line; do
    data=($line)
    fpga=${data[1]}
    board=$(echo ${data[0]} | sed -e 's/\(.*\)/\L\1/')
    printf "%-${align}s %s\n" "${board}__FPGA" ":= $fpga" >>$OUTFILE
done

# Add boards from Apicula project (Gowin FPGAs).
# See https://github.com/YosysHQ/apicula/tree/master
printf "%-${align}s %s\n" "tec0117__FPGA"       ":= GOWIN-GW1NR-UV9QN881C6/I5"   >>$OUTFILE
printf "%-${align}s %s\n" "tangnano__FPGA"      ":= GOWIN-GW1N-LV1QN48C6/I5"     >>$OUTFILE
printf "%-${align}s %s\n" "tangnano1k__FPGA"    ":= GOWIN-GW1NZ-LV1QN48C6/I5"    >>$OUTFILE
printf "%-${align}s %s\n" "tangnano4k__FPGA"    ":= GOWIN-GW1NSR-LV4CQN48PC7/I6" >>$OUTFILE
printf "%-${align}s %s\n" "tangnano9k__FPGA"    ":= GOWIN-GW1NR-LV9QN88PC6/I5"   >>$OUTFILE
printf "%-${align}s %s\n" "tangnano20k__FPGA"   ":= GOWIN-GW2AR-LV18QN88C8/I7"   >>$OUTFILE  # SEE NOTE (*) BELOW.
printf "%-${align}s %s\n" "tangprimer20k__FPGA" ":= GOWIN-GW2A-LV18PG256C8/I7"   >>$OUTFILE
printf "%-${align}s %s\n" "runber__FPGA"        ":= GOWIN-GW1N-UV4LQ144C6/I5"    >>$OUTFILE
printf "%-${align}s %s\n" "honeycomb__FPGA"     ":= GOWIN-GW1NS-UX2CQN48C5/I4"   >>$OUTFILE

align=33
echo -e "\n############ FPGA DATA FOR NEXTPNR ############\n" >>$OUTFILE
for fpga in $FPGAS; do
    data=($(echo $fpga | tr '-' ' '))
    arch=$(echo ${data[0]} | sed -e 's/\(.*\)/\L\1/')
    case $arch in
        ice40)  type=$(echo ${data[1]} | sed -e 's/\(.*\)/\L\1/')
                pkg=$(echo ${data[2]} | sed -e 's/\(.*\)/\L\1/')
                if [[ $type == *4k ]]; then
                    pkg=$pkg:4k
                fi
                ;;
        ecp5)   type=$(echo ${data[1]})
                size=$(echo ${data[2]} | sed -e 's/\(.*\)F/\L\1k/')
                pkg=$(echo ${data[3]})
                case $type in
                    LFE5U)      type=$size
                                ;;
                    LFE5UM)     type=um-$size
                                ;;
                    LFE5UM5G)   type=um5g-$size
                                ;;
                    *)          fatalerror "unknow FPGA type \"$type\""
                                ;;
                esac
                ;;
        *)      fatalerror "unknow FPGA architecture \"$arch\""
                ;;
    esac
    printf "%-${align}s %s\n" "${fpga}__DATA" ":= $arch $type $pkg" >>$OUTFILE
done

# Add FPGAS data for the above Gowin boards (arch, family, device).
# See https://github.com/YosysHQ/apicula/blob/master/examples/Makefile
printf "%-${align}s %s\n" "GOWIN-GW1NR-UV9QN881C6/I5__DATA"   ":= gowin GW1N-9 GW1NR-LV9QN88C6/I5"     >>$OUTFILE
printf "%-${align}s %s\n" "GOWIN-GW1N-LV1QN48C6/I5__DATA"     ":= gowin GW1N-1 GW1N-LV1QN48C6/I5"      >>$OUTFILE
printf "%-${align}s %s\n" "GOWIN-GW1NZ-LV1QN48C6/I5__DATA"    ":= gowin GW1NZ-1 GW1NZ-LV1QN48C6/I5"    >>$OUTFILE
printf "%-${align}s %s\n" "GOWIN-GW1NSR-LV4CQN48PC7/I6__DATA" ":= gowin GW1NS-4 GW1NSR-LV4CQN48PC7/I6" >>$OUTFILE
printf "%-${align}s %s\n" "GOWIN-GW1NR-LV9QN88PC6/I5__DATA"   ":= gowin GW1N-9C GW1NR-LV9QN88PC6/I5"   >>$OUTFILE
printf "%-${align}s %s\n" "GOWIN-GW2AR-LV18QN88C8/I7__DATA"   ":= gowin GW2A-18C GW2AR-LV18QN88C8/I7"  >>$OUTFILE  # SEE NOTE (*) BELOW.
printf "%-${align}s %s\n" "GOWIN-GW2A-LV18PG256C8/I7__DATA"   ":= gowin GW2A-18 GW2A-LV18PG256C8/I7"   >>$OUTFILE
printf "%-${align}s %s\n" "GOWIN-GW1N-UV4LQ144C6/I5__DATA"    ":= gowin GW1N-4 GW1N-UV4LQ144C6/I5"     >>$OUTFILE
printf "%-${align}s %s\n" "GOWIN-GW1NS-UX2CQN48C5/I4__DATA"   ":= gowin GW1NS-2 GW1NS-UX2CQN48C5/I4"   >>$OUTFILE

echo -e "\n# File generated from $(apio --version) (note: boards with" >>$OUTFILE
echo "# Gowin FPGAs are not included in apio, they are added by me)." >>$OUTFILE

sleep 1

echo "File $OUTFILE generated."
if has_duplicates "$OUTFILE"; then
  fatalerror "duplicate lines found in $OUTFILE, check the generator script!"
fi

[[ -n "$VIRTUAL_ENV" ]] && [[ -n "$APIO_VENV" ]] && deactivate && rm -rf "$APIO_VENV"

echo "All ok"
exit 0

# NOTE (*): Place and Route for Tang Nano 20K does not yet
#           work (support for GW2AR-LV18QN88C8/I7 is missing
#           in official nextpnr-gowin). Simulation should work.
