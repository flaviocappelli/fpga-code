
# FPGA Sample Code

This repository contains sample code for the "TinyFPGA BX" and other FPGA boards,
as well as some general purpose Verilog modules, useful for various projects and
different FPGAs (see the "modules" directory). All examples and simulation tests
can be built with my [makefile](./make/README.md), provided in the "make" directory.

To know more about the "TinyFPGA BX" board see
[my description](https://gitlab.com/flaviocappelli/tinyfpga-bx-doc)
(available in English and Italian). For the Sipeed "Tang" boards see
[this page](https://wiki.sipeed.com/hardware/en/tang/).
For the "OrangeCrab" see the
[github repo](https://github.com/orangecrab-fpga/orangecrab-hardware).
<br/>


## WARNING

Although I mentioned the APIO environment in my description of the "TinyFPGA BX" (see
above), I strongly advise against it for new code development. APIO is simple to use but
it is quite limited compared to my [makefile](./make/README.md) (for example, it does not
provide post-synthesis simulation and does not include support for Gowin FPGAs). I removed
APIO from my development environment and I'm now testing my code only with my makefile,
so I no longer guarantee the compatibility of such code with the Apio environment.
<br/>


## IMPORTANT NOTES

Verilog modules from the "modules" directory are imported into my projects and simulation tests using
"symbolic links". Symbolic links permit to have the same file in different folders without creating
multiple copies; also they are transparent to applications. If you don't know them, I suggest that you
look at the manpage of the "ln" command (in Linux, MacOS, FreeBSD and other Unix-like environments) and
[this post](https://www.howtogeek.com/16226/complete-guide-to-symbolic-links-symlinks-on-windows-or-linux/)
(in Windows).

In all Unix-like environments, the use of symbolic links is straightforward. Unfortunately,
in Windows it is not: you need programs that support symbolic links (many applications do not)
and additional permissions for the user or the application (in Windows, symbolic links can be
used to create potential vulnerabilities, so they are not allowed by default). These are the
steps you should perform, to successfully clone this repository in Windows:

1. Download [git-scm](https://git-scm.com/downloads) for Windows: version 2.11.1 or later
   has support for symbolic links, but please note that such support must be enabled during
   the installation.

2. In Windows 7, 8 and 8.1 symbolic links require administrator rights, so you must run
   "Git Bash" with administrator rights. Instead, in Windows 10 (starting from Insiders
   build 14972) and Windows 11, symbolic links can be created without administrator
   rights by enabling the "developer mode", so you can run "Git Bash" with normal
   rights after the developer mode has been enabled.

3. Globally enable support for symbolic links in Git using the command
   "git config \-\-global core.symlinks true" or clone this repository
   using "git clone -c core.symlinks=true"

If, for any reason, symbolic links don't work in Windows, they are replaced by:

* a copy of the target file

    OR

* a "symlink text file", i.e. a text file containing the path of the target file
  (this is the [format](https://stackoverflow.com/questions/954560) employed by
  Git to store symbolic links, and what we see browsing the remote repository
  with a web browser).

While the former case doesn't pose many troubles (aside from duplicating the files
and breaking the simbolic links), the latter requires to manually replace all the
"symlink text files" with the target they point, otherwise the hardware synthesis
and the simulation of projects and tests will fail. Another possibility is to write
a [post-checkout script](https://stackoverflow.com/questions/5917249) to recursively
look for the "symlink text files" and automatically replace them.

Note that you can also download an archive of the repository (instead of cloning
it with Git): in that case the symbolic links will be preserved or not in Windows,
depending on the archive type, the unpacking application and the current rights
of the user and the application.

At the end, it is probably easier to use a virtualization environment (such as
VirtualBox or WSL2) and a Linux distribution. I apologize for these difficulties,
but Linux has been my favorite system for at least 25 years and symbolic links
are a very useful feature.

Have fun!
