
// (C) 2021-2025 Flavio Cappelli
// Released under the MIT License.
//
// See https://www.fpgatutorial.com/how-to-write-a-basic-verilog-testbench/

`default_nettype none                       // Do not allow undeclared signals.
`define DUMPSTR(x) `"x.vcd`"                // Required for GTKWave.
`define SIMULATION_TIME 4ms                 // Simulation time (in s, ms, us or ns).
`include "clkgen_sim_helper.vh"             // Must be included before timescale.vh
`include "timescale.vh"                     // Timescale defined globally.

module testbench();
    initial begin
        $dumpfile(`DUMPSTR(`VCD_OUTPUT));   // Required for GTKWave.
        $dumpvars(0, testbench);            // Dump all variables in all instantiated modules.
        $printtimescale(testbench);         // Display the time scale and resolution (see above).
        #`SIMULATION_TIME                   // Wait until the simulation time is elapsed.
        $timeformat(-6, 2, "us", 1);        // Display the time elapsed in "us".
        $display("Simulation terminated after %t", $realtime);
        $finish;                            // Terminate the simulation.
    end


    // ---- Board 48MHz Clock Simulation ----

    wire CLK_48MHz;
    clkgen_sim_helper #(.FREQ_MHz(48)) CLKGEN (.o__clk(CLK_48MHz));


    // ---- Board Simulation ----

    wire LED_RGB_R, LED_RGB_G, LED_RGB_B;   // Simulated outputs.

    OrangeCrab_R02 BOARD (
        .CLK_48MHz(CLK_48MHz),
        .LED_RGB_R(LED_RGB_R),
        .LED_RGB_G(LED_RGB_G),
        .LED_RGB_B(LED_RGB_B)
    );

endmodule
