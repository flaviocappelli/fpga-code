
// (C) 2021-2025 Flavio Cappelli
// Released under the MIT License.
//
// Blinking led (test the on-board oscillator).
//
// INPUT: none.
//
// OUPUT: led.
//
// For the pin names on the OrangeCrab R02 board see OrangeCrab-R02-pins.lpf

`default_nettype none                   // Do not allow undeclared signals.
`include "timescale.vh"                 // Timescale defined globally.

(* top *)                               // Mark this as top module, because
module OrangeCrab_R02 (                 // sometimes Yosys doesn't detect it.
    input  wire CLK_48MHz,              // 48.0MHz on-board clock.
    output wire LED_RGB_R,              // User led (red component).
    output wire LED_RGB_G,              // User led (green component).
    output wire LED_RGB_B               // User led (blue component).
);

    ////////////////////////////////////
    // Blink dividing the 48MHz clock //
    ////////////////////////////////////

    `ifdef SIMULATION
      `define BLINK_FREQ_HZ     1000    // Simulation.
    `else
      `define BLINK_FREQ_HZ     1       // Build.
    `endif

    reg [2:0] rgb = 0;
    reg [25:0] counter = 0;
    localparam MAXCOUNT = 48000000/(`BLINK_FREQ_HZ*2)-1;

    /* verilator lint_off WIDTH */
    always @(posedge CLK_48MHz) begin
        if (counter == MAXCOUNT)
            begin
                counter <= 0;
                rgb <= rgb + 1;
            end
        else
            counter <= counter + 1;
    end

    assign LED_RGB_R = (rgb == 1 || rgb == 7);    // Blink rotating colors, i.e.
    assign LED_RGB_G = (rgb == 3 || rgb == 7);    // off,red,off,green,off,blue,off,white.
    assign LED_RGB_B = (rgb == 5 || rgb == 7);

endmodule
