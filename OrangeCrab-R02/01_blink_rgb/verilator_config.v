
// (C) 2021-2025 Flavio Cappelli
// Released under the MIT License.
//
// Disable verilator 'PINMISSING' warnings on */yosys/ecp5/cells_sim.v
// See https://verilator.org/guide/latest/exe_verilator.html#configuration-files

`ifdef VERILATOR

`verilator_config
lint_off -rule PINMISSING -file "*/yosys/ecp5/cells_sim.v"

`endif
